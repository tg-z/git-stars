# Repositories defined by: client, server, android

also defined by the following keywords: chat, telnet, written, twitter, api, http, na

- [oldweb-today/shepherd-client](https://github.com/oldweb-today/shepherd-client)
- [psi-im/psi](https://github.com/psi-im/psi)
  XMPP client
- [meganz/webclient](https://github.com/meganz/webclient)
  The mega.nz web client
- [datenknoten/ngx-ssb-client](https://github.com/datenknoten/ngx-ssb-client)
  Experimentation into scuttlebut
- [ritiek/meganz-android](https://github.com/ritiek/meganz-android)
  MEGA Android Client
- [sachaos/todoist](https://github.com/sachaos/todoist)
  Todoist CLI Client. I ❤️ Todoist and CLI.
- [icyphox/twsh](https://github.com/icyphox/twsh)
  :bird: a twtxt client written in bash
- [dzucconi/ervell](https://github.com/dzucconi/ervell)
  Are.na front-end client
- [aredotna/ervell](https://github.com/aredotna/ervell)
  Are.na front-end client
- [t3chnoboy/thepiratebay](https://github.com/t3chnoboy/thepiratebay)
  :skull: The Pirate Bay node.js client
- [dino/dino](https://github.com/dino/dino)
  Modern XMPP ("Jabber") Chat Client using GTK+/Vala
- [lightning-viz/lightning-r](https://github.com/lightning-viz/lightning-r)
  R Client for lightning
- [louipc/turses](https://github.com/louipc/turses)
  A Twitter client for the console.
- [Cotix/cReddit](https://github.com/Cotix/cReddit)
  CLI Reddit client written in C. Oh, crossplatform too!
- [martydill/telnet](https://github.com/martydill/telnet)
  A minimal telnet client
- [maxmind/geoipupdate](https://github.com/maxmind/geoipupdate)
  GeoIP update client code
- [Utazukin/Ichaival](https://github.com/Utazukin/Ichaival)
  Android client for the LANraragi manga/doujinshi web manager.
- [conversejs/converse.js](https://github.com/conversejs/converse.js)
  Web-based XMPP/Jabber chat client written in JavaScript
- [dnbrwstr/speaking-tube](https://github.com/dnbrwstr/speaking-tube)
  Write-only Twitter client
- [Tribler/tribler](https://github.com/Tribler/tribler)
  Privacy enhanced BitTorrent client with P2P content discovery
- [aredotna/cambridge-books-arena-client](https://github.com/aredotna/cambridge-books-arena-client)
- [odeke-em/drive](https://github.com/odeke-em/drive)
  Google Drive client for the commandline
- [mkozjak/node-telnet-client](https://github.com/mkozjak/node-telnet-client)
  A simple telnet client for Node.js
- [cryptee/web-client](https://github.com/cryptee/web-client)
  Cryptee's web client source code for all platforms.
- [nolanlawson/pinafore](https://github.com/nolanlawson/pinafore)
  Alternative web client for Mastodon
- [shrimpza/aurial](https://github.com/shrimpza/aurial)
  A browser client for streaming music from the Subsonic music server
- [ruby/net-telnet](https://github.com/ruby/net-telnet)
  Provides telnet client functionality.
- [axios/axios](https://github.com/axios/axios)
  Promise based HTTP client for the browser and node.js
- [refactorsaurusrex/easy-vpn](https://github.com/refactorsaurusrex/easy-vpn)
  Command line automation for GlobalProtect VPN client
- [artsy/ezel](https://github.com/artsy/ezel)
  A boilerplate for Backbone projects that share code server/client, render server/client, and scale through modular architecture.
- [openid/AppAuth-Android](https://github.com/openid/AppAuth-Android)
  Android client SDK for communicating with OAuth 2.0 and OpenID Connect providers.
- [textileio/js-http-client](https://github.com/textileio/js-http-client)
  Official Textile JS HTTP Wrapper Client
- [Upload/Up1](https://github.com/Upload/Up1)
  Client-side encrypted image host web server
- [berbaquero/reeddit](https://github.com/berbaquero/reeddit)
  Minimal, elastic Reddit reader web-app client
- [facebookarchive/parse](https://github.com/facebookarchive/parse)
  Package parse provides a client for the Parse API.
- [fabiospampinato/webtorrent-cli](https://github.com/fabiospampinato/webtorrent-cli)
  WebTorrent, the streaming torrent client. For the command line.
- [rhysd/Mstdn](https://github.com/rhysd/Mstdn)
  Tiny web-based mastodon client for your desktop
- [cryptomator/cryptomator](https://github.com/cryptomator/cryptomator)
  Multi-platform transparent client-side encryption of your files in the cloud
- [mickael-kerjean/filestash](https://github.com/mickael-kerjean/filestash)
  🦄 A modern web client for SFTP, S3, FTP, WebDAV, Git, S3, FTPS, Minio, LDAP, Caldav, Carddav, Mysql, Backblaze, ...
- [shughes-uk/python-youtubechat](https://github.com/shughes-uk/python-youtubechat)
  provides a simple client library for the youtube live streaming chat api
- [Pinperepette/vue-twitter-stream](https://github.com/Pinperepette/vue-twitter-stream)
  A dashboard capturing live sentiment of tweets based on hashtag. Vue.js as client, Twitter API for streaming, Watson Tone Analyzer, socket.io to tie server and client
- [gdrive-org/gdrive](https://github.com/gdrive-org/gdrive)
  Google Drive CLI Client
- [xanzy/go-gitlab](https://github.com/xanzy/go-gitlab)
  A GitLab API client enabling Go programs to interact with GitLab in a simple and uniform way
- [api-platform/api-platform](https://github.com/api-platform/api-platform)
  REST and GraphQL framework to build modern API-driven projects (server-side and client-side)
- [Marten4n6/TinyTor](https://github.com/Marten4n6/TinyTor)
  A tiny Tor client implementation (in pure python).
- [essandess/macos-openvpn-server](https://github.com/essandess/macos-openvpn-server)
  macOS OpenVPN Server and Client Configuration (OpenVPN, Tunnelblick, PF)
- [jpillora/cloud-torrent](https://github.com/jpillora/cloud-torrent)
  ☁️ Cloud Torrent: a self-hosted remote torrent client
- [textileio/minimal-client-demo](https://github.com/textileio/minimal-client-demo)
  Simple project with minimal http client functionality to add files to an existing local thread
- [textileio/notes-desktop](https://github.com/textileio/notes-desktop)
  Example app built with reactjs and @textile/js-http-client
- [1j01/pesterchum](https://github.com/1j01/pesterchum)
  Pesterchum Chat Client, and also a Doc Scratch lightning text effect reaction GIF generator
- [Pinperepette/httpie](https://github.com/Pinperepette/httpie)
  HTTPie is a command line HTTP client, a user-friendly cURL replacement.
- [ZeeRooo/MaterialFBook](https://github.com/ZeeRooo/MaterialFBook)
  A light alternative client for Facebook based on the web with a modern look (Material Design)
- [tutao/tutanota](https://github.com/tutao/tutanota)
  Tutanota is an email client with a strong focus on security and privacy that lets you encrypt emails on all your devices.
- [eKoopmans/html2pdf.js](https://github.com/eKoopmans/html2pdf.js)
  Client-side HTML-to-PDF rendering using pure JS.
- [remigallego/winampify](https://github.com/remigallego/winampify)
  ⚡ A Spotify web client with an OS-looking interface and a reimplementation of the classic audio player Winamp.
- [ssbc/patchbay](https://github.com/ssbc/patchbay)
  An alternative Secure Scuttlebutt client interface that is fully compatible with Patchwork
- [bluquar/react-redux-soundcloud](https://github.com/bluquar/react-redux-soundcloud)
  Learn React + Redux by building a SoundCloud Client. Full tutorial included. Source Code for main tutorial but also extensions included.
- [jakubroztocil/httpie](https://github.com/jakubroztocil/httpie)
  As easy as HTTPie /aitch-tee-tee-pie/ 🥧 Modern command line HTTP client – user-friendly curl alternative with intuitive UI, JSON support, syntax highlighting, wget-like downloads, extensions, etc.  https://twitter.com/clihttp
- [ikreymer/cdx-index-client](https://github.com/ikreymer/cdx-index-client)
  A command-line tool for using CommonCrawl Index API at http://index.commoncrawl.org/
- [ggrossman/telechat-ng](https://github.com/ggrossman/telechat-ng)
  Telnet-based chat server from the early '90s
- [ianepperson/telnetsrvlib](https://github.com/ianepperson/telnetsrvlib)
  Telnet server using gevent
- [skyepn/telechat-py](https://github.com/skyepn/telechat-py)
  A Python rewrite of the classic Telechat telnet chat server
- [ggrossman/unix-cb](https://github.com/ggrossman/unix-cb)
  Unix-CB, an ancient telnet-based chat server from the early 90s
- [twitter/twurl](https://github.com/twitter/twurl)
  OAuth-enabled curl for the Twitter API
- [daentech/Telnet](https://github.com/daentech/Telnet)
  A simple telnet server
- [ivangreene/arena-js](https://github.com/ivangreene/arena-js)
  are.na API wrapper for JavaScript
