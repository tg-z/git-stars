# Repositories defined by: github, stars, io

also defined by the following keywords: repository, website, https, script, issue, history, com

- [nteract/galleria](https://github.com/nteract/galleria)
  GitHub bot that uploads screenshots from PR builds
- [bkhezry/FuckYouGithub](https://github.com/bkhezry/FuckYouGithub)
  Fuck You Github
- [ropenscilabs/stellar](https://github.com/ropenscilabs/stellar)
  Search your github stars in R
- [actions/toolkit](https://github.com/actions/toolkit)
  The GitHub ToolKit for developing GitHub Actions.
- [SevenOutman/Hubble](https://github.com/SevenOutman/Hubble)
  :telescope: Travel through GitHub Stars' history
- [fontforge/fontforge.github.io](https://github.com/fontforge/fontforge.github.io)
  The FontForge homepage
- [nikitavoloboev/github-stars](https://github.com/nikitavoloboev/github-stars)
  Curated list of my GitHub stars
- [ArtSabintsev/GitFame](https://github.com/ArtSabintsev/GitFame)
  A Swift CLI script that logs your GitHub Stars and Forks.
- [yuki-kimoto/gitprep](https://github.com/yuki-kimoto/gitprep)
  Portable GitHub system into your own server
- [adereth/counting-stars](https://github.com/adereth/counting-stars)
  Data processing for my Counting Stars on Github post
- [pnowy/github-stars-history](https://github.com/pnowy/github-stars-history)
  The missing github stars history!
- [abhijithvijayan/stargazed](https://github.com/abhijithvijayan/stargazed)
  Creating your own Awesome List of GitHub stars! 
- [udpsec/github-tools](https://github.com/udpsec/github-tools)
  this github tools to  Batch update stars ,clone repository and unstars 
- [lorey/github-stars-by-topic](https://github.com/lorey/github-stars-by-topic)
  :star: Generate a list of your GitHub stars by topic - automatically!
- [JuanPotato/GitHub-WinClassic](https://github.com/JuanPotato/GitHub-WinClassic)
  Windows Classic style for GitHub
- [jensteichert/OpenCashew](https://github.com/jensteichert/OpenCashew)
  Cashew macOS Github Issue Tracker 
- [ryanmcdermott/starmark](https://github.com/ryanmcdermott/starmark)
  :octocat: Turn your GitHub stars into Chrome bookmarks
- [3lo1i/WinHub-98](https://github.com/3lo1i/WinHub-98)
  A modern-looking userstyle for GitHub
- [wolfg1969/oh-my-stars](https://github.com/wolfg1969/oh-my-stars)
  An offline CLI tool to search your GitHub Stars.
- [MediaFire/mediafire-cpp-sdk](https://github.com/MediaFire/mediafire-cpp-sdk)
  MediaFire's C++ SDK Github Repository
- [adamzolyak/mirror-labels-to-child-action](https://github.com/adamzolyak/mirror-labels-to-child-action)
  👩‍👧‍👦 A GitHub action that mirrors a parent issue's labels to a child issue.
- [newyorkdata/newyorkdata.github.io](https://github.com/newyorkdata/newyorkdata.github.io)
  website
- [lvxianchao/the-fucking-github](https://github.com/lvxianchao/the-fucking-github)
  A Chrome extension for Github. View starred repositories, organizing stars, searching stars and searching repositories online for Github。
- [sindresorhus/github-markdown-css](https://github.com/sindresorhus/github-markdown-css)
  The minimal amount of CSS to replicate the GitHub Markdown style
- [kamranahmedse/githunt](https://github.com/kamranahmedse/githunt)
  Hunt the most starred projects on any date on GitHub
- [vhanla/github-windows-classic](https://github.com/vhanla/github-windows-classic)
  Give GitHub a Windows Classic look
- [ritiek/GitFeed](https://github.com/ritiek/GitFeed)
  Check your GitHub Newsfeed via the command-line
- [vmarkovtsev/GitHubStars](https://github.com/vmarkovtsev/GitHubStars)
  Python script to fetch GitHub repos metadata.
- [dessant/move-issues](https://github.com/dessant/move-issues)
  :robot: GitHub App that moves issues between repositories
- [1j01/octicons](https://github.com/1j01/octicons)
  A scalable set of icons handcrafted with <3 by GitHub
- [uwdata/datalib](https://github.com/uwdata/datalib)
  We've moved! Please see https://github.com/vega/datalib
- [Pinperepette/legit](https://github.com/Pinperepette/legit)
  Git for Humans, Inspired by GitHub for Mac™.
- [Sep0lkit/git-issues-blog](https://github.com/Sep0lkit/git-issues-blog)
  Auto build issues blog from github repository
- [gabrielgodoy/github-stars-manager](https://github.com/gabrielgodoy/github-stars-manager)
  Chrome extension that allows you to manage your Github stars with tags, and to create a bookmark folder with all your stars organized by the tags you created
- [Shpota/github-activity-generator](https://github.com/Shpota/github-activity-generator)
  A script that helps you generate a beautiful GitHub activity graph
- [atomotic/uv-app-starter](https://github.com/atomotic/uv-app-starter)
  Create a IIIF-enabled website using universalviewer.io and host it for free on Github Pages
- [kenogo/spotify-lyrics-cli](https://github.com/kenogo/spotify-lyrics-cli)
  Migrated to Gitlab, this Github repo will not receive updates.
- [nmap/nmap](https://github.com/nmap/nmap)
  Nmap - the Network Mapper. Github mirror of official SVN repository.
- [xxhomey19/github-file-icon](https://github.com/xxhomey19/github-file-icon)
  🌈 🗂 A browser extension which gives different filetypes different icons to GitHub, GitLab, gitea and gogs.
- [mananpal1997/GF-Overflow](https://github.com/mananpal1997/GF-Overflow)
  Fill up any user's github feed with spam activities, doesn't matter if user follows you or not. Can be also used to mask your activities. ԅ(≖‿≖ԅ)                                **No more working after github interface update**
- [utterance/utterances](https://github.com/utterance/utterances)
  :crystal_ball: A lightweight comments widget built on GitHub issues
- [pd4d10/git-touch](https://github.com/pd4d10/git-touch)
  Open source GitHub App built with Flutter
- [Unseen-Worlds/unseenworlds.github.io](https://github.com/Unseen-Worlds/unseenworlds.github.io)
  WIP 
- [dpobel/github-add-youtube-video](https://github.com/dpobel/github-add-youtube-video)
  A Greasemonkey script to better integrate Youtube videos in Github markdown (pull requests, issues, comments, ...).
- [Hack-with-Github/Awesome-Security-Gists](https://github.com/Hack-with-Github/Awesome-Security-Gists)
  A collection of various GitHub gists for hackers, pentesters and security researchers
- [MinhasKamal/DownGit](https://github.com/MinhasKamal/DownGit)
  Create GitHub Resource Download Link (git-github-direct-zip-directory-folder-file)
- [HumanCellAtlas/zentool](https://github.com/HumanCellAtlas/zentool)
  CLI tool for working with ZenHub / GitHub / Google Sheets
- [railsgirls/railsgirls.github.io](https://github.com/railsgirls/railsgirls.github.io)
  Rails Girls Guides
- [liyasthomas/mnmlurl](https://github.com/liyasthomas/mnmlurl)
  🔗 Minimal URL - Modern URL shortener with support for custom alias & can be hosted even in GitHub pages
- [CraryPrimitiveMan/awesome-with-star](https://github.com/CraryPrimitiveMan/awesome-with-star)
  😎 Curated list of awesome lists with github star
- [simogeo/Filemanager](https://github.com/simogeo/Filemanager)
  An open-source file manager released under MIT license. Up-to-date for PHP connector. This package is DEPRECATED. Now, please use RichFileManager available at : https://github.com/servocoder/RichFilemanager.
- [SDRausty/buildAPKs](https://github.com/SDRausty/buildAPKs)
  Really quickly build APKs on device (smartphone and tablet) in Termux📲  See https://buildapks.github.io/docsBuildAPKs/setup to start building apps on  Amazon Fire, Android and Chromebook. 
- [liyasthomas/mnmlurl-extension](https://github.com/liyasthomas/mnmlurl-extension)
  💁 Browser extension for Minimal URL - Modern URL shortener with support for custom alias & can be hosted even in GitHub pages
- [BackMarket/github-mermaid-extension](https://github.com/BackMarket/github-mermaid-extension)
  A browser extension for Chrome, Opera & Firefox that adds Mermaid language support to Github
- [MCApollo/Athena-project](https://github.com/MCApollo/Athena-project)
  makepkg like build-files for cross-building iOS packages (Moved to https://github.com/MCApollo/repo)
- [markdowncss/markdowncss.github.io](https://github.com/markdowncss/markdowncss.github.io)
  Where all the MarkdownCSS themes live.
- [github/markup](https://github.com/github/markup)
  Determines which markup library to use to render a content file (e.g. README) on GitHub
- [sindresorhus/refined-github](https://github.com/sindresorhus/refined-github)
  Browser extension that simplifies the GitHub interface and adds useful features
- [ozh/ascii-tables](https://github.com/ozh/ascii-tables)
  Quickly format table in ASCII. Great for code comments, or Github Markdown!
- [Piwigo/Piwigo](https://github.com/Piwigo/Piwigo)
  Manage your photos with Piwigo, a full featured open source photo gallery application for the web. Star us on Github! More than 200 plugins and themes available. Join us and contribute!
- [leereilly/games](https://github.com/leereilly/games)
  :video_game: A list of popular/awesome videos games, add-ons, maps, etc. hosted on GitHub. Any genre. Any platform. Any engine.
- [MagicStack/MagicPython](https://github.com/MagicStack/MagicPython)
  Cutting edge Python syntax highlighter for Sublime Text, Atom and Visual Studio Code. Used by GitHub to highlight your Python code!
- [gatewayapps/kamino](https://github.com/gatewayapps/kamino)
  Github issue cloning tool
- [Pinperepette/bashstrap](https://github.com/Pinperepette/bashstrap)
  Bootstrap for your terminal. A quick way to spruce up OSX terminal. It cuts out the fluff, adds in timesaving features, and provides a solid foundation for customizing your terminal style. Based on Mathias Bynens epic dotfiles - https://github.com/mathiasbynens/dotfiles
- [720kb/ndm](https://github.com/720kb/ndm)
  :computer: npm desktop manager https://720kb.github.io/ndm
- [docker/docker.github.io](https://github.com/docker/docker.github.io)
  Source repo for Docker's Documentation
- [nginx/nginx](https://github.com/nginx/nginx)
  An official read-only mirror of http://hg.nginx.org/nginx/ which is updated hourly. Pull requests on GitHub cannot be accepted and will be automatically closed. The proper way to submit changes to nginx is via the nginx development mailing list, see http://nginx.org/en/docs/contributing_changes.html
- [ikreymer/webarchiveplayer](https://github.com/ikreymer/webarchiveplayer)
  NOTE: This project is no longer being actively developed.. Check out Webrecorder Player for the latest player. https://github.com/webrecorder/webrecorderplayer-electron) (Legacy: Desktop application for browsing web archives (WARC and ARC)
- [HumanCellAtlas/humancellatlas.github.io](https://github.com/HumanCellAtlas/humancellatlas.github.io)
  Provides manageable URLs for the Human Cell Atlas JSON schemas. 
- [matomo-org/matomo](https://github.com/matomo-org/matomo)
  Liberating Web Analytics. Star us on Github? +1. Matomo is the leading open alternative to Google Analytics that gives you full control over your data. Matomo lets you easily collect data from websites, apps & the IoT and visualise this data and extract insights. Privacy is built-in. We love Pull Requests! 
- [jmn/jmn.github.io](https://github.com/jmn/jmn.github.io)
  edit on the dev branch, use yarn deploy to deploy
- [Synzvato/decentraleyes](https://github.com/Synzvato/decentraleyes)
  This repository has a new home: https://git.synz.io/Synzvato/decentraleyes
- [csinva/csinva.github.io](https://github.com/csinva/csinva.github.io)
  Slides, paper notes, class notes, blog posts, and research on machine learning, statistics, and artificial intelligence.
- [jarv/cmdchallenge](https://github.com/jarv/cmdchallenge)
  This repo is mirror of https://gitlab.com/jarv/cmdchallenge
- [Prettyhtml/prettyhtml](https://github.com/Prettyhtml/prettyhtml)
  💅 The formatter for the modern web https://prettyhtml.netlify.com/
- [cri-o/cri-o.io](https://github.com/cri-o/cri-o.io)
  website content
- [ipzn/ipzn](https://github.com/ipzn/ipzn)
  InterPlanetary ZeroNet (Mirror)  Wiki: https://gitlab.com/ipzn/ipzn/wikis/
- [torproject/tor](https://github.com/torproject/tor)
  unofficial git repo -- report bugs/issues/pull requests on https://trac.torproject.org/ --
- [Ezelia/eureca.io](https://github.com/Ezelia/eureca.io)
  eureca.io : a nodejs bidirectional RPC that can use WebSocket, WebRTC or XHR fallback as transport layers
- [atomotic/bagweb](https://github.com/atomotic/bagweb)
  mirror a website, put it in a bag
