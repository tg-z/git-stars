# Repositories defined by: tree, library, syntax

also defined by the following keywords: website, nodes, structure, format, abstract, view, node

- [treenotation/pytree](https://github.com/treenotation/pytree)
  Tree Notation Python Library
- [treenotation/jtree](https://github.com/treenotation/jtree)
  Tree Notation TypeScript/Javascript library.
- [1j01/organeq](https://github.com/1j01/organeq)
  🔢➗🔢 Plant a phantasmagorical mathematical syntax tree
- [treenotation/lab.treenotation.org](https://github.com/treenotation/lab.treenotation.org)
  Website for Tree Notation Lab
- [dduan/tre](https://github.com/dduan/tre)
  Tree command, improved.
- [MohammedGomaaS/tree-heirarchyFE](https://github.com/MohammedGomaaS/tree-heirarchyFE)
  front-end for a tree hierarchy f 
- [eflynch/baobab](https://github.com/eflynch/baobab)
  Generate Tree Diagrams
- [tree-sitter/node-tree-sitter](https://github.com/tree-sitter/node-tree-sitter)
  Node.js bindings for tree-sitter
- [weibangtuo/vue-tree](https://github.com/weibangtuo/vue-tree)
  A tree component
- [react-component/tree](https://github.com/react-component/tree)
  React Tree
- [MohammedGomaaS/tree-heirarchy](https://github.com/MohammedGomaaS/tree-heirarchy)
  about storing a tree hierarchy in a DB
- [vpusher/paper-tree](https://github.com/vpusher/paper-tree)
   Browsable tree of nodes with expandable/collapsible capabilities and actions menu
- [1j01/process-tree](https://github.com/1j01/process-tree)
  Node library to find all the child processes
- [syntax-tree/nlcst](https://github.com/syntax-tree/nlcst)
  Natural Language Concrete Syntax Tree format
- [syntax-tree/hast](https://github.com/syntax-tree/hast)
  Hypertext Abstract Syntax Tree format
- [piroor/treestyletab](https://github.com/piroor/treestyletab)
  Tree Style Tab, Show tabs like a tree.
- [parrt/dtreeviz](https://github.com/parrt/dtreeviz)
  A python library for decision tree visualization and model interpretation.
- [edent/TweeView](https://github.com/edent/TweeView)
  A Tree View For Tweets
- [yi-ge/js-tree-list](https://github.com/yi-ge/js-tree-list)
  Convert list to tree, managing a tree and its nodes.
- [w8r/avl](https://github.com/w8r/avl)
  :eyeglasses: Fast AVL tree for Node and browser
- [litten/folder2tree](https://github.com/litten/folder2tree)
  Show your folder structure with tree nodes. 用树形节点展示文件夹结构。
- [syntax-tree/nlcst-search](https://github.com/syntax-tree/nlcst-search)
  utility to search for patterns in an nlcst tree
- [syntax-tree/mdast](https://github.com/syntax-tree/mdast)
  Markdown Abstract Syntax Tree format
- [vakata/jstree](https://github.com/vakata/jstree)
  jquery tree plugin
- [syntax-tree/unist](https://github.com/syntax-tree/unist)
  Universal Syntax Tree used by @unifiedjs
- [mbraak/jqTree](https://github.com/mbraak/jqTree)
  Tree widget for jQuery
- [DenQ/iron-tree](https://github.com/DenQ/iron-tree)
  Tree (data structure). Many methods
- [internetarchive/wayback-radial-tree](https://github.com/internetarchive/wayback-radial-tree)
- [mhinz/vim-tree](https://github.com/mhinz/vim-tree)
  🌲 Use tree(1) for quick navigation.
- [chylex/objtree](https://github.com/chylex/objtree)
  JavaScript snippet that generates a nicely formatted representation of an object tree.
- [Atlantic18/DoctrineExtensions](https://github.com/Atlantic18/DoctrineExtensions)
  Doctrine2 behavioral extensions, Translatable, Sluggable, Tree-NestedSet, Timestampable, Loggable, Sortable
- [hukaibaihu/vue-org-tree](https://github.com/hukaibaihu/vue-org-tree)
  A simple organization tree based on Vue2.x
- [javaparser/javaparser](https://github.com/javaparser/javaparser)
   Java 1-13 Parser and Abstract Syntax Tree for Java –
- [jonmiles/bootstrap-treeview](https://github.com/jonmiles/bootstrap-treeview)
  Tree View for Twitter Bootstrap - 
- [mihneadb/node-directory-tree](https://github.com/mihneadb/node-directory-tree)
  Convert a directory tree to a JS object.
- [tjeffree/Brackets-ExtensionHighlight](https://github.com/tjeffree/Brackets-ExtensionHighlight)
  File extension colours in Brackets' file tree.
- [liushuping/ascii-tree](https://github.com/liushuping/ascii-tree)
  A node module for generating tree structure in ASCII
- [ZhipingYang/VerticalTree](https://github.com/ZhipingYang/VerticalTree)
  🥶Provides a vertical drawing of the tree structure which can view information about the tree‘s nodes and supports console debug views & layers and so on
- [yangshun/tree-node-cli](https://github.com/yangshun/tree-node-cli)
  🌲Lists the contents of directories in a tree-like format, similar to the Linux tree command
- [ivogabe/Brackets-Icons](https://github.com/ivogabe/Brackets-Icons)
  File icons in Brackets' file tree
- [idevelop/treeql](https://github.com/idevelop/treeql)
  JSON query and mutation library. It traverses a tree structure in post-order (leaves first, root last), across objects and arrays, returning the nodes which match the partial structure passed in the query, as well as allowing you to mutate or replace matched nodes.
- [tinwan/vue2-data-tree](https://github.com/tinwan/vue2-data-tree)
  A tree that data is lazy loaded. Support dragging node, checking node, editing node's name and selecting node.
- [CIRCL/lookyloo](https://github.com/CIRCL/lookyloo)
  Lookyloo is a web interface allowing to scrape a website and then displays a tree of domains calling each other.
- [halower/vue-tree](https://github.com/halower/vue-tree)
  tree and multi-select component based on Vue.js 2.0
- [frontend-collective/react-sortable-tree-theme-file-explorer](https://github.com/frontend-collective/react-sortable-tree-theme-file-explorer)
  A file explorer theme for React Sortable Tree
- [Klortho/d3-flextree](https://github.com/Klortho/d3-flextree)
  Flexible tree layout algorithm that allows for variable node sizes
- [racker/node-elementtree](https://github.com/racker/node-elementtree)
  Port of Python's Element Tree module to Node.js
- [vpistis/OrganizeMediaFiles](https://github.com/vpistis/OrganizeMediaFiles)
  a collection of Python scripts that help you organize media files into a directory tree "year/month" based on metadata , using exiftool
- [alferov/array-to-tree](https://github.com/alferov/array-to-tree)
  Convert a plain array of nodes (with pointers to parent nodes) to a nested data structure
- [lightning-viz/three.js](https://github.com/lightning-viz/three.js)
  Three.js library.
- [tree-sitter/tree-sitter](https://github.com/tree-sitter/tree-sitter)
  An incremental parsing system for programming tools
- [wesbos/Syntax](https://github.com/wesbos/Syntax)
  A website for the Syntax Podcast
- [parse-community/ParseTwitterUtils-Android](https://github.com/parse-community/ParseTwitterUtils-Android)
  A utility library to authenticate ParseUsers with Twitter
- [vega/datalib](https://github.com/vega/datalib)
  JavaScript data utility library.
- [himaratsu/HMRScrollFeedView](https://github.com/himaratsu/HMRScrollFeedView)
  HRMScrollFeedView is library for realizing UI like SmartNews App.
- [fivethirtyeight/ap-election-loader](https://github.com/fivethirtyeight/ap-election-loader)
  A Ruby library for loading election data from The Associated Press into MySQL
- [williamngan/pts](https://github.com/williamngan/pts)
  A library for visualization and creative-coding
- [unstoppabledomains/namicorn](https://github.com/unstoppabledomains/namicorn)
  A library for interacting with blockchain domain names.
- [vega/vega-lite-ui](https://github.com/vega/vega-lite-ui)
  Common UI Library that powers Polestar and Voyager
- [webrecorder/wombat](https://github.com/webrecorder/wombat)
  WIP - Splitting wombat.js library from pywb
- [mitchellh/cli](https://github.com/mitchellh/cli)
  A Go library for implementing command-line interfaces.
- [rubygems/rubygems](https://github.com/rubygems/rubygems)
  Library packaging and distribution for Ruby.
- [Tomn94/Playlists-Maker](https://github.com/Tomn94/Playlists-Maker)
  🎵 Sort your music library and put your songs in playlists!
- [aredotna/arena-php](https://github.com/aredotna/arena-php)
  Arena PHP Library
- [substance/substance](https://github.com/substance/substance)
  A JavaScript library for web-based content editing.
- [mikepenz/AboutLibraries](https://github.com/mikepenz/AboutLibraries)
  AboutLibraries is a library to offer some information of libraries.
- [atomotic/ruby-oai](https://github.com/atomotic/ruby-oai)
  a Ruby library for building OAI-PMH clients and servers
- [sighingnow/parsec.py](https://github.com/sighingnow/parsec.py)
  A universal Python parser combinator library inspired by Parsec library of Haskell.
- [fourlastor/dante](https://github.com/fourlastor/dante)
  A sane rich text parsing and styling library.
- [lokesh-coder/pretty-checkbox](https://github.com/lokesh-coder/pretty-checkbox)
  A pure CSS library to beautify checkbox and radio buttons.
- [TeaMeow/TocasUI](https://github.com/TeaMeow/TocasUI)
  Tocas UI — A CSS3 and SASS UI library.
- [benedekrozemberczki/awesome-decision-tree-papers](https://github.com/benedekrozemberczki/awesome-decision-tree-papers)
  A collection of research papers on decision, classification and regression trees with implementations.
- [kosma/minmea](https://github.com/kosma/minmea)
  a lightweight GPS NMEA 0183 parser library in pure C
- [seanmiddleditch/libtelnet](https://github.com/seanmiddleditch/libtelnet)
  Simple RFC-complient TELNET implementation as a C library.
- [datamade/probablepeople](https://github.com/datamade/probablepeople)
  :family: a python library for parsing unstructured western names into name components.
- [resident-archive/resident-archive-lambdas](https://github.com/resident-archive/resident-archive-lambdas)
  Sync residentadvisor's library with songs available on Spotify
- [react-spring/react-spring](https://github.com/react-spring/react-spring)
  ✌️ A spring physics based React animation library
- [givanz/VvvebJs](https://github.com/givanz/VvvebJs)
  Drag and drop website builder javascript library.
- [zircleUI/zircleUI](https://github.com/zircleUI/zircleUI)
  🚀 zircle-ui is a frontend library to develop zoomable user interfaces.
- [2d-inc/Flare-Flutter](https://github.com/2d-inc/Flare-Flutter)
  Load and get full control of your Flare files in a Flutter project using this library.
- [mozilla/page-metadata-parser](https://github.com/mozilla/page-metadata-parser)
  A Javascript library for parsing metadata on a web page.
- [google/python-fire](https://github.com/google/python-fire)
  Python Fire is a library for automatically generating command line interfaces (CLIs) from absolutely any Python object.
- [altdesktop/playerctl](https://github.com/altdesktop/playerctl)
  🎧 mpris command-line controller and library for spotify, vlc, audacious, bmp, cmus, and others.
- [edmondburnett/twitter-text-python](https://github.com/edmondburnett/twitter-text-python)
  Twitter text processing library (auto linking and extraction of usernames, lists and hashtags).
- [rgalus/sticky-js](https://github.com/rgalus/sticky-js)
  Library for sticky elements written in vanilla javascript
- [t184256/hacks](https://github.com/t184256/hacks)
  hacks, a python plugin library that doesn't play by the rules
- [benji6/virtual-audio-graph](https://github.com/benji6/virtual-audio-graph)
  :notes: Library for declaratively manipulating the Web Audio API
- [sbstjn/timesheet.js](https://github.com/sbstjn/timesheet.js)
  JavaScript library for HTML5 & CSS3 time sheets
- [BonsaiDen/twitter-text-python](https://github.com/BonsaiDen/twitter-text-python)
  Twitter text processing library (auto linking and extraction of usernames, lists and hashtags). Based on the Java implementation by Matt Sanford
- [nteract/scrapbook](https://github.com/nteract/scrapbook)
  A library for recording and reading data in notebooks.
- [plataformatec/nimble_parsec](https://github.com/plataformatec/nimble_parsec)
  A simple and fast library for text-based parser combinators
- [pqina/filepond](https://github.com/pqina/filepond)
  🌊 A flexible and fun JavaScript file upload library
- [DocNow/twarc](https://github.com/DocNow/twarc)
  A command line tool (and Python library) for archiving Twitter JSON
- [mroth/unindexed](https://github.com/mroth/unindexed)
  :mag_right::grey_question: website that irrevocably deletes itself once indexed
- [mudcube/Color.Space.js](https://github.com/mudcube/Color.Space.js)
  :rainbow: Library to convert between color spaces: HEX, RGB, RGBA, HSL, HSLA, CMY, CMYK. This covers the conversion between W3 compatible colors. Conversion is versatile accepting strings, arrays, and objects.
- [maxatwork/form2js](https://github.com/maxatwork/form2js)
  Javascript library for collecting form data
- [N3-components/N3-components](https://github.com/N3-components/N3-components)
  N3-components , Powerful Vue UI Library.
- [hxrts/cybercon](https://github.com/hxrts/cybercon)
  cybercon website
- [internetarchive/warc](https://github.com/internetarchive/warc)
  Python library for reading and writing warc files
- [OpenMined/PySyft](https://github.com/OpenMined/PySyft)
  A library for encrypted, privacy preserving deep learning
- [1j01/tuna](https://github.com/1j01/tuna)
  An audio effects library for Web Audio, made by www.dinahmoe.com
- [1j01/os-gui](https://github.com/1j01/os-gui)
  Retro OS GUI JS library
- [funkyfuture/todo.txt-pylib](https://github.com/funkyfuture/todo.txt-pylib)
  An easy to extend Python 3 library to parse, manipulate, query and render tasks in the todo.txt-format in a pythonic manner.
- [sparanoid/lightense-images](https://github.com/sparanoid/lightense-images)
  A dependency-free pure JavaScript image zooming library less than 2 KB (gzipped). Inspired by tholman/intense-images
- [coleifer/micawber](https://github.com/coleifer/micawber)
  a small library for extracting rich content from urls
- [github/markup](https://github.com/github/markup)
  Determines which markup library to use to render a content file (e.g. README) on GitHub
- [lc-soft/LCUI](https://github.com/lc-soft/LCUI)
  A small C library for building user interfaces with C, XML and CSS.
- [NorthwoodsSoftware/GoJS](https://github.com/NorthwoodsSoftware/GoJS)
  JavaScript diagramming library for interactive flowcharts, org charts, design tools, planning tools, visual languages.
- [padraigfl/packard-belle](https://github.com/padraigfl/packard-belle)
  Windows 98 React Component Library
- [schmittjoh/serializer](https://github.com/schmittjoh/serializer)
  Library for (de-)serializing data of any complexity (supports XML, JSON, YAML)
- [rohitkumbhar/record-hacks](https://github.com/rohitkumbhar/record-hacks)
  record-hacks is a Java library that will help you track "creative solutions" in your code. 
- [crazychicken/t-scroll](https://github.com/crazychicken/t-scroll)
  A modern reveal-on-scroll library with useful options and animations. (Animate Elements On Reveal)
- [tmk907/PlaylistsNET](https://github.com/tmk907/PlaylistsNET)
  Simple library for reading and writing playlist's files. Supported formats: m3u, pls, wpl, zpl.
- [React95/React95](https://github.com/React95/React95)
  A React components library with Win95 UI
- [syntax-tree/unist-builder](https://github.com/syntax-tree/unist-builder)
  unist utility to create a new trees with a nice syntax
- [alecthomas/chroma](https://github.com/alecthomas/chroma)
  A general purpose syntax highlighter in pure Go 
- [artsy/force](https://github.com/artsy/force)
  The Artsy.net website
- [alexdevero/front-end-dojo](https://github.com/alexdevero/front-end-dojo)
  Library full of useful CSS, JS and Sass functions, mixins and utilities.
- [newyorkdata/newyorkdata.github.io](https://github.com/newyorkdata/newyorkdata.github.io)
  website
- [anseki/plain-draggable](https://github.com/anseki/plain-draggable)
  The simple and high performance library to allow HTML/SVG element to be dragged.
- [adulau/url_archiver](https://github.com/adulau/url_archiver)
  url-archiver is a simple library to fetch and archive URL on the file-system
- [textile/textile-mark](https://github.com/textile/textile-mark)
  Use one of these logo marks wherever you use Textile, the markup syntax for writers.
- [shughes-uk/python-youtubechat](https://github.com/shughes-uk/python-youtubechat)
  provides a simple client library for the youtube live streaming chat api
- [KeenRivals/bestmotherfucking.website](https://github.com/KeenRivals/bestmotherfucking.website)
  The Best Motherfucking Website
- [frontend-collective/react-sortable-tree](https://github.com/frontend-collective/react-sortable-tree)
  Drag-and-drop sortable component for nested data and hierarchies
- [Arkiver2/warcio](https://github.com/Arkiver2/warcio)
  Streaming WARC/ARC library for fast web archive IO
- [emotion-js/emotion](https://github.com/emotion-js/emotion)
  👩‍🎤 CSS-in-JS library designed for high performance style composition
- [Simonwep/pickr](https://github.com/Simonwep/pickr)
  🍭 Flat, simple, multi-themed, responsive and hackable Color-Picker library. No dependencies, no jQuery. Compatible with all CSS Frameworks e.g. Bootstrap, Materialize. Supports alpha channel, rgba, hsla, hsva and more!
- [EvilSourcerer/CSStudio](https://github.com/EvilSourcerer/CSStudio)
  Boundless Website Editor
- [danielgtaylor/bibviz](https://github.com/danielgtaylor/bibviz)
  BibViz.com Website and Scripts
