# Repositories defined by: browser, based, cross

also defined by the following keywords: privacy, typescript, distributed, extension, experimental, os, firefox

- [blockstack/blockstack-browser](https://github.com/blockstack/blockstack-browser)
  The Blockstack Browser
- [captbaritone/webamp](https://github.com/captbaritone/webamp)
  Winamp 2 reimplemented for the browser
- [freethenation/DFPM](https://github.com/freethenation/DFPM)
  DFPM is a browser extension for detecting browser fingerprinting.
- [webrecorder/browsertrix](https://github.com/webrecorder/browsertrix)
  Browsertrix: Containerized High-Fidelity Browser-Based Automated Crawling + Behavior System
- [HumanCellAtlas/data-browser](https://github.com/HumanCellAtlas/data-browser)
- [ant4g0nist/vegvisir](https://github.com/ant4g0nist/vegvisir)
  A browser based GUI for **LLDB** Debugger. 
- [ipfs-shipyard/discussify-browser-extension](https://github.com/ipfs-shipyard/discussify-browser-extension)
  Discussify's browser extension app
- [eversum/galacteek](https://github.com/eversum/galacteek)
  Browser for the distributed web
- [discordapp/qrcodejs](https://github.com/discordapp/qrcodejs)
  Cross-browser QRCode generator for javascript
- [ghostwords/chameleon](https://github.com/ghostwords/chameleon)
  Browser fingerprinting protection for everybody.
- [dBrowser/dbrowser](https://github.com/dBrowser/dbrowser)
  Web Browser For The Distributed Web (dweb://)
- [leggett/simplify](https://github.com/leggett/simplify)
  Browser extension to simplify Gmail's interface
- [yasirkula/UnitySimpleFileBrowser](https://github.com/yasirkula/UnitySimpleFileBrowser)
  A uGUI based runtime file browser for Unity 3D (draggable and resizable)
- [uzbl/uzbl](https://github.com/uzbl/uzbl)
  A web browser that adheres to the unix philosophy.
- [benji6/andromeda](https://github.com/benji6/andromeda)
  :musical_keyboard: Make Music In Your Browser!
- [1j01/elementary.css](https://github.com/1j01/elementary.css)
  elementary OS's stylesheet converted to browser CSS
- [UprootLabs/gngr](https://github.com/UprootLabs/gngr)
  a cross-platform browser focussed on privacy.
- [internetarchive/brozzler](https://github.com/internetarchive/brozzler)
  brozzler - distributed browser-based web crawler
- [LulumiProject/lulumi-browser](https://github.com/LulumiProject/lulumi-browser)
  Lulumi-browser is a lightweight browser coded with Vue.js 2 and Electron.
- [filamentgroup/select-css](https://github.com/filamentgroup/select-css)
  Cross-browser styles for consistent select element styling
- [philc/vimium](https://github.com/philc/vimium)
  The hacker's browser.
- [lecram/rover](https://github.com/lecram/rover)
  simple file browser for the terminal
- [w8r/avl](https://github.com/w8r/avl)
  :eyeglasses: Fast AVL tree for Node and browser
- [oldweb-today/browser-chrome](https://github.com/oldweb-today/browser-chrome)
  Chrome containerized browser for Webrecorder
- [JadenGeller/Helium](https://github.com/JadenGeller/Helium)
  A floating browser window for OS X
- [browserhtml/browserhtml](https://github.com/browserhtml/browserhtml)
  Experimental Servo browser built in HTML
- [Treora/precise-links](https://github.com/Treora/precise-links)
  Browser extension to support Web Annotation Selectors in URIs
- [kaktus/kaktus](https://github.com/kaktus/kaktus)
  Experimental web browser with minimalistic design
- [schollz/hostyoself](https://github.com/schollz/hostyoself)
  Host yo' self from your browser, your phone, your toaster.
- [WebMemex/webmemex-extension](https://github.com/WebMemex/webmemex-extension)
  📇 Your digital memory extension, as a browser extension
- [axios/axios](https://github.com/axios/axios)
  Promise based HTTP client for the browser and node.js
- [ferrolho/magnet-player](https://github.com/ferrolho/magnet-player)
  :movie_camera: A place for streaming torrents directly from your browser
- [mattdesl/color-wander](https://github.com/mattdesl/color-wander)
  :art: Generative artwork in node/browser based on a seeded random
- [kern/filepizza](https://github.com/kern/filepizza)
  :pizza: Peer-to-peer file transfers in your browser
- [PavelDoGreat/WebGL-Fluid-Simulation](https://github.com/PavelDoGreat/WebGL-Fluid-Simulation)
  Play with fluids in your browser (works even on mobile)
- [mattdesl/canvas-sketch](https://github.com/mattdesl/canvas-sketch)
  [beta] A framework for making generative artwork in JavaScript and the browser.
- [sebpiq/paulstretch.js](https://github.com/sebpiq/paulstretch.js)
  PaulStretch sound stretching algorithm in your browser
- [xbrowsersync/app](https://github.com/xbrowsersync/app)
  xBrowserSync browser extensions / mobile app
- [chylex/BetterTweetDeck](https://github.com/chylex/BetterTweetDeck)
  A browser extension to improve TweetDeck with a lot of features
- [mikecrittenden/shortkeys](https://github.com/mikecrittenden/shortkeys)
  A browser extension for custom keyboard shortcuts 
- [Jaredk3nt/homepage](https://github.com/Jaredk3nt/homepage)
  Custom homepage for use locally in browser
- [EFForg/privacybadger](https://github.com/EFForg/privacybadger)
  Privacy Badger is a browser extension that automatically learns to block invisible trackers.
- [johnste/finicky](https://github.com/johnste/finicky)
  A macOS app for customizing which browser to start
- [wexond/desktop](https://github.com/wexond/desktop)
  A privacy-focused, extensible and beautiful web browser
- [dot-browser/desktop](https://github.com/dot-browser/desktop)
  🌍 A beautiful browser with material UI, with built-in adblock, based on Wexond.
- [shrimpza/aurial](https://github.com/shrimpza/aurial)
  A browser client for streaming music from the Subsonic music server
- [gcarq/inox-patchset](https://github.com/gcarq/inox-patchset)
  Inox patchset tries to provide a minimal Chromium based browser with focus on privacy by disabling data transmission to Google.
- [bitwarden/browser](https://github.com/bitwarden/browser)
  The browser extension vault (Chrome, Firefox, Opera, Edge, Safari, & more).
- [jarun/Buku](https://github.com/jarun/Buku)
  :bookmark: Browser-independent bookmark manager
- [qutebrowser/qutebrowser](https://github.com/qutebrowser/qutebrowser)
  A keyboard-driven, vim-like browser based on PyQt5.
- [nuxt/consola](https://github.com/nuxt/consola)
  🐨 Elegant Console Logger for Node.js and Browser
- [mozilla-mobile/focus-android](https://github.com/mozilla-mobile/focus-android)
  Firefox Focus: The privacy browser - Browse like no one’s watching.
- [beakerbrowser/beaker](https://github.com/beakerbrowser/beaker)
  An experimental peer-to-peer Web browser
- [browsh-org/browsh](https://github.com/browsh-org/browsh)
  A fully-modern text-based browser, rendering to TTY and browsers
- [filebrowser/filebrowser](https://github.com/filebrowser/filebrowser)
  📂 Web File Browser which can be used as a middleware or standalone app.
- [kalcaddle/KodExplorer](https://github.com/kalcaddle/KodExplorer)
  A web based file manager,web IDE / browser based code editor
- [textileio/dapp-template](https://github.com/textileio/dapp-template)
  a basic template to build simple ipfs-based browser dapps
- [webextension-toolbox/webextension-toolbox](https://github.com/webextension-toolbox/webextension-toolbox)
  Small CLI toolbox for cross-browser WebExtension development
- [uwdata/datavore](https://github.com/uwdata/datavore)
  A small, fast, in-browser database engine written in JavaScript.
- [ggrossman/tty.js](https://github.com/ggrossman/tty.js)
  A terminal for your browser, using node/express/socket.io
- [chjj/tty.js](https://github.com/chjj/tty.js)
  A terminal for your browser, using node/express/socket.io
- [gkngkc/UnityStandaloneFileBrowser](https://github.com/gkngkc/UnityStandaloneFileBrowser)
  A native file browser for unity standalone platforms
- [andryou/scriptsafe](https://github.com/andryou/scriptsafe)
  a browser extension to bring security and privacy to chrome, firefox, and opera
- [cypress-io/cypress](https://github.com/cypress-io/cypress)
  Fast, easy and reliable testing for anything that runs in a browser.
- [madeas/box-shadows.css](https://github.com/madeas/box-shadows.css)
   :pisces: A cross-browser collection of CSS box-shadows
- [d4t4x/data-selfie](https://github.com/d4t4x/data-selfie)
  Data Selfie - a browser extension to track yourself on Facebook and analyze your data. 
- [bengarrett/RetroTxt](https://github.com/bengarrett/RetroTxt)
  RetroTxt is the WebExtension that turns ANSI, ASCII, NFO text into in-browser HTML
- [ritwickdey/vscode-live-sass-compiler](https://github.com/ritwickdey/vscode-live-sass-compiler)
  Compile Sass or Scss file to CSS at realtime with live browser reload feature.
- [unshiftio/url-parse](https://github.com/unshiftio/url-parse)
  Small footprint URL parser that works seamlessly across Node.js and browser environments.
- [duckduckgo/duckduckgo-privacy-extension](https://github.com/duckduckgo/duckduckgo-privacy-extension)
  DuckDuckGo Privacy Essentials browser extension for Firefox, Chrome, Safari.
- [textileio/duckduckgo-privacy-extension](https://github.com/textileio/duckduckgo-privacy-extension)
  DuckDuckGo Privacy Essentials browser extension for Firefox, Chrome, Safari.
- [cassidyjames/ephemeral](https://github.com/cassidyjames/ephemeral)
  A private-by-default, always-incognito browser for elementary OS. Inspired by Firefox Focus.
- [EFForg/https-everywhere](https://github.com/EFForg/https-everywhere)
  A browser extension that encrypts your communications with many websites that offer HTTPS but still allow unencrypted connections.
- [freeCodeCamp/devdocs](https://github.com/freeCodeCamp/devdocs)
  API Documentation Browser
- [EmailThis/extension-boilerplate](https://github.com/EmailThis/extension-boilerplate)
  ⚡️ A template for building cross browser extensions for Chrome, Opera & Firefox.
- [WorldBrain/Memex](https://github.com/WorldBrain/Memex)
  Browser Extension to full-text search your browsing history & bookmarks.
- [uptick/react-keyed-file-browser](https://github.com/uptick/react-keyed-file-browser)
  Folder based file browser given a flat keyed list of objects, powered by React.
- [xxhomey19/github-file-icon](https://github.com/xxhomey19/github-file-icon)
  🌈 🗂 A browser extension which gives different filetypes different icons to GitHub, GitLab, gitea and gogs.
- [google/lovefield](https://github.com/google/lovefield)
  Lovefield is a relational database for web apps. Written in JavaScript, works cross-browser. Provides SQL-like APIs that are fast, safe, and easy to use.
- [danny0838/webscrapbook](https://github.com/danny0838/webscrapbook)
  A browser extension that captures the web page faithfully with highly customizable configurations. This project inherits from ScrapBook X.
- [sindresorhus/refined-twitter](https://github.com/sindresorhus/refined-twitter)
  Browser extension that simplifies the Twitter interface and adds useful features
- [MrSaints/Morphext](https://github.com/MrSaints/Morphext)
  A simple, high-performance and cross-browser jQuery rotating / carousel plugin for text phrases powered by Animate.css.
- [cpojer/mootools-filemanager](https://github.com/cpojer/mootools-filemanager)
  A filemanager for the web based on MooTools that allows you to (pre)view, upload and modify files and folders via the browser.
- [ipfs-shipyard/ipfs-companion](https://github.com/ipfs-shipyard/ipfs-companion)
  Browser extension that simplifies access to IPFS resources
- [unconed/TermKit](https://github.com/unconed/TermKit)
  Experimental Terminal platform built on WebKit + node.js. Currently only for Mac and Windows, though the prototype works 90% in any WebKit browser.
- [Augustyniak/FileExplorer](https://github.com/Augustyniak/FileExplorer)
  FileExplorer is a powerful iOS file browser that allows its users to choose and remove files and/or directories
- [bokeh/bokeh](https://github.com/bokeh/bokeh)
  Interactive Data Visualization in the browser, from  Python
- [mi-g/weh](https://github.com/mi-g/weh)
  Toolkit to save time when developing a browser add-on for Firefox, Chrome, Edge, Opera and Vivaldi
- [mochajs/mocha](https://github.com/mochajs/mocha)
  ☕️ simple, flexible, fun javascript test framework for node.js & the browser
- [BackMarket/github-mermaid-extension](https://github.com/BackMarket/github-mermaid-extension)
  A browser extension for Chrome, Opera & Firefox that adds Mermaid language support to Github
- [jdepoix/youtube-transcript-api](https://github.com/jdepoix/youtube-transcript-api)
  This is an python API which allows you to get the transcripts/subtitles for a given YouTube video. It also works for automatically generated subtitles and it does not require a headless browser, like other selenium based solutions do!
- [chylex/Discord-History-Tracker](https://github.com/chylex/Discord-History-Tracker)
  Browser script that saves Discord chat history into a file, and an offline viewer that displays the file.
- [jvilk/BrowserFS](https://github.com/jvilk/BrowserFS)
  BrowserFS is an in-browser filesystem that emulates the Node JS filesystem API and supports storing and retrieving files from various backends.
- [oldweb-today/netcapsule](https://github.com/oldweb-today/netcapsule)
  Browse old web pages the old way with virtual browsers in the browser
- [sindresorhus/refined-github](https://github.com/sindresorhus/refined-github)
  Browser extension that simplifies the GitHub interface and adds useful features
- [Pinperepette/web-terminal](https://github.com/Pinperepette/web-terminal)
  Web-Terminal is a terminal server that provides remote CLI via standard web browser and HTTP protocol.
- [Vaporbook/epub-parser](https://github.com/Vaporbook/epub-parser)
  A web-friendly epub file parser. Useful in browser-based reading systems, command-line ebook production toolsets, and more.
- [liyasthomas/mnmlurl-extension](https://github.com/liyasthomas/mnmlurl-extension)
  💁 Browser extension for Minimal URL - Modern URL shortener with support for custom alias & can be hosted even in GitHub pages
- [allinurl/goaccess](https://github.com/allinurl/goaccess)
  GoAccess is a real-time web log analyzer and interactive viewer that runs in a terminal in *nix systems or through your browser.
- [tutorialzine/cute-files](https://github.com/tutorialzine/cute-files)
  A command line utility that turns the current working directory into a pretty online file browser
- [Jermolene/TiddlyWiki5](https://github.com/Jermolene/TiddlyWiki5)
  A self-contained JavaScript wiki for the browser, Node.js, AWS Lambda etc.
- [PrivateBin/PrivateBin](https://github.com/PrivateBin/PrivateBin)
  A minimalist, open source online pastebin where the server has zero knowledge of pasted data. Data is encrypted/decrypted in the browser using 256 bits AES.
- [samyk/evercookie](https://github.com/samyk/evercookie)
  Produces persistent, respawning "super" cookies in a browser, abusing over a dozen techniques. Its goal is to identify users after they've removed standard cookies and other privacy data such as Flash cookies (LSOs), HTML5 storage, SilverLight storage, and others.
- [pirate/ArchiveBox](https://github.com/pirate/ArchiveBox)
  🗃 The open source self-hosted web archive. Takes browser history/bookmarks/Pocket/Pinboard/etc., saves HTML, JS, PDFs, media, and more...
- [kamlekar/slim-scroll](https://github.com/kamlekar/slim-scroll)
  HTML element scroll bar as replacement for default browser's scroll-bar. Design as you want using CSS.
- [1j01/BrowserFS](https://github.com/1j01/BrowserFS)
  "BrowserFS is an in-browser filesystem that emulates the Node JS filesystem API and supports storing and retrieving files from various backends." It's pretty cool. I'm using it for https://98.js.org/
- [ptsource/PTLynx](https://github.com/ptsource/PTLynx)
  :eight_spoked_asterisk: PTLynx is a PTSource port for windows of the famous Lynx browser with mutt, tabbed browsing, pdf reader, image viewer, video player and TOR ready.
- [vutran/browser-bookmarks](https://github.com/vutran/browser-bookmarks)
  Retrieve bookmarks from different browsers.
- [ZMYaro/paintz](https://github.com/ZMYaro/paintz)
  A simple drawing app that runs in a web browser, designed to be an MS Paint substitute for Chromebooks and other Chrome OS devices.  PaintZ is free, but please consider supporting development at https://ko-fi.com/ZMYaro.
- [peers/peerjs](https://github.com/peers/peerjs)
  Peer-to-peer data in the browser.
- [jdittrich/quickMockup](https://github.com/jdittrich/quickMockup)
  HTML based interface mockups
