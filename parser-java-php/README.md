# Repositories defined by: parser, java, php

also defined by the following keywords: json, fast, api, written, markdown, vega, xml

- [videojs/m3u8-parser](https://github.com/videojs/m3u8-parser)
  An m3u8 parser.
- [alecalve/python-bitcoin-blockchain-parser](https://github.com/alecalve/python-bitcoin-blockchain-parser)
  A Python 3 Bitcoin blockchain parser
- [apiaryio/snowcrash](https://github.com/apiaryio/snowcrash)
  API Blueprint Parser
- [nikic/PHP-Parser](https://github.com/nikic/PHP-Parser)
  A PHP parser written in PHP
- [ustbhuangyi/lyric-parser](https://github.com/ustbhuangyi/lyric-parser)
  lyric-parser base on javascript
- [ndevilla/iniparser](https://github.com/ndevilla/iniparser)
  ini file parser
- [andrejewski/himalaya](https://github.com/andrejewski/himalaya)
  JavaScript HTML to JSON Parser
- [Instagram/ig-json-parser](https://github.com/Instagram/ig-json-parser)
  Fast JSON parser for java projects
- [carlanton/m3u8-parser](https://github.com/carlanton/m3u8-parser)
  A simple HLS playlist parser for Java
- [idank/bashlex](https://github.com/idank/bashlex)
  Python parser for bash
- [yacy/yacy_grid_parser](https://github.com/yacy/yacy_grid_parser)
  Parser Microservice for the YaCy Grid
- [sivareddyg/graph-parser](https://github.com/sivareddyg/graph-parser)
  GraphParser is a semantic parser which can convert natural language sentences to logical forms and graphs. 
- [euske/pdfminer](https://github.com/euske/pdfminer)
  Python PDF Parser
- [ben-strasser/fast-cpp-csv-parser](https://github.com/ben-strasser/fast-cpp-csv-parser)
  fast-cpp-csv-parser
- [udp/json-parser](https://github.com/udp/json-parser)
  Very low footprint JSON parser written in portable ANSI C
- [tanakh/cmdline](https://github.com/tanakh/cmdline)
  A Command Line Parser
- [swagger-api/swagger-parser](https://github.com/swagger-api/swagger-parser)
  Swagger Spec to Java POJOs
- [iktakahiro/youtube-url-parser](https://github.com/iktakahiro/youtube-url-parser)
  Parser for Youtube URL
- [lexborisov/myhtml](https://github.com/lexborisov/myhtml)
  Fast C/C++ HTML 5 Parser. Using threads.
- [erusev/parsedown](https://github.com/erusev/parsedown)
  Better Markdown Parser in PHP
- [graphql-dotnet/parser](https://github.com/graphql-dotnet/parser)
  A lexer and parser for GraphQL in .NET
- [Geal/nom](https://github.com/Geal/nom)
  Rust parser combinator framework
- [PulseMedia/python-tv-parser](https://github.com/PulseMedia/python-tv-parser)
  A lightweight python playlist parser
- [postlight/mercury-parser-api](https://github.com/postlight/mercury-parser-api)
  🚀 A drop-in replacement for the Mercury Parser API.
- [nuovo/vCard-parser](https://github.com/nuovo/vCard-parser)
  Easier and more concise vCard (.vcf) parser for PHP
- [kosma/minmea](https://github.com/kosma/minmea)
  a lightweight GPS NMEA 0183 parser library in pure C
- [shachaf/jsgif](https://github.com/shachaf/jsgif)
  JavaScript GIF parser and player
- [ikekonglp/TweeboParser](https://github.com/ikekonglp/TweeboParser)
  A Dependency Parser for Tweets
- [plataformatec/nimble_parsec](https://github.com/plataformatec/nimble_parsec)
  A simple and fast library for text-based parser combinators
- [miaolz123/vue-markdown](https://github.com/miaolz123/vue-markdown)
  A Powerful and Highspeed Markdown Parser for Vue
- [nikitakit/self-attentive-parser](https://github.com/nikitakit/self-attentive-parser)
  High-accuracy NLP parser with models for 11 languages.
- [orchestral/parser](https://github.com/orchestral/parser)
  [Package] XML Document Parser for Laravel and PHP
- [topfunky/hpple](https://github.com/topfunky/hpple)
  An XML/HTML parser for Objective-C, inspired by Hpricot.
- [textile/php-textile](https://github.com/textile/php-textile)
  Textile markup language parser for PHP
- [javaparser/javaparser](https://github.com/javaparser/javaparser)
   Java 1-13 Parser and Abstract Syntax Tree for Java –
- [globocom/m3u8](https://github.com/globocom/m3u8)
  Python m3u8 Parser for HTTP Live Streaming (HLS) Transmissions
- [vega/vega-expression](https://github.com/vega/vega-expression)
  Vega expression parser and code generator.
- [uwdata/vega-expression](https://github.com/uwdata/vega-expression)
  Vega expression parser and code generator.
- [markedjs/marked](https://github.com/markedjs/marked)
  A markdown parser and compiler. Built for speed.
- [jhy/jsoup](https://github.com/jhy/jsoup)
  jsoup: Java HTML Parser, with best of DOM, CSS, and jquery
- [remarkablemark/html-react-parser](https://github.com/remarkablemark/html-react-parser)
  :memo: HTML to React parser.
- [leethomason/tinyxml2](https://github.com/leethomason/tinyxml2)
  TinyXML2 is a simple, small, efficient, C++ XML parser that can be easily integrated into other programs.
- [scrapinghub/dateparser](https://github.com/scrapinghub/dateparser)
  python parser for human readable dates
- [mozilla/mp4parse-rust](https://github.com/mozilla/mp4parse-rust)
  Parser for ISO Base Media Format aka video/mp4 written in Rust.
- [sighingnow/parsec.py](https://github.com/sighingnow/parsec.py)
  A universal Python parser combinator library inspired by Parsec library of Haskell.
- [unshiftio/url-parse](https://github.com/unshiftio/url-parse)
  Small footprint URL parser that works seamlessly across Node.js and browser environments.
- [iron/body-parser](https://github.com/iron/body-parser)
  JSON body parsing for iron
- [mwaterfall/MWFeedParser](https://github.com/mwaterfall/MWFeedParser)
  An Objective-C RSS / Atom Feed Parser for iOS
- [mholt/PapaParse](https://github.com/mholt/PapaParse)
  Fast and powerful CSV (delimited text) parser that gracefully handles large files and malformed input
- [paquettg/php-html-parser](https://github.com/paquettg/php-html-parser)
  An HTML DOM parser. It allows you to manipulate HTML. Find tags on an HTML page with selectors just like jQuery.
- [feedparser/feedparser](https://github.com/feedparser/feedparser)
  feedparser gem - (universal) web feed parser and normalizer (XML w/ Atom or RSS, JSON Feed, HTML w/ Microformats e.g. h-entry/h-feed or Feed.HTML, Feed.TXT w/ YAML, JSON or INI & Markdown, etc.)
- [markdown-it/markdown-it](https://github.com/markdown-it/markdown-it)
  Markdown parser, done right. 100% CommonMark support, extensions, syntax plugins & high speed
- [expressjs/body-parser](https://github.com/expressjs/body-parser)
  Node.js body parsing middleware
- [Zod-/jsVideoUrlParser](https://github.com/Zod-/jsVideoUrlParser)
  A javascript parser to extract informations like provider, channel, id, start time from YouTube, Vimeo, Dailymotion, Twitch,... urls
- [sannies/mp4parser](https://github.com/sannies/mp4parser)
  A Java API to read, write and create MP4 files
- [leogps/ItunesLibraryParser](https://github.com/leogps/ItunesLibraryParser)
  Itunes Library Parser can parse the Itunes library, read the Tracks & Playlists as Java objects and thereby perform all sorts of operations on the files such as read itunes stored data about the tracks and playlists.
- [Vaporbook/epub-parser](https://github.com/Vaporbook/epub-parser)
  A web-friendly epub file parser. Useful in browser-based reading systems, command-line ebook production toolsets, and more.
- [mozilla/page-metadata-parser](https://github.com/mozilla/page-metadata-parser)
  A Javascript library for parsing metadata on a web page.
- [postlight/mercury-parser](https://github.com/postlight/mercury-parser)
  📜 Extract meaningful content from the chaos of a web page
- [Borewit/music-metadata](https://github.com/Borewit/music-metadata)
  Stream and file based music metadata parser for node. Supporting a wide range of audio and tag formats.
- [calibr/node-bookmarks-parser](https://github.com/calibr/node-bookmarks-parser)
  Parses Firefox/Chrome HTML bookmarks files
- [chylex/Better-Sprinting](https://github.com/chylex/Better-Sprinting)
  Minecraft mod that improves and expands sprinting, sneaking, and flying mechanics.
- [agnelvishal/Condense.press](https://github.com/agnelvishal/Condense.press)
- [vega/vega-lite-to-api](https://github.com/vega/vega-lite-to-api)
  Convert Vega-Lite JSON spec to Vega-Lite JS API
- [tn5250j/tn5250j](https://github.com/tn5250j/tn5250j)
  A 5250 terminal emulator for the AS/400 written in Java
- [schmittjoh/serializer](https://github.com/schmittjoh/serializer)
  Library for (de-)serializing data of any complexity (supports XML, JSON, YAML)
- [square/javapoet](https://github.com/square/javapoet)
  A Java API for generating .java source files.
- [jhipster/prettier-java](https://github.com/jhipster/prettier-java)
  Prettier Java Plugin
- [thephpleague/html-to-markdown](https://github.com/thephpleague/html-to-markdown)
  Convert HTML to Markdown with PHP
- [oracle/opengrok](https://github.com/oracle/opengrok)
  OpenGrok is a fast and usable source code search and cross reference engine, written in Java
- [1j01/nano-memoize](https://github.com/1j01/nano-memoize)
  Faster than fast, smaller than micro ... a nano speed and nano size memoize.
