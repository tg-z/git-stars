# Repositories defined by: web, application, archiving

also defined by the following keywords: audio, distributed, framework, apps, view, page, pages

- [itkach/aard2-web](https://github.com/itkach/aard2-web)
- [walkor/workerman-todpole-web](https://github.com/walkor/workerman-todpole-web)
  小蝌蚪互动聊天室-适合虚拟空间使用（后端直接与workerman主机通讯。请保留页面上workerman的相关链接）
- [w3ctag/ethical-web-principles](https://github.com/w3ctag/ethical-web-principles)
  W3C TAG Ethical Web Principles
- [web-scrobbler/web-scrobbler](https://github.com/web-scrobbler/web-scrobbler)
  Scrobble music all around the web!
- [meganz/webclient](https://github.com/meganz/webclient)
  The mega.nz web client
- [dBrowser/dbrowser](https://github.com/dBrowser/dbrowser)
  Web Browser For The Distributed Web (dweb://)
- [borismus/spectrogram](https://github.com/borismus/spectrogram)
  Web Audio Spectrogram
- [PACTCare/Dweb.page](https://github.com/PACTCare/Dweb.page)
  Your Gateway to the Distributed Web
- [webrecorder/webrecorder](https://github.com/webrecorder/webrecorder)
  Web Archiving For All!
- [atomotic/webrecorder](https://github.com/atomotic/webrecorder)
  Web Archiving For All!
- [espebra/filebin](https://github.com/espebra/filebin)
  Filebin is a web application that facilitates convenient file sharing over the web.
- [beakerbrowser/beaker-sidebar-app](https://github.com/beakerbrowser/beaker-sidebar-app)
  Web application that drives Beaker's sidebar view
- [tdewolff/minify](https://github.com/tdewolff/minify)
  Go minifiers for web formats
- [sveltejs/svelte](https://github.com/sveltejs/svelte)
  Cybernetically enhanced web apps
- [Pinperepette/cansina](https://github.com/Pinperepette/cansina)
  Web Content Discovery Tool
- [eversum/galacteek](https://github.com/eversum/galacteek)
  Browser for the distributed web
- [xtermjs/xterm.js](https://github.com/xtermjs/xterm.js)
  A terminal for the web
- [m4ll0k/WAScan](https://github.com/m4ll0k/WAScan)
  WAScan - Web Application Scanner
- [Pinperepette/awesome-web-hacking](https://github.com/Pinperepette/awesome-web-hacking)
  A list of web application security
- [BowenYin/harker-bell](https://github.com/BowenYin/harker-bell)
  Bell schedule app built for the future of the web.
- [tdewolff/parse](https://github.com/tdewolff/parse)
  Go parsers for web formats
- [angular/angular.js](https://github.com/angular/angular.js)
  AngularJS - HTML enhanced for web apps!
- [zadam/trilium-web-clipper](https://github.com/zadam/trilium-web-clipper)
  Save web clippings to Trilium Notes.
- [tidyverse/rvest](https://github.com/tidyverse/rvest)
  Simple web scraping for R
- [birros/web-archives](https://github.com/birros/web-archives)
  A web archives reader
- [jgthms/web-design-in-4-minutes](https://github.com/jgthms/web-design-in-4-minutes)
  Learn the basics of web design in 4 minutes
- [dhamaniasad/HeadlessBrowsers](https://github.com/dhamaniasad/HeadlessBrowsers)
  A list of (almost) all headless web browsers in existence
- [yudai/gotty](https://github.com/yudai/gotty)
  Share your terminal as a web application
- [atomotic/pmwct](https://github.com/atomotic/pmwct)
  Poor Man's Web Curator Tool
- [gasolin/webbymouse](https://github.com/gasolin/webbymouse)
  Turn your mobile phone into Air Mouse and touchpad with full web technology
- [ShizukuIchi/winXP](https://github.com/ShizukuIchi/winXP)
  🏁 Web based Windows XP desktop recreation. 
- [uzbl/uzbl](https://github.com/uzbl/uzbl)
  A web browser that adheres to the unix philosophy.
- [broskoski/pilgrim](https://github.com/broskoski/pilgrim)
  Bookmarklet and manual webcrawler to aid in web research
- [tg-z/pilgrim](https://github.com/tg-z/pilgrim)
  Bookmarklet and manual webcrawler to aid in web research
- [aredotna/pilgrim](https://github.com/aredotna/pilgrim)
  Bookmarklet and manual webcrawler to aid in web research
- [uwdata/draco-vis](https://github.com/uwdata/draco-vis)
  Draco on the web
- [cybercongress/cyb](https://github.com/cybercongress/cyb)
  🌎 Personal immortal robot for the The Great Web
- [snwolak/steemblr](https://github.com/snwolak/steemblr)
  Microblogging web app powered by steem blockchain.
- [Pinperepette/Spaghetti](https://github.com/Pinperepette/Spaghetti)
  Spaghetti - Web Application Security Scanner
- [webrecorder/pywb](https://github.com/webrecorder/pywb)
  Core Python Web Archiving Toolkit for replay and recording of web archives
- [Prettyhtml/prettyhtml](https://github.com/Prettyhtml/prettyhtml)
  💅 The formatter for the modern web https://prettyhtml.netlify.com/
- [henshin/filebuster](https://github.com/henshin/filebuster)
  An extremely fast and flexible web fuzzer
- [ivanceras/sauron](https://github.com/ivanceras/sauron)
  Sauron is an html web framework for building web-apps. It is heavily inspired by elm.
- [aroneiermann/readability-luin](https://github.com/aroneiermann/readability-luin)
  Turn any web page into a clean view
- [broskoski/readability](https://github.com/broskoski/readability)
  Turn any web page into a clean view
- [OwlyStuff/amazium](https://github.com/OwlyStuff/amazium)
  The responsive CSS web framework
- [webtorrent/instant.io](https://github.com/webtorrent/instant.io)
  🚀 Streaming file transfer over WebTorrent (torrents on the web)
- [get-set-fetch/extension](https://github.com/get-set-fetch/extension)
  web scraping extension
- [google/WebFundamentals](https://github.com/google/WebFundamentals)
  Best practices for modern web development
- [duyetdev/awesome-web-scraper](https://github.com/duyetdev/awesome-web-scraper)
  A collection of awesome web scaper, crawler.
- [1j01/choon.js](https://github.com/1j01/choon.js)
  🎹 Choon language interpreter in javascript with the Web Audio API.
- [trailsjs/trails](https://github.com/trailsjs/trails)
  :evergreen_tree: Modern Web Application Framework for Node.js.
- [ShizukuIchi/minesweeper](https://github.com/ShizukuIchi/minesweeper)
  💣 Windows XP minesweeper in web. Also support mobile. 🎉📱
- [leinov/awesome-web-you-should-know](https://github.com/leinov/awesome-web-you-should-know)
  🌎awesome web you should know
- [clappr/clappr](https://github.com/clappr/clappr)
  :clapper: An extensible media player for the web.
- [Treora/precise-links](https://github.com/Treora/precise-links)
  Browser extension to support Web Annotation Selectors in URIs
- [emmetio/emmet](https://github.com/emmetio/emmet)
  The essential toolkit for web-developers
- [internetarchive/brozzler](https://github.com/internetarchive/brozzler)
  brozzler - distributed browser-based web crawler
- [gaojiuli/toapi](https://github.com/gaojiuli/toapi)
  Every web site provides APIs.
- [niruix/sshwifty](https://github.com/niruix/sshwifty)
  A SSH and Telnet connector made for the Web (Web SSH and Telnet)
- [mojolicious/mojo](https://github.com/mojolicious/mojo)
  :sparkles: Mojolicious - Perl real-time web framework
- [dvoiss/android-geocities-theme](https://github.com/dvoiss/android-geocities-theme)
  Bringing the best of the web to Android!
- [jstrieb/urlpages](https://github.com/jstrieb/urlpages)
  Create and view web pages stored entirely in the URL 
- [postlight/mercury-parser](https://github.com/postlight/mercury-parser)
  📜 Extract meaningful content from the chaos of a web page
- [kaktus/kaktus](https://github.com/kaktus/kaktus)
  Experimental web browser with minimalistic design
- [Utazukin/Ichaival](https://github.com/Utazukin/Ichaival)
  Android client for the LANraragi manga/doujinshi web manager.
- [kazzkiq/CodeFlask](https://github.com/kazzkiq/CodeFlask)
  A micro code-editor for awesome web pages.
- [Pinperepette/webpwn3r](https://github.com/Pinperepette/webpwn3r)
  WebPwn3r - Web Applications Security Scanner.
- [aframevr/aframe](https://github.com/aframevr/aframe)
  :a: web framework for building virtual reality experiences.
- [zeit/hyper](https://github.com/zeit/hyper)
  A terminal built on web technologies
- [SubstratumNetwork/SubstratumNode](https://github.com/SubstratumNetwork/SubstratumNode)
  WE ARE THE FOUNDATION  OF THE DECENTRALIZED WEB.
- [m4rk3r/pywb](https://github.com/m4rk3r/pywb)
  Python WayBack for web archive replay and url-rewriting HTTP/S web proxy
- [1j01/pipes](https://github.com/1j01/pipes)
  💿 Classic 3D Pipes screensaver remake (web-based)
- [substance/substance](https://github.com/substance/substance)
  A JavaScript library for web-based content editing.
- [kjam/python-web-scraping-tutorial](https://github.com/kjam/python-web-scraping-tutorial)
  A Python-based web and data scraping tutorial
- [nepaul/awesome-web-components](https://github.com/nepaul/awesome-web-components)
  A curated list of awesome web components.
- [HaNdTriX/generator-chrome-extension-kickstart](https://github.com/HaNdTriX/generator-chrome-extension-kickstart)
  Scaffold out a Web Extension http://yeoman.io
- [uwdata/vega-editor](https://github.com/uwdata/vega-editor)
  Web-based Vega authoring environment.
- [conversejs/converse.js](https://github.com/conversejs/converse.js)
  Web-based XMPP/Jabber chat client written in JavaScript
- [cryptee/web-client](https://github.com/cryptee/web-client)
  Cryptee's web client source code for all platforms.
- [gdisf/teaching-materials](https://github.com/gdisf/teaching-materials)
  GDI SF - Web Development and Programming Curriculum
- [benji6/virtual-audio-graph](https://github.com/benji6/virtual-audio-graph)
  :notes: Library for declaratively manipulating the Web Audio API
- [wulkano/kap](https://github.com/wulkano/kap)
  An open-source screen recorder built with web technology
- [nolanlawson/pinafore](https://github.com/nolanlawson/pinafore)
  Alternative web client for Mastodon
- [mohayonao/web-audio-engine](https://github.com/mohayonao/web-audio-engine)
  Pure JS implementation of the Web Audio API
- [hemanth/awesome-pwa](https://github.com/hemanth/awesome-pwa)
  Awesome list of progressive web apps! (PR welcomed ;))
- [kalcaddle/KodExplorer](https://github.com/kalcaddle/KodExplorer)
  A web based file manager,web IDE / browser based code editor
- [internetarchive/heritrix3](https://github.com/internetarchive/heritrix3)
  Heritrix is the Internet Archive's open-source, extensible, web-scale, archival-quality web crawler project.  
- [iipc/awesome-web-archiving](https://github.com/iipc/awesome-web-archiving)
  An Awesome List for getting started with web archiving
- [atomotic/awesome-web-archiving](https://github.com/atomotic/awesome-web-archiving)
  An Awesome List for getting started with web archiving
- [dmstern/html2biblatex](https://github.com/dmstern/html2biblatex)
  A tiny bookmarklet for exporting web pages to BibLaTeX (all browsers / no installation).
- [FacettsOpen/exodify](https://github.com/FacettsOpen/exodify)
  Wonder how much an app is tracking you? Now you can see it from within the Google Play web interface thanks to ExodusPrivacy.
- [pirate/awesome-web-archiving](https://github.com/pirate/awesome-web-archiving)
  An Awesome List for getting started with web archiving
- [juancarlospaco/css-html-js-minify](https://github.com/juancarlospaco/css-html-js-minify)
  StandAlone Async cross-platform Minifier for the Web.
- [logeshpaul/UI-Components](https://github.com/logeshpaul/UI-Components)
  Most used UI Components — of web applications. Curated/Most loved components for web development
- [DistributedWeb/dweb](https://github.com/DistributedWeb/dweb)
  New Protocol For The Decentralized Web
- [choojs/bankai](https://github.com/choojs/bankai)
  :station: - friendly web compiler
- [atomotic/public-web-archives](https://github.com/atomotic/public-web-archives)
  A listing of world wide web archives, for humans and machines using Web Archive Manifest (WAM) yaml format
- [KennethWangDotDev/TypographyHandbook](https://github.com/KennethWangDotDev/TypographyHandbook)
  A concise, referential guide on best web typographic practices.
- [atomotic/webarchiveplayer](https://github.com/atomotic/webarchiveplayer)
  Desktop application for browsing web archives (WARC and ARC)
- [mstefaniuk/graph-viz-d3-js](https://github.com/mstefaniuk/graph-viz-d3-js)
  Graphviz web D3.js renderer
- [ionic-team/capacitor](https://github.com/ionic-team/capacitor)
  Build cross-platform Native Progressive Web Apps for iOS, Android, and the web ⚡️
- [pybites/pytip](https://github.com/pybites/pytip)
  Building a Simple Web App With Bottle, SQLAlchemy, and the Twitter API
- [mozilla/page-metadata-parser](https://github.com/mozilla/page-metadata-parser)
  A Javascript library for parsing metadata on a web page.
- [enilu/web-flash](https://github.com/enilu/web-flash)
  WEB-FLASH --  Admin Framework and Mobile Website Based on Spring Boot and Vue.js
- [theaveragedude/mediacat](https://github.com/theaveragedude/mediacat)
  Simple tool to grab magnet URLs from the web
- [WebMemex/freeze-dry](https://github.com/WebMemex/freeze-dry)
  Snapshots a web page to get it as a static, self-contained HTML document.
- [chylex/Userscripts](https://github.com/chylex/Userscripts)
  A collection of small userscripts I made, because to hell with modern web assholery.
- [ritiek/spotify-remote](https://github.com/ritiek/spotify-remote)
  Web-UI for Spotify Desktop that actually works
- [machawk1/wail](https://github.com/machawk1/wail)
  :whale2: Web Archiving Integration Layer: One-Click User Instigated Preservation
- [rhysd/Mstdn](https://github.com/rhysd/Mstdn)
  Tiny web-based mastodon client for your desktop
- [1j01/translate-great](https://github.com/1j01/translate-great)
  Translate web app UI inline (just an experiment! less than a day's work!)
- [lintool/warcbase](https://github.com/lintool/warcbase)
  Warcbase is an open-source platform for managing analyzing web archives
- [gildas-lormeau/SingleFileZ](https://github.com/gildas-lormeau/SingleFileZ)
  SingleFileZ is a Web Extension for saving web pages as self-extracting HTML/ZIP hybrid files.
- [hrbrmstr/cdx](https://github.com/hrbrmstr/cdx)
  🕸 Query Web Archive Crawl Indexes ('CDX')
- [twbs/bootstrap](https://github.com/twbs/bootstrap)
  The most popular HTML, CSS, and JavaScript framework for developing responsive, mobile first projects on the web.
- [TheOdinProject/curriculum](https://github.com/TheOdinProject/curriculum)
  The open curriculum for learning web development
- [atomotic/pywb-recorder-tor](https://github.com/atomotic/pywb-recorder-tor)
  pywb recorder over tor, anonymously records the web. (docker image)
- [Pinperepette/web-terminal](https://github.com/Pinperepette/web-terminal)
  Web-Terminal is a terminal server that provides remote CLI via standard web browser and HTTP protocol.
- [1j01/jsoneditor](https://github.com/1j01/jsoneditor)
  A web-based tool to view, edit and format JSON
- [duzun/hQuery.php](https://github.com/duzun/hQuery.php)
  An extremely fast web scraper that parses megabytes of HTML in a blink of an eye. PHP5.3+, no dependencies.
- [1j01/tuna](https://github.com/1j01/tuna)
  An audio effects library for Web Audio, made by www.dinahmoe.com
- [discordapp/electron](https://github.com/discordapp/electron)
  Build cross platform desktop apps with web technologies
- [alexgibson/wavepad](https://github.com/alexgibson/wavepad)
  An experimental synthesizer built using the Web Audio API (PWA).
- [harvard-lil/warcgames](https://github.com/harvard-lil/warcgames)
  Hacking challenges to learn web archive security.
- [wummel/linkchecker](https://github.com/wummel/linkchecker)
  check links in web documents or full websites
- [filebrowser/filebrowser](https://github.com/filebrowser/filebrowser)
  📂 Web File Browser which can be used as a middleware or standalone app.
- [arewedistributedyet/arewedistributedyet](https://github.com/arewedistributedyet/arewedistributedyet)
  Website + Community effort to unlock the peer-to-peer web at arewedistributedyet.com ⚡🌐🔑
- [RobinLinus/snapdrop](https://github.com/RobinLinus/snapdrop)
  A Progressive Web App for local file sharing 
- [mickael-kerjean/filestash](https://github.com/mickael-kerjean/filestash)
  🦄 A modern web client for SFTP, S3, FTP, WebDAV, Git, S3, FTPS, Minio, LDAP, Caldav, Carddav, Mysql, Backblaze, ...
- [BruceDone/awesome-crawler](https://github.com/BruceDone/awesome-crawler)
  A collection of awesome web crawler,spider in different languages
- [wallabag/wallabag](https://github.com/wallabag/wallabag)
  wallabag is a self hostable application for saving web pages: Save and classify articles. Read them later. Freely.
- [feross/bitmidi.com](https://github.com/feross/bitmidi.com)
  🎹 Listen to free MIDI songs, download the best MIDI files, and share the best MIDIs on the web
- [danburzo/percollate](https://github.com/danburzo/percollate)
  🌐 → 📖 A command-line tool to turn web pages into beautifully formatted PDFs
- [berbaquero/reeddit](https://github.com/berbaquero/reeddit)
  Minimal, elastic Reddit reader web-app client
- [a11yproject/a11yproject.com](https://github.com/a11yproject/a11yproject.com)
  A community–driven effort to make web accessibility easier.
- [parcel-bundler/parcel](https://github.com/parcel-bundler/parcel)
  📦🚀 Blazing fast, zero configuration web application bundler
- [yacy/yacy_search_server](https://github.com/yacy/yacy_search_server)
  Distributed Peer-to-Peer Web Search Engine and Intranet Search Appliance
- [wexond/desktop](https://github.com/wexond/desktop)
  A privacy-focused, extensible and beautiful web browser
- [ansilove/ansilove.js](https://github.com/ansilove/ansilove.js)
  A script to display ANSi and artscene related file formats on web pages
- [gokulkrishh/demo-progressive-web-app](https://github.com/gokulkrishh/demo-progressive-web-app)
  🎉 A demo for progressive web application with features like offline, push notifications, background sync etc,
- [RitikPatni/Front-End-Web-Development-Resources](https://github.com/RitikPatni/Front-End-Web-Development-Resources)
  This repository contains content which will be helpful in your journey as a front-end Web Developer
- [Upload/Up1](https://github.com/Upload/Up1)
  Client-side encrypted image host web server
- [andrew--r/channels](https://github.com/andrew--r/channels)
  📺 A collection of useful YouTube channels for javascript developers and web designers
- [beakerbrowser/beaker](https://github.com/beakerbrowser/beaker)
  An experimental peer-to-peer Web browser
- [danny0838/webscrapbook](https://github.com/danny0838/webscrapbook)
  A browser extension that captures the web page faithfully with highly customizable configurations. This project inherits from ScrapBook X.
- [N0taN3rd/node-warc](https://github.com/N0taN3rd/node-warc)
  Parse And Create Web ARChive  (WARC) files with node.js
- [RelaxedJS/ReLaXed](https://github.com/RelaxedJS/ReLaXed)
  Create PDF documents using web technologies
- [Cyclenerd/gallery_shell](https://github.com/Cyclenerd/gallery_shell)
  📷 Bash Script to generate static responsive image web galleries.
- [RonenNess/RPGUI](https://github.com/RonenNess/RPGUI)
  Lightweight framework for old-school RPG GUI in web!
- [lolstring/window98-html-css-js](https://github.com/lolstring/window98-html-css-js)
  Windows 98 on the Web using HTML5, CSS3 and JS.
- [eugeneware/warc](https://github.com/eugeneware/warc)
  Parse WARC (Web Archive Files) as a node.js stream
- [pure-css/pure](https://github.com/pure-css/pure)
  A set of small, responsive CSS modules that you can use in every web project.
- [andrewbrey/netflix-tweaked](https://github.com/andrewbrey/netflix-tweaked)
  A web extension to tweak the Netflix home screen, preventing auto-play trailers and moving your lists to the top.
- [ArchiveTeam/grab-site](https://github.com/ArchiveTeam/grab-site)
  The archivist's web crawler: WARC output, dashboard for all crawls, dynamic ignore patterns
- [cambrant/txtngin](https://github.com/cambrant/txtngin)
  A simple web.py project which implements a static file hyperlink-less wiki, of sorts.
- [MorvanZhou/easy-scraping-tutorial](https://github.com/MorvanZhou/easy-scraping-tutorial)
  Simple but useful Python web scraping tutorial code. 
- [jakubroztocil/cloudtunes](https://github.com/jakubroztocil/cloudtunes)
  Web-based music player for the cloud :cloud: :notes: Play music from YouTube, Dropbox, etc.
- [nisrulz/app-privacy-policy-generator](https://github.com/nisrulz/app-privacy-policy-generator)
  A simple web app to generate a generic privacy policy for your Android/iOS apps
- [caddyserver/caddy](https://github.com/caddyserver/caddy)
  Fast, cross-platform HTTP/2 web server with automatic HTTPS
- [adobe/brackets](https://github.com/adobe/brackets)
  An open source code editor for the web, written in JavaScript, HTML and CSS.
- [mozilla/web-ext](https://github.com/mozilla/web-ext)
  A command line tool to help build, run, and test web extensions
- [Kozea/WeasyPrint](https://github.com/Kozea/WeasyPrint)
  WeasyPrint converts web documents (HTML with CSS, SVG, …) to PDF.
- [ZeeRooo/MaterialFBook](https://github.com/ZeeRooo/MaterialFBook)
  A light alternative client for Facebook based on the web with a modern look (Material Design)
- [danielmahal/Rumpetroll](https://github.com/danielmahal/Rumpetroll)
  Rumpetroll is a massive-multiplayer experiment. It's purpose was to try out new open web technologies like WebSockets and Canvas.
- [ericyd/gdrive-copy](https://github.com/ericyd/gdrive-copy)
  Web app to copy a Google Drive folder
- [ikreymer/islandora_solution_pack_web_archive](https://github.com/ikreymer/islandora_solution_pack_web_archive)
  Adds all required Fedora objects to allow users to ingest and retrieve web archives through the Islandora interface.
- [remigallego/winampify](https://github.com/remigallego/winampify)
  ⚡ A Spotify web client with an OS-looking interface and a reimplementation of the classic audio player Winamp.
- [arthurbergmz/webpack-pwa-manifest](https://github.com/arthurbergmz/webpack-pwa-manifest)
  Progressive Web App Manifest Generator for Webpack, with auto icon resizing and fingerprinting support.
- [zTrix/webpage2html](https://github.com/zTrix/webpage2html)
  save/convert web pages to a standalone editable html file for offline archive/view/edit/play/whatever
- [Y2Z/monolith](https://github.com/Y2Z/monolith)
  :black_large_square: CLI tool for saving complete web pages as a single HTML file
- [1j01/98](https://github.com/1j01/98)
  💿 Web-based Windows 98 desktop recreation 💾
- [webclipper/web-clipper](https://github.com/webclipper/web-clipper)
  Support Notion OneNote Bear Yuque。Clip anything to anywhere
- [zinas/the-90s-polyfil](https://github.com/zinas/the-90s-polyfil)
  Only a true visionary could think about using one of the most modern tools the web platform offers, to re-create some of the most horrific things ever created
- [CIRCL/lookyloo](https://github.com/CIRCL/lookyloo)
  Lookyloo is a web interface allowing to scrape a website and then displays a tree of domains calling each other.
- [canonical-web-and-design/vanilla-framework](https://github.com/canonical-web-and-design/vanilla-framework)
  From community websites to web applications, this CSS framework will help you achieve a consistent look and feel.
- [Arkiver2/warcio](https://github.com/Arkiver2/warcio)
  Streaming WARC/ARC library for fast web archive IO
- [oldweb-today/netcapsule](https://github.com/oldweb-today/netcapsule)
  Browse old web pages the old way with virtual browsers in the browser
- [gildas-lormeau/SingleFile](https://github.com/gildas-lormeau/SingleFile)
  Web Extension for Firefox/Chrome and CLI tool to save a faithful copy of a complete web page as a single HTML file
- [efeiefei/node-file-manager](https://github.com/efeiefei/node-file-manager)
  File manager web server based on Node.js with Koa, Angular.js and Bootstrap
- [preziotte/party-mode](https://github.com/preziotte/party-mode)
  An experimental music visualizer using d3.js and the web audio api.
- [nikolak/django_reddit](https://github.com/nikolak/django_reddit)
  Reddit clone written in python using django web framework and twitter's bootstrap.
- [essence/essence](https://github.com/essence/essence)
  Extracts information about web pages, like youtube videos, twitter statuses or blog articles.
- [faressoft/terminalizer](https://github.com/faressoft/terminalizer)
  🦄 Record your terminal and generate animated gif images or share a web player
- [alvarcarto/url-to-pdf-api](https://github.com/alvarcarto/url-to-pdf-api)
  Web page PDF/PNG rendering done right. Self-hosted service for rendering receipts, invoices, or any content.
- [syxanash/awesome-gui-websites](https://github.com/syxanash/awesome-gui-websites)
  Websites, web apps, portfolios which look like desktop computer graphical user interfaces
- [ZachSaucier/Just-Read](https://github.com/ZachSaucier/Just-Read)
  A customizable read mode web extension.
- [Taritsyn/WebMarkupMin](https://github.com/Taritsyn/WebMarkupMin)
  The Web Markup Minifier (abbreviated WebMarkupMin) - a .NET library that contains a set of markup minifiers. The objective of this project is to improve the performance of web applications by reducing the size of HTML, XHTML and XML code.
- [zero-to-mastery/resources](https://github.com/zero-to-mastery/resources)
  Here is a list of best resources for the developers (mostly web). Feel free to add your resources as well because sharing is caring 
- [google/lovefield](https://github.com/google/lovefield)
  Lovefield is a relational database for web apps. Written in JavaScript, works cross-browser. Provides SQL-like APIs that are fast, safe, and easy to use.
- [webrecorder/warcit](https://github.com/webrecorder/warcit)
  Convert Directories, Files and ZIP Files to Web Archives (WARC)
- [cpojer/mootools-filemanager](https://github.com/cpojer/mootools-filemanager)
  A filemanager for the web based on MooTools that allows you to (pre)view, upload and modify files and folders via the browser.
- [Piwigo/Piwigo](https://github.com/Piwigo/Piwigo)
  Manage your photos with Piwigo, a full featured open source photo gallery application for the web. Star us on Github! More than 200 plugins and themes available. Join us and contribute!
- [Moonbase59/kplaylist](https://github.com/Moonbase59/kplaylist)
  Manage your music via the Web: Stream, upload, make playlists, share, search, download and a lot more.
- [Markshall/MaterialFormComponents](https://github.com/Markshall/MaterialFormComponents)
  A few basic web-form components styled with inspiration from the Google Material design language.
- [ikreymer/webarchiveplayer](https://github.com/ikreymer/webarchiveplayer)
  NOTE: This project is no longer being actively developed.. Check out Webrecorder Player for the latest player. https://github.com/webrecorder/webrecorderplayer-electron) (Legacy: Desktop application for browsing web archives (WARC and ARC)
- [Treora/dom-anchor-selector](https://github.com/Treora/dom-anchor-selector)
  WIP Convert a Web Annotation Selector to a DOM Range object, and vice versa.
- [prasathmani/tinyfilemanager](https://github.com/prasathmani/tinyfilemanager)
  Web based File Manager in single PHP file, Manage your files efficiently and easily with Tiny File Manager
- [ricklamers/gridstudio](https://github.com/ricklamers/gridstudio)
  Grid studio is a web-based application for data science with full integration of open source data science frameworks and languages.
- [CognitionGuidedSurgery/cli2web](https://github.com/CognitionGuidedSurgery/cli2web)
  CLI application as a Service via Restful HTTP
- [webrecorder/warcio](https://github.com/webrecorder/warcio)
  Streaming WARC/ARC library for fast web archive IO
- [kerrbrittany9/tamagotchi-react](https://github.com/kerrbrittany9/tamagotchi-react)
  web app using moment and in React.js to feed, play and rest a little creature you've created
- [allinurl/goaccess](https://github.com/allinurl/goaccess)
  GoAccess is a real-time web log analyzer and interactive viewer that runs in a terminal in *nix systems or through your browser.
- [transloadit/uppy](https://github.com/transloadit/uppy)
  The next open source file uploader for web browsers :dog: 
- [PatMartin/Dex](https://github.com/PatMartin/Dex)
  Dex : The Data Explorer -- A data visualization tool written in Java/Groovy/JavaFX capable of powerful ETL and publishing web visualizations.
- [expo/expo](https://github.com/expo/expo)
  An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
- [NativeScript/NativeScript](https://github.com/NativeScript/NativeScript)
  NativeScript is an open source framework for building truly native mobile apps with JavaScript. Use web skills, like Angular and Vue.js, FlexBox and CSS, and get native UI and performance on iOS and Android.
- [google/web-starter-kit](https://github.com/google/web-starter-kit)
  Web Starter Kit - a workflow for multi-device websites
- [pirate/ArchiveBox](https://github.com/pirate/ArchiveBox)
  🗃 The open source self-hosted web archive. Takes browser history/bookmarks/Pocket/Pinboard/etc., saves HTML, JS, PDFs, media, and more...
- [containers-everywhere/contain-google](https://github.com/containers-everywhere/contain-google)
  Google Container isolates your Google activity from the rest of your web activity in order to prevent Google from tracking you outside of the Google website via third party cookies.
- [ritiek/muxnect](https://github.com/ritiek/muxnect)
  Send input to just about any interactive command-line tool through a local web server
- [chinchang/web-maker](https://github.com/chinchang/web-maker)
  A blazing fast & offline frontend playground
- [OnsenUI/OnsenUI](https://github.com/OnsenUI/OnsenUI)
  Mobile app development framework and SDK using HTML5 and JavaScript. Create beautiful and performant cross-platform mobile apps. Based on Web Components, and provides bindings for Angular 1, 2, React and Vue.js.
- [vuejs/vue](https://github.com/vuejs/vue)
  🖖 Vue.js is a progressive, incrementally-adoptable JavaScript framework for building UI on the web.
- [Vaporbook/epub-parser](https://github.com/Vaporbook/epub-parser)
  A web-friendly epub file parser. Useful in browser-based reading systems, command-line ebook production toolsets, and more.
- [Kickball/awesome-selfhosted](https://github.com/Kickball/awesome-selfhosted)
  This is a list of Free Software network services and web applications which can be hosted locally. Selfhosting is the process of locally hosting and managing applications instead of renting from SaaS providers.
- [ZMYaro/paintz](https://github.com/ZMYaro/paintz)
  A simple drawing app that runs in a web browser, designed to be an MS Paint substitute for Chromebooks and other Chrome OS devices.  PaintZ is free, but please consider supporting development at https://ko-fi.com/ZMYaro.
- [oVirt/ovirt-web-ui](https://github.com/oVirt/ovirt-web-ui)
  Modern lightweight UI for standard (non-admin) oVirt users
- [Softwire/Workflowy2Web](https://github.com/Softwire/Workflowy2Web)
  A tool for converting a content outline in Workflowy to a working prototype in HTML
- [matomo-org/matomo](https://github.com/matomo-org/matomo)
  Liberating Web Analytics. Star us on Github? +1. Matomo is the leading open alternative to Google Analytics that gives you full control over your data. Matomo lets you easily collect data from websites, apps & the IoT and visualise this data and extract insights. Privacy is built-in. We love Pull Requests! 
- [feedparser/feedparser](https://github.com/feedparser/feedparser)
  feedparser gem - (universal) web feed parser and normalizer (XML w/ Atom or RSS, JSON Feed, HTML w/ Microformats e.g. h-entry/h-feed or Feed.HTML, Feed.TXT w/ YAML, JSON or INI & Markdown, etc.)
- [blackdotsh/getIPIntel](https://github.com/blackdotsh/getIPIntel)
  IP Intelligence is a free Proxy VPN TOR and Bad IP detection tool to prevent Fraud, stolen content, and malicious users. Block proxies, VPN connections, web host IPs, TOR IPs, and compromised systems with a simple API. GeoIP lookup available.
- [jcubic/jquery.terminal](https://github.com/jcubic/jquery.terminal)
  jQuery Terminal Emulator - web based terminal
- [atomotic/oaidc-datacite-web](https://github.com/atomotic/oaidc-datacite-web)
  Generate a DataCite xml record and mint a DOI identifier from an oaidc xml record.
- [donnemartin/dev-setup](https://github.com/donnemartin/dev-setup)
  macOS development environment setup:  Easy-to-understand instructions with automated setup scripts for developer tools like Vim, Sublime Text, Bash, iTerm, Python data analysis, Spark, Hadoop MapReduce, AWS, Heroku, JavaScript web development, Android development, common data stores, and dev-based OS X defaults.
- [standardnotes/web](https://github.com/standardnotes/web)
  A free, open-source, and completely encrypted notes app. | https://standardnotes.org
