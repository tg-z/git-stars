# Repositories defined by: file, manager, php

also defined by the following keywords: single, package, filemanager, plugin, script, sharing, secure

- [stephenmcd/filebrowser-safe](https://github.com/stephenmcd/filebrowser-safe)
  File manager for Mezzanine
- [alexantr/filemanager](https://github.com/alexantr/filemanager)
  File manager in a single php file
- [gokcehan/lf](https://github.com/gokcehan/lf)
  Terminal file manager
- [Arno0x/EmbedInHTML](https://github.com/Arno0x/EmbedInHTML)
  Embed and hide any file in an HTML file
- [2ndalpha/gasmask](https://github.com/2ndalpha/gasmask)
  Hosts file manager for OS X
- [YouweGit/FileManager](https://github.com/YouweGit/FileManager)
  A secure file manager bundle for symfony2
- [jcampbell1/simple-file-manager](https://github.com/jcampbell1/simple-file-manager)
  A Simple PHP file manager.  The code is a single php file.  
- [artgris/FileManagerBundle](https://github.com/artgris/FileManagerBundle)
  FileManager is a simple Multilingual File Manager Bundle for Symfony
- [openintents/filemanager](https://github.com/openintents/filemanager)
  OpenIntents file manager
- [misterunknown/ifm](https://github.com/misterunknown/ifm)
  Improved File Manager
- [prasathmani/tinyfilemanager](https://github.com/prasathmani/tinyfilemanager)
  Web based File Manager in single PHP file, Manage your files efficiently and easily with Tiny File Manager
- [microsoft/winfile](https://github.com/microsoft/winfile)
  Original Windows File Manager (winfile) with enhancements
- [TeamAmaze/AmazeFileManager](https://github.com/TeamAmaze/AmazeFileManager)
  Material design file manager for Android
- [thoughtbot/rcm](https://github.com/thoughtbot/rcm)
  rc file (dotfile) management
- [ndevilla/iniparser](https://github.com/ndevilla/iniparser)
  ini file parser
- [openstyles/stylus](https://github.com/openstyles/stylus)
  Stylus - Userstyles Manager
- [dmytrodanylyk/folding-plugin](https://github.com/dmytrodanylyk/folding-plugin)
  Android File Grouping Plugin
- [jarun/nnn](https://github.com/jarun/nnn)
  :dolphin: The missing terminal file manager for X
- [fusic/filebinder](https://github.com/fusic/filebinder)
  Filebinder: Simple file attachment plugin for CakePHP
- [servocoder/RichFilemanager](https://github.com/servocoder/RichFilemanager)
  An open-source file manager. Up-to-date for PHP, Java, ASHX, ASP, NodeJs & Python 3 Flask. Contributions are welcome!
- [efeiefei/node-file-manager](https://github.com/efeiefei/node-file-manager)
  File manager web server based on Node.js with Koa, Angular.js and Bootstrap
- [kalcaddle/KodExplorer](https://github.com/kalcaddle/KodExplorer)
  A web based file manager,web IDE / browser based code editor
- [zeit/serve](https://github.com/zeit/serve)
  Static file serving and directory listing
- [tjeffree/Brackets-ExtensionHighlight](https://github.com/tjeffree/Brackets-ExtensionHighlight)
  File extension colours in Brackets' file tree.
- [nea/MarkdownViewerPlusPlus](https://github.com/nea/MarkdownViewerPlusPlus)
  A Notepad++ Plugin to view a Markdown file rendered on-the-fly
- [2b3ez/FileManager4TinyMCE](https://github.com/2b3ez/FileManager4TinyMCE)
  Plugin for manage and upload file for TinyMCE 4
- [simogeo/Filemanager](https://github.com/simogeo/Filemanager)
  An open-source file manager released under MIT license. Up-to-date for PHP connector. This package is DEPRECATED. Now, please use RichFileManager available at : https://github.com/servocoder/RichFilemanager.
- [plangrid/react-file-viewer](https://github.com/plangrid/react-file-viewer)
- [webtorrent/instant.io](https://github.com/webtorrent/instant.io)
  🚀 Streaming file transfer over WebTorrent (torrents on the web)
- [inloop/sqlite-viewer](https://github.com/inloop/sqlite-viewer)
  View SQLite file online
- [halgatewood/file-directory-list](https://github.com/halgatewood/file-directory-list)
  Free Super Clean PHP File Directory Listing Script
- [ivogabe/Brackets-Icons](https://github.com/ivogabe/Brackets-Icons)
  File icons in Brackets' file tree
- [usnationalarchives/File-Analyzer](https://github.com/usnationalarchives/File-Analyzer)
  NARA File Analyzer and Metadata Harvester
- [Tyrrrz/DiscordChatExporter](https://github.com/Tyrrrz/DiscordChatExporter)
  Exports Discord chat logs to a file
- [lecram/rover](https://github.com/lecram/rover)
  simple file browser for the terminal
- [PyCQA/pycodestyle](https://github.com/PyCQA/pycodestyle)
  Simple Python style checker in one Python file
- [espebra/filebin](https://github.com/espebra/filebin)
  Filebin is a web application that facilitates convenient file sharing over the web.
- [gulpjs/vinyl](https://github.com/gulpjs/vinyl)
  Virtual file format.
- [danpros/htmly](https://github.com/danpros/htmly)
  Databaseless Blogging Platform, Flat-File Blog and Flat-File CMS
- [yasirkula/UnitySimpleFileBrowser](https://github.com/yasirkula/UnitySimpleFileBrowser)
  A uGUI based runtime file browser for Unity 3D (draggable and resizable)
- [vuejs/vueify](https://github.com/vuejs/vueify)
  Browserify transform for single-file Vue components
- [nokiatech/heif](https://github.com/nokiatech/heif)
  High Efficiency Image File Format
- [thomasp85/shinyFiles](https://github.com/thomasp85/shinyFiles)
  A shiny extension for server side file access
- [GooBox/file-share-desktop](https://github.com/GooBox/file-share-desktop)
  💻 Goobox file share desktop app
- [davidsun/HostsFile](https://github.com/davidsun/HostsFile)
  Hosts file for FourSquare, DropBox, Facebook, Twitter & Google
- [happyfish100/fastdfs](https://github.com/happyfish100/fastdfs)
  FastDFS is an open source high performance distributed file system (DFS). It's major functions include: file storing, file syncing and file accessing, and design for high capacity and load balance.
- [s-p-k/foxy](https://github.com/s-p-k/foxy)
  A simple script that handles bookmarks stored in a txt file.
- [kern/filepizza](https://github.com/kern/filepizza)
  :pizza: Peer-to-peer file transfers in your browser
- [makto/FileHub](https://github.com/makto/FileHub)
  file sharing for a small circle of people
- [shrinerb/shrine](https://github.com/shrinerb/shrine)
  File Attachment toolkit for Ruby applications
- [chylex/Discord-History-Tracker](https://github.com/chylex/Discord-History-Tracker)
  Browser script that saves Discord chat history into a file, and an offline viewer that displays the file.
- [dadoonet/fscrawler](https://github.com/dadoonet/fscrawler)
  Elasticsearch File System Crawler (FS Crawler)
- [Peergos/Peergos](https://github.com/Peergos/Peergos)
  A decentralised, secure file storage and social network
- [JasonHinds13/PyFiling](https://github.com/JasonHinds13/PyFiling)
  Python script that organizes files in a folder or directory according to file type/extension.
- [dutchcoders/transfer.sh](https://github.com/dutchcoders/transfer.sh)
  Easy and fast file sharing from the command-line.
- [danfolkes/Magnet2Torrent](https://github.com/danfolkes/Magnet2Torrent)
  This will convert a magnet link into a .torrent file
- [mozilla/send](https://github.com/mozilla/send)
  Simple, private file sharing from the makers of Firefox
- [afterlogic/aurora-files](https://github.com/afterlogic/aurora-files)
  Aurora Files is an open-source file storage platform.
- [haiwen/seafile](https://github.com/haiwen/seafile)
  High performance file syncing and sharing, with also Markdown WYSIWYG editing, Wiki, file label and other knowledge management features.
- [npm/cli](https://github.com/npm/cli)
  a package manager for JavaScript, report bugs & get support at:
- [RobinLinus/snapdrop](https://github.com/RobinLinus/snapdrop)
  A Progressive Web App for local file sharing 
- [filebrowser/filebrowser](https://github.com/filebrowser/filebrowser)
  📂 Web File Browser which can be used as a middleware or standalone app.
- [inloop/svg2android](https://github.com/inloop/svg2android)
  SVG to Android VectorDrawable XML resource file
- [cn-uofbasel/ssbdrv](https://github.com/cn-uofbasel/ssbdrv)
  SSB Drive - a decentralized file system over Secure Scuttlebutt
- [picturepan2/fileicon.css](https://github.com/picturepan2/fileicon.css)
  Fileicon.css - The customizable pure CSS file icons
- [danielgtaylor/homebrew](https://github.com/danielgtaylor/homebrew)
  :beer: The missing package manager for OS X.
- [gkngkc/UnityStandaloneFileBrowser](https://github.com/gkngkc/UnityStandaloneFileBrowser)
  A native file browser for unity standalone platforms
- [Kloudless/file-explorer](https://github.com/Kloudless/file-explorer)
  The Kloudless File Explorer is a file picker and uploader for apps that integrates with 20+ cloud storage services with a few lines of code
- [eisenjulian/fb-page-chat-download](https://github.com/eisenjulian/fb-page-chat-download)
  Python script to download messages from a Facebook page to a CSV file
- [zboxfs/zbox](https://github.com/zboxfs/zbox)
  Zero-details, privacy-focused in-app file system.
- [pqina/filepond](https://github.com/pqina/filepond)
  🌊 A flexible and fun JavaScript file upload library
- [Xyntax/FileSensor](https://github.com/Xyntax/FileSensor)
  Dynamic file detection tool based on crawler 基于爬虫的动态敏感文件探测工具 
- [jorgebucaran/fisher](https://github.com/jorgebucaran/fisher)
  A package manager for the fish shell.
- [Dirkster99/fsc](https://github.com/Dirkster99/fsc)
  A set of themeable WPF File System Controls similar to some parts of Windows (7-10) Explorer
- [frontend-collective/react-sortable-tree-theme-file-explorer](https://github.com/frontend-collective/react-sortable-tree-theme-file-explorer)
  A file explorer theme for React Sortable Tree
- [webtorrent/parse-torrent](https://github.com/webtorrent/parse-torrent)
  Parse a torrent identifier (magnet uri, .torrent file, info hash)
- [ansilove/ansilove.js](https://github.com/ansilove/ansilove.js)
  A script to display ANSi and artscene related file formats on web pages
- [makinacorpus/Leaflet.FileLayer](https://github.com/makinacorpus/Leaflet.FileLayer)
  Loads files locally (GeoJSON, KML, GPX) as layers using HTML5 File API
- [Dijji/FileMeta](https://github.com/Dijji/FileMeta)
  Enable Explorer in Vista, Windows 7 and later to see, edit and search on tags and other metadata for any file type
- [cambrant/txtngin](https://github.com/cambrant/txtngin)
  A simple web.py project which implements a static file hyperlink-less wiki, of sorts.
- [atomotic/epub-linkchecker](https://github.com/atomotic/epub-linkchecker)
  extract and check links from an epub file
- [divio/django-filer](https://github.com/divio/django-filer)
  File and Image Management Application for django
- [ryanoasis/vim-devicons](https://github.com/ryanoasis/vim-devicons)
  Adds file type icons to Vim plugins such as: NERDTree, vim-airline, CtrlP, unite, Denite, lightline, vim-startify and many more
- [todotxt/todo.txt-cli](https://github.com/todotxt/todo.txt-cli)
  ☑️ A simple and extensible shell script for managing your todo.txt file.
- [ritwickdey/vscode-live-sass-compiler](https://github.com/ritwickdey/vscode-live-sass-compiler)
  Compile Sass or Scss file to CSS at realtime with live browser reload feature.
- [Colllect/Colllect](https://github.com/Colllect/Colllect)
  Your visual bookmark manager (Community Edition)
- [jfhbrook/node-ecstatic](https://github.com/jfhbrook/node-ecstatic)
  A static file server middleware that works with core http, express or on the CLI!
- [Utazukin/Ichaival](https://github.com/Utazukin/Ichaival)
  Android client for the LANraragi manga/doujinshi web manager.
- [Y2Z/monolith](https://github.com/Y2Z/monolith)
  :black_large_square: CLI tool for saving complete web pages as a single HTML file
- [junegunn/vim-plug](https://github.com/junegunn/vim-plug)
  :hibiscus: Minimalist Vim Plugin Manager
- [busterc/boomlet](https://github.com/busterc/boomlet)
  :boom: Bookmarklet compiler encloses, encodes, minifies your Javascript file and opens an HTML page with your new bookmarklet for immediate use.
- [Homebrew/brew](https://github.com/Homebrew/brew)
  🍺 The missing package manager for macOS (or Linux)
- [go-shiori/shiori](https://github.com/go-shiori/shiori)
  Simple bookmark manager built with Go
- [thedevsaddam/renderer](https://github.com/thedevsaddam/renderer)
  Simple, lightweight and faster response (JSON, JSONP, XML, YAML, HTML, File) rendering package for Go
- [pomber/git-history](https://github.com/pomber/git-history)
  Quickly browse the history of a file from any git repository
- [luetage/extension_control](https://github.com/luetage/extension_control)
  Elementary extension manager. Compact design, no superfluous features.
- [WiedenKennedyNYC/Internal-SAG](https://github.com/WiedenKennedyNYC/Internal-SAG)
  CLI to create animated SVGs from directory of frames. These work similar to GIFs, but have better colour spectrum, and on the whole lower file size when using compressed JPGs
- [Homebrew/homebrew-core](https://github.com/Homebrew/homebrew-core)
  🍻 Default formulae for the missing package manager for macOS
- [RichiH/vcsh](https://github.com/RichiH/vcsh)
  config manager based on Git
- [lawlite19/PythonCrawler-Scrapy-Mysql-File-Template](https://github.com/lawlite19/PythonCrawler-Scrapy-Mysql-File-Template)
  scrapy爬虫框架模板，将数据保存到Mysql数据库或者文件中。
- [github/markup](https://github.com/github/markup)
  Determines which markup library to use to render a content file (e.g. README) on GitHub
- [uptick/react-keyed-file-browser](https://github.com/uptick/react-keyed-file-browser)
  Folder based file browser given a flat keyed list of objects, powered by React.
- [LeCoupa/awesome-cheatsheets](https://github.com/LeCoupa/awesome-cheatsheets)
  👩‍💻👨‍💻 Awesome cheatsheets for popular programming languages, frameworks and development tools. They include everything you should know in one single file.
- [adulau/url_archiver](https://github.com/adulau/url_archiver)
  url-archiver is a simple library to fetch and archive URL on the file-system
- [tutorialzine/cute-files](https://github.com/tutorialzine/cute-files)
  A command line utility that turns the current working directory into a pretty online file browser
- [MinhasKamal/DownGit](https://github.com/MinhasKamal/DownGit)
  Create GitHub Resource Download Link (git-github-direct-zip-directory-folder-file)
- [svetlyak40wt/dotfiler](https://github.com/svetlyak40wt/dotfiler)
  Shell agnostic git based dotfiles package manager, written in Python.
- [jarun/Buku](https://github.com/jarun/Buku)
  :bookmark: Browser-independent bookmark manager
- [ofek/hatch](https://github.com/ofek/hatch)
  A modern project, package, and virtual env manager for Python
- [elipapa/markdown-cv](https://github.com/elipapa/markdown-cv)
  a simple template to write your CV in a readable markdown file and use CSS to publish/print it.
- [kanishka-linux/reminiscence](https://github.com/kanishka-linux/reminiscence)
  Self-Hosted Bookmark And Archive Manager
- [tbellembois/gobkm](https://github.com/tbellembois/gobkm)
  Ultra minimalist single user open source bookmark manager.
- [Augustyniak/FileExplorer](https://github.com/Augustyniak/FileExplorer)
  FileExplorer is a powerful iOS file browser that allows its users to choose and remove files and/or directories
- [csu/export-saved-reddit](https://github.com/csu/export-saved-reddit)
  Export saved Reddit posts into a HTML file for import into Google Chrome.
- [butteff/Ubuntu-Telemetry-Free-Privacy-Secure](https://github.com/butteff/Ubuntu-Telemetry-Free-Privacy-Secure)
  This Bash script just removes a pre-installed Telemetry, a pre-installed software and libs with some potentional or high risk. Script removes them to make your experience better and more secure. Also, the script installs an additional software for the protection. You will find more advices in Readme file about "what you can do more".
- [ikreymer/webarchive-indexing](https://github.com/ikreymer/webarchive-indexing)
  Tools for bulk indexing of WARC/ARC files on Hadoop, EMR or local file system.
- [ffoodd/a11y.css](https://github.com/ffoodd/a11y.css)
  This CSS file intends to warn developers about possible risks and mistakes that exist in HTML code. It can also be used to roughly evaluate a site's quality by simply including it as an external stylesheet.
- [zTrix/webpage2html](https://github.com/zTrix/webpage2html)
  save/convert web pages to a standalone editable html file for offline archive/view/edit/play/whatever
- [backup-manager/backup-manager](https://github.com/backup-manager/backup-manager)
  Database backup manager for dumping to and restoring databases from S3, Dropbox, FTP, SFTP, and Rackspace Cloud
- [mfisk/filemap](https://github.com/mfisk/filemap)
  File-Based Map-Reduce.   Zero-install: easily use any collection of computers as a map-reduce cluster for command-line analytics.
- [broskoski/react-player](https://github.com/broskoski/react-player)
  A React component for playing a variety of URLs, including file paths, YouTube, Facebook, Twitch, SoundCloud, Streamable, Vimeo, Wistia and DailyMotion
- [gildas-lormeau/SingleFile](https://github.com/gildas-lormeau/SingleFile)
  Web Extension for Firefox/Chrome and CLI tool to save a faithful copy of a complete web page as a single HTML file
- [transloadit/uppy](https://github.com/transloadit/uppy)
  The next open source file uploader for web browsers :dog: 
- [CookPete/react-player](https://github.com/CookPete/react-player)
  A React component for playing a variety of URLs, including file paths, YouTube, Facebook, Twitch, SoundCloud, Streamable, Vimeo, Wistia and DailyMotion
- [CenterForOpenScience/modular-file-renderer](https://github.com/CenterForOpenScience/modular-file-renderer)
  A Python package for rendering files to HTML via an embeddable iframe
- [nvm-sh/nvm](https://github.com/nvm-sh/nvm)
  Node Version Manager - POSIX-compliant bash script to manage multiple active node.js versions
- [naucon/File](https://github.com/naucon/File)
  This package contains php classes to access, change, copy, delete, move, rename files and directories.
- [muicss/johnnydepp](https://github.com/muicss/johnnydepp)
  A tiny dependency manager for modern browsers (992 bytes)
- [prithvibhola/WorkManager](https://github.com/prithvibhola/WorkManager)
  A simple example to demonstrate the working of the Work Manager.
- [JamesTheHacker/passman](https://github.com/JamesTheHacker/passman)
  PassMan is a secure, and easy to use password manager that runs on the terminal! Written in bash.
- [Vaporbook/epub-parser](https://github.com/Vaporbook/epub-parser)
  A web-friendly epub file parser. Useful in browser-based reading systems, command-line ebook production toolsets, and more.
- [gigablast/open-source-search-engine](https://github.com/gigablast/open-source-search-engine)
  Nov 20 2017 -- A distributed open source search engine and spider/crawler written in C/C++ for Linux on Intel/AMD. From gigablast dot com, which has binaries for download. See the README.md file at the very bottom of this page for instructions.
- [Borewit/music-metadata](https://github.com/Borewit/music-metadata)
  Stream and file based music metadata parser for node. Supporting a wide range of audio and tag formats.
- [knadh/listmonk](https://github.com/knadh/listmonk)
  High performance, self-hosted newsletter and mailing list manager with a modern dashboard. Go + React.
- [victordomingos/Count-files](https://github.com/victordomingos/Count-files)
  A CLI utility written in Python to help you count files, grouped by extension, in a directory. By default, it will count files recursively in current working directory and all of its subdirectories, and will display a table showing the frequency for each file extension (e.g.: .txt, .py, .html, .css) and the total number of files found.
- [agnelvishal/Condense.press](https://github.com/agnelvishal/Condense.press)
- [HumanCellAtlas/ingest-file-archiver](https://github.com/HumanCellAtlas/ingest-file-archiver)
  Service for storing HCA sequencing data in the EBI archives.
- [chrislusf/seaweedfs](https://github.com/chrislusf/seaweedfs)
  SeaweedFS is a simple and highly scalable distributed file system. There are two objectives:  to store billions of files! to serve the files fast! SeaweedFS implements an object store with O(1) disk seek and an optional Filer with POSIX interface, supporting S3 API, Rack-Aware Erasure Coding for warm storage, FUSE mount, Hadoop compatible, WebDAV.
- [ahmed-musallam/chrome-bookmarklet-manager](https://github.com/ahmed-musallam/chrome-bookmarklet-manager)
  A chrome extension to manage bookmarklets!
- [xxhomey19/github-file-icon](https://github.com/xxhomey19/github-file-icon)
  🌈 🗂 A browser extension which gives different filetypes different icons to GitHub, GitLab, gitea and gogs.
- [nijikokun/file-size](https://github.com/nijikokun/file-size)
  Lightweight filesize to human-readable / proportions w/o dependencies for node.js & browsers.
- [MediaFire/mediafire-php-open-sdk](https://github.com/MediaFire/mediafire-php-open-sdk)
- [YunoHost-Apps/wekan_ynh](https://github.com/YunoHost-Apps/wekan_ynh)
  Attempt to package Wekan for Yunohost....
- [YunoHost-Apps/wallabag2_ynh](https://github.com/YunoHost-Apps/wallabag2_ynh)
  Wallabag v2 package for YunoHost
- [720kb/ndm](https://github.com/720kb/ndm)
  :computer: npm desktop manager https://720kb.github.io/ndm
