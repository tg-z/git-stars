# Repositories defined by: css, framework, pure

also defined by the following keywords: responsive, pc9kaxy, theme, functional, cross, build, use

- [clementvalla/binder](https://github.com/clementvalla/binder)
- [m4rk3r/cubebot](https://github.com/m4rk3r/cubebot)
- [uncss/uncss](https://github.com/uncss/uncss)
  Remove unused styles from CSS
- [doyoe/css-handbook](https://github.com/doyoe/css-handbook)
  CSS参考手册
- [papercss/papercss](https://github.com/papercss/papercss)
  The Less Formal CSS Framework
- [kazzkiq/balloon.css](https://github.com/kazzkiq/balloon.css)
  Simple tooltips made of pure CSS
- [mrclay/minify](https://github.com/mrclay/minify)
  Combines. minifies, and serves CSS or Javascript files
- [1j01/elementary.css](https://github.com/1j01/elementary.css)
  elementary OS's stylesheet converted to browser CSS
- [Lonefy/vscode-JS-CSS-HTML-formatter](https://github.com/Lonefy/vscode-JS-CSS-HTML-formatter)
  JS,CSS,HTML formatter for vscode
- [OwlyStuff/amazium](https://github.com/OwlyStuff/amazium)
  The responsive CSS web framework
- [Automattic/juice](https://github.com/Automattic/juice)
  Juice inlines CSS stylesheets into your HTML source.
- [jh3y/driveway](https://github.com/jh3y/driveway)
  pure CSS masonry layouts
- [picturepan2/fileicon.css](https://github.com/picturepan2/fileicon.css)
  Fileicon.css - The customizable pure CSS file icons
- [brajeshwar/entities](https://github.com/brajeshwar/entities)
  Character Entities for HTML, CSS (content) and Javascript.
- [lokesh-coder/pretty-checkbox](https://github.com/lokesh-coder/pretty-checkbox)
  A pure CSS library to beautify checkbox and radio buttons.
- [c-smile/sciter-sdk](https://github.com/c-smile/sciter-sdk)
  Sciter is an embeddable HTML/CSS/scripting engine
- [csstools/sanitize.css](https://github.com/csstools/sanitize.css)
  A best-practices CSS foundation
- [milligram/milligram](https://github.com/milligram/milligram)
  A minimalist CSS framework.
- [louismerlin/concrete.css](https://github.com/louismerlin/concrete.css)
  A simple and to the point CSS microframework
- [adamschwartz/chrome-tabs](https://github.com/adamschwartz/chrome-tabs)
  Chrome-style tabs in HTML/CSS.
- [KennethWangDotDev/Google-Homepage-Clone](https://github.com/KennethWangDotDev/Google-Homepage-Clone)
- [tachyons-css/tachyons](https://github.com/tachyons-css/tachyons)
  Functional css for humans
- [loadingio/css-spinner](https://github.com/loadingio/css-spinner)
  small, elegant pure css spinner for ajax or loading animation
- [awssat/tailwindo](https://github.com/awssat/tailwindo)
  🔌  Convert Bootstrap CSS code to Tailwind CSS code
- [arizzitano/css3wordart](https://github.com/arizzitano/css3wordart)
  1990s MS WordArt. In CSS3.
- [CodyHouse/radial-svg-slider](https://github.com/CodyHouse/radial-svg-slider)
- [andersevenrud/retro-css-shell-demo](https://github.com/andersevenrud/retro-css-shell-demo)
  AnderShell 3000 - Retro looking terminal in CSS
- [madeas/box-shadows.css](https://github.com/madeas/box-shadows.css)
   :pisces: A cross-browser collection of CSS box-shadows
- [web-tiki/responsive-grid-of-hexagons](https://github.com/web-tiki/responsive-grid-of-hexagons)
  CSS responsive grid of hexagons
- [fb55/css-select](https://github.com/fb55/css-select)
  a CSS selector compiler & engine
- [twbs/bootstrap](https://github.com/twbs/bootstrap)
  The most popular HTML, CSS, and JavaScript framework for developing responsive, mobile first projects on the web.
- [jgthms/bulma](https://github.com/jgthms/bulma)
  Modern CSS framework based on Flexbox
- [rstudio/pagedown](https://github.com/rstudio/pagedown)
  Paginate the HTML Output of R Markdown with CSS for Print
- [sindresorhus/github-markdown-css](https://github.com/sindresorhus/github-markdown-css)
  The minimal amount of CSS to replicate the GitHub Markdown style
- [StefanKovac/flex-layout-attribute](https://github.com/StefanKovac/flex-layout-attribute)
  HTML layout helper based on CSS flexbox specification  — 
- [umpox/TinyEditor](https://github.com/umpox/TinyEditor)
  A functional HTML/CSS/JS editor in less than 400 bytes
- [sghall/d3-threejs](https://github.com/sghall/d3-threejs)
  CSS 3D Transforms with D3 and THREE.js
- [pure-css/pure](https://github.com/pure-css/pure)
  A set of small, responsive CSS modules that you can use in every web project.
- [twbs/ratchet](https://github.com/twbs/ratchet)
  Build mobile apps with simple HTML, CSS, and JavaScript components. 
- [melizeche/listaHu](https://github.com/melizeche/listaHu)
  A crowdsourced platform to blacklist sms spammers and blackmailers
- [oliver-gomes/csswand](https://github.com/oliver-gomes/csswand)
  🎨✨ Hover your wand and use your magic spell to copy beautiful css
- [rgrove/sanitize](https://github.com/rgrove/sanitize)
  Ruby HTML and CSS sanitizer.
- [you-dont-need/You-Dont-Need-JavaScript](https://github.com/you-dont-need/You-Dont-Need-JavaScript)
  CSS is powerful, you can do a lot of things without JS.
- [cassidoo/desktop-lava](https://github.com/cassidoo/desktop-lava)
  A lava lamp for your desktop built with CSS and Electron.
- [jgthms/marksheet](https://github.com/jgthms/marksheet)
  Free tutorial to learn HTML and CSS
- [aantron/lambdasoup](https://github.com/aantron/lambdasoup)
  Functional HTML scraping and rewriting with CSS in OCaml
- [nicothin/NTH-start-project](https://github.com/nicothin/NTH-start-project)
  Startkit for HTML / CSS / JS pages layout.
- [filamentgroup/select-css](https://github.com/filamentgroup/select-css)
  Cross-browser styles for consistent select element styling
- [AmirTugi/tea-school](https://github.com/AmirTugi/tea-school)
  Simplified HTML + CSS --> PDF Generator for Nodejs
- [2003team/2003page.ga](https://github.com/2003team/2003page.ga)
  2003page: The 2003team Project of Yesteryear
- [litehtml/litehtml](https://github.com/litehtml/litehtml)
  Fast and lightweight HTML/CSS rendering engine
- [electron/electron](https://github.com/electron/electron)
  :electron: Build cross-platform desktop apps with JavaScript, HTML, and CSS
- [afandilham/Udemy-Advanced-CSS](https://github.com/afandilham/Udemy-Advanced-CSS)
  Advanced CSS course from Udemy
- [clehner/vim-scissors](https://github.com/clehner/vim-scissors)
  Vim server for live CSS editing
- [kognise/water.css](https://github.com/kognise/water.css)
  A just-add-css collection of styles to make simple websites just a little nicer
- [ritwickdey/vscode-live-sass-compiler](https://github.com/ritwickdey/vscode-live-sass-compiler)
  Compile Sass or Scss file to CSS at realtime with live browser reload feature.
- [srod/node-minify](https://github.com/srod/node-minify)
  Light Node.js module that compress javascript, css and html files
- [1j01/multifiddle](https://github.com/1j01/multifiddle)
  (development inactive) Fiddle with coffee, js, css, html, and all that, in a minimalistic collaborative environment
- [canonical-web-and-design/vanilla-framework](https://github.com/canonical-web-and-design/vanilla-framework)
  From community websites to web applications, this CSS framework will help you achieve a consistent look and feel.
- [ndrwhr/mobeh-dick](https://github.com/ndrwhr/mobeh-dick)
  Silly experiment translating a bit of moby dick into internet speak
- [alexdevero/front-end-dojo](https://github.com/alexdevero/front-end-dojo)
  Library full of useful CSS, JS and Sass functions, mixins and utilities.
- [connors/photon](https://github.com/connors/photon)
  The fastest way to build beautiful Electron apps using simple HTML and CSS
- [davatorium/rofi-themes](https://github.com/davatorium/rofi-themes)
  Themes for Rofi
- [adobe/brackets](https://github.com/adobe/brackets)
  An open source code editor for the web, written in JavaScript, HTML and CSS.
- [newyorkdata/newyorkdata.github.io](https://github.com/newyorkdata/newyorkdata.github.io)
  website
- [lc-soft/LCUI](https://github.com/lc-soft/LCUI)
  A small C library for building user interfaces with C, XML and CSS.
- [scrapy/parsel](https://github.com/scrapy/parsel)
  Parsel lets you extract data from XML/HTML documents using XPath or CSS selectors
- [epicmaxco/epic-spinners](https://github.com/epicmaxco/epic-spinners)
  Easy to use css spinners collection with Vue.js integration
- [elipapa/markdown-cv](https://github.com/elipapa/markdown-cv)
  a simple template to write your CV in a readable markdown file and use CSS to publish/print it.
- [jhy/jsoup](https://github.com/jhy/jsoup)
  jsoup: Java HTML Parser, with best of DOM, CSS, and jquery
- [Kozea/WeasyPrint](https://github.com/Kozea/WeasyPrint)
  WeasyPrint converts web documents (HTML with CSS, SVG, …) to PDF.
- [microsoft/frontend-bootcamp](https://github.com/microsoft/frontend-bootcamp)
  Frontend Workshop from HTML/CSS/JS to TypeScript/React/Redux
- [ffoodd/a11y.css](https://github.com/ffoodd/a11y.css)
  This CSS file intends to warn developers about possible risks and mistakes that exist in HTML code. It can also be used to roughly evaluate a site's quality by simply including it as an external stylesheet.
- [r0x0r/pywebview](https://github.com/r0x0r/pywebview)
  Build GUI for your Python program with JavaScript, HTML, and CSS
- [emotion-js/emotion](https://github.com/emotion-js/emotion)
  👩‍🎤 CSS-in-JS library designed for high performance style composition
- [K-Kraken/Pixel-Portfolio-Webite](https://github.com/K-Kraken/Pixel-Portfolio-Webite)
  A Nintendo inspired lightweight, device friendly single-page portfolio website built with HTML and CSS :space_invader:🌏
- [MrSaints/Morphext](https://github.com/MrSaints/Morphext)
  A simple, high-performance and cross-browser jQuery rotating / carousel plugin for text phrases powered by Animate.css.
- [cri-o/cri-o.io](https://github.com/cri-o/cri-o.io)
  website content
- [markdowncss/markdowncss.github.io](https://github.com/markdowncss/markdowncss.github.io)
  Where all the MarkdownCSS themes live.
- [tpenguinltg/winclassic](https://github.com/tpenguinltg/winclassic)
  Windows Classic theme designer
- [revolunet/PythonBooks](https://github.com/revolunet/PythonBooks)
  Directory of free Python ebooks
- [logeshpaul/Frontend-Cheat-Sheets](https://github.com/logeshpaul/Frontend-Cheat-Sheets)
  Collection of cheat sheets(HTML, CSS, JS, Git, Gulp, etc.,) for your frontend development needs & reference
- [jamiew/heroku-static-site](https://github.com/jamiew/heroku-static-site)
  A basic Ruby/Rack app for publishing a static HTML/CSS/javascript website on Heroku (for free)
- [CodyHouse/codyhouse-framework](https://github.com/CodyHouse/codyhouse-framework)
  A lightweight front-end framework for building accessible, bespoke interfaces.
- [leggett/simplify](https://github.com/leggett/simplify)
  Browser extension to simplify Gmail's interface
- [NativeScript/NativeScript](https://github.com/NativeScript/NativeScript)
  NativeScript is an open source framework for building truly native mobile apps with JavaScript. Use web skills, like Angular and Vue.js, FlexBox and CSS, and get native UI and performance on iOS and Android.
- [trezp/flashcards-vue](https://github.com/trezp/flashcards-vue)
  A flashcard app in Vue.js
- [3lo1i/WinHub-98](https://github.com/3lo1i/WinHub-98)
  A modern-looking userstyle for GitHub
- [codeneuro/notebooks](https://github.com/codeneuro/notebooks)
  Interactive notebooks for trying analyses and exploring datasets
- [oilstel/gossips](https://github.com/oilstel/gossips)
  Index of artist run spaces
- [wtfutil/wtfdocs](https://github.com/wtfutil/wtfdocs)
  The source for https://wtfutil.com
- [30-seconds/30-seconds-of-css](https://github.com/30-seconds/30-seconds-of-css)
  A curated collection of useful CSS snippets you can understand in 30 seconds or less.
- [BoyMechanicus/Dark-Materia](https://github.com/BoyMechanicus/Dark-Materia)
  Dark Material Theme for Wallabag
- [juancarlospaco/css-html-js-minify](https://github.com/juancarlospaco/css-html-js-minify)
  StandAlone Async cross-platform Minifier for the Web.
- [JuanPotato/GitHub-WinClassic](https://github.com/JuanPotato/GitHub-WinClassic)
  Windows Classic style for GitHub
- [taniarascia/new-moon-brackets](https://github.com/taniarascia/new-moon-brackets)
  🌙 New Moon Theme for Brackets.
- [Simonwep/pickr](https://github.com/Simonwep/pickr)
  🍭 Flat, simple, multi-themed, responsive and hackable Color-Picker library. No dependencies, no jQuery. Compatible with all CSS Frameworks e.g. Bootstrap, Materialize. Supports alpha channel, rgba, hsla, hsva and more!
- [inloop/sqlite-viewer](https://github.com/inloop/sqlite-viewer)
  View SQLite file online
- [kamlekar/slim-scroll](https://github.com/kamlekar/slim-scroll)
  HTML element scroll bar as replacement for default browser's scroll-bar. Design as you want using CSS.
- [nodegui/react-nodegui](https://github.com/nodegui/react-nodegui)
  Build performant, native and cross-platform desktop applications with native React + powerful CSS like styling.🚀
- [squibbleFish/theme-bones](https://github.com/squibbleFish/theme-bones)
  A Mobile-First, Responsive WordPress starter theme based off 320 and Up & HTML5 Boilerplate.
- [DiscordCollab/Discord98](https://github.com/DiscordCollab/Discord98)
  Discord Windows 98 theme
- [hacks-guide/Guide_Switch](https://github.com/hacks-guide/Guide_Switch)
  A complete guide to Switch homebrew and homebrew development.
- [vincelwt/harmony-themes](https://github.com/vincelwt/harmony-themes)
  A repository of themes for Harmony (music player)
- [hongkonggong/tldr-digital-security](https://github.com/hongkonggong/tldr-digital-security)
  TLDR Digital Safety Checklist
- [wg-dashboard/wg-dashboard](https://github.com/wg-dashboard/wg-dashboard)
  an easy-to-use dashboard for wireguard vpn
- [kaisiemek/oceanic-dark-theme](https://github.com/kaisiemek/oceanic-dark-theme)
  A dark theme for the Brackets.io editor.
- [vhanla/github-windows-classic](https://github.com/vhanla/github-windows-classic)
  Give GitHub a Windows Classic look
- [hxrts/spice](https://github.com/hxrts/spice)
  A distributed project space for research and practice.
- [arcticicestudio/nord-brackets](https://github.com/arcticicestudio/nord-brackets)
  An arctic, north-bluish clean and elegant Brackets theme.
- [tadeubarbosa/msn-messenger-nostalgia](https://github.com/tadeubarbosa/msn-messenger-nostalgia)
  A [unofficial] project to an old MSN Messenger user. 
- [t7/construct.css](https://github.com/t7/construct.css)
  Focus on the content and structure of your HTML
- [textileio/textile.photos](https://github.com/textileio/textile.photos)
  Textile Photos Website
- [arcticicestudio/nord](https://github.com/arcticicestudio/nord)
  An arctic, north-bluish color palette.
- [yixuan/prettydoc](https://github.com/yixuan/prettydoc)
  Creating Pretty HTML From R Markdown
- [trvswgnr/codepen-brackets-theme](https://github.com/trvswgnr/codepen-brackets-theme)
  A theme for Brackets based on the http://codepen.io default theme.
- [victordomingos/Count-files](https://github.com/victordomingos/Count-files)
  A CLI utility written in Python to help you count files, grouped by extension, in a directory. By default, it will count files recursively in current working directory and all of its subdirectories, and will display a table showing the frequency for each file extension (e.g.: .txt, .py, .html, .css) and the total number of files found.
- [atomotic/pywb-recorder-tor](https://github.com/atomotic/pywb-recorder-tor)
  pywb recorder over tor, anonymously records the web. (docker image)
- [padraigfl/packard-belle](https://github.com/padraigfl/packard-belle)
  Windows 98 React Component Library
- [perf-rocks/perf.rocks](https://github.com/perf-rocks/perf.rocks)
  Curated resources that help you build lightning fast websites
- [ariona/hover3d](https://github.com/ariona/hover3d)
  Simple jQuery plugin for 3d Hover effect
- [lolstring/window98-html-css-js](https://github.com/lolstring/window98-html-css-js)
  Windows 98 on the Web using HTML5, CSS3 and JS.
- [textileio/dapp-template](https://github.com/textileio/dapp-template)
  a basic template to build simple ipfs-based browser dapps
- [coreui/coreui](https://github.com/coreui/coreui)
  Free WebApp UI Kit built on top of Bootstrap 4
- [sidneys/youtube-playlist-player](https://github.com/sidneys/youtube-playlist-player)
  Watch and edit YouTube playlists on the desktop.
- [surjithctly/documentation-html-template](https://github.com/surjithctly/documentation-html-template)
  A Sample Documentation Template for Themes, Templates and Plugins
- [danielquinn/aletheia](https://github.com/danielquinn/aletheia)
  Fight fake news with cryptography & human nature
- [luvit/luvit](https://github.com/luvit/luvit)
  Lua + libUV + jIT = pure awesomesauce
- [Markshall/MaterialFormComponents](https://github.com/Markshall/MaterialFormComponents)
  A few basic web-form components styled with inspiration from the Google Material design language.
- [ryanoasis/nerd-fonts](https://github.com/ryanoasis/nerd-fonts)
  Iconic font aggregator, collection, & patcher. 3,600+ icons, 40+ patched fonts: Hack, Source Code Pro, more. Glyph collections: Font Awesome, Material Design Icons, Octicons, & more
- [tr1s/tris-webpack-boilerplate](https://github.com/tr1s/tris-webpack-boilerplate)
  A Webpack boilerplate for static websites that has all the necessary modern tools and optimizations built-in. Score a perfect 10/10 on performance.
- [SqueezerIO/squeezer](https://github.com/SqueezerIO/squeezer)
  Squeezer Framework - Build serverless dApps
- [gokulkrishh/demo-progressive-web-app](https://github.com/gokulkrishh/demo-progressive-web-app)
  🎉 A demo for progressive web application with features like offline, push notifications, background sync etc,
