# Repositories defined by: js, node, vue

also defined by the following keywords: framework, using, module, telnet, protocol, support, typescript

- [webtorrent/webtorrent-hybrid](https://github.com/webtorrent/webtorrent-hybrid)
  WebTorrent (with WebRTC support in Node.js)
- [t3chnoboy/thepiratebay](https://github.com/t3chnoboy/thepiratebay)
  :skull: The Pirate Bay node.js client
- [tree-sitter/node-tree-sitter](https://github.com/tree-sitter/node-tree-sitter)
  Node.js bindings for tree-sitter
- [expressjs/body-parser](https://github.com/expressjs/body-parser)
  Node.js body parsing middleware
- [wez/telnetjs](https://github.com/wez/telnetjs)
  A Telnet protocol listener for Node.js
- [ggrossman/telnetjs](https://github.com/ggrossman/telnetjs)
  A Telnet protocol listener for Node.js
- [lightning-viz/three.js](https://github.com/lightning-viz/three.js)
  Three.js library.
- [jesusprubio/bluebox-ng](https://github.com/jesusprubio/bluebox-ng)
  Pentesting framework using Node.js powers, focused in VoIP.
- [trezp/flashcards-vue](https://github.com/trezp/flashcards-vue)
  A flashcard app in Vue.js
- [mkozjak/node-telnet-client](https://github.com/mkozjak/node-telnet-client)
  A simple telnet client for Node.js
- [svg/svgo](https://github.com/svg/svgo)
  :tiger: Node.js tool for optimizing SVG files
- [Jeffreyrn/vue-simple-flowchart](https://github.com/Jeffreyrn/vue-simple-flowchart)
  flowchart editor for Vue.js
- [trailsjs/trails](https://github.com/trailsjs/trails)
  :evergreen_tree: Modern Web Application Framework for Node.js.
- [srod/node-minify](https://github.com/srod/node-minify)
  Light Node.js module that compress javascript, css and html files
- [Zulko/eagle.js](https://github.com/Zulko/eagle.js)
  A hackable slideshow framework built with Vue.js
- [newyorkdata/heroku-buildpack-nodejs-gulp](https://github.com/newyorkdata/heroku-buildpack-nodejs-gulp)
  A slightly modified version of Heroku's official Node.js buildpack with added gulp.js support
- [axios/axios](https://github.com/axios/axios)
  Promise based HTTP client for the browser and node.js
- [IonicaBizau/scrape-it](https://github.com/IonicaBizau/scrape-it)
  🔮 A Node.js scraper for humans.
- [oclif/oclif](https://github.com/oclif/oclif)
  Node.js Open CLI Framework. Built with 💜 by Heroku.
- [nuxt/consola](https://github.com/nuxt/consola)
  🐨 Elegant Console Logger for Node.js and Browser
- [lightning-viz/heroku-buildpack-nodejs](https://github.com/lightning-viz/heroku-buildpack-nodejs)
  The official Heroku buildpack for Node.js apps.
- [vue-gl/vue-gl](https://github.com/vue-gl/vue-gl)
  Vue.js components rendering 3D graphics reactively via three.js
- [efeiefei/node-file-manager](https://github.com/efeiefei/node-file-manager)
  File manager web server based on Node.js with Koa, Angular.js and Bootstrap
- [textileio/js-http-playground](https://github.com/textileio/js-http-playground)
- [Lonefy/vscode-JS-CSS-HTML-formatter](https://github.com/Lonefy/vscode-JS-CSS-HTML-formatter)
  JS,CSS,HTML formatter for vscode
- [racker/node-elementtree](https://github.com/racker/node-elementtree)
  Port of Python's Element Tree module to Node.js
- [Beg-in/vue-babylonjs](https://github.com/Beg-in/vue-babylonjs)
  A ready-to-go 3d environment for Vue.js using Babylon.js
- [vuejs/vue-cli](https://github.com/vuejs/vue-cli)
  🛠️ Standard Tooling for Vue.js Development
- [cuduy197/vue-flashcard](https://github.com/cuduy197/vue-flashcard)
  Rich flashcard component for vue js 2 :tada:
- [N0taN3rd/node-warc](https://github.com/N0taN3rd/node-warc)
  Parse And Create Web ARChive  (WARC) files with node.js
- [Pinperepette/image-to-ascii](https://github.com/Pinperepette/image-to-ascii)
  :floppy_disk: A Node.js module that converts images to ASCII art.
- [dan-divy/spruce](https://github.com/dan-divy/spruce)
  A social networking platform made using Node.js and MongoDB
- [eugeneware/warc](https://github.com/eugeneware/warc)
  Parse WARC (Web Archive Files) as a node.js stream
- [unshiftio/url-parse](https://github.com/unshiftio/url-parse)
  Small footprint URL parser that works seamlessly across Node.js and browser environments.
- [nvm-sh/nvm](https://github.com/nvm-sh/nvm)
  Node Version Manager - POSIX-compliant bash script to manage multiple active node.js versions
- [zhw2590582/WFPlayer](https://github.com/zhw2590582/WFPlayer)
  :ocean: WFPlayer.js is an audio waveform generator
- [electerious/Ackee](https://github.com/electerious/Ackee)
  Self-hosted, Node.js based analytics tool for those who care about privacy.
- [nodesource/distributions](https://github.com/nodesource/distributions)
  NodeSource Node.js Binary Distributions
- [mochajs/mocha](https://github.com/mochajs/mocha)
  ☕️ simple, flexible, fun javascript test framework for node.js & the browser
- [beautify-web/js-beautify](https://github.com/beautify-web/js-beautify)
  Beautifier for javascript 
- [textileio/js-textile-go-daemon](https://github.com/textileio/js-textile-go-daemon)
  Spawn and control the Textile daemon from Node/Javascript
- [beakerbrowser/node-spellchecker](https://github.com/beakerbrowser/node-spellchecker)
  SpellChecker Node Module
- [nijikokun/file-size](https://github.com/nijikokun/file-size)
  Lightweight filesize to human-readable / proportions w/o dependencies for node.js & browsers.
- [webrecorder/wombat](https://github.com/webrecorder/wombat)
  WIP - Splitting wombat.js library from pywb
- [LulumiProject/lulumi-browser](https://github.com/LulumiProject/lulumi-browser)
  Lulumi-browser is a lightweight browser coded with Vue.js 2 and Electron.
- [hamed-ehtesham/pretty-checkbox-vue](https://github.com/hamed-ehtesham/pretty-checkbox-vue)
  Quickly integrate pretty checkbox components with Vue.js
- [tinwan/vue2-data-tree](https://github.com/tinwan/vue2-data-tree)
  A tree that data is lazy loaded. Support dragging node, checking node, editing node's name and selecting node.
- [scrumpy/tiptap](https://github.com/scrumpy/tiptap)
  A rich-text editor for Vue.js
- [loverajoel/jstips](https://github.com/loverajoel/jstips)
  This is about useful JS tips!
- [ggrossman/tty.js](https://github.com/ggrossman/tty.js)
  A terminal for your browser, using node/express/socket.io
- [chjj/tty.js](https://github.com/chjj/tty.js)
  A terminal for your browser, using node/express/socket.io
- [jvilk/BrowserFS](https://github.com/jvilk/BrowserFS)
  BrowserFS is an in-browser filesystem that emulates the Node JS filesystem API and supports storing and retrieving files from various backends.
- [johan/QuickJSON](https://github.com/johan/QuickJSON)
  A QuickLook plugin to pretty-print (node.js style) JSON. Click to fold/unfold sub-trees!
- [1j01/BrowserFS](https://github.com/1j01/BrowserFS)
  "BrowserFS is an in-browser filesystem that emulates the Node JS filesystem API and supports storing and retrieving files from various backends." It's pretty cool. I'm using it for https://98.js.org/
- [mstefaniuk/graph-viz-d3-js](https://github.com/mstefaniuk/graph-viz-d3-js)
  Graphviz web D3.js renderer
- [1j01/process-tree](https://github.com/1j01/process-tree)
  Node library to find all the child processes
- [enilu/web-flash](https://github.com/enilu/web-flash)
  WEB-FLASH --  Admin Framework and Mobile Website Based on Spring Boot and Vue.js
- [unconed/TermKit](https://github.com/unconed/TermKit)
  Experimental Terminal platform built on WebKit + node.js. Currently only for Mac and Windows, though the prototype works 90% in any WebKit browser.
- [troxler/vue-headful](https://github.com/troxler/vue-headful)
   Set document title and meta tags with Vue.js
- [sghall/d3-threejs](https://github.com/sghall/d3-threejs)
  CSS 3D Transforms with D3 and THREE.js
- [halower/vue-tree](https://github.com/halower/vue-tree)
  tree and multi-select component based on Vue.js 2.0
- [mihneadb/node-directory-tree](https://github.com/mihneadb/node-directory-tree)
  Convert a directory tree to a JS object.
- [wyzant-dev/vue-radial-progress](https://github.com/wyzant-dev/vue-radial-progress)
  Radial progress bar component for Vue.js
- [Jermolene/TiddlyWiki5](https://github.com/Jermolene/TiddlyWiki5)
  A self-contained JavaScript wiki for the browser, Node.js, AWS Lambda etc.
- [epicmaxco/epic-spinners](https://github.com/epicmaxco/epic-spinners)
  Easy to use css spinners collection with Vue.js integration
- [textileio/js-http-client](https://github.com/textileio/js-http-client)
  Official Textile JS HTTP Wrapper Client
- [hparton/terminal.reddit](https://github.com/hparton/terminal.reddit)
  An online terminal-based interface for reading Reddit built with Vue.js.
- [learn-co-students/js-if-else-files-lab-bootcamp-prep-000](https://github.com/learn-co-students/js-if-else-files-lab-bootcamp-prep-000)
- [lolstring/window98-html-css-js](https://github.com/lolstring/window98-html-css-js)
  Windows 98 on the Web using HTML5, CSS3 and JS.
- [vinta/pangu.js](https://github.com/vinta/pangu.js)
  Paranoid text spacing in JavaScript
- [mohayonao/web-audio-engine](https://github.com/mohayonao/web-audio-engine)
  Pure JS implementation of the Web Audio API
- [mcamis/saturn.fm](https://github.com/mcamis/saturn.fm)
  A javascript audio visualizer powered by three.js and nostalgia.
- [wesbos/JavaScript30](https://github.com/wesbos/JavaScript30)
  30 Day Vanilla JS Challenge
- [umpox/TinyEditor](https://github.com/umpox/TinyEditor)
  A functional HTML/CSS/JS editor in less than 400 bytes
- [textileio/notes-desktop](https://github.com/textileio/notes-desktop)
  Example app built with reactjs and @textile/js-http-client
- [nicothin/NTH-start-project](https://github.com/nicothin/NTH-start-project)
  Startkit for HTML / CSS / JS pages layout.
- [videojs/video.js](https://github.com/videojs/video.js)
  Video.js - open source HTML5 & Flash video player
- [1j01/os-gui](https://github.com/1j01/os-gui)
  Retro OS GUI JS library
- [1j01/multifiddle](https://github.com/1j01/multifiddle)
  (development inactive) Fiddle with coffee, js, css, html, and all that, in a minimalistic collaborative environment
- [eKoopmans/html2pdf.js](https://github.com/eKoopmans/html2pdf.js)
  Client-side HTML-to-PDF rendering using pure JS.
- [posthtml/posthtml](https://github.com/posthtml/posthtml)
  PostHTML is a tool to transform HTML/XML with JS plugins
- [ipfs-shipyard/ipfs-webui](https://github.com/ipfs-shipyard/ipfs-webui)
  A frontend for an IPFS node.
- [you-dont-need/You-Dont-Need-JavaScript](https://github.com/you-dont-need/You-Dont-Need-JavaScript)
  CSS is powerful, you can do a lot of things without JS.
- [VincentLoy/tweetParser.js](https://github.com/VincentLoy/tweetParser.js)
  tweetParser.js Parse an element containing a tweet and turn URLS, @user & #hashtags into working urls
- [fiatjaf/node-dependencies-view](https://github.com/fiatjaf/node-dependencies-view)
  node dependency trees as a service.
- [vuejs/vue](https://github.com/vuejs/vue)
  🖖 Vue.js is a progressive, incrementally-adoptable JavaScript framework for building UI on the web.
- [vuejs/vue-next](https://github.com/vuejs/vue-next)
- [w8r/avl](https://github.com/w8r/avl)
  :eyeglasses: Fast AVL tree for Node and browser
- [uwdata/vegaserver](https://github.com/uwdata/vegaserver)
  A simple node server that renders vega specs to SVG or PNG.
- [vega/vega-lite-to-api](https://github.com/vega/vega-lite-to-api)
  Convert Vega-Lite JSON spec to Vega-Lite JS API
- [preziotte/party-mode](https://github.com/preziotte/party-mode)
  An experimental music visualizer using d3.js and the web audio api.
- [Pinperepette/vue-twitter-stream](https://github.com/Pinperepette/vue-twitter-stream)
  A dashboard capturing live sentiment of tweets based on hashtag. Vue.js as client, Twitter API for streaming, Watson Tone Analyzer, socket.io to tie server and client
- [Soundnode/soundnode-app](https://github.com/Soundnode/soundnode-app)
  Soundnode App is the Soundcloud for desktop. Built with Electron, Angular.js and Soundcloud API.
- [microsoft/frontend-bootcamp](https://github.com/microsoft/frontend-bootcamp)
  Frontend Workshop from HTML/CSS/JS to TypeScript/React/Redux
- [gskinner/regexr](https://github.com/gskinner/regexr)
  RegExr is a HTML/JS based tool for creating, testing, and learning about Regular Expressions.
- [chjj/term.js](https://github.com/chjj/term.js)
  A terminal written in javascript.
- [ggrossman/term.js](https://github.com/ggrossman/term.js)
  A terminal written in javascript.
- [NativeScript/NativeScript](https://github.com/NativeScript/NativeScript)
  NativeScript is an open source framework for building truly native mobile apps with JavaScript. Use web skills, like Angular and Vue.js, FlexBox and CSS, and get native UI and performance on iOS and Android.
- [alexdevero/front-end-dojo](https://github.com/alexdevero/front-end-dojo)
  Library full of useful CSS, JS and Sass functions, mixins and utilities.
- [OnsenUI/OnsenUI](https://github.com/OnsenUI/OnsenUI)
  Mobile app development framework and SDK using HTML5 and JavaScript. Create beautiful and performant cross-platform mobile apps. Based on Web Components, and provides bindings for Angular 1, 2, React and Vue.js.
- [archilogic-com/3dio-js](https://github.com/archilogic-com/3dio-js)
  JavaScript toolkit for interior apps
- [textileio/protobuf.js](https://github.com/textileio/protobuf.js)
  Protocol Buffers for JavaScript (& TypeScript).
- [xtermjs/xterm.js](https://github.com/xtermjs/xterm.js)
  A terminal for the web
- [talenet/talenet](https://github.com/talenet/talenet)
- [mattdesl/color-wander](https://github.com/mattdesl/color-wander)
  :art: Generative artwork in node/browser based on a seeded random
- [emotion-js/emotion](https://github.com/emotion-js/emotion)
  👩‍🎤 CSS-in-JS library designed for high performance style composition
- [yairEO/tagify](https://github.com/yairEO/tagify)
  lightweight, efficient Tags input component in Vanilla JS / React / Angular
- [angular/angular.js](https://github.com/angular/angular.js)
  AngularJS - HTML enhanced for web apps!
- [salcido/discogs-enhancer](https://github.com/salcido/discogs-enhancer)
  100% vanilla JS Chrome extension that adds useful features and functionality to Discogs.com
- [1j01/choon.js](https://github.com/1j01/choon.js)
  🎹 Choon language interpreter in javascript with the Web Audio API.
- [vuejs/rollup-plugin-vue](https://github.com/vuejs/rollup-plugin-vue)
  Roll .vue files
- [liushuping/ascii-tree](https://github.com/liushuping/ascii-tree)
  A node module for generating tree structure in ASCII
- [tsayen/dom-to-image](https://github.com/tsayen/dom-to-image)
  Generates an image from a DOM node using HTML5 canvas
- [logeshpaul/Frontend-Cheat-Sheets](https://github.com/logeshpaul/Frontend-Cheat-Sheets)
  Collection of cheat sheets(HTML, CSS, JS, Git, Gulp, etc.,) for your frontend development needs & reference
- [kerrbrittany9/tamagotchi-react](https://github.com/kerrbrittany9/tamagotchi-react)
  web app using moment and in React.js to feed, play and rest a little creature you've created
- [pqina/vue-filepond](https://github.com/pqina/vue-filepond)
  🔌 A handy FilePond adapter component for Vue
- [oskca/gopherjs-vue](https://github.com/oskca/gopherjs-vue)
  VueJS bindings for gopherjs
- [fivethirtyeight/pym.js](https://github.com/fivethirtyeight/pym.js)
  Resize an iframe responsively depending on the height of its content and the width of its container.
- [ratracegrad/vue-flashcards](https://github.com/ratracegrad/vue-flashcards)
  Vue Flashcards to test your knowledge of Vue
- [Klortho/d3-flextree](https://github.com/Klortho/d3-flextree)
  Flexible tree layout algorithm that allows for variable node sizes
- [pirate/ArchiveBox](https://github.com/pirate/ArchiveBox)
  🗃 The open source self-hosted web archive. Takes browser history/bookmarks/Pocket/Pinboard/etc., saves HTML, JS, PDFs, media, and more...
- [ivangreene/arena-js](https://github.com/ivangreene/arena-js)
  are.na API wrapper for JavaScript
- [maticzav/emma-cli](https://github.com/maticzav/emma-cli)
  📦 Terminal assistant to find and install node packages.
- [getify/You-Dont-Know-JS](https://github.com/getify/You-Dont-Know-JS)
  A book series on JavaScript. @YDKJS on twitter.
- [carbon-design-system/carbon-components-vue](https://github.com/carbon-design-system/carbon-components-vue)
  Vue implementation of the Carbon Design System
- [maxatwork/form2js](https://github.com/maxatwork/form2js)
  Javascript library for collecting form data
- [sbstjn/timesheet.js](https://github.com/sbstjn/timesheet.js)
  JavaScript library for HTML5 & CSS3 time sheets
- [n8n-io/n8n](https://github.com/n8n-io/n8n)
  Free node based Workflow Automation Tool. Easily automate tasks across different services.
- [rgalus/sticky-js](https://github.com/rgalus/sticky-js)
  Library for sticky elements written in vanilla javascript
- [sebpiq/paulstretch.js](https://github.com/sebpiq/paulstretch.js)
  PaulStretch sound stretching algorithm in your browser
- [vparadis/vue-radial-menu](https://github.com/vparadis/vue-radial-menu)
  Simple vue radial menu
- [1j01/anypalette.js](https://github.com/1j01/anypalette.js)
  🌈🎨🏳‍🌈 Load all kinds of palette files 💄🧡🎃💛🍋🍏💚📗💙📘🔮💜
- [conversejs/converse.js](https://github.com/conversejs/converse.js)
  Web-based XMPP/Jabber chat client written in JavaScript
- [vuejs/vueify](https://github.com/vuejs/vueify)
  Browserify transform for single-file Vue components
- [Borewit/music-metadata](https://github.com/Borewit/music-metadata)
  Stream and file based music metadata parser for node. Supporting a wide range of audio and tag formats.
- [miaolz123/vue-markdown](https://github.com/miaolz123/vue-markdown)
  A Powerful and Highspeed Markdown Parser for Vue
- [N3-components/N3-components](https://github.com/N3-components/N3-components)
  N3-components , Powerful Vue UI Library.
- [smore-inc/clippy.js](https://github.com/smore-inc/clippy.js)
  Add Clippy or his friends to any website for instant nostalgia.
- [textileio/node-chat](https://github.com/textileio/node-chat)
  A simple cli chat app using Textile
- [juancarlospaco/css-html-js-minify](https://github.com/juancarlospaco/css-html-js-minify)
  StandAlone Async cross-platform Minifier for the Web.
- [ndrwhr/limbo.js](https://github.com/ndrwhr/limbo.js)
  "You're waiting for a train, a train that will take you far away. You know where you hope this train will take you, but you don't know for sure. But it doesn't matter."
- [ritz078/embed-js](https://github.com/ritz078/embed-js)
  🌻  A lightweight plugin to embed emojis, media, maps, tweets, code and more. ✨
- [radial-color-picker/vue-color-picker](https://github.com/radial-color-picker/vue-color-picker)
  Radial Color Picker - Vue
- [jfhbrook/node-ecstatic](https://github.com/jfhbrook/node-ecstatic)
  A static file server middleware that works with core http, express or on the CLI!
- [bonustrack/steemconnect](https://github.com/bonustrack/steemconnect)
  Signer app for Steem
- [mudcube/Color.Space.js](https://github.com/mudcube/Color.Space.js)
  :rainbow: Library to convert between color spaces: HEX, RGB, RGBA, HSL, HSLA, CMY, CMYK. This covers the conversion between W3 compatible colors. Conversion is versatile accepting strings, arrays, and objects.
- [c8r/vue-styled-system](https://github.com/c8r/vue-styled-system)
  Design system utilities wrapper for Vue components, based on styled-system
- [moarpepes/awesome-mesh](https://github.com/moarpepes/awesome-mesh)
  This is a list for mesh networking: Documentation, Free Software mesh protocols, and applications. A mesh network is a network topology in which each node relays data for the network. All mesh nodes cooperate in the distribution of data in the network.
- [pixijs/pixi.js](https://github.com/pixijs/pixi.js)
  The HTML5 Creation Engine: Create beautiful digital content with the fastest, most flexible 2D WebGL renderer.
- [sandoche/Darkmode.js](https://github.com/sandoche/Darkmode.js)
  🌓 Add a dark-mode / night-mode to your website in a few seconds
- [L-Chris/vue-design](https://github.com/L-Chris/vue-design)
  Be the best website visualization builder with Vue and Electron.
- [ansilove/ansilove.js](https://github.com/ansilove/ansilove.js)
  A script to display ANSi and artscene related file formats on web pages
- [yi-ge/js-tree-list](https://github.com/yi-ge/js-tree-list)
  Convert list to tree, managing a tree and its nodes.
- [spatie/vue-table-component](https://github.com/spatie/vue-table-component)
  A straight to the point Vue component to display tables
- [BowenYin/harker-bell](https://github.com/BowenYin/harker-bell)
  Bell schedule app built for the future of the web.
- [styled-components/vue-styled-components](https://github.com/styled-components/vue-styled-components)
  Visual primitives for the component age. A simple port for Vue of styled-components 💅
- [werk85/node-html-to-text](https://github.com/werk85/node-html-to-text)
  Advanced html to text converter
- [calibr/node-bookmarks-parser](https://github.com/calibr/node-bookmarks-parser)
  Parses Firefox/Chrome HTML bookmarks files
- [giekaton/vipassana-app](https://github.com/giekaton/vipassana-app)
  Gamified Meditation PWA
- [ianepperson/telnetsrvlib](https://github.com/ianepperson/telnetsrvlib)
  Telnet server using gevent
