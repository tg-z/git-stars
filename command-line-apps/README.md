# Repositories defined by: command, line, apps

also defined by the following keywords: tool, interface, page, tools, shell, interfaces, videos

- [genuinetools/weather](https://github.com/genuinetools/weather)
  Weather via the command line.
- [sharkdp/hyperfine](https://github.com/sharkdp/hyperfine)
  A command-line benchmarking tool
- [Pinperepette/the-art-of-command-line](https://github.com/Pinperepette/the-art-of-command-line)
  Master the command line, in one page
- [jlevy/the-art-of-command-line](https://github.com/jlevy/the-art-of-command-line)
  Master the command line, in one page
- [tanakh/cmdline](https://github.com/tanakh/cmdline)
  A Command Line Parser
- [ascii-boxes/boxes](https://github.com/ascii-boxes/boxes)
  Command line ASCII boxes unlimited!
- [ritiek/AskQuora](https://github.com/ritiek/AskQuora)
  Quora Q&A right from the command-line
- [joshuaclayton/unused](https://github.com/joshuaclayton/unused)
  A command line tool to identify unused code.
- [node-js-libs/cli](https://github.com/node-js-libs/cli)
  Rapidly build command line apps
- [piotrmurach/tty](https://github.com/piotrmurach/tty)
  Toolkit for developing sleek command line apps.
- [wting/autojump](https://github.com/wting/autojump)
  A cd command that learns - easily navigate directories from the command line
- [jeroenjanssens/data-science-at-the-command-line](https://github.com/jeroenjanssens/data-science-at-the-command-line)
  Data Science at the Command Line
- [mitchellh/cli](https://github.com/mitchellh/cli)
  A Go library for implementing command-line interfaces.
- [jerry-git/thug-memes](https://github.com/jerry-git/thug-memes)
  Command line Thug Meme generator written in Python
- [nishanths/lyft](https://github.com/nishanths/lyft)
  Create and manage Lyft rides from the command line
- [cdown/yturl](https://github.com/cdown/yturl)
  YouTube videos on the command line
- [exercism/cli](https://github.com/exercism/cli)
  A Go based command line tool for exercism.io.
- [ivangreene/arena-cli](https://github.com/ivangreene/arena-cli)
  are.na Command Line Interface
- [bombs-kim/pythonp](https://github.com/bombs-kim/pythonp)
  A powerful utility that empowers pythonistas in the command line
- [ritiek/GitFeed](https://github.com/ritiek/GitFeed)
  Check your GitHub Newsfeed via the command-line
- [Pinperepette/fish-shell](https://github.com/Pinperepette/fish-shell)
  The user-friendly command line shell.
- [aonez/openterm](https://github.com/aonez/openterm)
  OpenTerm, a sandboxed command line interface for iOS
- [jrnl-org/jrnl](https://github.com/jrnl-org/jrnl)
  Collect your thoughts and notes without leaving the command line.
- [denisidoro/navi](https://github.com/denisidoro/navi)
  An interactive cheatsheet tool for the command-line
- [Pinperepette/awesome-cli-apps](https://github.com/Pinperepette/awesome-cli-apps)
  A curated list of command line apps
- [refactorsaurusrex/easy-vpn](https://github.com/refactorsaurusrex/easy-vpn)
  Command line automation for GlobalProtect VPN client
- [8Gitbrix/2048Haskell](https://github.com/8Gitbrix/2048Haskell)
  Play 2048 from the command line /terminal !
- [agarrharr/awesome-cli-apps](https://github.com/agarrharr/awesome-cli-apps)
  🖥 📊 🕹 🛠 A curated list of command line apps
- [Jaymon/captain](https://github.com/Jaymon/captain)
  command line python scripts for humans
- [ageitgey/face_recognition](https://github.com/ageitgey/face_recognition)
  The world's simplest facial recognition api for Python and the command line
- [idank/explainshell](https://github.com/idank/explainshell)
  match command-line arguments to their help text
- [dbohdan/structured-text-tools](https://github.com/dbohdan/structured-text-tools)
  A list of command line tools for manipulating structured text data
- [webpack/webpack-cli](https://github.com/webpack/webpack-cli)
  Webpack's Command Line Interface
- [altdesktop/playerctl](https://github.com/altdesktop/playerctl)
  🎧 mpris command-line controller and library for spotify, vlc, audacious, bmp, cmus, and others.
- [klaussinani/taskbook](https://github.com/klaussinani/taskbook)
  Tasks, boards & notes for the command-line habitat
- [m4rk3r/s4cmd](https://github.com/m4rk3r/s4cmd)
  Super S3 command line tool
- [dutchcoders/transfer.sh](https://github.com/dutchcoders/transfer.sh)
  Easy and fast file sharing from the command-line.
- [bmatzelle/gow](https://github.com/bmatzelle/gow)
  Unix command line utilities installer for Windows.
- [gleitz/howdoi](https://github.com/gleitz/howdoi)
  instant coding answers via the command line
- [antonmedv/fx](https://github.com/antonmedv/fx)
  Command-line tool and terminal JSON viewer 🔥
- [marvelm/erised](https://github.com/marvelm/erised)
  Bookmark and archive webpages from the command line
- [1j01/simple-console](https://github.com/1j01/simple-console)
  Add an elegant command-line interface to any page
- [mkideal/cli](https://github.com/mkideal/cli)
  CLI - A package for building command line app with go
- [chriskiehl/Gooey](https://github.com/chriskiehl/Gooey)
  Turn (almost) any Python command line program into a full GUI application with one line
- [martyav/git-CLI-cheat-sheet](https://github.com/martyav/git-CLI-cheat-sheet)
  A cheatsheet for using Git on the command line
- [react-native-community/cli](https://github.com/react-native-community/cli)
  React Native command line tools
- [dylanaraps/neofetch](https://github.com/dylanaraps/neofetch)
  🖼️  A command-line system information tool written in bash 3.2+
- [guarinogabriel/Mac-CLI](https://github.com/guarinogabriel/Mac-CLI)
   OS X command line tools for developers – The ultimate tool to manage your Mac. It provides a huge set of command line commands that automatize the usage of your OS X system.
- [megous/megatools](https://github.com/megous/megatools)
  Open-source command line tools for accessing Mega.co.nz cloud storage.
- [morgant/tools-osx](https://github.com/morgant/tools-osx)
  A small collection of command line tools for Mac OS X, incl.: clipcat, dict, eject, ql, swuser, trash & with.
- [symfony/console](https://github.com/symfony/console)
  The Console component eases the creation of beautiful and testable command line interfaces.
- [DocNow/twarc](https://github.com/DocNow/twarc)
  A command line tool (and Python library) for archiving Twitter JSON
- [unknownblueguy6/MineSweeper](https://github.com/unknownblueguy6/MineSweeper)
  Command Line version of MineSweeper for Unix-like systems
- [fabiospampinato/webtorrent-cli](https://github.com/fabiospampinato/webtorrent-cli)
  WebTorrent, the streaming torrent client. For the command line.
- [pybites/packtebooks](https://github.com/pybites/packtebooks)
  Script to manage your ebook (downloads) from command line
- [arnellebalane/mdi-cli](https://github.com/arnellebalane/mdi-cli)
  Generate material design icons from the command line
- [danburzo/percollate](https://github.com/danburzo/percollate)
  🌐 → 📖 A command-line tool to turn web pages into beautifully formatted PDFs
- [dduan/tre](https://github.com/dduan/tre)
  Tree command, improved.
- [piotrmurach/tty-prompt](https://github.com/piotrmurach/tty-prompt)
  A beautiful and powerful interactive command line prompt
- [sharkdp/pastel](https://github.com/sharkdp/pastel)
  A command-line tool to generate, analyze, convert and manipulate colors
- [Urigo/graphql-cli](https://github.com/Urigo/graphql-cli)
  📟  Command line tool for common GraphQL development workflows
- [urfave/cli](https://github.com/urfave/cli)
  A simple, fast, and fun package for building command line apps in Go
- [ritiek/plugin.video.megacmd](https://github.com/ritiek/plugin.video.megacmd)
  A very minimal Kodi addon to stream videos from your MEGA account using MEGAcmd command-line tool
- [stebbins/cage-the-bird](https://github.com/stebbins/cage-the-bird)
  Cage the Bird is a simple Ruby command line application for purging your Twitter account of Tweets, Retweets, and Likes
- [Pinperepette/httpie](https://github.com/Pinperepette/httpie)
  HTTPie is a command line HTTP client, a user-friendly cURL replacement.
- [alebcay/awesome-shell](https://github.com/alebcay/awesome-shell)
  A curated list of awesome command-line frameworks, toolkits, guides and gizmos. Inspired by awesome-php.
- [Pinperepette/awesome-shell](https://github.com/Pinperepette/awesome-shell)
  A curated list of awesome command-line frameworks, toolkits, guides and gizmos. Inspired by awesome-php.
- [s-p-k/awesome-shell](https://github.com/s-p-k/awesome-shell)
  A curated list of awesome command-line frameworks, toolkits, guides and gizmos. Inspired by awesome-php.
- [BurntSushi/xsv](https://github.com/BurntSushi/xsv)
  A fast CSV command line toolkit written in Rust.
- [m3ng9i/IP-resolver](https://github.com/m3ng9i/IP-resolver)
  A command-line tool for getting a domain's IPs from multiple name servers.
- [Pinperepette/terminal-notifier](https://github.com/Pinperepette/terminal-notifier)
  Send User Notifications on Mac OS X 10.8 from the command-line.
- [google/python-fire](https://github.com/google/python-fire)
  Python Fire is a library for automatically generating command line interfaces (CLIs) from absolutely any Python object.
- [mozilla/web-ext](https://github.com/mozilla/web-ext)
  A command line tool to help build, run, and test web extensions
- [kumabook/bebop](https://github.com/kumabook/bebop)
  Groovy WebExtension that offers command line interface like emacs helm for browsing.
- [ytdl-org/youtube-dl](https://github.com/ytdl-org/youtube-dl)
  Command-line program to download videos from YouTube.com and other video sites
- [ikreymer/cdx-index-client](https://github.com/ikreymer/cdx-index-client)
  A command-line tool for using CommonCrawl Index API at http://index.commoncrawl.org/
- [Pinperepette/youtube-dl](https://github.com/Pinperepette/youtube-dl)
  Small command-line program to download videos from YouTube.com and other video sites
- [wang502/Toptal-API](https://github.com/wang502/Toptal-API)
  :computer: Command line interface to read engineering blogs from http://toptal.com/blog
- [donnemartin/haxor-news](https://github.com/donnemartin/haxor-news)
  Browse Hacker News like a haxor: A Hacker News command line interface (CLI).
- [tutorialzine/cute-files](https://github.com/tutorialzine/cute-files)
  A command line utility that turns the current working directory into a pretty online file browser
- [chrippa/livestreamer](https://github.com/chrippa/livestreamer)
  Command-line utility that extracts streams from various services and pipes them into a video player of choice. No longer maintained, use streamlink or youtube-dl instead.
- [gphotosuploader/gphotos-uploader-cli](https://github.com/gphotosuploader/gphotos-uploader-cli)
  Command line tool to mass upload media folders to your google photos account(s) (Mac OS / Linux)
- [mfisk/filemap](https://github.com/mfisk/filemap)
  File-Based Map-Reduce.   Zero-install: easily use any collection of computers as a map-reduce cluster for command-line analytics.
- [ritiek/muxnect](https://github.com/ritiek/muxnect)
  Send input to just about any interactive command-line tool through a local web server
- [nvbn/thefuck](https://github.com/nvbn/thefuck)
  Magnificent app which corrects your previous console command.
- [Vaporbook/epub-parser](https://github.com/Vaporbook/epub-parser)
  A web-friendly epub file parser. Useful in browser-based reading systems, command-line ebook production toolsets, and more.
- [posener/complete](https://github.com/posener/complete)
  bash completion written in go + bash completion for go command
- [teensy-hacking/teensy3.2-projects](https://github.com/teensy-hacking/teensy3.2-projects)
  Teensy 3.2 Projects - Teensy with CMD Command Execution Attack Example 💣
- [jakubroztocil/httpie](https://github.com/jakubroztocil/httpie)
  As easy as HTTPie /aitch-tee-tee-pie/ 🥧 Modern command line HTTP client – user-friendly curl alternative with intuitive UI, JSON support, syntax highlighting, wget-like downloads, extensions, etc.  https://twitter.com/clihttp
- [Pinperepette/awesome-command-line-apps](https://github.com/Pinperepette/awesome-command-line-apps)
  :shell: Use your terminal shell to do awesome things.
- [jefftriplett/url2markdown-cli](https://github.com/jefftriplett/url2markdown-cli)
  :page_with_curl: Fetch a url and translate it to markdown in one command
- [Pinperepette/awesome-osx-command-line](https://github.com/Pinperepette/awesome-osx-command-line)
  Use your OS X terminal shell to do awesome things.
- [dvorka/hstr](https://github.com/dvorka/hstr)
  Bash and zsh shell history suggest box - easily view, navigate, search and manage your command history.
- [athityakumar/colorls](https://github.com/athityakumar/colorls)
  A Ruby gem that beautifies the terminal's ls command, with color and font-awesome icons. :tada:
- [udilia/create-social-network](https://github.com/udilia/create-social-network)
  Create Social Network by running one command. Demo: https://worldexplorer.netlify.com/
- [kroitor/asciichart](https://github.com/kroitor/asciichart)
  Nice-looking lightweight console ASCII line charts ╭┈╯ for NodeJS, browsers and terminal, no dependencies
- [yangshun/tree-node-cli](https://github.com/yangshun/tree-node-cli)
  🌲Lists the contents of directories in a tree-like format, similar to the Linux tree command
- [donnemartin/data-science-ipython-notebooks](https://github.com/donnemartin/data-science-ipython-notebooks)
  Data science Python notebooks: Deep learning (TensorFlow, Theano, Caffe, Keras), scikit-learn, Kaggle, big data (Spark, Hadoop MapReduce, HDFS), matplotlib, pandas, NumPy, SciPy, Python essentials, AWS, and various command lines.
- [ritiek/data-science-ipython-notebooks](https://github.com/ritiek/data-science-ipython-notebooks)
  Recently updated with 50 new notebooks! Data science Python notebooks: Deep learning (TensorFlow, Theano, Caffe, Keras), scikit-learn, Kaggle, big data (Spark, Hadoop MapReduce, HDFS), matplotlib, pandas, NumPy, SciPy, Python essentials, AWS, and various command lines.
