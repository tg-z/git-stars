# Repositories defined by: search, engine, java

also defined by the following keywords: distributed, com, https, elasticsearch, fn5, reference, aware

- [senseidb/bobo](https://github.com/senseidb/bobo)
  faceted search engine
- [pavelgonchar/face-search](https://github.com/pavelgonchar/face-search)
  Face search engine
- [approach0/search-engine](https://github.com/approach0/search-engine)
  A math-aware search engine.
- [subins2000/search](https://github.com/subins2000/search)
  An Open Source Search Engine
- [fossasia/susper.com](https://github.com/fossasia/susper.com)
  Susper Decentralised Search Engine https://susper.com
- [toshi-search/Toshi](https://github.com/toshi-search/Toshi)
  A full-text search engine in rust
- [jivesearch/jivesearch](https://github.com/jivesearch/jivesearch)
  A search engine that doesn't track you.
- [yacy/yacy_search_server](https://github.com/yacy/yacy_search_server)
  Distributed Peer-to-Peer Web Search Engine and Intranet Search Appliance
- [Fanping/iveely.search](https://github.com/Fanping/iveely.search)
   Pure java realize search engine, try to directly hit the user's search for answers.
- [elastic/elasticsearch](https://github.com/elastic/elasticsearch)
  Open Source, Distributed, RESTful Search Engine
- [ndmitchell/hoogle](https://github.com/ndmitchell/hoogle)
  Haskell API search engine
- [tuan3w/visual_search](https://github.com/tuan3w/visual_search)
  A visual search engine based on Elasticsearch and Tensorflow
- [jaeksoft/opensearchserver](https://github.com/jaeksoft/opensearchserver)
  Open-source Enterprise Grade Search Engine Software
- [marcobiedermann/search-engine-optimization](https://github.com/marcobiedermann/search-engine-optimization)
  :mag: A helpful checklist / collection of Search Engine Optimization (SEO) tips and techniques.
- [go-ego/riot](https://github.com/go-ego/riot)
  Go Open Source, Distributed, Simple and efficient Search Engine 
- [boramalper/magnetico](https://github.com/boramalper/magnetico)
  Autonomous (self-hosted) BitTorrent DHT search engine suite.
- [Treora/indexeddb-full-text-search-comparison](https://github.com/Treora/indexeddb-full-text-search-comparison)
- [kpumuk/meta-tags](https://github.com/kpumuk/meta-tags)
  Search Engine Optimization (SEO) for Ruby on Rails applications.
- [oracle/opengrok](https://github.com/oracle/opengrok)
  OpenGrok is a fast and usable source code search and cross reference engine, written in Java
- [doctrine/search](https://github.com/doctrine/search)
  Generic Search extension for indexing and querying ODM/ORM objects with different text-search engine implementations
- [HumanCellAtlas/query-service](https://github.com/HumanCellAtlas/query-service)
  Prototype implementation of the DCP Query Service, a metadata search engine for HCA
- [syntax-tree/nlcst-search](https://github.com/syntax-tree/nlcst-search)
  utility to search for patterns in an nlcst tree
- [AshveenBansal98/Python-Text-Search](https://github.com/AshveenBansal98/Python-Text-Search)
   A search engine for searching text/phrase in set of .txt files, based on inverted indexing and boyer moore algorithm. 
- [cybercongress/cyberd](https://github.com/cybercongress/cyberd)
  InterPlanetary Search Engine & Knowledge Consensus Supercomputer in Cosmos-SDK/Tendermint/CUDA #fuckgoogle
- [gigablast/open-source-search-engine](https://github.com/gigablast/open-source-search-engine)
  Nov 20 2017 -- A distributed open source search engine and spider/crawler written in C/C++ for Linux on Intel/AMD. From gigablast dot com, which has binaries for download. See the README.md file at the very bottom of this page for instructions.
- [s-p-k/lud](https://github.com/s-p-k/lud)
  cli tool to search Urban Dictionary
- [c-smile/sciter-sdk](https://github.com/c-smile/sciter-sdk)
  Sciter is an embeddable HTML/CSS/scripting engine
- [ovity/octotree](https://github.com/ovity/octotree)
  Core engine of Octotree
- [Dirkster99/FilterTreeView](https://github.com/Dirkster99/FilterTreeView)
  WPF/MVVM Search and Filter Reference Application
- [ropenscilabs/stellar](https://github.com/ropenscilabs/stellar)
  Search your github stars in R
- [unbug/codelf](https://github.com/unbug/codelf)
  A search tool helps dev to solve the naming things problem.
- [ycm-core/YouCompleteMe](https://github.com/ycm-core/YouCompleteMe)
  A code-completion engine for Vim
- [fossasia/query-server](https://github.com/fossasia/query-server)
  Query Server Search Engines https://query-server.herokuapp.com
- [JoelOtter/termloop](https://github.com/JoelOtter/termloop)
  Terminal-based game engine for Go, built on top of Termbox
- [Pinperepette/GeoTweet](https://github.com/Pinperepette/GeoTweet)
  Program to search tweets, tag, hashtag, user, with locations and maps
- [Pinperepette/passive-spider](https://github.com/Pinperepette/passive-spider)
  Passive Spider uses search engines to find interesting information about a target domain.
- [lusakasa/saka](https://github.com/lusakasa/saka)
  Elegant tab, bookmark and history search
- [litehtml/litehtml](https://github.com/litehtml/litehtml)
  Fast and lightweight HTML/CSS rendering engine
- [fb55/css-select](https://github.com/fb55/css-select)
  a CSS selector compiler & engine
- [asciimoo/searx](https://github.com/asciimoo/searx)
  Privacy-respecting metasearch engine
- [uwdata/datavore](https://github.com/uwdata/datavore)
  A small, fast, in-browser database engine written in JavaScript.
- [WorldBrain/Memex](https://github.com/WorldBrain/Memex)
  Browser Extension to full-text search your browsing history & bookmarks.
- [wolfg1969/oh-my-stars](https://github.com/wolfg1969/oh-my-stars)
  An offline CLI tool to search your GitHub Stars.
- [Pinperepette/SCANNER-INURLBR](https://github.com/Pinperepette/SCANNER-INURLBR)
  Advanced search in search engines, enables analysis provided to exploit GET / POST capturing emails & urls, with an internal custom validation junction for each target / url found.
- [Dijji/FileMeta](https://github.com/Dijji/FileMeta)
  Enable Explorer in Vista, Windows 7 and later to see, edit and search on tags and other metadata for any file type
- [jakejarvis/awesome-shodan-queries](https://github.com/jakejarvis/awesome-shodan-queries)
  🔍 A collection of interesting, funny, and depressing search queries to plug into https://shodan.io/ 👩‍💻
- [Warflop/CloudBunny](https://github.com/Warflop/CloudBunny)
  CloudBunny is a tool to capture the real IP of the server that uses a WAF as a proxy or protection. In this tool we used three search engines to search domain information: Shodan, Censys and Zoomeye.
- [dmllr/IdealMedia](https://github.com/dmllr/IdealMedia)
  Awesome app to listen music and audiobooks on the device and online at vk.com. Search, download, set as ringtone, sort by albums, authors, folder. Powerful equalizer.
- [vega/compass](https://github.com/vega/compass)
  Visualization Recommendation Engine, powered by Vega-Lite Specification Language
- [dvorka/hstr](https://github.com/dvorka/hstr)
  Bash and zsh shell history suggest box - easily view, navigate, search and manage your command history.
- [trimstray/sandmap](https://github.com/trimstray/sandmap)
  Nmap on steroids! Simple CLI with the ability to run pure Nmap engine, 31 modules with 459 scan profiles.
- [Moonbase59/kplaylist](https://github.com/Moonbase59/kplaylist)
  Manage your music via the Web: Stream, upload, make playlists, share, search, download and a lot more.
- [1j01/skele2d](https://github.com/1j01/skele2d)
  ☠😎 A 2D game engine based around skeletal structures, with an in-game editor and animation support (pre-alpha)
- [SadConsole/SadConsole](https://github.com/SadConsole/SadConsole)
  A .NET ascii/ansi console engine written in C# for MonoGame and XNA. Create your own text roguelike (or other) games!
- [chylex/Resource-Pack-Organizer](https://github.com/chylex/Resource-Pack-Organizer)
  Minecraft mod that enhances the Resource Pack menu with folder support for easier pack organization, ability to search packs by name or description, new sorting options, and several other features.
- [pixijs/pixi.js](https://github.com/pixijs/pixi.js)
  The HTML5 Creation Engine: Create beautiful digital content with the fastest, most flexible 2D WebGL renderer.
- [leereilly/games](https://github.com/leereilly/games)
  :video_game: A list of popular/awesome videos games, add-ons, maps, etc. hosted on GitHub. Any genre. Any platform. Any engine.
- [jasonqng/genius-lyrics-search](https://github.com/jasonqng/genius-lyrics-search)
  Python script for searching Genius.com API
- [clangen/musikcube](https://github.com/clangen/musikcube)
  a cross-platform, terminal-based music player, audio engine, metadata indexer, and server in c++
- [nylas/sync-engine](https://github.com/nylas/sync-engine)
  :incoming_envelope: IMAP/SMTP sync system with modern APIs
- [mohayonao/web-audio-engine](https://github.com/mohayonao/web-audio-engine)
  Pure JS implementation of the Web Audio API
- [uwdata/Prefuse](https://github.com/uwdata/Prefuse)
  Prefuse is a set of software tools for creating rich interactive data visualizations in the Java programming language. Prefuse supports a rich set of features for data modeling, visualization, and interaction. It provides optimized data structures for tables, graphs, and trees, a host of layout and visual encoding techniques, and support for animation, dynamic queries, integrated search, and database connectivity. 
- [DistributedWeb/ddrive](https://github.com/DistributedWeb/ddrive)
  Distributed Hard Drives For The dWeb://
