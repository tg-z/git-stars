# Repositories defined by: react, component, native

also defined by the following keywords: vue, components, 8j, pgjyic8, based, pgi, ui

- [pqina/react-filepond](https://github.com/pqina/react-filepond)
  🔌 A handy FilePond adapter component for React
- [lovasoa/react-contenteditable](https://github.com/lovasoa/react-contenteditable)
  React component for a div with editable contents
- [react-R/reactR](https://github.com/react-R/reactR)
  React for R
- [fivethirtyeight/multi-slider](https://github.com/fivethirtyeight/multi-slider)
  React component for multiple values slider (allocate values)
- [jsdf/react-native-htmlview](https://github.com/jsdf/react-native-htmlview)
  A React Native component which renders HTML content as native views
- [akveo/react-native-reddit-reader](https://github.com/akveo/react-native-reddit-reader)
  react native
- [aredotna/attache](https://github.com/aredotna/attache)
  Are.na + React Native
- [m4rk3r/react-collapsible](https://github.com/m4rk3r/react-collapsible)
  React component to wrap content in Collapsible element with trigger to open and close.
- [adamterlson/cairn](https://github.com/adamterlson/cairn)
  Hierarchical shared and component-based style definitions with selector-based style application, for React Native
- [idyll-lang/react-latex](https://github.com/idyll-lang/react-latex)
  React component to render Latex strings
- [padraigfl/packard-belle](https://github.com/padraigfl/packard-belle)
  Windows 98 React Component Library
- [React95/React95](https://github.com/React95/React95)
  A React components library with Win95 UI
- [archriss/react-native-render-html](https://github.com/archriss/react-native-render-html)
  iOS/Android pure javascript react-native component that renders your HTML into 100% native views
- [nitin42/terminal-in-react](https://github.com/nitin42/terminal-in-react)
  👨‍💻  A component that renders a terminal
- [arturbien/React95](https://github.com/arturbien/React95)
  🌈🕹  Refreshed Windows 95 style UI components for your React app
- [jc4p/ReactiveArena](https://github.com/jc4p/ReactiveArena)
  React Native based front-end for http://are.na
- [react-spring/react-spring](https://github.com/react-spring/react-spring)
  ✌️ A spring physics based React animation library
- [callstack/react-native-paper](https://github.com/callstack/react-native-paper)
  Material Design for React Native (Android & iOS)
- [pqina/vue-filepond](https://github.com/pqina/vue-filepond)
  🔌 A handy FilePond adapter component for Vue
- [m4rk3r/react-router-breadcrumbs-hoc](https://github.com/m4rk3r/react-router-breadcrumbs-hoc)
  Just a tiny, flexible, higher order component for rendering breadcrumbs with react-router 4.x
- [broskoski/react-player](https://github.com/broskoski/react-player)
  A React component for playing a variety of URLs, including file paths, YouTube, Facebook, Twitch, SoundCloud, Streamable, Vimeo, Wistia and DailyMotion
- [CookPete/react-player](https://github.com/CookPete/react-player)
  A React component for playing a variety of URLs, including file paths, YouTube, Facebook, Twitch, SoundCloud, Streamable, Vimeo, Wistia and DailyMotion
- [hshoff/vx](https://github.com/hshoff/vx)
  🐯react + d3 = vx | visualization components
- [react-component/tree](https://github.com/react-component/tree)
  React Tree
- [nodegui/react-nodegui](https://github.com/nodegui/react-nodegui)
  Build performant, native and cross-platform desktop applications with native React + powerful CSS like styling.🚀
- [plangrid/react-file-viewer](https://github.com/plangrid/react-file-viewer)
- [react-native-community/cli](https://github.com/react-native-community/cli)
  React Native command line tools
- [roman01la/html-to-react-components](https://github.com/roman01la/html-to-react-components)
  Converts HTML pages into React components
- [Arish-Shah/win95](https://github.com/Arish-Shah/win95)
  Windows 95 desktop built using React
- [yairEO/tagify](https://github.com/yairEO/tagify)
  lightweight, efficient Tags input component in Vanilla JS / React / Angular
- [nteract/semiotic](https://github.com/nteract/semiotic)
  A data visualization framework combining React & D3
- [fivethirtyeight/victory](https://github.com/fivethirtyeight/victory)
  A collection of composable React components for building interactive data visualizations
- [weibangtuo/vue-tree](https://github.com/weibangtuo/vue-tree)
  A tree component
- [remarkablemark/html-react-parser](https://github.com/remarkablemark/html-react-parser)
  :memo: HTML to React parser.
- [textileio/mozaik](https://github.com/textileio/mozaik)
  Mozaïk is a tool based on nodejs / react / d3 to easily craft beautiful dashboards.
- [ndom91/youtube-playlists](https://github.com/ndom91/youtube-playlists)
  🎥 React app for creating on-the-fly YouTube playlists.
- [cuduy197/vue-flashcard](https://github.com/cuduy197/vue-flashcard)
  Rich flashcard component for vue js 2 :tada:
- [microsoft/frontend-bootcamp](https://github.com/microsoft/frontend-bootcamp)
  Frontend Workshop from HTML/CSS/JS to TypeScript/React/Redux
- [styled-components/vue-styled-components](https://github.com/styled-components/vue-styled-components)
  Visual primitives for the component age. A simple port for Vue of styled-components 💅
- [spatie/vue-table-component](https://github.com/spatie/vue-table-component)
  A straight to the point Vue component to display tables
- [frontend-collective/react-sortable-tree-theme-file-explorer](https://github.com/frontend-collective/react-sortable-tree-theme-file-explorer)
  A file explorer theme for React Sortable Tree
- [halower/vue-tree](https://github.com/halower/vue-tree)
  tree and multi-select component based on Vue.js 2.0
- [fivethirtyeight/victory-chart](https://github.com/fivethirtyeight/victory-chart)
  Chart Component for Victory
- [expo/expo](https://github.com/expo/expo)
  An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
- [OnsenUI/OnsenUI](https://github.com/OnsenUI/OnsenUI)
  Mobile app development framework and SDK using HTML5 and JavaScript. Create beautiful and performant cross-platform mobile apps. Based on Web Components, and provides bindings for Angular 1, 2, React and Vue.js.
- [starship/starship](https://github.com/starship/starship)
  ☄🌌️ The cross-shell prompt for astronauts.
- [AlexBarinov/UIBubbleTableView](https://github.com/AlexBarinov/UIBubbleTableView)
  Cocoa UI component for chat bubbles with avatars and images support
- [wyzant-dev/vue-radial-progress](https://github.com/wyzant-dev/vue-radial-progress)
  Radial progress bar component for Vue.js
- [uptick/react-keyed-file-browser](https://github.com/uptick/react-keyed-file-browser)
  Folder based file browser given a flat keyed list of objects, powered by React.
- [gcla/termshark](https://github.com/gcla/termshark)
  A terminal UI for tshark, inspired by Wireshark
- [codesandbox/codesandbox-client](https://github.com/codesandbox/codesandbox-client)
  An online code editor tailored for web application development 🏖️
- [matchai/spacefish](https://github.com/matchai/spacefish)
  🚀🐟 The fish shell prompt for astronauts
- [bluquar/react-redux-soundcloud](https://github.com/bluquar/react-redux-soundcloud)
  Learn React + Redux by building a SoundCloud Client. Full tutorial included. Source Code for main tutorial but also extensions included.
- [entropic-dev/entropic](https://github.com/entropic-dev/entropic)
  🦝 :package: a package registry for anything, but mostly javascript 🦝 🦝 🦝
- [knadh/listmonk](https://github.com/knadh/listmonk)
  High performance, self-hosted newsletter and mailing list manager with a modern dashboard. Go + React.
- [arunpkqt/RadialBarDemo](https://github.com/arunpkqt/RadialBarDemo)
  Custom radial Progress Bar QML component
- [vega/vega-view](https://github.com/vega/vega-view)
  View component for Vega visualizations.
- [gautamkrishnar/nothing-private](https://github.com/gautamkrishnar/nothing-private)
  Do you think you are safe using private browsing or incognito mode?. :smile: :imp: This will prove that you're wrong.   
- [joshwcomeau/react-retro-hit-counter](https://github.com/joshwcomeau/react-retro-hit-counter)
  🆕 Go back in time with this 90s-style hit counter.
- [kerrbrittany9/tamagotchi-react](https://github.com/kerrbrittany9/tamagotchi-react)
  web app using moment and in React.js to feed, play and rest a little creature you've created
- [symfony/console](https://github.com/symfony/console)
  The Console component eases the creation of beautiful and testable command line interfaces.
- [kefranabg/readme-md-generator](https://github.com/kefranabg/readme-md-generator)
  📄 CLI that generates beautiful README.md files
- [gatewayapps/kamino](https://github.com/gatewayapps/kamino)
  Github issue cloning tool
- [N3-components/N3-components](https://github.com/N3-components/N3-components)
  N3-components , Powerful Vue UI Library.
- [ahmed-dinar/vuex-flash](https://github.com/ahmed-dinar/vuex-flash)
  VueJs Flash Message Component within Vuex
- [wtfutil/wtf](https://github.com/wtfutil/wtf)
  The personal information dashboard for your terminal.
- [frontend-collective/react-sortable-tree](https://github.com/frontend-collective/react-sortable-tree)
  Drag-and-drop sortable component for nested data and hierarchies
- [joshuasoehn/Are.na-Safari-Extension](https://github.com/joshuasoehn/Are.na-Safari-Extension)
  A native safari extension for Are.na
- [sezna/nps](https://github.com/sezna/nps)
  NPM Package Scripts -- All the benefits of npm scripts without the cost of a bloated package.json and limits of json
- [NativeScript/NativeScript](https://github.com/NativeScript/NativeScript)
  NativeScript is an open source framework for building truly native mobile apps with JavaScript. Use web skills, like Angular and Vue.js, FlexBox and CSS, and get native UI and performance on iOS and Android.
- [talenet/talenet](https://github.com/talenet/talenet)
- [c8r/vue-styled-system](https://github.com/c8r/vue-styled-system)
  Design system utilities wrapper for Vue components, based on styled-system
- [fivethirtyeight/react-color](https://github.com/fivethirtyeight/react-color)
  :art: Color Pickers from Sketch, Photoshop, Chrome & more
- [vuejs/vue-next](https://github.com/vuejs/vue-next)
- [jordanoverbye/gatsby-theme-portfolio-are.na](https://github.com/jordanoverbye/gatsby-theme-portfolio-are.na)
  Gatsby + Are.na + Theme UI + MDX
- [30-seconds/30-seconds-of-react](https://github.com/30-seconds/30-seconds-of-react)
  Curated collection of useful React snippets that you can understand in 30 seconds or less.
- [syncthing/syncthing-macos](https://github.com/syncthing/syncthing-macos)
  Frugal and native macOS Syncthing application bundle
- [vuejs/vueify](https://github.com/vuejs/vueify)
  Browserify transform for single-file Vue components
- [carbon-design-system/carbon-components-vue](https://github.com/carbon-design-system/carbon-components-vue)
  Vue implementation of the Carbon Design System
- [cybercongress/cyberd](https://github.com/cybercongress/cyberd)
  InterPlanetary Search Engine & Knowledge Consensus Supercomputer in Cosmos-SDK/Tendermint/CUDA #fuckgoogle
- [dot-browser/desktop](https://github.com/dot-browser/desktop)
  🌍 A beautiful browser with material UI, with built-in adblock, based on Wexond.
- [oskca/gopherjs-vue](https://github.com/oskca/gopherjs-vue)
  VueJS bindings for gopherjs
- [Tomotoes/scrcpy-gui](https://github.com/Tomotoes/scrcpy-gui)
  ✨ A simple & beautiful GUI application for scrcpy. QQ群:734330215
- [hamed-ehtesham/pretty-checkbox-vue](https://github.com/hamed-ehtesham/pretty-checkbox-vue)
  Quickly integrate pretty checkbox components with Vue.js
- [mblaul/todo-challenge-react](https://github.com/mblaul/todo-challenge-react)
  A fun challenge with friends to create a todo app.
- [vuejs/rollup-plugin-vue](https://github.com/vuejs/rollup-plugin-vue)
  Roll .vue files
- [ionic-team/capacitor](https://github.com/ionic-team/capacitor)
  Build cross-platform Native Progressive Web Apps for iOS, Android, and the web ⚡️
- [thedaviddias/Front-End-Performance-Checklist](https://github.com/thedaviddias/Front-End-Performance-Checklist)
  🎮 The only Front-End Performance Checklist that runs faster than the others
- [logeshpaul/UI-Components](https://github.com/logeshpaul/UI-Components)
  Most used UI Components — of web applications. Curated/Most loved components for web development
- [vue-gl/vue-gl](https://github.com/vue-gl/vue-gl)
  Vue.js components rendering 3D graphics reactively via three.js
- [jcubic/jquery.terminal](https://github.com/jcubic/jquery.terminal)
  jQuery Terminal Emulator - web based terminal
- [lukakerr/Pine](https://github.com/lukakerr/Pine)
  A modern, native macOS markdown editor
- [zircleUI/zircleUI](https://github.com/zircleUI/zircleUI)
  🚀 zircle-ui is a frontend library to develop zoomable user interfaces.
- [trimstray/the-book-of-secret-knowledge](https://github.com/trimstray/the-book-of-secret-knowledge)
  A collection of inspiring lists, manuals, cheatsheets, blogs, hacks, one-liners, cli/web tools and more.
- [ShizukuIchi/winXP](https://github.com/ShizukuIchi/winXP)
  🏁 Web based Windows XP desktop recreation. 
- [radial-color-picker/vue-color-picker](https://github.com/radial-color-picker/vue-color-picker)
  Radial Color Picker - Vue
- [gkngkc/UnityStandaloneFileBrowser](https://github.com/gkngkc/UnityStandaloneFileBrowser)
  A native file browser for unity standalone platforms
- [vuejs/vue](https://github.com/vuejs/vue)
  🖖 Vue.js is a progressive, incrementally-adoptable JavaScript framework for building UI on the web.
- [ratracegrad/vue-flashcards](https://github.com/ratracegrad/vue-flashcards)
  Vue Flashcards to test your knowledge of Vue
- [er1chu/norde-arena-theme](https://github.com/er1chu/norde-arena-theme)
  Custom Are.na Theme
- [vparadis/vue-radial-menu](https://github.com/vparadis/vue-radial-menu)
  Simple vue radial menu
- [xizon/uix-kit](https://github.com/xizon/uix-kit)
  A free UI toolkit based on some common libraries for building beautiful responsive website, compatible with Bootstrap v4.
- [danielquinn/aletheia](https://github.com/danielquinn/aletheia)
  Fight fake news with cryptography & human nature
- [TeaMeow/TocasUI](https://github.com/TeaMeow/TocasUI)
  Tocas UI — A CSS3 and SASS UI library.
- [nepaul/awesome-web-components](https://github.com/nepaul/awesome-web-components)
  A curated list of awesome web components.
