# Repositories defined by: python, script, twitter

also defined by the following keywords: fn5, scripts, api, notebook, generator, jupyter, time

- [webrecorder/autobrowser](https://github.com/webrecorder/autobrowser)
- [internetarchive/warctools](https://github.com/internetarchive/warctools)
  warctools
- [m4rk3r/coalesce](https://github.com/m4rk3r/coalesce)
- [m4rk3r/MMMM](https://github.com/m4rk3r/MMMM)
- [jm33-m0/mec](https://github.com/jm33-m0/mec)
  for concurrent exploiting
- [Kontari/Tadpole](https://github.com/Kontari/Tadpole)
  A genetic survival simulation involving tadpoles in a tidepool 
- [YunoHost-Apps/jupyterlab_ynh](https://github.com/YunoHost-Apps/jupyterlab_ynh)
  Jupyterlab for Yunohost
- [Treora/locator](https://github.com/Treora/locator)
- [mopidy/pyspotify](https://github.com/mopidy/pyspotify)
  Python bindings for libspotify
- [realpython/python-scripts](https://github.com/realpython/python-scripts)
  because i'm tired of gists
- [tweepy/tweepy](https://github.com/tweepy/tweepy)
  Twitter for Python!
- [alecalve/python-bitcoin-blockchain-parser](https://github.com/alecalve/python-bitcoin-blockchain-parser)
  A Python 3 Bitcoin blockchain parser
- [MediaFire/mediafire-python-open-sdk](https://github.com/MediaFire/mediafire-python-open-sdk)
- [theSundayProgrammer/heirarchy_PY](https://github.com/theSundayProgrammer/heirarchy_PY)
  Python Version
- [m4rk3r/lan-before-time](https://github.com/m4rk3r/lan-before-time)
  :volcano: :hourglass_flowing_sand: :satellite:
- [mehransab101/Learning-Pentesting-With-Python](https://github.com/mehransab101/Learning-Pentesting-With-Python)
  Learning-Pentesting-With-Python
- [rahulshinde/image-outliner](https://github.com/rahulshinde/image-outliner)
- [Pinperepette/PyPhisher](https://github.com/Pinperepette/PyPhisher)
  A simple python tool for phishing
- [ibrahimokdadov/upload_file_python](https://github.com/ibrahimokdadov/upload_file_python)
  Uploading files using Python and Flask
- [Cartman720/PySitemap](https://github.com/Cartman720/PySitemap)
  Simple sitemap generator with Python 3
- [treenotation/pytree](https://github.com/treenotation/pytree)
  Tree Notation Python Library
- [geekcomputers/Python](https://github.com/geekcomputers/Python)
  My Python Examples
- [xxg1413/python](https://github.com/xxg1413/python)
  Python Books && Courses
- [PyCQA/pycodestyle](https://github.com/PyCQA/pycodestyle)
  Simple Python style checker in one Python file
- [faif/python-patterns](https://github.com/faif/python-patterns)
  A collection of design patterns/idioms in Python
- [TheAlgorithms/Python](https://github.com/TheAlgorithms/Python)
  All Algorithms implemented in Python
- [rbrook22/txtFiles.py](https://github.com/rbrook22/txtFiles.py)
- [Show-Me-the-Code/python](https://github.com/Show-Me-the-Code/python)
  Show Me the Code Python version.
- [coucoueric/Python](https://github.com/coucoueric/Python)
  Python script and Documents
- [gil9red/SimplePyScripts](https://github.com/gil9red/SimplePyScripts)
  A set of Python scripts.
- [idank/bashlex](https://github.com/idank/bashlex)
  Python parser for bash
- [ritiek/lyricwikia](https://github.com/ritiek/lyricwikia)
  Python API to get song lyrics from LyricWikia
- [euske/pdfminer](https://github.com/euske/pdfminer)
  Python PDF Parser
- [Huc91/Nostalgia](https://github.com/Huc91/Nostalgia)
  Bringing back dial-up modem era feels
- [python/cpython](https://github.com/python/cpython)
  The Python programming language
- [floscha/tinycards-python-api](https://github.com/floscha/tinycards-python-api)
  An unofficial Python API for Tinycards by Duolingo
- [pytorch/pytorch](https://github.com/pytorch/pytorch)
  Tensors and Dynamic neural networks in Python with strong GPU acceleration
- [bitagoras/PyPadPlusPlus](https://github.com/bitagoras/PyPadPlusPlus)
  Python IDE based on Notepad++ and PythonScript
- [lxml/lxml](https://github.com/lxml/lxml)
  The lxml XML toolkit for Python
- [bear/python-twitter](https://github.com/bear/python-twitter)
  A Python wrapper around the Twitter API.
- [joeyajames/Python](https://github.com/joeyajames/Python)
  Python code for YouTube videos.
- [ksmith97/GzipSimpleHTTPServer](https://github.com/ksmith97/GzipSimpleHTTPServer)
  A modification of the very useful SimpleHTTPServer python script to add gzip compression.
- [reddit/baseplate.py](https://github.com/reddit/baseplate.py)
  reddit's python service framework
- [cython/cython](https://github.com/cython/cython)
  The most widely used Python to C compiler
- [zhiwehu/Python-programming-exercises](https://github.com/zhiwehu/Python-programming-exercises)
  100+ Python challenging programming exercises
- [revolunet/PythonBooks](https://github.com/revolunet/PythonBooks)
  Directory of free Python ebooks
- [chunqiuyiyu/python-tools](https://github.com/chunqiuyiyu/python-tools)
  Some software tools built with Python. 
- [seemoo-lab/opendrop](https://github.com/seemoo-lab/opendrop)
  An open Apple AirDrop implementation written in Python
- [rubik/radon](https://github.com/rubik/radon)
  Various code metrics for Python code
- [jerry-git/thug-memes](https://github.com/jerry-git/thug-memes)
  Command line Thug Meme generator written in Python
- [zhiyzuo/python-tutorial](https://github.com/zhiyzuo/python-tutorial)
  Python tutorial series
- [vibhavp/dotty](https://github.com/vibhavp/dotty)
  A python script to sync your dotfiles
- [Dirkster99/PyNotes](https://github.com/Dirkster99/PyNotes)
  My notebook on using Python with Jupyter Notebook, PySpark etc
- [Akuli/python-tutorial](https://github.com/Akuli/python-tutorial)
  A Python 3 programming tutorial for beginners.
- [vmarkovtsev/GitHubStars](https://github.com/vmarkovtsev/GitHubStars)
  Python script to fetch GitHub repos metadata.
- [HumanCellAtlas/metadata-api](https://github.com/HumanCellAtlas/metadata-api)
- [chubin/pyphoon](https://github.com/chubin/pyphoon)
  :waning_gibbous_moon:  ASCII Art Phase of the Moon (Python version)
- [Jaymon/captain](https://github.com/Jaymon/captain)
  command line python scripts for humans
- [superdima05/tidalgrabber](https://github.com/superdima05/tidalgrabber)
  If you have a lossless subscription, and you want to get .flac files, you can use this python script.
- [ajminich/Jarvis](https://github.com/ajminich/Jarvis)
  A very simple AI-based personal assistant written in Python.
- [PulseMedia/python-tv-parser](https://github.com/PulseMedia/python-tv-parser)
  A lightweight python playlist parser
- [prompt-toolkit/pyvim](https://github.com/prompt-toolkit/pyvim)
  Pure Python Vim clone.
- [Pinperepette/newspaper](https://github.com/Pinperepette/newspaper)
  News, full-text, and article metadata extraction in python 2.6 - 3.4. 
- [danielgtaylor/paodate](https://github.com/danielgtaylor/paodate)
  Simpler Python date handling
- [pydot/pydot](https://github.com/pydot/pydot)
  Python interface to Graphviz's Dot language
- [ageitgey/face_recognition](https://github.com/ageitgey/face_recognition)
  The world's simplest facial recognition api for Python and the command line
- [binhex/moviegrabber](https://github.com/binhex/moviegrabber)
  Python based fully automated Usenet and Torrent movie downloader
- [sectool/Python-Random-Password-Generator](https://github.com/sectool/Python-Random-Password-Generator)
  Python - Random Password Generator ( R.P.G. )
- [jasonqng/genius-lyrics-search](https://github.com/jasonqng/genius-lyrics-search)
  Python script for searching Genius.com API
- [babygame0ver/calculator-travis](https://github.com/babygame0ver/calculator-travis)
  project sample to use travis , organize Python code with test cases as well as python module
- [pybites/blog_code](https://github.com/pybites/blog_code)
  Python sample code for our blog
- [0b01/macintoshplus](https://github.com/0b01/macintoshplus)
  Vaporwave aesthetics generator
- [gdub/python-archive](https://github.com/gdub/python-archive)
  Python package providing a common interface for unpacking zip and tar achives.
- [kjam/python-web-scraping-tutorial](https://github.com/kjam/python-web-scraping-tutorial)
  A Python-based web and data scraping tutorial
- [eloyz/reddit](https://github.com/eloyz/reddit)
  Scrapy (Python Framework) Example using reddit.com
- [hindupuravinash/the-gan-zoo](https://github.com/hindupuravinash/the-gan-zoo)
  A list of all named GANs!
- [mahmoud/glom](https://github.com/mahmoud/glom)
  ☄️ Python's nested data operator (and CLI), for all your declarative restructuring needs. Got data? Glom it! ☄️
- [globocom/m3u8](https://github.com/globocom/m3u8)
  Python m3u8 Parser for HTTP Live Streaming (HLS) Transmissions
- [aayars/py-noisemaker](https://github.com/aayars/py-noisemaker)
  Classic procedural noise generation algorithms, adapted for Python 3 and TensorFlow.
- [codeneuro/neurofinder-python](https://github.com/codeneuro/neurofinder-python)
  python module for testing neuron finding algorithms
- [tiagocoutinho/multivisor](https://github.com/tiagocoutinho/multivisor)
  Centralized supervisor WebUI and CLI
- [Ankur1401/Youtube-Playlist-Downloader](https://github.com/Ankur1401/Youtube-Playlist-Downloader)
  A fast and stable Youtube playlist downloader script created in Python.
- [datamade/probablepeople](https://github.com/datamade/probablepeople)
  :family: a python library for parsing unstructured western names into name components.
- [imranghory/urlextractor](https://github.com/imranghory/urlextractor)
  Python URL Extractor - extract URLs from text
- [Pinperepette/word_cloud](https://github.com/Pinperepette/word_cloud)
  A little word cloud generator in Python
- [patch0000/Python3-PDF2TXT-sample](https://github.com/patch0000/Python3-PDF2TXT-sample)
  pdf2txt sample
- [google/python-fire](https://github.com/google/python-fire)
  Python Fire is a library for automatically generating command line interfaces (CLIs) from absolutely any Python object.
- [JasonHinds13/PyFiling](https://github.com/JasonHinds13/PyFiling)
  Python script that organizes files in a folder or directory according to file type/extension.
- [Pinperepette/tweetfeels](https://github.com/Pinperepette/tweetfeels)
  Real-time sentiment analysis in Python using twitter's streaming api
- [DocNow/twarc](https://github.com/DocNow/twarc)
  A command line tool (and Python library) for archiving Twitter JSON
- [pwildenhain/terminal_playing_cards](https://github.com/pwildenhain/terminal_playing_cards)
  Python :package: for playing card games in the terminal
- [wrf/lavaLampPlot](https://github.com/wrf/lavaLampPlot)
  instructions, python and R code for generating lava lamp plots of kmer coverage
- [mahmoud/awesome-python-applications](https://github.com/mahmoud/awesome-python-applications)
  💿 Free software that works great, and also happens to be open-source Python. 
- [andysturrock/find-duplicate-files](https://github.com/andysturrock/find-duplicate-files)
  Python scripts for finding duplicate files
- [SouravJohar/python-app-with-electron-gui](https://github.com/SouravJohar/python-app-with-electron-gui)
  A better way to make GUIs for your python apps
- [CenterForOpenScience/modular-file-renderer](https://github.com/CenterForOpenScience/modular-file-renderer)
  A Python package for rendering files to HTML via an embeddable iframe
- [parrt/dtreeviz](https://github.com/parrt/dtreeviz)
  A python library for decision tree visualization and model interpretation.
- [skyepn/telechat-py](https://github.com/skyepn/telechat-py)
  A Python rewrite of the classic Telechat telnet chat server
- [pybites/pynews](https://github.com/pybites/pynews)
  Python news aggregator - prioritises news because dev time is scarce  
- [antonio6643/Terminal-Games](https://github.com/antonio6643/Terminal-Games)
  Random terminal games I make in Python when I'm bored/have spare time at school
- [mGalarnyk/Python_Tutorials](https://github.com/mGalarnyk/Python_Tutorials)
  Python tutorials in both Jupyter Notebook and youtube format. 
- [gto76/python-cheatsheet](https://github.com/gto76/python-cheatsheet)
  Comprehensive Python Cheatsheet
- [webrecorder/pywb](https://github.com/webrecorder/pywb)
  Core Python Web Archiving Toolkit for replay and recording of web archives
- [eisenjulian/fb-page-chat-download](https://github.com/eisenjulian/fb-page-chat-download)
  Python script to download messages from a Facebook page to a CSV file
- [karthikeyankc/HistoryAnalyzer](https://github.com/karthikeyankc/HistoryAnalyzer)
  A Python script to grab some tasty data from your Chrome's history and analyze it.
- [internetarchive/warc](https://github.com/internetarchive/warc)
  Python library for reading and writing warc files
- [scrapinghub/dateparser](https://github.com/scrapinghub/dateparser)
  python parser for human readable dates
- [knipknap/exscript](https://github.com/knipknap/exscript)
  A Python module making Telnet and SSH easy
- [ankitshekhawat/twitter-to-rss](https://github.com/ankitshekhawat/twitter-to-rss)
  Simple python script to parse twitter feed to generate a rss feed.
- [ofek/hatch](https://github.com/ofek/hatch)
  A modern project, package, and virtual env manager for Python
- [nikolak/django_reddit](https://github.com/nikolak/django_reddit)
  Reddit clone written in python using django web framework and twitter's bootstrap.
- [svetlyak40wt/dotfiler](https://github.com/svetlyak40wt/dotfiler)
  Shell agnostic git based dotfiles package manager, written in Python.
- [t184256/hacks](https://github.com/t184256/hacks)
  hacks, a python plugin library that doesn't play by the rules
- [codelucas/flask_reddit](https://github.com/codelucas/flask_reddit)
  Reddit clone in flask + python + nginx + https. View site:
- [Marten4n6/TinyTor](https://github.com/Marten4n6/TinyTor)
  A tiny Tor client implementation (in pure python).
- [sighingnow/parsec.py](https://github.com/sighingnow/parsec.py)
  A universal Python parser combinator library inspired by Parsec library of Haskell.
- [m4rk3r/pywb](https://github.com/m4rk3r/pywb)
  Python WayBack for web archive replay and url-rewriting HTTP/S web proxy
- [vinta/awesome-python](https://github.com/vinta/awesome-python)
  A curated list of awesome Python frameworks, libraries, software and resources
- [Pinperepette/awesome-python](https://github.com/Pinperepette/awesome-python)
  A curated list of awesome Python frameworks, libraries, software and resources
- [MorvanZhou/easy-scraping-tutorial](https://github.com/MorvanZhou/easy-scraping-tutorial)
  Simple but useful Python web scraping tutorial code. 
- [jakevdp/PythonDataScienceHandbook](https://github.com/jakevdp/PythonDataScienceHandbook)
  Python Data Science Handbook: full text in Jupyter Notebooks
- [tcoxon/fishpye](https://github.com/tcoxon/fishpye)
  A non-recursive voxel-based real-time raytracer in Python, OpenCL and OpenGL, with support for fish-eye/panoramic field-of-views, lighting (camera as the light source) and portals.
- [vpistis/OrganizeMediaFiles](https://github.com/vpistis/OrganizeMediaFiles)
  a collection of Python scripts that help you organize media files into a directory tree "year/month" based on metadata , using exiftool
- [hardikvasa/google-images-download](https://github.com/hardikvasa/google-images-download)
  Python Script to download hundreds of images from 'Google Images'. It is a ready-to-run code!
- [t7hm1/GhostNET](https://github.com/t7hm1/GhostNET)
  GhostNET script that will help you be safer on the cyber
- [Dirkster99/MachineLearningWithPython](https://github.com/Dirkster99/MachineLearningWithPython)
  Starter files for Pluralsight course: Understanding Machine Learning with Python
- [Pierian-Data/Complete-Python-3-Bootcamp](https://github.com/Pierian-Data/Complete-Python-3-Bootcamp)
  Course Files for Complete Python 3 Bootcamp Course on Udemy
- [xaroth8088/SystemPanic](https://github.com/xaroth8088/SystemPanic)
  A game where anything can happen
- [MagicStack/MagicPython](https://github.com/MagicStack/MagicPython)
  Cutting edge Python syntax highlighter for Sublime Text, Atom and Visual Studio Code. Used by GitHub to highlight your Python code!
- [D4Vinci/Twitter-Info](https://github.com/D4Vinci/Twitter-Info)
  A simple python script to grab twitter account info just by username or profile link
- [fate0/pychrome](https://github.com/fate0/pychrome)
  A Python Package for the Google Chrome Dev Protocol [threading base]
- [chriskiehl/Gooey](https://github.com/chriskiehl/Gooey)
  Turn (almost) any Python command line program into a full GUI application with one line
- [codebox/reading-list-mover](https://github.com/codebox/reading-list-mover)
  A Python utility for moving bookmarks/reading lists between services
- [rhiever/reddit-analysis](https://github.com/rhiever/reddit-analysis)
  A Python script that parses post titles, self-texts, and comments on reddit and makes word clouds out of the word frequencies.
- [weskerfoot/DeleteFB](https://github.com/weskerfoot/DeleteFB)
  Automate Scrubbing your Facebook Presence
- [nikhilgaba001/YoutubeToSpotify](https://github.com/nikhilgaba001/YoutubeToSpotify)
  A Python script to fetch tracks of music channels on Youtube, find them on Spotify and add them to a playlist
- [HoussemCharf/FunUtils](https://github.com/HoussemCharf/FunUtils)
  Some codes i wrote to help me with me with my daily errands ;)
- [lipoja/URLExtract](https://github.com/lipoja/URLExtract)
  URLExtract is python class for collecting (extracting) URLs from given text based on locating TLD.
- [mashedkeyboard/Dashday](https://github.com/mashedkeyboard/Dashday)
  Your day's dashboard, printed.
- [Pinperepette/instabot.py](https://github.com/Pinperepette/instabot.py)
  Instagram bot. It works without instagram api, need only login and password. Write on python.
- [python-hyper/hyperlink](https://github.com/python-hyper/hyperlink)
  🔗 Immutable, Pythonic, correct URLs.
- [r0x0r/pywebview](https://github.com/r0x0r/pywebview)
  Build GUI for your Python program with JavaScript, HTML, and CSS
- [Ivoah/minimap](https://github.com/Ivoah/minimap)
  Generate minimaps of your code
- [briankendall/check-time-machine](https://github.com/briankendall/check-time-machine)
  Python script for checking to make sure all of your files are actually backed up by Time Machine, and offers options for fixing files that are not.
- [tevora-threat/Scout](https://github.com/tevora-threat/Scout)
  Surveillance Detection Scout: Your Lookout on Autopilot
- [milesrichardson/ParsePy](https://github.com/milesrichardson/ParsePy)
  A relatively up-to-date fork of ParsePy, the Python wrapper for the Parse.com API. Originally maintained by @dgrtwo 
- [atomotic/wikidata_suggest](https://github.com/atomotic/wikidata_suggest)
  a CLI suggestion tool for Wikidata entities
- [racker/node-elementtree](https://github.com/racker/node-elementtree)
  Port of Python's Element Tree module to Node.js
- [edmondburnett/twitter-text-python](https://github.com/edmondburnett/twitter-text-python)
  Twitter text processing library (auto linking and extraction of usernames, lists and hashtags).
- [stephenmcd/filebrowser-safe](https://github.com/stephenmcd/filebrowser-safe)
  File manager for Mezzanine
- [martinblech/xmltodict](https://github.com/martinblech/xmltodict)
  Python module that makes working with XML feel like you are working with JSON
- [r0oth3x49/udemy-dl](https://github.com/r0oth3x49/udemy-dl)
  A cross-platform python based utility to download courses from udemy for personal offline use.
- [thp/urlwatch](https://github.com/thp/urlwatch)
  urlwatch monitors webpages for you
- [sepandhaghighi/art](https://github.com/sepandhaghighi/art)
  🎨 ASCII art library for Python
- [c-bata/go-prompt](https://github.com/c-bata/go-prompt)
  Building powerful interactive prompts in Go, inspired by python-prompt-toolkit.
- [sherlock-project/sherlock](https://github.com/sherlock-project/sherlock)
  🔎 Find usernames across social networks
- [bfontaine/term2048](https://github.com/bfontaine/term2048)
  :tada: 2048 in your terminal
- [twintproject/twint](https://github.com/twintproject/twint)
  An advanced Twitter scraping & OSINT tool written in Python that doesn't use Twitter's API, allowing you to scrape a user's followers, following, Tweets and more while evading most API limitations.
- [bokeh/bokeh](https://github.com/bokeh/bokeh)
  Interactive Data Visualization in the browser, from  Python
- [webrecorder/webrecorder](https://github.com/webrecorder/webrecorder)
  Web Archiving For All!
- [atomotic/webrecorder](https://github.com/atomotic/webrecorder)
  Web Archiving For All!
- [cea-sec/ivre](https://github.com/cea-sec/ivre)
  Network recon framework.
- [BonsaiDen/twitter-text-python](https://github.com/BonsaiDen/twitter-text-python)
  Twitter text processing library (auto linking and extraction of usernames, lists and hashtags). Based on the Java implementation by Matt Sanford
- [polygamma/aurman](https://github.com/polygamma/aurman)
  AUR Helper
- [servocoder/RichFilemanager](https://github.com/servocoder/RichFilemanager)
  An open-source file manager. Up-to-date for PHP, Java, ASHX, ASP, NodeJs & Python 3 Flask. Contributions are welcome!
- [evilsocket/pwnagotchi](https://github.com/evilsocket/pwnagotchi)
  (⌐■_■) - Deep Reinforcement Learning instrumenting bettercap for WiFi pwning.
- [lawlite19/PythonCrawler-Scrapy-Mysql-File-Template](https://github.com/lawlite19/PythonCrawler-Scrapy-Mysql-File-Template)
  scrapy爬虫框架模板，将数据保存到Mysql数据库或者文件中。
- [r9y9/wavenet_vocoder](https://github.com/r9y9/wavenet_vocoder)
  WaveNet vocoder
- [thombashi/pytablewriter](https://github.com/thombashi/pytablewriter)
  pytablewriter is a Python library to write a table in various formats: CSV / Elasticsearch / HTML / JavaScript / JSON / LaTeX / LDJSON / LTSV / Markdown / MediaWiki / NumPy / Excel / Pandas / Python / reStructuredText / SQLite / TOML / TSV.
- [funkyfuture/todo.txt-pylib](https://github.com/funkyfuture/todo.txt-pylib)
  An easy to extend Python 3 library to parse, manipulate, query and render tasks in the todo.txt-format in a pythonic manner.
- [jdepoix/youtube-transcript-api](https://github.com/jdepoix/youtube-transcript-api)
  This is an python API which allows you to get the transcripts/subtitles for a given YouTube video. It also works for automatically generated subtitles and it does not require a headless browser, like other selenium based solutions do!
- [in28minutes/python-tutorial-for-beginners](https://github.com/in28minutes/python-tutorial-for-beginners)
  Python Tutorial for Beginners with 500 Code Examples
- [alexander-hamme/Tadpole-Tracker-Python](https://github.com/alexander-hamme/Tadpole-Tracker-Python)
  Research project that tracks and records movement data of many Xenopus laevis tadpoles at once, in real time.
- [louipc/turses](https://github.com/louipc/turses)
  A Twitter client for the console.
- [jdjkelly/www.aaronsw.com](https://github.com/jdjkelly/www.aaronsw.com)
  An archival copy.
- [digitalmethodsinitiative/trackmap](https://github.com/digitalmethodsinitiative/trackmap)
  Scripts needed to support Trackography project
- [shughes-uk/python-youtubechat](https://github.com/shughes-uk/python-youtubechat)
  provides a simple client library for the youtube live streaming chat api
- [joowani/dtags](https://github.com/joowani/dtags)
  Directory Tags for Lazy Programmers
- [pybites/challenges](https://github.com/pybites/challenges)
  PyBites Code Challenges
- [webrecorder/browsertrix](https://github.com/webrecorder/browsertrix)
  Browsertrix: Containerized High-Fidelity Browser-Based Automated Crawling + Behavior System
- [pavelgonchar/face-search](https://github.com/pavelgonchar/face-search)
  Face search engine
- [joestump/python-oauth2](https://github.com/joestump/python-oauth2)
  A fully tested, abstract interface to creating OAuth clients and servers.
- [frnsys/port](https://github.com/frnsys/port)
  lightweight blogging platform
- [davidsun/HostsFile](https://github.com/davidsun/HostsFile)
  Hosts file for FourSquare, DropBox, Facebook, Twitter & Google
- [dantaki/vapeplot](https://github.com/dantaki/vapeplot)
  matplotlib extension for vaporwave aesthetics
- [eversum/galacteek](https://github.com/eversum/galacteek)
  Browser for the distributed web
- [cozy/cozy-setup](https://github.com/cozy/cozy-setup)
  Cozy installation files and information
- [jmwerner/TXT2PYNB](https://github.com/jmwerner/TXT2PYNB)
  Converts text script to iPython notebook
- [HumanCellAtlas/ingest-archiver](https://github.com/HumanCellAtlas/ingest-archiver)
  Ingest Archiver service
- [usableprivacy/upribox](https://github.com/usableprivacy/upribox)
  Usable Privacy Box
- [ritiek/Lyrics4Song](https://github.com/ritiek/Lyrics4Song)
  Provides lyrics for almost any song
- [ianepperson/telnetsrvlib](https://github.com/ianepperson/telnetsrvlib)
  Telnet server using gevent
- [hxrts/rhizome.org](https://github.com/hxrts/rhizome.org)
  Contemporary art and technology, online since 1996.
- [sloria/doitlive](https://github.com/sloria/doitlive)
  Because sometimes you need to do it live
- [aidlearning/AidLearning-FrameWork](https://github.com/aidlearning/AidLearning-FrameWork)
  🔥🔥AidLearning build Linux environment  running on the Android devices with GUI, Deep-Learning and Python Visual Programming support. One-click install.
- [pybites/pytip](https://github.com/pybites/pytip)
  Building a Simple Web App With Bottle, SQLAlchemy, and the Twitter API
- [philipbl/duplicate-images](https://github.com/philipbl/duplicate-images)
  A script to find and delete duplicate images using pHash.
- [HumanCellAtlas/matrix-service](https://github.com/HumanCellAtlas/matrix-service)
  DCP Expression Matrix Service
- [rougier/matplotlib-tutorial](https://github.com/rougier/matplotlib-tutorial)
  Matplotlib tutorial for beginner
- [uwdata/draco-learn](https://github.com/uwdata/draco-learn)
  Learning Weights for Draco
- [m4ll0k/Infoga](https://github.com/m4ll0k/Infoga)
  Infoga - Email OSINT
- [dannyvai/reddit_crawlers](https://github.com/dannyvai/reddit_crawlers)
  will try to make interesting reddit crawlers that give some insight
- [UndeadSec/EvilURL](https://github.com/UndeadSec/EvilURL)
  Generate unicode evil domains for IDN Homograph Attack and detect them.
- [uwdata/termite-data-server](https://github.com/uwdata/termite-data-server)
  Data Server for Topic Models
- [donnemartin/data-science-ipython-notebooks](https://github.com/donnemartin/data-science-ipython-notebooks)
  Data science Python notebooks: Deep learning (TensorFlow, Theano, Caffe, Keras), scikit-learn, Kaggle, big data (Spark, Hadoop MapReduce, HDFS), matplotlib, pandas, NumPy, SciPy, Python essentials, AWS, and various command lines.
- [Zettelkasten-Method/zkviz](https://github.com/Zettelkasten-Method/zkviz)
  Zettel Network Visualizer
- [ycm-core/YouCompleteMe](https://github.com/ycm-core/YouCompleteMe)
  A code-completion engine for Vim
- [webrecorder/cdxj-indexer](https://github.com/webrecorder/cdxj-indexer)
  CDXJ Indexing of WARC/ARCs
- [ritiek/data-science-ipython-notebooks](https://github.com/ritiek/data-science-ipython-notebooks)
  Recently updated with 50 new notebooks! Data science Python notebooks: Deep learning (TensorFlow, Theano, Caffe, Keras), scikit-learn, Kaggle, big data (Spark, Hadoop MapReduce, HDFS), matplotlib, pandas, NumPy, SciPy, Python essentials, AWS, and various command lines.
- [cdown/yturl](https://github.com/cdown/yturl)
  YouTube videos on the command line
- [mblaul/dailyprogramming](https://github.com/mblaul/dailyprogramming)
  Repository for daily programming challenges
- [lightning-viz/nbviewer](https://github.com/lightning-viz/nbviewer)
  Nbconvert as a webservice (rendering ipynb to static HTML)
- [m4ll0k/WAScan](https://github.com/m4ll0k/WAScan)
  WAScan - Web Application Scanner
- [ritiek/AskQuora](https://github.com/ritiek/AskQuora)
  Quora Q&A right from the command-line
- [nteract/bookstore](https://github.com/nteract/bookstore)
  📚 Notebook storage and publishing workflows for the masses
- [HoverHell/RedditImageGrab](https://github.com/HoverHell/RedditImageGrab)
  Downloads images from sub-reddits of reddit.com.
- [nylas/sync-engine](https://github.com/nylas/sync-engine)
  :incoming_envelope: IMAP/SMTP sync system with modern APIs
- [gaojiuli/toapi](https://github.com/gaojiuli/toapi)
  Every web site provides APIs.
- [metachar/Hatch](https://github.com/metachar/Hatch)
  Hatch is a brute force tool that is used to brute force most websites
- [asciimoo/searx](https://github.com/asciimoo/searx)
  Privacy-respecting metasearch engine
- [6IX7ine/shodanwave](https://github.com/6IX7ine/shodanwave)
  Shodanwave is a tool for exploring and obtaining information from Netwave IP Camera. 
- [Zeecka/PassGen](https://github.com/Zeecka/PassGen)
  Wordlist Generator using key-words, options and recursivity
- [victordomingos/Count-files](https://github.com/victordomingos/Count-files)
  A CLI utility written in Python to help you count files, grouped by extension, in a directory. By default, it will count files recursively in current working directory and all of its subdirectories, and will display a table showing the frequency for each file extension (e.g.: .txt, .py, .html, .css) and the total number of files found.
- [gruns/furl](https://github.com/gruns/furl)
  🌐 URL parsing and manipulation made easy.
- [prasanthlouis/Youtube-Downloader](https://github.com/prasanthlouis/Youtube-Downloader)
  Downloads Youtube Videos and Playlists
- [shadowmoose/RedditDownloader](https://github.com/shadowmoose/RedditDownloader)
  Scrapes Reddit to download media of your choice.
- [buckket/twtxt](https://github.com/buckket/twtxt)
  Decentralised, minimalist microblogging service for hackers.
- [Tribler/tribler](https://github.com/Tribler/tribler)
  Privacy enhanced BitTorrent client with P2P content discovery
- [mps-youtube/mps-youtube](https://github.com/mps-youtube/mps-youtube)
  Terminal based YouTube player and downloader
- [pytest-dev/pytest-html](https://github.com/pytest-dev/pytest-html)
  Plugin for generating HTML reports for pytest results
- [resident-archive/resident-archive-lambdas](https://github.com/resident-archive/resident-archive-lambdas)
  Sync residentadvisor's library with songs available on Spotify
- [bluquar/reddit_scraper](https://github.com/bluquar/reddit_scraper)
  Choose subreddits to scrape the top images from
- [chubin/wttr.in](https://github.com/chubin/wttr.in)
  :partly_sunny: The right way to check the weather
- [jarun/ddgr](https://github.com/jarun/ddgr)
  :duck: DuckDuckGo from the terminal
- [nvbn/thefuck](https://github.com/nvbn/thefuck)
  Magnificent app which corrects your previous console command.
- [x89/Shreddit](https://github.com/x89/Shreddit)
  Remove your comment history on Reddit as deleting an account does not do so.
- [DocNow/diffengine](https://github.com/DocNow/diffengine)
  track changes to the news, where news is anything with an RSS feed
- [nbedos/termtosvg](https://github.com/nbedos/termtosvg)
  Record terminal sessions as SVG animations
- [psf/requests-html](https://github.com/psf/requests-html)
  Pythonic HTML Parsing for Humans™
- [finalion/WordQuery](https://github.com/finalion/WordQuery)
  word fast-querying addon for anki
- [idealo/imagededup](https://github.com/idealo/imagededup)
  😎 Finding duplicate images made easy!
- [Pinperepette/Spaghetti](https://github.com/Pinperepette/Spaghetti)
  Spaghetti - Web Application Security Scanner
- [bombs-kim/pythonp](https://github.com/bombs-kim/pythonp)
  A powerful utility that empowers pythonistas in the command line
- [OpenMined/PySyft](https://github.com/OpenMined/PySyft)
  A library for encrypted, privacy preserving deep learning
- [internetarchive/brozzler](https://github.com/internetarchive/brozzler)
  brozzler - distributed browser-based web crawler
- [Pinperepette/maybe](https://github.com/Pinperepette/maybe)
   :open_file_folder: :rabbit2: :tophat: See what a program does before deciding whether you really want it to happen.
- [thehackingsage/hacktronian](https://github.com/thehackingsage/hacktronian)
  All in One Hacking Tool for Linux & Android
- [feeluown/FeelUOwn](https://github.com/feeluown/FeelUOwn)
  trying to be a user-friendly and hackable music player
- [DotPodcast/dpx](https://github.com/DotPodcast/dpx)
  Public API and frontend site for adding legacy RSS podcast feeds to DotPodcast
- [ritiek/GitFeed](https://github.com/ritiek/GitFeed)
  Check your GitHub Newsfeed via the command-line
- [sJohnsonStoever/redditPostArchiver](https://github.com/sJohnsonStoever/redditPostArchiver)
  Easily archive important Reddit post threads onto your computer
- [HumanCellAtlas/falcon](https://github.com/HumanCellAtlas/falcon)
  The workflow starter of secondary analysis service
- [nteract/scrapbook](https://github.com/nteract/scrapbook)
  A library for recording and reading data in notebooks.
- [pybites/packtebooks](https://github.com/pybites/packtebooks)
  Script to manage your ebook (downloads) from command line
- [Shpota/github-activity-generator](https://github.com/Shpota/github-activity-generator)
  A script that helps you generate a beautiful GitHub activity graph
- [Jaffa/amazon-music](https://github.com/Jaffa/amazon-music)
  Provide programmatic access to Amazon Music/Prime Music's streaming service
- [darkarp/chromepass](https://github.com/darkarp/chromepass)
  Chromepass - Hacking Chrome Saved Passwords
- [rossem/RedditStorage](https://github.com/rossem/RedditStorage)
  Store files onto reddit subreddits.
- [jefftriplett/url2markdown-cli](https://github.com/jefftriplett/url2markdown-cli)
  :page_with_curl: Fetch a url and translate it to markdown in one command
- [rougier/numpy-tutorial](https://github.com/rougier/numpy-tutorial)
  Numpy beginner tutorial
- [AshveenBansal98/Python-Text-Search](https://github.com/AshveenBansal98/Python-Text-Search)
   A search engine for searching text/phrase in set of .txt files, based on inverted indexing and boyer moore algorithm. 
- [mooz/percol](https://github.com/mooz/percol)
  adds flavor of interactive filtering to the traditional pipe concept of UNIX shell
- [sawyerf/Youtube_subscription_manager](https://github.com/sawyerf/Youtube_subscription_manager)
  Youtube_subscription_manager is a program to retrieve your subscriptions or analyze channels and playlists
- [HumanCellAtlas/schema-template-generator](https://github.com/HumanCellAtlas/schema-template-generator)
  Interactive UI for generating metadata schema templates
- [fabtools/fabtools](https://github.com/fabtools/fabtools)
  Tools for writing awesome Fabric files
- [jrnl-org/jrnl](https://github.com/jrnl-org/jrnl)
  Collect your thoughts and notes without leaving the command line.
- [0xPrateek/ci_edit](https://github.com/0xPrateek/ci_edit)
  A terminal text editor with mouse support and ctrl+Q to quit.
- [qutebrowser/qutebrowser](https://github.com/qutebrowser/qutebrowser)
  A keyboard-driven, vim-like browser based on PyQt5.
- [encode/typesystem](https://github.com/encode/typesystem)
  Data validation, serialization, deserialization & form rendering. 🔢
- [nikitakit/self-attentive-parser](https://github.com/nikitakit/self-attentive-parser)
  High-accuracy NLP parser with models for 11 languages.
- [oduwsdl/ipwb](https://github.com/oduwsdl/ipwb)
  InterPlanetary Wayback: A distributed and persistent archive replay system using IPFS
- [trivio/common_crawl_index](https://github.com/trivio/common_crawl_index)
  Index URLs in Common Crawl 
- [edc/bass](https://github.com/edc/bass)
  Make Bash utilities usable in Fish shell
- [retext-project/retext](https://github.com/retext-project/retext)
  ReText: Simple but powerful editor for Markdown and reStructuredText
- [internetarchive/warcprox](https://github.com/internetarchive/warcprox)
  WARC writing MITM HTTP/S proxy
- [algorithmica-repository/datascience](https://github.com/algorithmica-repository/datascience)
  It consists of examples, assignments discussed in data science course taken at algorithmica.
- [Arkiver2/warcio](https://github.com/Arkiver2/warcio)
  Streaming WARC/ARC library for fast web archive IO
- [wting/autojump](https://github.com/wting/autojump)
  A cd command that learns - easily navigate directories from the command line
- [idank/explainshell](https://github.com/idank/explainshell)
  match command-line arguments to their help text
- [webrecorder/autoscalar](https://github.com/webrecorder/autoscalar)
  Webrecorder Auto Archiver for Scalar Prototype
- [ezaquarii/vpn-at-home](https://github.com/ezaquarii/vpn-at-home)
  1-click, self-hosted deployment of OpenVPN with DNS ad blocking sinkhole
- [Eloston/ungoogled-chromium](https://github.com/Eloston/ungoogled-chromium)
  Google Chromium, sans integration with Google
- [cls1991/ng](https://github.com/cls1991/ng)
  Get password of the wifi you're connected, and your current ip address.
- [wummel/linkchecker](https://github.com/wummel/linkchecker)
  check links in web documents or full websites
- [jsvine/waybackpack](https://github.com/jsvine/waybackpack)
  Download the entire Wayback Machine archive for a given URL.
- [sihaelov/harser](https://github.com/sihaelov/harser)
  Easy way for HTML parsing and building XPath
- [robbyrussell/oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)
  🙃 A delightful community-driven (with 1,300+ contributors) framework for managing your zsh configuration. Includes 200+ optional plugins (rails, git, OSX, hub, capistrano, brew, ant, php, python, etc), over 140 themes to spice up your morning, and an auto-update tool so that makes it easy to keep up with the latest updates from the community.
- [jarun/Buku](https://github.com/jarun/Buku)
  :bookmark: Browser-independent bookmark manager
- [danfolkes/Magnet2Torrent](https://github.com/danfolkes/Magnet2Torrent)
  This will convert a magnet link into a .torrent file
- [HumanCellAtlas/cloud-blobstore](https://github.com/HumanCellAtlas/cloud-blobstore)
  Simple API that abstracts out differences between the blobstores across different cloud providers.
- [ritiek/spotify-remote](https://github.com/ritiek/spotify-remote)
  Web-UI for Spotify Desktop that actually works
- [juancarlospaco/css-html-js-minify](https://github.com/juancarlospaco/css-html-js-minify)
  StandAlone Async cross-platform Minifier for the Web.
- [machawk1/wail](https://github.com/machawk1/wail)
  :whale2: Web Archiving Integration Layer: One-Click User Instigated Preservation
- [asciidoc/asciidoc](https://github.com/asciidoc/asciidoc)
  AsciiDoc is a text document format for writing notes, documentation, articles, books, slideshows, man pages & blogs. AsciiDoc can be translated to many formats including HTML, DocBook, PDF, EPUB, and man pages. NOTE: This implementation is written in Python 2, which EOLs in Jan 2020. AsciiDoc development is being continued under @asciidoctor.
- [Xyntax/FileSensor](https://github.com/Xyntax/FileSensor)
  Dynamic file detection tool based on crawler 基于爬虫的动态敏感文件探测工具 
- [firecat53/urlscan](https://github.com/firecat53/urlscan)
  Mutt and terminal url selector (similar to urlview)
- [tldr-pages/tldr](https://github.com/tldr-pages/tldr)
  :books: Simplified and community-driven man pages
- [chubin/cheat.sh](https://github.com/chubin/cheat.sh)
  the only cheat sheet you need
- [harvard-lil/warcgames](https://github.com/harvard-lil/warcgames)
  Hacking challenges to learn web archive security.
- [drduh/macOS-Security-and-Privacy-Guide](https://github.com/drduh/macOS-Security-and-Privacy-Guide)
  Guide to securing and improving privacy on macOS
- [SwagLyrics/SwagLyrics-For-Spotify](https://github.com/SwagLyrics/SwagLyrics-For-Spotify)
  📃 Get lyrics of currently playing Spotify song so you don't sing along with the wrong ones and embarrass yourself later. Very fast.
- [attardi/wikiextractor](https://github.com/attardi/wikiextractor)
  A tool for extracting plain text from Wikipedia dumps
- [gleitz/howdoi](https://github.com/gleitz/howdoi)
  instant coding answers via the command line
- [webrecorder/webrecorder-tests](https://github.com/webrecorder/webrecorder-tests)
  QA tests for webrecorder player (WORK IN PROGRESS)
- [ritiek/spotify-downloader](https://github.com/ritiek/spotify-downloader)
  Download Spotify playlists with albumart and meta-tags
- [Robpol86/terminaltables](https://github.com/Robpol86/terminaltables)
  Generate simple tables in terminals from a nested list of strings.
- [TheSpeedX/TPlay](https://github.com/TheSpeedX/TPlay)
  Play Music in Termux Like a GeeK
- [x0rz/tweets_analyzer](https://github.com/x0rz/tweets_analyzer)
  Tweets metadata scraper & activity analyzer
- [Pinperepette/tweets_analyzer](https://github.com/Pinperepette/tweets_analyzer)
  Tweets metadata scraper & activity analyzer
- [s0md3v/Photon](https://github.com/s0md3v/Photon)
  Incredibly fast crawler designed for OSINT.
- [nhviet1009/ASCII-generator](https://github.com/nhviet1009/ASCII-generator)
  ASCII generator (image to text, image to image, video to video)
- [donnemartin/dev-setup](https://github.com/donnemartin/dev-setup)
  macOS development environment setup:  Easy-to-understand instructions with automated setup scripts for developer tools like Vim, Sublime Text, Bash, iTerm, Python data analysis, Spark, Hadoop MapReduce, AWS, Heroku, JavaScript web development, Android development, common data stores, and dev-based OS X defaults.
- [ytdl-org/youtube-dl](https://github.com/ytdl-org/youtube-dl)
  Command-line program to download videos from YouTube.com and other video sites
- [webrecorder/warcio](https://github.com/webrecorder/warcio)
  Streaming WARC/ARC library for fast web archive IO
- [3jackdaws/soundcloud-lib](https://github.com/3jackdaws/soundcloud-lib)
  Soundcloud API wrapper for tracks & playlists that doesn't require API credentials.  Asyncio support.
- [datamade/parserator](https://github.com/datamade/parserator)
  :bookmark: A toolkit for making domain-specific probabilistic parsers
- [scrapy/parsel](https://github.com/scrapy/parsel)
  Parsel lets you extract data from XML/HTML documents using XPath or CSS selectors
- [pybites/packt](https://github.com/pybites/packt)
  Use Selenium to retrieve Packt's free daily ebook/video info and send a notification to Twitter and Slack
- [adulau/url_archiver](https://github.com/adulau/url_archiver)
  url-archiver is a simple library to fetch and archive URL on the file-system
- [HumanCellAtlas/query-service](https://github.com/HumanCellAtlas/query-service)
  Prototype implementation of the DCP Query Service, a metadata search engine for HCA
- [glue-viz/glue](https://github.com/glue-viz/glue)
  Linked Data Visualizations Across Multiple Files
- [Pinperepette/GeoTweet](https://github.com/Pinperepette/GeoTweet)
  Program to search tweets, tag, hashtag, user, with locations and maps
- [pydata/pandas-datareader](https://github.com/pydata/pandas-datareader)
  Extract data from a wide range of Internet sources into a pandas DataFrame.
- [coleifer/micawber](https://github.com/coleifer/micawber)
  a small library for extracting rich content from urls
- [streamlink/streamlink](https://github.com/streamlink/streamlink)
  CLI for extracting streams from various websites to a video player of your choosing
- [kenogo/spotify-lyrics-cli](https://github.com/kenogo/spotify-lyrics-cli)
  Migrated to Gitlab, this Github repo will not receive updates.
