# Repositories defined by: simple, plugin, telnet

also defined by the following keywords: macos, server, swift, mac, twitter, parsing, generator

- [ndrwhr/tumbler](https://github.com/ndrwhr/tumbler)
  A simple physics simulation/meditation/relaxation experiment.
- [s-yadav/radialIndicator](https://github.com/s-yadav/radialIndicator)
  A simple and light weight circular indicator / progressbar plugin.
- [ndrwhr/blob](https://github.com/ndrwhr/blob)
  A loveable blob built on some simple physics.
- [zeit/arg](https://github.com/zeit/arg)
  Simple argument parsing
- [Pinperepette/PyPhisher](https://github.com/Pinperepette/PyPhisher)
  A simple python tool for phishing
- [Cartman720/PySitemap](https://github.com/Cartman720/PySitemap)
  Simple sitemap generator with Python 3
- [norcross/comment-hierarchy-adjust](https://github.com/norcross/comment-hierarchy-adjust)
  A simple metabox to change a comment heirarchy
- [ServerCentral/praeco](https://github.com/ServerCentral/praeco)
  Elasticsearch alerting made simple.
- [daentech/Telnet](https://github.com/daentech/Telnet)
  A simple telnet server
- [Zeqiang-Lai/Timer-APP](https://github.com/Zeqiang-Lai/Timer-APP)
  A Simple Stopwatch App for macOS.
- [jedisct1/dsvpn](https://github.com/jedisct1/dsvpn)
  A Dead Simple VPN.
- [meliot/filewatcher](https://github.com/meliot/filewatcher)
  A simple auditing utility for macOS
- [tidyverse/rvest](https://github.com/tidyverse/rvest)
  Simple web scraping for R
- [michaelvillar/timer-app](https://github.com/michaelvillar/timer-app)
  A simple Timer app for Mac
- [aonez/timer-app](https://github.com/aonez/timer-app)
  A simple Timer app for Mac
- [fusic/filebinder](https://github.com/fusic/filebinder)
  Filebinder: Simple file attachment plugin for CakePHP
- [d11z/asperitas](https://github.com/d11z/asperitas)
  A simple reddit clone
- [kazzkiq/balloon.css](https://github.com/kazzkiq/balloon.css)
  Simple tooltips made of pure CSS
- [lecram/rover](https://github.com/lecram/rover)
  simple file browser for the terminal
- [PyCQA/pycodestyle](https://github.com/PyCQA/pycodestyle)
  Simple Python style checker in one Python file
- [seanmiddleditch/libtelnet](https://github.com/seanmiddleditch/libtelnet)
  Simple RFC-complient TELNET implementation as a C library.
- [s-p-k/bm](https://github.com/s-p-k/bm)
  A simple perl script for text bookmarks
- [louismerlin/concrete.css](https://github.com/louismerlin/concrete.css)
  A simple and to the point CSS microframework
- [go-shiori/shiori](https://github.com/go-shiori/shiori)
  Simple bookmark manager built with Go
- [jesseduffield/lazygit](https://github.com/jesseduffield/lazygit)
  simple terminal UI for git commands
- [YunoHost-Apps/reel2bits_ynh](https://github.com/YunoHost-Apps/reel2bits_ynh)
  Soundcloud-like but simple, easy and KISS (and ActivityPub).
- [Kilian/fromscratch](https://github.com/Kilian/fromscratch)
  Autosaving Scratchpad. A simple but smart note-taking app
- [ajminich/Jarvis](https://github.com/ajminich/Jarvis)
  A very simple AI-based personal assistant written in Python.
- [artgris/FileManagerBundle](https://github.com/artgris/FileManagerBundle)
  FileManager is a simple Multilingual File Manager Bundle for Symfony
- [retext-project/retext](https://github.com/retext-project/retext)
  ReText: Simple but powerful editor for Markdown and reStructuredText
- [carlanton/m3u8-parser](https://github.com/carlanton/m3u8-parser)
  A simple HLS playlist parser for Java
- [Katee/quietnet](https://github.com/Katee/quietnet)
  Simple chat program that communicates using inaudible sounds
- [dhg/Redditate](https://github.com/dhg/Redditate)
  A simple, minimal viewer for Reddit
- [pybites/pytip](https://github.com/pybites/pytip)
  Building a Simple Web App With Bottle, SQLAlchemy, and the Twitter API
- [s-p-k/foxy](https://github.com/s-p-k/foxy)
  A simple script that handles bookmarks stored in a txt file.
- [uwdata/vegaserver](https://github.com/uwdata/vegaserver)
  A simple node server that renders vega specs to SVG or PNG.
- [plataformatec/nimble_parsec](https://github.com/plataformatec/nimble_parsec)
  A simple and fast library for text-based parser combinators
- [blockstack-radiks/banter](https://github.com/blockstack-radiks/banter)
  A simple Twitter-style app to demo Radiks
- [mkozjak/node-telnet-client](https://github.com/mkozjak/node-telnet-client)
  A simple telnet client for Node.js
- [vparadis/vue-radial-menu](https://github.com/vparadis/vue-radial-menu)
  Simple vue radial menu
- [theaveragedude/mediacat](https://github.com/theaveragedude/mediacat)
  Simple tool to grab magnet URLs from the web
- [dnote/dnote](https://github.com/dnote/dnote)
  A simple personal knowledge base
- [Zeqiang-Lai/IconHide-App](https://github.com/Zeqiang-Lai/IconHide-App)
  A Simple Application for Hiding Desktop Icons on Macos
- [sharkdp/fd](https://github.com/sharkdp/fd)
  A simple, fast and user-friendly alternative to 'find'
- [ariona/hover3d](https://github.com/ariona/hover3d)
  Simple jQuery plugin for 3d Hover effect
- [Robpol86/terminaltables](https://github.com/Robpol86/terminaltables)
  Generate simple tables in terminals from a nested list of strings.
- [hukaibaihu/vue-org-tree](https://github.com/hukaibaihu/vue-org-tree)
  A simple organization tree based on Vue2.x
- [parkr/ping](https://github.com/parkr/ping)
  :bar_chart: Your very own Google Analytics replacement, without all of the Google. Simple as pie.
- [moul/sshportal](https://github.com/moul/sshportal)
  :tophat: simple, fun and transparent SSH (and telnet) bastion server
- [HumanCellAtlas/cloud-blobstore](https://github.com/HumanCellAtlas/cloud-blobstore)
  Simple API that abstracts out differences between the blobstores across different cloud providers.
- [jcampbell1/simple-file-manager](https://github.com/jcampbell1/simple-file-manager)
  A Simple PHP file manager.  The code is a single php file.  
- [skx/simple-vpn](https://github.com/skx/simple-vpn)
  A simple VPN allowing mesh-like communication between nodes, over websockets
- [mozilla/send](https://github.com/mozilla/send)
  Simple, private file sharing from the makers of Firefox
- [aonez/KeychainAccess](https://github.com/aonez/KeychainAccess)
  Simple Swift wrapper for Keychain that works on iOS and OS X
- [twbs/ratchet](https://github.com/twbs/ratchet)
  Build mobile apps with simple HTML, CSS, and JavaScript components. 
- [dnbrwstr/link-dump](https://github.com/dnbrwstr/link-dump)
  Simple link sharing board powered by Parse
- [go-ego/riot](https://github.com/go-ego/riot)
  Go Open Source, Distributed, Simple and efficient Search Engine 
- [textileio/node-chat](https://github.com/textileio/node-chat)
  A simple cli chat app using Textile
- [ndrwhr/tracer](https://github.com/ndrwhr/tracer)
  Simple hacked together tool for creating traced doodles that live on my home page.
- [textileio/textile-facebook](https://github.com/textileio/textile-facebook)
  simple parsing tool to get your data out of a facebook export
- [jondot/hygen](https://github.com/jondot/hygen)
  The simple, fast, and scalable code generator that lives in your project.
- [xanzy/go-gitlab](https://github.com/xanzy/go-gitlab)
  A GitLab API client enabling Go programs to interact with GitLab in a simple and uniform way
- [florent37/RxRetroJsoup](https://github.com/florent37/RxRetroJsoup)
  A simple API-like from html website (scrapper) for Android, RxJava2 ready !
- [PermalightNYC/sass-radial-menu](https://github.com/PermalightNYC/sass-radial-menu)
  A simple sass+compass dynamic radial menu.
- [prithvibhola/WorkManager](https://github.com/prithvibhola/WorkManager)
  A simple example to demonstrate the working of the Work Manager.
- [IMGIITRoorkee/django-filemanager](https://github.com/IMGIITRoorkee/django-filemanager)
  A simple Django app to browse files on server
- [leethomason/tinyxml2](https://github.com/leethomason/tinyxml2)
  TinyXML2 is a simple, small, efficient, C++ XML parser that can be easily integrated into other programs.
- [mileswd/mac2imgur](https://github.com/mileswd/mac2imgur)
  ⬆ A simple Mac app designed to make uploading images and screenshots to Imgur quick and effortless.
- [stebbins/cage-the-bird](https://github.com/stebbins/cage-the-bird)
  Cage the Bird is a simple Ruby command line application for purging your Twitter account of Tweets, Retweets, and Likes
- [urfave/cli](https://github.com/urfave/cli)
  A simple, fast, and fun package for building command line apps in Go
- [cambrant/txtngin](https://github.com/cambrant/txtngin)
  A simple web.py project which implements a static file hyperlink-less wiki, of sorts.
- [gribnoysup/wunderbar](https://github.com/gribnoysup/wunderbar)
  Simple horizontal bar chart printer for your terminal
- [tmk907/PlaylistsNET](https://github.com/tmk907/PlaylistsNET)
  Simple library for reading and writing playlist's files. Supported formats: m3u, pls, wpl, zpl.
- [nisrulz/app-privacy-policy-generator](https://github.com/nisrulz/app-privacy-policy-generator)
  A simple web app to generate a generic privacy policy for your Android/iOS apps
- [MorvanZhou/easy-scraping-tutorial](https://github.com/MorvanZhou/easy-scraping-tutorial)
  Simple but useful Python web scraping tutorial code. 
- [ankitshekhawat/twitter-to-rss](https://github.com/ankitshekhawat/twitter-to-rss)
  Simple python script to parse twitter feed to generate a rss feed.
- [Tomotoes/scrcpy-gui](https://github.com/Tomotoes/scrcpy-gui)
  ✨ A simple & beautiful GUI application for scrcpy. QQ群:734330215
- [textileio/dapp-template](https://github.com/textileio/dapp-template)
  a basic template to build simple ipfs-based browser dapps
- [D4Vinci/Twitter-Info](https://github.com/D4Vinci/Twitter-Info)
  A simple python script to grab twitter account info just by username or profile link
- [MrSaints/Morphext](https://github.com/MrSaints/Morphext)
  A simple, high-performance and cross-browser jQuery rotating / carousel plugin for text phrases powered by Animate.css.
- [todotxt/todo.txt-cli](https://github.com/todotxt/todo.txt-cli)
  ☑️ A simple and extensible shell script for managing your todo.txt file.
- [shughes-uk/python-youtubechat](https://github.com/shughes-uk/python-youtubechat)
  provides a simple client library for the youtube live streaming chat api
- [adulau/url_archiver](https://github.com/adulau/url_archiver)
  url-archiver is a simple library to fetch and archive URL on the file-system
- [trimstray/sandmap](https://github.com/trimstray/sandmap)
  Nmap on steroids! Simple CLI with the ability to run pure Nmap engine, 31 modules with 459 scan profiles.
- [anseki/plain-draggable](https://github.com/anseki/plain-draggable)
  The simple and high performance library to allow HTML/SVG element to be dragged.
- [chylex/Backup-Essentials](https://github.com/chylex/Backup-Essentials)
  Simple backup utility to use with cloud solutions or external hard drives.
- [connors/photon](https://github.com/connors/photon)
  The fastest way to build beautiful Electron apps using simple HTML and CSS
- [kognise/water.css](https://github.com/kognise/water.css)
  A just-add-css collection of styles to make simple websites just a little nicer
- [mochajs/mocha](https://github.com/mochajs/mocha)
  ☕️ simple, flexible, fun javascript test framework for node.js & the browser
- [styled-components/vue-styled-components](https://github.com/styled-components/vue-styled-components)
  Visual primitives for the component age. A simple port for Vue of styled-components 💅
- [thedevsaddam/renderer](https://github.com/thedevsaddam/renderer)
  Simple, lightweight and faster response (JSON, JSONP, XML, YAML, HTML, File) rendering package for Go
- [usefathom/fathom](https://github.com/usefathom/fathom)
  Fathom. Simple, privacy-focused website analytics. Built with Golang & Preact.
- [elipapa/markdown-cv](https://github.com/elipapa/markdown-cv)
  a simple template to write your CV in a readable markdown file and use CSS to publish/print it.
- [textileio/minimal-client-demo](https://github.com/textileio/minimal-client-demo)
  Simple project with minimal http client functionality to add files to an existing local thread
- [marktext/marktext](https://github.com/marktext/marktext)
  📝A simple and elegant markdown editor, available for Linux, macOS and Windows.
- [Jeffreyrn/vue-simple-flowchart](https://github.com/Jeffreyrn/vue-simple-flowchart)
  flowchart editor for Vue.js
- [DukeOfMarshall/PHP-Array-Heirarchy-Display](https://github.com/DukeOfMarshall/PHP-Array-Heirarchy-Display)
  A quick and simple class to easily display single or multidimensional arrays in a human readable heirarchal form. Even works for PHP objects. Many customizations available to fit your needs.
- [Simonwep/pickr](https://github.com/Simonwep/pickr)
  🍭 Flat, simple, multi-themed, responsive and hackable Color-Picker library. No dependencies, no jQuery. Compatible with all CSS Frameworks e.g. Bootstrap, Materialize. Supports alpha channel, rgba, hsla, hsva and more!
- [ZMYaro/paintz](https://github.com/ZMYaro/paintz)
  A simple drawing app that runs in a web browser, designed to be an MS Paint substitute for Chromebooks and other Chrome OS devices.  PaintZ is free, but please consider supporting development at https://ko-fi.com/ZMYaro.
- [blackdotsh/getIPIntel](https://github.com/blackdotsh/getIPIntel)
  IP Intelligence is a free Proxy VPN TOR and Bad IP detection tool to prevent Fraud, stolen content, and malicious users. Block proxies, VPN connections, web host IPs, TOR IPs, and compromised systems with a simple API. GeoIP lookup available.
- [chrislusf/seaweedfs](https://github.com/chrislusf/seaweedfs)
  SeaweedFS is a simple and highly scalable distributed file system. There are two objectives:  to store billions of files! to serve the files fast! SeaweedFS implements an object store with O(1) disk seek and an optional Filer with POSIX interface, supporting S3 API, Rack-Aware Erasure Coding for warm storage, FUSE mount, Hadoop compatible, WebDAV.
- [1j01/simple-console](https://github.com/1j01/simple-console)
  Add an elegant command-line interface to any page
