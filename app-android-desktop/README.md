# Repositories defined by: app, android, desktop

also defined by the following keywords: built, java, mac, macos, textile, privacy, reddit

- [bonustrack/steemconnect](https://github.com/bonustrack/steemconnect)
  Signer app for Steem
- [lbryio/lbry-android](https://github.com/lbryio/lbry-android)
  The LBRY Android app
- [saket/Dank](https://github.com/saket/Dank)
  Here lies the greatest Reddit app that was never released
- [schachmat/wego](https://github.com/schachmat/wego)
  weather app for the terminal
- [appacademy/welcome-to-open](https://github.com/appacademy/welcome-to-open)
  Overview of App Academy Open
- [BowenYin/harker-bell](https://github.com/BowenYin/harker-bell)
  Bell schedule app built for the future of the web.
- [aonez/timer-app](https://github.com/aonez/timer-app)
  A simple Timer app for Mac
- [michaelvillar/timer-app](https://github.com/michaelvillar/timer-app)
  A simple Timer app for Mac
- [Zeqiang-Lai/Timer-APP](https://github.com/Zeqiang-Lai/Timer-APP)
  A Simple Stopwatch App for macOS.
- [guardianproject/CameraV](https://github.com/guardianproject/CameraV)
  CameraV: InformaCam Default Android App
- [beakerbrowser/beaker-bookmarks-app](https://github.com/beakerbrowser/beaker-bookmarks-app)
  The Beaker "Bookmarks" app
- [DaylightingSociety/WhereAreTheEyes](https://github.com/DaylightingSociety/WhereAreTheEyes)
  Surveillance Detection and Mapping App
- [guardianproject/ObscuraCam](https://github.com/guardianproject/ObscuraCam)
  Photo and Video Filtering App for Privacy
- [chrisknepper/android-messages-desktop](https://github.com/chrisknepper/android-messages-desktop)
  Android Messages as a Cross-platform Desktop App
- [trezp/flashcards-vue](https://github.com/trezp/flashcards-vue)
  A flashcard app in Vue.js
- [39aldo39/DecSyncCC](https://github.com/39aldo39/DecSyncCC)
  Android app to sync contacts and calendars without a server using DecSync
- [tobykurien/WebApps](https://github.com/tobykurien/WebApps)
  Android app to provide sandboxed (private) browsing of webapps
- [webrecorder/webrecorder-desktop](https://github.com/webrecorder/webrecorder-desktop)
  Webrecorder Desktop App!
- [snwolak/steemblr](https://github.com/snwolak/steemblr)
  Microblogging web app powered by steem blockchain.
- [ipfs-shipyard/discussify-browser-extension](https://github.com/ipfs-shipyard/discussify-browser-extension)
  Discussify's browser extension app
- [lightning-viz/lightning-app](https://github.com/lightning-viz/lightning-app)
  Assets for bundling standalone Electron app
- [termux/termux-app](https://github.com/termux/termux-app)
  Android terminal and Linux environment - app repository.
- [textileio/notes](https://github.com/textileio/notes)
  Textile Notes App
- [bradtraversy/bookmarker](https://github.com/bradtraversy/bookmarker)
  Website bookmarker app built with pure JavaScript
- [textileio/desktop](https://github.com/textileio/desktop)
  Official Textile Desktop Tray App
- [GooBox/file-share-desktop](https://github.com/GooBox/file-share-desktop)
  💻 Goobox file share desktop app
- [sindresorhus/meow](https://github.com/sindresorhus/meow)
  CLI app helper
- [xbrowsersync/app](https://github.com/xbrowsersync/app)
  xBrowserSync browser extensions / mobile app
- [himaratsu/HMRScrollFeedView](https://github.com/himaratsu/HMRScrollFeedView)
  HRMScrollFeedView is library for realizing UI like SmartNews App.
- [nisrulz/app-privacy-policy-generator](https://github.com/nisrulz/app-privacy-policy-generator)
  A simple web app to generate a generic privacy policy for your Android/iOS apps
- [M66B/FairEmail](https://github.com/M66B/FairEmail)
  Open source, privacy friendly email app for Android
- [Kilian/fromscratch](https://github.com/Kilian/fromscratch)
  Autosaving Scratchpad. A simple but smart note-taking app
- [Zettelkasten-Method/The-Archive-Themes](https://github.com/Zettelkasten-Method/The-Archive-Themes)
  The Archive userland for app themes
- [nvbn/thefuck](https://github.com/nvbn/thefuck)
  Magnificent app which corrects your previous console command.
- [johnste/finicky](https://github.com/johnste/finicky)
  A macOS app for customizing which browser to start
- [FreeTubeApp/FreeTube](https://github.com/FreeTubeApp/FreeTube)
  An Open Source YouTube app for privacy
- [beakerbrowser/dat-photos-app](https://github.com/beakerbrowser/dat-photos-app)
  A peer-to-peer photos app
- [airsecure/airsecure](https://github.com/airsecure/airsecure)
  Decentralized 2 factor authentication app built on Textile & IPFS
- [guardianproject/PixelKnot](https://github.com/guardianproject/PixelKnot)
  Image stego app using the F5 algorithm
- [vladimiry/ElectronMail](https://github.com/vladimiry/ElectronMail)
  Unofficial desktop app for ProtonMail and Tutanota E2EE email providers
- [sindresorhus/create-dmg](https://github.com/sindresorhus/create-dmg)
  Create a good-looking DMG for your macOS app in seconds
- [FacettsOpen/exodify](https://github.com/FacettsOpen/exodify)
  Wonder how much an app is tracking you? Now you can see it from within the Google Play web interface thanks to ExodusPrivacy.
- [Soundnode/soundnode-app](https://github.com/Soundnode/soundnode-app)
  Soundnode App is the Soundcloud for desktop. Built with Electron, Angular.js and Soundcloud API.
- [textileio/notes-desktop](https://github.com/textileio/notes-desktop)
  Example app built with reactjs and @textile/js-http-client
- [1j01/translate-great](https://github.com/1j01/translate-great)
  Translate web app UI inline (just an experiment! less than a day's work!)
- [beakerbrowser/fritter](https://github.com/beakerbrowser/fritter)
  A peer-to-peer social feed app. (proof of concept)
- [thompsonate/Shifty](https://github.com/thompsonate/Shifty)
  ☀️ A macOS menu bar app that gives you more control over Night Shift.
- [textileio/node-chat](https://github.com/textileio/node-chat)
  A simple cli chat app using Textile
- [zboxfs/zbox](https://github.com/zboxfs/zbox)
  Zero-details, privacy-focused in-app file system.
- [pybites/pytip](https://github.com/pybites/pytip)
  Building a Simple Web App With Bottle, SQLAlchemy, and the Twitter API
- [berbaquero/reeddit](https://github.com/berbaquero/reeddit)
  Minimal, elastic Reddit reader web-app client
- [JP1016/Paper](https://github.com/JP1016/Paper)
  🚀A "no-cloud" note taking app with "networkless" sharing. https://paperapp.now.sh
- [filebrowser/filebrowser](https://github.com/filebrowser/filebrowser)
  📂 Web File Browser which can be used as a middleware or standalone app.
- [dessant/move-issues](https://github.com/dessant/move-issues)
  :robot: GitHub App that moves issues between repositories
- [blockstack-radiks/banter](https://github.com/blockstack-radiks/banter)
  A simple Twitter-style app to demo Radiks
- [mkideal/cli](https://github.com/mkideal/cli)
  CLI - A package for building command line app with go
- [pd4d10/git-touch](https://github.com/pd4d10/git-touch)
  Open source GitHub App built with Flutter
- [ndom91/youtube-playlists](https://github.com/ndom91/youtube-playlists)
  🎥 React app for creating on-the-fly YouTube playlists.
- [RobinLinus/snapdrop](https://github.com/RobinLinus/snapdrop)
  A Progressive Web App for local file sharing 
- [vega/vega-desktop](https://github.com/vega/vega-desktop)
  App for viewing visualizations created in Vega or Vega-lite
- [Permaweb/permaweb](https://github.com/Permaweb/permaweb)
  Source code for Permaweb, the writing app for busy people.
- [notable/notable](https://github.com/notable/notable)
  The Markdown-based note-taking app that doesn't suck.
- [mileswd/mac2imgur](https://github.com/mileswd/mac2imgur)
  ⬆ A simple Mac app designed to make uploading images and screenshots to Imgur quick and effortless.
- [devinhalladay/differance.network](https://github.com/devinhalladay/differance.network)
  An Are.na app for making unexpected connections between disparate pieces of content.
- [arturbien/React95](https://github.com/arturbien/React95)
  🌈🕹  Refreshed Windows 95 style UI components for your React app
- [hrik2001/macuninstaller](https://github.com/hrik2001/macuninstaller)
  macuninstaller is a lightweight uninstaller for mac. It makes making uninstallation of app easy and flexible :)
- [ericyd/gdrive-copy](https://github.com/ericyd/gdrive-copy)
  Web app to copy a Google Drive folder
- [julienvincent/Mubert](https://github.com/julienvincent/Mubert)
  An iOS mobile app that steams generated music from mubert.com servers.
- [giekaton/vipassana-app](https://github.com/giekaton/vipassana-app)
  Gamified Meditation PWA
- [IMGIITRoorkee/django-filemanager](https://github.com/IMGIITRoorkee/django-filemanager)
  A simple Django app to browse files on server
- [termux/termux-styling](https://github.com/termux/termux-styling)
  Termux add-on app for customizing the terminal font and color theme.
- [standardnotes/web](https://github.com/standardnotes/web)
  A free, open-source, and completely encrypted notes app. | https://standardnotes.org
- [beakerbrowser/dat-nexus-app](https://github.com/beakerbrowser/dat-nexus-app)
  A social feeds and profiles application
- [Rajkumrdusad/Tool-X](https://github.com/Rajkumrdusad/Tool-X)
  Tool-X is a kali linux hacking Tool installer. Tool-X developed for termux and other android terminals. using Tool-X you can install almost 263 hacking tools in termux app and other linux based distributions.
- [arso-project/archipel](https://github.com/arso-project/archipel)
  An app to share archives of files and folders in a peer to peer network
- [Zeqiang-Lai/IconHide-App](https://github.com/Zeqiang-Lai/IconHide-App)
  A Simple Application for Hiding Desktop Icons on Macos
- [arthurbergmz/webpack-pwa-manifest](https://github.com/arthurbergmz/webpack-pwa-manifest)
  Progressive Web App Manifest Generator for Webpack, with auto icon resizing and fingerprinting support.
- [Graviton-Code-Editor/Graviton-App](https://github.com/Graviton-Code-Editor/Graviton-App)
  A Code Editor
- [beakerbrowser/beaker-sidebar-app](https://github.com/beakerbrowser/beaker-sidebar-app)
  Web application that drives Beaker's sidebar view
- [kerrbrittany9/tamagotchi-react](https://github.com/kerrbrittany9/tamagotchi-react)
  web app using moment and in React.js to feed, play and rest a little creature you've created
- [mblaul/todo-challenge-react](https://github.com/mblaul/todo-challenge-react)
  A fun challenge with friends to create a todo app.
- [jamiew/heroku-static-site](https://github.com/jamiew/heroku-static-site)
  A basic Ruby/Rack app for publishing a static HTML/CSS/javascript website on Heroku (for free)
- [dmllr/IdealMedia](https://github.com/dmllr/IdealMedia)
  Awesome app to listen music and audiobooks on the device and online at vk.com. Search, download, set as ringtone, sort by albums, authors, folder. Powerful equalizer.
- [jariz/Noti](https://github.com/jariz/Noti)
  Receive Android notifications on your mac. (w/PushBullet) ⛺
- [nteract/create-nteract-app](https://github.com/nteract/create-nteract-app)
  :zap: Create an nteractive application with zero configuration
- [moezbhatti/qksms](https://github.com/moezbhatti/qksms)
  The most beautiful SMS messenger for Android
- [OnlyInAmerica/GlanceReader](https://github.com/OnlyInAmerica/GlanceReader)
  RSVP reader for Android.
- [ritiek/meganz-android](https://github.com/ritiek/meganz-android)
  MEGA Android Client
- [OnsenUI/OnsenUI](https://github.com/OnsenUI/OnsenUI)
  Mobile app development framework and SDK using HTML5 and JavaScript. Create beautiful and performant cross-platform mobile apps. Based on Web Components, and provides bindings for Angular 1, 2, React and Vue.js.
- [ZMYaro/paintz](https://github.com/ZMYaro/paintz)
  A simple drawing app that runs in a web browser, designed to be an MS Paint substitute for Chromebooks and other Chrome OS devices.  PaintZ is free, but please consider supporting development at https://ko-fi.com/ZMYaro.
- [SouravJohar/python-app-with-electron-gui](https://github.com/SouravJohar/python-app-with-electron-gui)
  A better way to make GUIs for your python apps
- [apsun/uSticker](https://github.com/apsun/uSticker)
  Custom stickers for Gboard on Android.
- [prithvibhola/Palette](https://github.com/prithvibhola/Palette)
  Android application to get the #hexcode and rgb() values for any image
- [39aldo39/spaRSS-DecSync](https://github.com/39aldo39/spaRSS-DecSync)
  Android application to sync RSS without a server using DecSync
- [beakerbrowser/dat-rssreader-app](https://github.com/beakerbrowser/dat-rssreader-app)
  Read RSS feeds for blogs hosted over dat://
- [atomotic/uv-app-starter](https://github.com/atomotic/uv-app-starter)
  Create a IIIF-enabled website using universalviewer.io and host it for free on Github Pages
- [prithvibhola/MVVMPagination](https://github.com/prithvibhola/MVVMPagination)
  Android Kotlin MVVM Pagination example
- [crazyhitty/Munch](https://github.com/crazyhitty/Munch)
  [Deprecated] A minimalistic Rss Reader for Android devices
- [dmytrodanylyk/folding-plugin](https://github.com/dmytrodanylyk/folding-plugin)
  Android File Grouping Plugin
- [gokulkrishh/demo-progressive-web-app](https://github.com/gokulkrishh/demo-progressive-web-app)
  🎉 A demo for progressive web application with features like offline, push notifications, background sync etc,
- [dvoiss/android-geocities-theme](https://github.com/dvoiss/android-geocities-theme)
  Bringing the best of the web to Android!
- [prithvibhola/MVVMStarter](https://github.com/prithvibhola/MVVMStarter)
  Android Kotlin MVVM Starter
- [thehackingsage/hacktronian](https://github.com/thehackingsage/hacktronian)
  All in One Hacking Tool for Linux & Android
- [frakbot/BlinkTextView](https://github.com/frakbot/BlinkTextView)
  Remember the '90s? Geocities? Blinking text? Dat. On Android.
- [florinpop17/app-ideas](https://github.com/florinpop17/app-ideas)
  A Collection of application ideas which can be used to improve your coding skills.
- [sathishmscict/BubbleSeekBar](https://github.com/sathishmscict/BubbleSeekBar)
  A beautiful Android custom seekbar, which has a bubble view with progress appearing upon when seeking SeekBar
- [ipfs-shipyard/ipfs-desktop](https://github.com/ipfs-shipyard/ipfs-desktop)
  An unobtrusive and user-friendly desktop application for IPFS on Windows, Mac and Linux. 
- [openid/AppAuth-Android](https://github.com/openid/AppAuth-Android)
  Android client SDK for communicating with OAuth 2.0 and OpenID Connect providers.
- [Arish-Shah/win95](https://github.com/Arish-Shah/win95)
  Windows 95 desktop built using React
- [florent37/RxRetroJsoup](https://github.com/florent37/RxRetroJsoup)
  A simple API-like from html website (scrapper) for Android, RxJava2 ready !
- [cassidoo/desktop-lava](https://github.com/cassidoo/desktop-lava)
  A lava lamp for your desktop built with CSS and Electron.
- [termux/termux-packages](https://github.com/termux/termux-packages)
  Android terminal and Linux environment - packages repository.
- [webrecorder/webrecorder-player](https://github.com/webrecorder/webrecorder-player)
  Webrecorder Player for Desktop (OSX/Windows/Linux). (Built with Electron + Webrecorder)
- [Utazukin/Ichaival](https://github.com/Utazukin/Ichaival)
  Android client for the LANraragi manga/doujinshi web manager.
- [DmitriyZaitsev/RadarChartView](https://github.com/DmitriyZaitsev/RadarChartView)
  Android view (widget) for rendering radial diagrams
- [zeit/now-desktop](https://github.com/zeit/now-desktop)
  An example of building a desktop application using the Now API
- [openintents/calendar-sync](https://github.com/openintents/calendar-sync)
  Android Sync for OI Calendar, the Private, Encrypted Calendar in Your Cloud
- [Pinperepette/chromeos-apk](https://github.com/Pinperepette/chromeos-apk)
  Run Android APKs in Chrome OS OR Chrome in OS X, Linux and Windows.
- [TeamAmaze/AmazeFileManager](https://github.com/TeamAmaze/AmazeFileManager)
  Material design file manager for Android
- [inloop/svg2android](https://github.com/inloop/svg2android)
  SVG to Android VectorDrawable XML resource file
- [szwacz/sputnik](https://github.com/szwacz/sputnik)
  Desktop RSS Reader
- [jaredrummler/HtmlBuilder](https://github.com/jaredrummler/HtmlBuilder)
  Build valid HTML for Android TextView
- [ritiek/nativefier-shortcut](https://github.com/ritiek/nativefier-shortcut)
  [WIP] Menu and desktop shortcuts for apps built with nativefier
- [nikolamilosevic86/owasp-seraphimdroid](https://github.com/nikolamilosevic86/owasp-seraphimdroid)
  OWASP Seraphimdroid is an open source project with aim to create, as a community, an open platform for education and protection of Android users against privacy and security threats.
- [ionic-team/capacitor](https://github.com/ionic-team/capacitor)
  Build cross-platform Native Progressive Web Apps for iOS, Android, and the web ⚡️
- [koreader/koreader](https://github.com/koreader/koreader)
  An ebook reader application supporting PDF, DjVu, EPUB, FB2 and many more formats, running on Cervantes, Kindle, Kobo, PocketBook and Android devices
- [gsantner/markor](https://github.com/gsantner/markor)
  Text editor - Notes & ToDo (for Android) - Markdown, todo.txt, plaintext, math, ..
- [SDRausty/buildAPKs](https://github.com/SDRausty/buildAPKs)
  Really quickly build APKs on device (smartphone and tablet) in Termux📲  See https://buildapks.github.io/docsBuildAPKs/setup to start building apps on  Amazon Fire, Android and Chromebook. 
