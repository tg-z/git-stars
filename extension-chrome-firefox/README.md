# Repositories defined by: extension, chrome, firefox

also defined by the following keywords: bookmarks, bookmarklet, page, safari, opera, create, features

- [broskoski/chrome-extension](https://github.com/broskoski/chrome-extension)
- [hxrts/chrome-extension](https://github.com/hxrts/chrome-extension)
- [aredotna/chrome-extension](https://github.com/aredotna/chrome-extension)
- [dutiyesh/chrome-extension-cli](https://github.com/dutiyesh/chrome-extension-cli)
  🚀 The CLI for your next Chrome Extension
- [legierski/bookmarklet-to-extension](https://github.com/legierski/bookmarklet-to-extension)
  Bookmarklet to Chrome Extension
- [panicsteve/cloud-to-butt](https://github.com/panicsteve/cloud-to-butt)
  Chrome extension that replaces occurrences of 'the cloud' with 'my butt'
- [ahmed-musallam/chrome-bookmarklet-manager](https://github.com/ahmed-musallam/chrome-bookmarklet-manager)
  A chrome extension to manage bookmarklets!
- [altryne/extensionizr](https://github.com/altryne/extensionizr)
  Extensionizr! Create a chrome extension in 15 seconds!
- [iamadamdev/bypass-paywalls-chrome](https://github.com/iamadamdev/bypass-paywalls-chrome)
  Bypass Paywalls for Chrome
- [textileio/duckduckgo-privacy-extension](https://github.com/textileio/duckduckgo-privacy-extension)
  DuckDuckGo Privacy Essentials browser extension for Firefox, Chrome, Safari.
- [duckduckgo/duckduckgo-privacy-extension](https://github.com/duckduckgo/duckduckgo-privacy-extension)
  DuckDuckGo Privacy Essentials browser extension for Firefox, Chrome, Safari.
- [m4rk3r/PARALLELOWEB](https://github.com/m4rk3r/PARALLELOWEB)
  a chrome extension to network the cursors of every user on the current page
- [int128/bntp](https://github.com/int128/bntp)
  A chrome extension providing Bookmarks in the New Tab Page
- [andryou/scriptsafe](https://github.com/andryou/scriptsafe)
  a browser extension to bring security and privacy to chrome, firefox, and opera
- [bitwarden/browser](https://github.com/bitwarden/browser)
  The browser extension vault (Chrome, Firefox, Opera, Edge, Safari, & more).
- [HaNdTriX/generator-chrome-extension-kickstart](https://github.com/HaNdTriX/generator-chrome-extension-kickstart)
  Scaffold out a Web Extension http://yeoman.io
- [machawk1/warcreate](https://github.com/machawk1/warcreate)
  Chrome extension to "Create WARC files from any webpage"
- [ptgamr/upnext](https://github.com/ptgamr/upnext)
  Chrome Extension for streaming music from SoundCloud & YouTube
- [BackMarket/github-mermaid-extension](https://github.com/BackMarket/github-mermaid-extension)
  A browser extension for Chrome, Opera & Firefox that adds Mermaid language support to Github
- [WebMemex/webmemex-extension](https://github.com/WebMemex/webmemex-extension)
  📇 Your digital memory extension, as a browser extension
- [adam-p/markdown-here](https://github.com/adam-p/markdown-here)
  Google Chrome, Firefox, and Thunderbird extension that lets you write email in Markdown and render it before sending.
- [chintown/monocle](https://github.com/chintown/monocle)
  A chrome extension which provides a better scroll bar with whole-page-snapshot preview
- [gildas-lormeau/Scrapbook-for-SingleFile](https://github.com/gildas-lormeau/Scrapbook-for-SingleFile)
  PageArchiver (previously called "Scrapbook for SingleFile") is a Chrome extension that helps to archive pages for offline reading
- [ipfs-shipyard/discussify-browser-extension](https://github.com/ipfs-shipyard/discussify-browser-extension)
  Discussify's browser extension app
- [freethenation/DFPM](https://github.com/freethenation/DFPM)
  DFPM is a browser extension for detecting browser fingerprinting.
- [get-set-fetch/extension](https://github.com/get-set-fetch/extension)
  web scraping extension
- [dantaki/vapeplot](https://github.com/dantaki/vapeplot)
  matplotlib extension for vaporwave aesthetics
- [leggett/simplify](https://github.com/leggett/simplify)
  Browser extension to simplify Gmail's interface
- [AInoob/NooBoss](https://github.com/AInoob/NooBoss)
  NooBoss is an extension that handles your extensions like a boss!
- [salcido/discogs-enhancer](https://github.com/salcido/discogs-enhancer)
  100% vanilla JS Chrome extension that adds useful features and functionality to Discogs.com
- [new-xkit/XKit](https://github.com/new-xkit/XKit)
  Fork of XKit, the Extension framework for Tumblr
- [gildas-lormeau/SingleFile](https://github.com/gildas-lormeau/SingleFile)
  Web Extension for Firefox/Chrome and CLI tool to save a faithful copy of a complete web page as a single HTML file
- [Treora/precise-links](https://github.com/Treora/precise-links)
  Browser extension to support Web Annotation Selectors in URIs
- [thomasp85/shinyFiles](https://github.com/thomasp85/shinyFiles)
  A shiny extension for server side file access
- [adamschwartz/chrome-tabs](https://github.com/adamschwartz/chrome-tabs)
  Chrome-style tabs in HTML/CSS.
- [luetage/extension_control](https://github.com/luetage/extension_control)
  Elementary extension manager. Compact design, no superfluous features.
- [cnwangjie/better-onetab](https://github.com/cnwangjie/better-onetab)
  :bookmark_tabs: A better OneTab for Chrome (Temporarily removed from firefox)
- [oldweb-today/browser-chrome](https://github.com/oldweb-today/browser-chrome)
  Chrome containerized browser for Webrecorder
- [EmailThis/extension-boilerplate](https://github.com/EmailThis/extension-boilerplate)
  ⚡️ A template for building cross browser extensions for Chrome, Opera & Firefox.
- [gabrielgodoy/github-stars-manager](https://github.com/gabrielgodoy/github-stars-manager)
  Chrome extension that allows you to manage your Github stars with tags, and to create a bookmark folder with all your stars organized by the tags you created
- [calibr/node-bookmarks-parser](https://github.com/calibr/node-bookmarks-parser)
  Parses Firefox/Chrome HTML bookmarks files
- [joshuasoehn/Are.na-Safari-Extension](https://github.com/joshuasoehn/Are.na-Safari-Extension)
  A native safari extension for Are.na
- [eight04/linkify-plus-plus](https://github.com/eight04/linkify-plus-plus)
  A userscript/extension which can linkify almost everything. Based on Linkify Plus.
- [lvxianchao/the-fucking-github](https://github.com/lvxianchao/the-fucking-github)
  A Chrome extension for Github. View starred repositories, organizing stars, searching stars and searching repositories online for Github。
- [alefragnani/vscode-bookmarks](https://github.com/alefragnani/vscode-bookmarks)
  Bookmarks Extension for Visual Studio Code
- [ddavison/chrome-bookmarklet-ide](https://github.com/ddavison/chrome-bookmarklet-ide)
  Chrome Bookmarklet IDE
- [chylex/BetterTweetDeck](https://github.com/chylex/BetterTweetDeck)
  A browser extension to improve TweetDeck with a lot of features
- [git-lfs/git-lfs](https://github.com/git-lfs/git-lfs)
  Git extension for versioning large files
- [beakerbrowser/dat-session-data-ext-msg](https://github.com/beakerbrowser/dat-session-data-ext-msg)
  Methods for DEP-0006: Session Data (Extension Message)
- [alefragnani/vscode-numbered-bookmarks](https://github.com/alefragnani/vscode-numbered-bookmarks)
  Numbered Bookmarks Extension for Visual Studio Code
- [beakerbrowser/dat-ephemeral-ext-msg](https://github.com/beakerbrowser/dat-ephemeral-ext-msg)
  Methods for DEP-0000: Ephemeral Message (Extension Message)
- [tjeffree/Brackets-ExtensionHighlight](https://github.com/tjeffree/Brackets-ExtensionHighlight)
  File extension colours in Brackets' file tree.
- [darkarp/chromepass](https://github.com/darkarp/chromepass)
  Chromepass - Hacking Chrome Saved Passwords
- [EFForg/privacybadger](https://github.com/EFForg/privacybadger)
  Privacy Badger is a browser extension that automatically learns to block invisible trackers.
- [ryanmcdermott/starmark](https://github.com/ryanmcdermott/starmark)
  :octocat: Turn your GitHub stars into Chrome bookmarks
- [Pinperepette/chromeos-apk](https://github.com/Pinperepette/chromeos-apk)
  Run Android APKs in Chrome OS OR Chrome in OS X, Linux and Windows.
- [mikecrittenden/shortkeys](https://github.com/mikecrittenden/shortkeys)
  A browser extension for custom keyboard shortcuts 
- [JasonHinds13/PyFiling](https://github.com/JasonHinds13/PyFiling)
  Python script that organizes files in a folder or directory according to file type/extension.
- [westy92/html-pdf-chrome](https://github.com/westy92/html-pdf-chrome)
  HTML to PDF converter via Chrome/Chromium
- [danny0838/webscrapbook](https://github.com/danny0838/webscrapbook)
  A browser extension that captures the web page faithfully with highly customizable configurations. This project inherits from ScrapBook X.
- [fivethirtyeight/react-color](https://github.com/fivethirtyeight/react-color)
  :art: Color Pickers from Sketch, Photoshop, Chrome & more
- [WorldBrain/Memex](https://github.com/WorldBrain/Memex)
  Browser Extension to full-text search your browsing history & bookmarks.
- [andrewbrey/netflix-tweaked](https://github.com/andrewbrey/netflix-tweaked)
  A web extension to tweak the Netflix home screen, preventing auto-play trailers and moving your lists to the top.
- [d4t4x/data-selfie](https://github.com/d4t4x/data-selfie)
  Data Selfie - a browser extension to track yourself on Facebook and analyze your data. 
- [mi-g/weh](https://github.com/mi-g/weh)
  Toolkit to save time when developing a browser add-on for Firefox, Chrome, Edge, Opera and Vivaldi
- [atomotic/webrecorder-chrome-extension](https://github.com/atomotic/webrecorder-chrome-extension)
  record current active tab on webrecorder.io
- [sindresorhus/refined-twitter](https://github.com/sindresorhus/refined-twitter)
  Browser extension that simplifies the Twitter interface and adds useful features
- [saschpe/android-customtabs](https://github.com/saschpe/android-customtabs)
  Chrome CustomTabs for Android demystified. Simplifies development and provides higher level classes including fallback in case Chrome isn't available on device.
- [EFForg/https-everywhere](https://github.com/EFForg/https-everywhere)
  A browser extension that encrypts your communications with many websites that offer HTTPS but still allow unencrypted connections.
- [N0taN3rd/Squidwarc](https://github.com/N0taN3rd/Squidwarc)
  Squidwarc is a high fidelity, user scriptable, archival crawler that uses Chrome or Chromium with or without a head
- [karthikeyankc/HistoryAnalyzer](https://github.com/karthikeyankc/HistoryAnalyzer)
  A Python script to grab some tasty data from your Chrome's history and analyze it.
- [ZachSaucier/Just-Read](https://github.com/ZachSaucier/Just-Read)
  A customizable read mode web extension.
- [fate0/pychrome](https://github.com/fate0/pychrome)
  A Python Package for the Google Chrome Dev Protocol [threading base]
- [xxhomey19/github-file-icon](https://github.com/xxhomey19/github-file-icon)
  🌈 🗂 A browser extension which gives different filetypes different icons to GitHub, GitLab, gitea and gogs.
- [ipfs-shipyard/ipfs-companion](https://github.com/ipfs-shipyard/ipfs-companion)
  Browser extension that simplifies access to IPFS resources
- [sindresorhus/refined-github](https://github.com/sindresorhus/refined-github)
  Browser extension that simplifies the GitHub interface and adds useful features
- [gildas-lormeau/SingleFileZ](https://github.com/gildas-lormeau/SingleFileZ)
  SingleFileZ is a Web Extension for saving web pages as self-extracting HTML/ZIP hybrid files.
- [liyasthomas/mnmlurl-extension](https://github.com/liyasthomas/mnmlurl-extension)
  💁 Browser extension for Minimal URL - Modern URL shortener with support for custom alias & can be hosted even in GitHub pages
- [RandomFractals/vscode-data-preview](https://github.com/RandomFractals/vscode-data-preview)
  Data Preview 🈸 extension for importing 📤 viewing 🔎 slicing 🔪 dicing 🎲  charting 📊 & exporting 📥 large JSON array/config, YAML, Apache Arrow, Avro & Excel data files
- [csu/export-saved-reddit](https://github.com/csu/export-saved-reddit)
  Export saved Reddit posts into a HTML file for import into Google Chrome.
- [victordomingos/Count-files](https://github.com/victordomingos/Count-files)
  A CLI utility written in Python to help you count files, grouped by extension, in a directory. By default, it will count files recursively in current working directory and all of its subdirectories, and will display a table showing the frequency for each file extension (e.g.: .txt, .py, .html, .css) and the total number of files found.
- [doctrine/search](https://github.com/doctrine/search)
  Generic Search extension for indexing and querying ODM/ORM objects with different text-search engine implementations
- [ZMYaro/paintz](https://github.com/ZMYaro/paintz)
  A simple drawing app that runs in a web browser, designed to be an MS Paint substitute for Chromebooks and other Chrome OS devices.  PaintZ is free, but please consider supporting development at https://ko-fi.com/ZMYaro.
