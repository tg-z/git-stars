# Repositories defined by: text, editor, code

also defined by the following keywords: based, markdown, image, macos, atom, document, rich

- [slowkow/ggrepel](https://github.com/slowkow/ggrepel)
  :round_pushpin: Repel overlapping text labels away from each other.
- [1j01/whitebread](https://github.com/1j01/whitebread)
  🍞🌸 A surreal text adventure (early stages)
- [zyedidia/micro](https://github.com/zyedidia/micro)
  A modern and intuitive terminal-based text editor
- [atom/atom](https://github.com/atom/atom)
  :atom: The hackable text editor
- [0xPrateek/ci_edit](https://github.com/0xPrateek/ci_edit)
  A terminal text editor with mouse support and ctrl+Q to quit.
- [CodingFriends/Tincta](https://github.com/CodingFriends/Tincta)
  The famous and much-loved text editor Tincta
- [vinta/pangu.js](https://github.com/vinta/pangu.js)
  Paranoid text spacing in JavaScript
- [basecamp/trix](https://github.com/basecamp/trix)
  A rich text editor for everyday writing
- [nteract/hydrogen](https://github.com/nteract/hydrogen)
  :atom: Run code interactively, inspect data, and plot. All the power of Jupyter kernels, inside your favorite text editor.
- [macvim-dev/macvim](https://github.com/macvim-dev/macvim)
  Vim - the text editor - for Mac OS X
- [FirebaseExtended/firepad](https://github.com/FirebaseExtended/firepad)
  Collaborative Text Editor Powered by Firebase
- [scrumpy/tiptap](https://github.com/scrumpy/tiptap)
  A rich-text editor for Vue.js
- [Treora/indexeddb-full-text-search-comparison](https://github.com/Treora/indexeddb-full-text-search-comparison)
- [vigna/ne](https://github.com/vigna/ne)
  ne, the nice editor
- [subethaedit/SubEthaEdit](https://github.com/subethaedit/SubEthaEdit)
  General purpose plain text editor for macOS. Widely known for its live collaboration feature.
- [Pin-Ni/textClassifier](https://github.com/Pin-Ni/textClassifier)
  Text classifier for Hierarchical Attention Networks for Document Classification 
- [textmate/textmate](https://github.com/textmate/textmate)
  TextMate is a graphical text editor for macOS 10.10 or later
- [Graviton-Code-Editor/Graviton-App](https://github.com/Graviton-Code-Editor/Graviton-App)
  A Code Editor
- [icewind1991/files_markdown](https://github.com/icewind1991/files_markdown)
  Nextcloud markdown editor
- [gsantner/markor](https://github.com/gsantner/markor)
  Text editor - Notes & ToDo (for Android) - Markdown, todo.txt, plaintext, math, ..
- [deanmalmgren/textract](https://github.com/deanmalmgren/textract)
  extract text from any document. no muss. no fuss.
- [s-p-k/bm](https://github.com/s-p-k/bm)
  A simple perl script for text bookmarks
- [fourlastor/dante](https://github.com/fourlastor/dante)
  A sane rich text parsing and styling library.
- [Zettlr/Zettlr](https://github.com/Zettlr/Zettlr)
  A powerful markdown editor.
- [EvilSourcerer/CSStudio](https://github.com/EvilSourcerer/CSStudio)
  Boundless Website Editor
- [untakenstupidnick/nbsdgames](https://github.com/untakenstupidnick/nbsdgames)
  The new bsdgames:  A package of 11 text-based games
- [plataformatec/nimble_parsec](https://github.com/plataformatec/nimble_parsec)
  A simple and fast library for text-based parser combinators
- [attardi/wikiextractor](https://github.com/attardi/wikiextractor)
  A tool for extracting plain text from Wikipedia dumps
- [ansilove/QLAnsilove](https://github.com/ansilove/QLAnsilove)
  macOS QuickLook for ANSi, ASCii, and other Text-mode Art
- [1j01/she-has-what](https://github.com/1j01/she-has-what)
  ⚪ Homestuck Doc Scratch text effect GIF generator
- [dbohdan/structured-text-tools](https://github.com/dbohdan/structured-text-tools)
  A list of command line tools for manipulating structured text data
- [photopea/photopea](https://github.com/photopea/photopea)
  Photopea is online image editor
- [jmwerner/TXT2PYNB](https://github.com/jmwerner/TXT2PYNB)
  Converts text script to iPython notebook
- [imranghory/urlextractor](https://github.com/imranghory/urlextractor)
  Python URL Extractor - extract URLs from text
- [viliusle/miniPaint](https://github.com/viliusle/miniPaint)
  online image editor
- [toshi-search/Toshi](https://github.com/toshi-search/Toshi)
  A full-text search engine in rust
- [Pinperepette/newspaper](https://github.com/Pinperepette/newspaper)
  News, full-text, and article metadata extraction in python 2.6 - 3.4. 
- [kazzkiq/CodeFlask](https://github.com/kazzkiq/CodeFlask)
  A micro code-editor for awesome web pages.
- [frakbot/BlinkTextView](https://github.com/frakbot/BlinkTextView)
  Remember the '90s? Geocities? Blinking text? Dat. On Android.
- [retext-project/retext](https://github.com/retext-project/retext)
  ReText: Simple but powerful editor for Markdown and reStructuredText
- [nhviet1009/ASCII-generator](https://github.com/nhviet1009/ASCII-generator)
  ASCII generator (image to text, image to image, video to video)
- [idank/explainshell](https://github.com/idank/explainshell)
  match command-line arguments to their help text
- [bengarrett/RetroTxt](https://github.com/bengarrett/RetroTxt)
  RetroTxt is the WebExtension that turns ANSI, ASCII, NFO text into in-browser HTML
- [Treora/quoteurl](https://github.com/Treora/quoteurl)
  Allow URLs to point to any text piece in a document
- [BonsaiDen/twitter-text-python](https://github.com/BonsaiDen/twitter-text-python)
  Twitter text processing library (auto linking and extraction of usernames, lists and hashtags). Based on the Java implementation by Matt Sanford
- [lukakerr/Pine](https://github.com/lukakerr/Pine)
  A modern, native macOS markdown editor
- [MagicStack/MagicPython](https://github.com/MagicStack/MagicPython)
  Cutting edge Python syntax highlighter for Sublime Text, Atom and Visual Studio Code. Used by GitHub to highlight your Python code!
- [edmondburnett/twitter-text-python](https://github.com/edmondburnett/twitter-text-python)
  Twitter text processing library (auto linking and extraction of usernames, lists and hashtags).
- [browsh-org/browsh](https://github.com/browsh-org/browsh)
  A fully-modern text-based browser, rendering to TTY and browsers
- [hit9/img2txt](https://github.com/hit9/img2txt)
  Image to Ascii Text with color support, can output to html or ansi terminal.
- [andre-simon/highlight](https://github.com/andre-simon/highlight)
  Source code to formatted text converter 
- [sajari/docconv](https://github.com/sajari/docconv)
  Converts PDF, DOC, DOCX, XML, HTML, RTF, etc to plain text
- [jung-kurt/gofpdf](https://github.com/jung-kurt/gofpdf)
  A PDF document generator with high level support for text, drawing and images
- [Jeffreyrn/vue-simple-flowchart](https://github.com/Jeffreyrn/vue-simple-flowchart)
  flowchart editor for Vue.js
- [jakevdp/PythonDataScienceHandbook](https://github.com/jakevdp/PythonDataScienceHandbook)
  Python Data Science Handbook: full text in Jupyter Notebooks
- [kalcaddle/KodExplorer](https://github.com/kalcaddle/KodExplorer)
  A web based file manager,web IDE / browser based code editor
- [werk85/node-html-to-text](https://github.com/werk85/node-html-to-text)
  Advanced html to text converter
- [hishamhm/htop](https://github.com/hishamhm/htop)
  htop is an interactive text-mode process viewer for Unix systems. It aims to be a better 'top'.
- [Pinperepette/htop](https://github.com/Pinperepette/htop)
  htop is an interactive text-mode process viewer for Unix systems. It aims to be a better 'top'.
- [1j01/pesterchum](https://github.com/1j01/pesterchum)
  Pesterchum Chat Client, and also a Doc Scratch lightning text effect reaction GIF generator
- [SadConsole/SadConsole](https://github.com/SadConsole/SadConsole)
  A .NET ascii/ansi console engine written in C# for MonoGame and XNA. Create your own text roguelike (or other) games!
- [WorldBrain/Memex](https://github.com/WorldBrain/Memex)
  Browser Extension to full-text search your browsing history & bookmarks.
- [TryCatchHCF/Cloakify](https://github.com/TryCatchHCF/Cloakify)
  CloakifyFactory - Data Exfiltration & Infiltration In Plain Sight; Convert any filetype into list of everyday strings, using Text-Based Steganography; Evade DLP/MLS Devices, Defeat Data Whitelisting Controls, Social Engineering of Analysts, Evade AV Detection
- [AshveenBansal98/Python-Text-Search](https://github.com/AshveenBansal98/Python-Text-Search)
   A search engine for searching text/phrase in set of .txt files, based on inverted indexing and boyer moore algorithm. 
- [lipoja/URLExtract](https://github.com/lipoja/URLExtract)
  URLExtract is python class for collecting (extracting) URLs from given text based on locating TLD.
- [adobe/brackets](https://github.com/adobe/brackets)
  An open source code editor for the web, written in JavaScript, HTML and CSS.
- [SoapBox/linkifyjs](https://github.com/SoapBox/linkifyjs)
  Linkify is a zero-dependency JavaScript plugin for finding links in plain-text and converting them to HTML <a> tags.
- [mholt/PapaParse](https://github.com/mholt/PapaParse)
  Fast and powerful CSV (delimited text) parser that gracefully handles large files and malformed input
- [idyll-lang/idyll-studio](https://github.com/idyll-lang/idyll-studio)
  A graphical editor for creating Idyll documents.
- [psharanda/Atributika](https://github.com/psharanda/Atributika)
  Convert text with HTML tags, links, hashtags, mentions into NSAttributedString. Make them clickable with UILabel drop-in replacement.
- [adrienjoly/npm-pdfreader](https://github.com/adrienjoly/npm-pdfreader)
  🚜 Read text and parse tables from PDF files. Includes automatic column detection, and rule-based parsing.
- [microsoft/presidio](https://github.com/microsoft/presidio)
  Context aware, pluggable and customizable data protection and PII data anonymization service for text and images
- [umpox/TinyEditor](https://github.com/umpox/TinyEditor)
  A functional HTML/CSS/JS editor in less than 400 bytes
- [MrSaints/Morphext](https://github.com/MrSaints/Morphext)
  A simple, high-performance and cross-browser jQuery rotating / carousel plugin for text phrases powered by Animate.css.
- [1j01/skele2d](https://github.com/1j01/skele2d)
  ☠😎 A 2D game engine based around skeletal structures, with an in-game editor and animation support (pre-alpha)
- [fontforge/fontforge](https://github.com/fontforge/fontforge)
  Free (libre) font editor for Windows, Mac OS X and GNU+Linux
- [datasciencedojo/IntroToTextAnalyticsWithR](https://github.com/datasciencedojo/IntroToTextAnalyticsWithR)
  Public repo for the Data Science Dojo YouTube tutorial series "Introduction to Text Analytics with R".
- [kaisiemek/oceanic-dark-theme](https://github.com/kaisiemek/oceanic-dark-theme)
  A dark theme for the Brackets.io editor.
- [doctrine/search](https://github.com/doctrine/search)
  Generic Search extension for indexing and querying ODM/ORM objects with different text-search engine implementations
- [marktext/marktext](https://github.com/marktext/marktext)
  📝A simple and elegant markdown editor, available for Linux, macOS and Windows.
- [asciidoctor/asciidoctor](https://github.com/asciidoctor/asciidoctor)
  :gem: A fast, open source text processor and publishing toolchain, written in Ruby, for converting AsciiDoc content to HTML 5, DocBook 5, and other formats.
- [victorqribeiro/imgToAscii](https://github.com/victorqribeiro/imgToAscii)
  A JavaScript implementation of a image to Ascii code
- [uwdata/vega-editor](https://github.com/uwdata/vega-editor)
  Web-based Vega authoring environment.
- [ptsource/Developer-Platform](https://github.com/ptsource/Developer-Platform)
  :eight_spoked_asterisk: PTSource Developer Platform is a free professional IDE, Source Code Editor and Compiler, tools, templates and  libs for professionals and students. It offers many powerful features for programming languages and syntax highlighing for over 100 languages.
- [donnemartin/dev-setup](https://github.com/donnemartin/dev-setup)
  macOS development environment setup:  Easy-to-understand instructions with automated setup scripts for developer tools like Vim, Sublime Text, Bash, iTerm, Python data analysis, Spark, Hadoop MapReduce, AWS, Heroku, JavaScript web development, Android development, common data stores, and dev-based OS X defaults.
- [asciidoc/asciidoc](https://github.com/asciidoc/asciidoc)
  AsciiDoc is a text document format for writing notes, documentation, articles, books, slideshows, man pages & blogs. AsciiDoc can be translated to many formats including HTML, DocBook, PDF, EPUB, and man pages. NOTE: This implementation is written in Python 2, which EOLs in Jan 2020. AsciiDoc development is being continued under @asciidoctor.
- [syl20bnr/spacemacs](https://github.com/syl20bnr/spacemacs)
  A community-driven Emacs distribution - The best editor is neither Emacs nor Vim,  it's Emacs *and* Vim!
- [chrishunt/git-pissed](https://github.com/chrishunt/git-pissed)
  gitting pissed about your code
- [rubik/radon](https://github.com/rubik/radon)
  Various code metrics for Python code
- [prettier/prettier](https://github.com/prettier/prettier)
  Prettier is an opinionated code formatter.
- [ozh/ascii-tables](https://github.com/ozh/ascii-tables)
  Quickly format table in ASCII. Great for code comments, or Github Markdown!
- [Ivoah/minimap](https://github.com/Ivoah/minimap)
  Generate minimaps of your code
- [microsoft/vscode](https://github.com/microsoft/vscode)
  Visual Studio Code
- [1j01/atom-minimap](https://github.com/1j01/atom-minimap)
  A preview of the full source code.
- [ycm-core/YouCompleteMe](https://github.com/ycm-core/YouCompleteMe)
  A code-completion engine for Vim
- [alefragnani/vscode-numbered-bookmarks](https://github.com/alefragnani/vscode-numbered-bookmarks)
  Numbered Bookmarks Extension for Visual Studio Code
- [alefragnani/vscode-bookmarks](https://github.com/alefragnani/vscode-bookmarks)
  Bookmarks Extension for Visual Studio Code
- [VSCodium/vscodium](https://github.com/VSCodium/vscodium)
  binary releases of VS Code without MS branding/telemetry/licensing
- [awssat/tailwindo](https://github.com/awssat/tailwindo)
  🔌  Convert Bootstrap CSS code to Tailwind CSS code
- [pybites/challenges](https://github.com/pybites/challenges)
  PyBites Code Challenges
- [algorithm-visualizer/algorithm-visualizer](https://github.com/algorithm-visualizer/algorithm-visualizer)
  :fireworks:Interactive Online Platform that Visualizes Algorithms from Code
- [fivethirtyeight/data](https://github.com/fivethirtyeight/data)
  Data and code behind the articles and graphics at FiveThirtyEight
- [JP1016/Markdown](https://github.com/JP1016/Markdown)
  📖WYSIWYG Generator for Markdown, 🔌Offline Support and Easy Generation of Markdown ⚡️ https://mdown.now.sh
- [Defacto2/Code-Breaker](https://github.com/Defacto2/Code-Breaker)
  Various tools, trainers and utilities created by Code Breaker during 1992-1995
- [maxmind/geoipupdate](https://github.com/maxmind/geoipupdate)
  GeoIP update client code
- [rivo/tview](https://github.com/rivo/tview)
  Rich interactive widgets for terminal-based UIs written in Go
- [Show-Me-the-Code/python](https://github.com/Show-Me-the-Code/python)
  Show Me the Code Python version.
