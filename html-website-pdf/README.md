# Repositories defined by: html, website, pdf

also defined by the following keywords: generator, static, rendering, xml, content, page, converter

- [zatGUY/ian.mcdonald](https://github.com/zatGUY/ian.mcdonald)
- [tchoi8/distributedwebofcare](https://github.com/tchoi8/distributedwebofcare)
  DWC 
- [cdx5/limited-fixed](https://github.com/cdx5/limited-fixed)
- [jonschlinkert/gulp-htmlmin](https://github.com/jonschlinkert/gulp-htmlmin)
  Minify HTML
- [doyoe/css-handbook](https://github.com/doyoe/css-handbook)
  CSS参考手册
- [laurelschwulst/name-generator](https://github.com/laurelschwulst/name-generator)
- [Arno0x/EmbedInHTML](https://github.com/Arno0x/EmbedInHTML)
  Embed and hide any file in an HTML file
- [whatwg/html](https://github.com/whatwg/html)
  HTML Standard
- [writeas/htmlhouse](https://github.com/writeas/htmlhouse)
  Publish HTML quickly.
- [brajeshwar/entities](https://github.com/brajeshwar/entities)
  Character Entities for HTML, CSS (content) and Javascript.
- [laurelschwulst/p2pforever.org](https://github.com/laurelschwulst/p2pforever.org)
- [Lonefy/vscode-JS-CSS-HTML-formatter](https://github.com/Lonefy/vscode-JS-CSS-HTML-formatter)
  JS,CSS,HTML formatter for vscode
- [Automattic/juice](https://github.com/Automattic/juice)
  Juice inlines CSS stylesheets into your HTML source.
- [fontforge/fontforge.github.io](https://github.com/fontforge/fontforge.github.io)
  The FontForge homepage
- [jdittrich/quickMockup](https://github.com/jdittrich/quickMockup)
  HTML based interface mockups
- [lightning-viz/nbviewer](https://github.com/lightning-viz/nbviewer)
  Nbconvert as a webservice (rendering ipynb to static HTML)
- [spatie/laravel-html](https://github.com/spatie/laravel-html)
  Painless html generation
- [angular/angular.js](https://github.com/angular/angular.js)
  AngularJS - HTML enhanced for web apps!
- [privacytoolsIO/privacytools.io](https://github.com/privacytoolsIO/privacytools.io)
  🛡️ encryption against global mass surveillance
- [wkhtmltopdf/wkhtmltopdf](https://github.com/wkhtmltopdf/wkhtmltopdf)
  Convert HTML to PDF using Webkit (QtWebKit)
- [c-smile/sciter-sdk](https://github.com/c-smile/sciter-sdk)
  Sciter is an embeddable HTML/CSS/scripting engine
- [webpack-contrib/html-loader](https://github.com/webpack-contrib/html-loader)
  HTML Loader
- [dompdf/dompdf](https://github.com/dompdf/dompdf)
  HTML to PDF converter (PHP5)
- [andrejewski/himalaya](https://github.com/andrejewski/himalaya)
  JavaScript HTML to JSON Parser
- [domchristie/turndown](https://github.com/domchristie/turndown)
  🛏 An HTML to Markdown converter written in JavaScript
- [hxrts/cybercon](https://github.com/hxrts/cybercon)
  cybercon website
- [iabudiab/HTMLKit](https://github.com/iabudiab/HTMLKit)
  An Objective-C framework for your everyday HTML needs.
- [thephpleague/html-to-markdown](https://github.com/thephpleague/html-to-markdown)
  Convert HTML to Markdown with PHP
- [benscabbia/x-ray](https://github.com/benscabbia/x-ray)
  Visual debugger for your HTML, executable via a bookmark
- [lexborisov/myhtml](https://github.com/lexborisov/myhtml)
  Fast C/C++ HTML 5 Parser. Using threads.
- [w3ctag/ethical-web-principles](https://github.com/w3ctag/ethical-web-principles)
  W3C TAG Ethical Web Principles
- [robinmoisson/staticrypt](https://github.com/robinmoisson/staticrypt)
  Password protect a static HTML page
- [AmirTugi/tea-school](https://github.com/AmirTugi/tea-school)
  Simplified HTML + CSS --> PDF Generator for Nodejs
- [rgrove/sanitize](https://github.com/rgrove/sanitize)
  Ruby HTML and CSS sanitizer.
- [psf/requests-html](https://github.com/psf/requests-html)
  Pythonic HTML Parsing for Humans™
- [LMsgSendNilSelf/WebPageParser](https://github.com/LMsgSendNilSelf/WebPageParser)
  A delightful  xml and html parsing relish for iOS
- [t7/construct.css](https://github.com/t7/construct.css)
  Focus on the content and structure of your HTML
- [westy92/html-pdf-chrome](https://github.com/westy92/html-pdf-chrome)
  HTML to PDF converter via Chrome/Chromium
- [nicothin/NTH-start-project](https://github.com/nicothin/NTH-start-project)
  Startkit for HTML / CSS / JS pages layout.
- [browserhtml/browserhtml](https://github.com/browserhtml/browserhtml)
  Experimental Servo browser built in HTML
- [CenterForOpenScience/modular-file-renderer](https://github.com/CenterForOpenScience/modular-file-renderer)
  A Python package for rendering files to HTML via an embeddable iframe
- [umpox/TinyEditor](https://github.com/umpox/TinyEditor)
  A functional HTML/CSS/JS editor in less than 400 bytes
- [shawnbot/custom-elements](https://github.com/shawnbot/custom-elements)
  All about HTML Custom Elements
- [topfunky/hpple](https://github.com/topfunky/hpple)
  An XML/HTML parser for Objective-C, inspired by Hpricot.
- [adamschwartz/chrome-tabs](https://github.com/adamschwartz/chrome-tabs)
  Chrome-style tabs in HTML/CSS.
- [WebMemex/freeze-dry](https://github.com/WebMemex/freeze-dry)
  Snapshots a web page to get it as a static, self-contained HTML document.
- [jgthms/html-reference](https://github.com/jgthms/html-reference)
  HTML Reference: a free guide to all HTML5 elements and attributes
- [spatie/menu](https://github.com/spatie/menu)
  Html menu generator
- [spatie/laravel-menu](https://github.com/spatie/laravel-menu)
  Html menu generator for Laravel
- [chrishunt/git-pissed](https://github.com/chrishunt/git-pissed)
  gitting pissed about your code
- [jgthms/marksheet](https://github.com/jgthms/marksheet)
  Free tutorial to learn HTML and CSS
- [d3/d3](https://github.com/d3/d3)
  Bring data to life with SVG, Canvas and HTML. :bar_chart::chart_with_upwards_trend::tada:
- [sajari/docconv](https://github.com/sajari/docconv)
  Converts PDF, DOC, DOCX, XML, HTML, RTF, etc to plain text
- [litehtml/litehtml](https://github.com/litehtml/litehtml)
  Fast and lightweight HTML/CSS rendering engine
- [roman01la/html-to-react-components](https://github.com/roman01la/html-to-react-components)
  Converts HTML pages into React components
- [eKoopmans/html2pdf.js](https://github.com/eKoopmans/html2pdf.js)
  Client-side HTML-to-PDF rendering using pure JS.
- [justalever/tumblrboilerplate](https://github.com/justalever/tumblrboilerplate)
  Tumblr Boiler Plate Theme
- [isocra/TableDnD](https://github.com/isocra/TableDnD)
  jQuery plug-in to drag and drop rows in HTML tables
- [gjtorikian/html-proofer](https://github.com/gjtorikian/html-proofer)
  Test your rendered HTML files to make sure they're accurate.
- [htmlhint/HTMLHint](https://github.com/htmlhint/HTMLHint)
  ⚙️ The static code analysis tool you need for your HTML
- [Softwire/Workflowy2Web](https://github.com/Softwire/Workflowy2Web)
  A tool for converting a content outline in Workflowy to a working prototype in HTML
- [calibr/node-bookmarks-parser](https://github.com/calibr/node-bookmarks-parser)
  Parses Firefox/Chrome HTML bookmarks files
- [microcosm-cc/bluemonday](https://github.com/microcosm-cc/bluemonday)
  bluemonday: a fast golang HTML sanitizer (inspired by the OWASP Java HTML Sanitizer) to scrub user generated content of XSS
- [paquettg/php-html-parser](https://github.com/paquettg/php-html-parser)
  An HTML DOM parser. It allows you to manipulate HTML. Find tags on an HTML page with selectors just like jQuery.
- [rstudio/pagedown](https://github.com/rstudio/pagedown)
  Paginate the HTML Output of R Markdown with CSS for Print
- [jxnblk/colorable](https://github.com/jxnblk/colorable)
  Color combination contrast tester
- [yixuan/prettydoc](https://github.com/yixuan/prettydoc)
  Creating Pretty HTML From R Markdown
- [Kozea/WeasyPrint](https://github.com/Kozea/WeasyPrint)
  WeasyPrint converts web documents (HTML with CSS, SVG, …) to PDF.
- [aantron/lambdasoup](https://github.com/aantron/lambdasoup)
  Functional HTML scraping and rewriting with CSS in OCaml
- [remarkablemark/html-react-parser](https://github.com/remarkablemark/html-react-parser)
  :memo: HTML to React parser.
- [posthtml/posthtml](https://github.com/posthtml/posthtml)
  PostHTML is a tool to transform HTML/XML with JS plugins
- [scrapy/parsel](https://github.com/scrapy/parsel)
  Parsel lets you extract data from XML/HTML documents using XPath or CSS selectors
- [ivanceras/sauron](https://github.com/ivanceras/sauron)
  Sauron is an html web framework for building web-apps. It is heavily inspired by elm.
- [sihaelov/harser](https://github.com/sihaelov/harser)
  Easy way for HTML parsing and building XPath
- [duzun/hQuery.php](https://github.com/duzun/hQuery.php)
  An extremely fast web scraper that parses megabytes of HTML in a blink of an eye. PHP5.3+, no dependencies.
- [florent37/RxRetroJsoup](https://github.com/florent37/RxRetroJsoup)
  A simple API-like from html website (scrapper) for Android, RxJava2 ready !
- [jamiew/heroku-static-site](https://github.com/jamiew/heroku-static-site)
  A basic Ruby/Rack app for publishing a static HTML/CSS/javascript website on Heroku (for free)
- [StefanKovac/flex-layout-attribute](https://github.com/StefanKovac/flex-layout-attribute)
  HTML layout helper based on CSS flexbox specification  — 
- [treenotation/lab.treenotation.org](https://github.com/treenotation/lab.treenotation.org)
  Website for Tree Notation Lab
- [twbs/ratchet](https://github.com/twbs/ratchet)
  Build mobile apps with simple HTML, CSS, and JavaScript components. 
- [KeenRivals/bestmotherfucking.website](https://github.com/KeenRivals/bestmotherfucking.website)
  The Best Motherfucking Website
- [1j01/multifiddle](https://github.com/1j01/multifiddle)
  (development inactive) Fiddle with coffee, js, css, html, and all that, in a minimalistic collaborative environment
- [thedevsaddam/renderer](https://github.com/thedevsaddam/renderer)
  Simple, lightweight and faster response (JSON, JSONP, XML, YAML, HTML, File) rendering package for Go
- [unrolled/render](https://github.com/unrolled/render)
  Go package for easily rendering JSON, XML, binary data, and HTML templates responses.
- [remy/html5demos](https://github.com/remy/html5demos)
  Collection of hacks and demos showing capability of HTML5 apps
- [srod/node-minify](https://github.com/srod/node-minify)
  Light Node.js module that compress javascript, css and html files
- [ManzDev/javagotchi](https://github.com/ManzDev/javagotchi)
  Javagotchi: Javascript tamagotchi clone
- [bengarrett/RetroTxt](https://github.com/bengarrett/RetroTxt)
  RetroTxt is the WebExtension that turns ANSI, ASCII, NFO text into in-browser HTML
- [werk85/node-html-to-text](https://github.com/werk85/node-html-to-text)
  Advanced html to text converter
- [adobe/brackets](https://github.com/adobe/brackets)
  An open source code editor for the web, written in JavaScript, HTML and CSS.
- [pytest-dev/pytest-html](https://github.com/pytest-dev/pytest-html)
  Plugin for generating HTML reports for pytest results
- [shinima/temme](https://github.com/shinima/temme)
  📄 Concise selector to extract JSON from HTML.
- [lise-henry/crowbook](https://github.com/lise-henry/crowbook)
  Converts books written in Markdown to HTML, LaTeX/PDF and EPUB
- [jhy/jsoup](https://github.com/jhy/jsoup)
  jsoup: Java HTML Parser, with best of DOM, CSS, and jquery
- [jaredrummler/HtmlBuilder](https://github.com/jaredrummler/HtmlBuilder)
  Build valid HTML for Android TextView
- [prat0318/json_resume](https://github.com/prat0318/json_resume)
  Generates pretty HTML, LaTeX, markdown, with biodata feeded as input in JSON
- [electron/electron](https://github.com/electron/electron)
  :electron: Build cross-platform desktop apps with JavaScript, HTML, and CSS
- [K-Kraken/Pixel-Portfolio-Webite](https://github.com/K-Kraken/Pixel-Portfolio-Webite)
  A Nintendo inspired lightweight, device friendly single-page portfolio website built with HTML and CSS :space_invader:🌏
- [hit9/img2txt](https://github.com/hit9/img2txt)
  Image to Ascii Text with color support, can output to html or ansi terminal.
- [jsdf/react-native-htmlview](https://github.com/jsdf/react-native-htmlview)
  A React Native component which renders HTML content as native views
- [busterc/boomlet](https://github.com/busterc/boomlet)
  :boom: Bookmarklet compiler encloses, encodes, minifies your Javascript file and opens an HTML page with your new bookmarklet for immediate use.
- [microsoft/frontend-bootcamp](https://github.com/microsoft/frontend-bootcamp)
  Frontend Workshop from HTML/CSS/JS to TypeScript/React/Redux
- [twbs/bootstrap](https://github.com/twbs/bootstrap)
  The most popular HTML, CSS, and JavaScript framework for developing responsive, mobile first projects on the web.
- [psharanda/Atributika](https://github.com/psharanda/Atributika)
  Convert text with HTML tags, links, hashtags, mentions into NSAttributedString. Make them clickable with UILabel drop-in replacement.
- [gskinner/regexr](https://github.com/gskinner/regexr)
  RegExr is a HTML/JS based tool for creating, testing, and learning about Regular Expressions.
- [hakuoku/saito-boilerplate](https://github.com/hakuoku/saito-boilerplate)
  Static website boilerplate for designers & everyone
- [connors/photon](https://github.com/connors/photon)
  The fastest way to build beautiful Electron apps using simple HTML and CSS
- [zTrix/webpage2html](https://github.com/zTrix/webpage2html)
  save/convert web pages to a standalone editable html file for offline archive/view/edit/play/whatever
- [1j01/mind-map](https://github.com/1j01/mind-map)
  🧠🗺 because your mind doesn't have ugly boxes everywhere
- [ripienaar/free-for-dev](https://github.com/ripienaar/free-for-dev)
  A list of SaaS, PaaS and IaaS offerings that have free tiers of interest to devops and infradev
- [borismus/spectrogram](https://github.com/borismus/spectrogram)
  Web Audio Spectrogram
- [railsgirls/railsgirls.github.io](https://github.com/railsgirls/railsgirls.github.io)
  Rails Girls Guides
- [Y2Z/monolith](https://github.com/Y2Z/monolith)
  :black_large_square: CLI tool for saving complete web pages as a single HTML file
- [feedparser/feedparser](https://github.com/feedparser/feedparser)
  feedparser gem - (universal) web feed parser and normalizer (XML w/ Atom or RSS, JSON Feed, HTML w/ Microformats e.g. h-entry/h-feed or Feed.HTML, Feed.TXT w/ YAML, JSON or INI & Markdown, etc.)
- [r0x0r/pywebview](https://github.com/r0x0r/pywebview)
  Build GUI for your Python program with JavaScript, HTML, and CSS
- [anseki/plain-draggable](https://github.com/anseki/plain-draggable)
  The simple and high performance library to allow HTML/SVG element to be dragged.
- [gildas-lormeau/SingleFileZ](https://github.com/gildas-lormeau/SingleFileZ)
  SingleFileZ is a Web Extension for saving web pages as self-extracting HTML/ZIP hybrid files.
- [microlinkhq/metascraper](https://github.com/microlinkhq/metascraper)
  Scrape data from websites using Open Graph metadata, regular HTML metadata, and a series of fallbacks.
- [archriss/react-native-render-html](https://github.com/archriss/react-native-render-html)
  iOS/Android pure javascript react-native component that renders your HTML into 100% native views
- [fontforge/designwithfontforge.com](https://github.com/fontforge/designwithfontforge.com)
  A book about how to design new typefaces with FontForge
- [csu/export-saved-reddit](https://github.com/csu/export-saved-reddit)
  Export saved Reddit posts into a HTML file for import into Google Chrome.
- [asciidoctor/asciidoctor](https://github.com/asciidoctor/asciidoctor)
  :gem: A fast, open source text processor and publishing toolchain, written in Ruby, for converting AsciiDoc content to HTML 5, DocBook 5, and other formats.
- [Show-Me-the-Code/python](https://github.com/Show-Me-the-Code/python)
  Show Me the Code Python version.
- [kamlekar/slim-scroll](https://github.com/kamlekar/slim-scroll)
  HTML element scroll bar as replacement for default browser's scroll-bar. Design as you want using CSS.
- [uwdata/vega](https://github.com/uwdata/vega)
  Vega Development Staging
- [ablwr/internetgirlfriend_club](https://github.com/ablwr/internetgirlfriend_club)
  cool site about the y2k generation 
- [kartik-v/yii2-export](https://github.com/kartik-v/yii2-export)
  A library to export server/db data in various formats (e.g. excel, html, pdf, csv etc.)
- [ffoodd/a11y.css](https://github.com/ffoodd/a11y.css)
  This CSS file intends to warn developers about possible risks and mistakes that exist in HTML code. It can also be used to roughly evaluate a site's quality by simply including it as an external stylesheet.
- [uwdata/perceptual-kernels](https://github.com/uwdata/perceptual-kernels)
  Data & source code for the perceptual kernels study
- [logeshpaul/Frontend-Cheat-Sheets](https://github.com/logeshpaul/Frontend-Cheat-Sheets)
  Collection of cheat sheets(HTML, CSS, JS, Git, Gulp, etc.,) for your frontend development needs & reference
- [jsulpis/basic-website-setup](https://github.com/jsulpis/basic-website-setup)
  A multi-page static website setup.
- [cure53/DOMPurify](https://github.com/cure53/DOMPurify)
  DOMPurify - a DOM-only, super-fast, uber-tolerant XSS sanitizer for HTML, MathML and SVG. DOMPurify works with a secure default, but offers a lot of configurability and hooks. Demo:
- [tabler/tabler](https://github.com/tabler/tabler)
  Tabler is free and open-source HTML Dashboard UI Kit built on Bootstrap
- [jh3y/driveway](https://github.com/jh3y/driveway)
  pure CSS masonry layouts
- [SoapBox/linkifyjs](https://github.com/SoapBox/linkifyjs)
  Linkify is a zero-dependency JavaScript plugin for finding links in plain-text and converting them to HTML <a> tags.
- [jgthms/web-design-in-4-minutes](https://github.com/jgthms/web-design-in-4-minutes)
  Learn the basics of web design in 4 minutes
- [corygibbons/yuki](https://github.com/corygibbons/yuki)
  ❄️ Responsive masonry style theme for Tumblr.
- [alvarcarto/url-to-pdf-api](https://github.com/alvarcarto/url-to-pdf-api)
  Web page PDF/PNG rendering done right. Self-hosted service for rendering receipts, invoices, or any content.
- [aroneiermann/readability-luin](https://github.com/aroneiermann/readability-luin)
  Turn any web page into a clean view
- [broskoski/readability](https://github.com/broskoski/readability)
  Turn any web page into a clean view
- [Taritsyn/WebMarkupMin](https://github.com/Taritsyn/WebMarkupMin)
  The Web Markup Minifier (abbreviated WebMarkupMin) - a .NET library that contains a set of markup minifiers. The objective of this project is to improve the performance of web applications by reducing the size of HTML, XHTML and XML code.
- [ndrwhr/clap.space](https://github.com/ndrwhr/clap.space)
  http://clap.space
- [atomotic/uv-app-starter](https://github.com/atomotic/uv-app-starter)
  Create a IIIF-enabled website using universalviewer.io and host it for free on Github Pages
- [google/WebFundamentals](https://github.com/google/WebFundamentals)
  Best practices for modern web development
- [pybites/blog_code](https://github.com/pybites/blog_code)
  Python sample code for our blog
- [pirate/ArchiveBox](https://github.com/pirate/ArchiveBox)
  🗃 The open source self-hosted web archive. Takes browser history/bookmarks/Pocket/Pinboard/etc., saves HTML, JS, PDFs, media, and more...
- [jglovier/gifs](https://github.com/jglovier/gifs)
  :joy: :camera: :sparkler: Storage place for all mah gifs.
- [isobar-us/code-standards](https://github.com/isobar-us/code-standards)
  Isobar Front-end development coding standards. Memorize them BY HEART.
- [gildas-lormeau/SingleFile](https://github.com/gildas-lormeau/SingleFile)
  Web Extension for Firefox/Chrome and CLI tool to save a faithful copy of a complete web page as a single HTML file
- [fivethirtyeight/actblue-analysis](https://github.com/fivethirtyeight/actblue-analysis)
  What does it mean when one organization raises $2.9 billion for democrats?
- [SouravJohar/python-app-with-electron-gui](https://github.com/SouravJohar/python-app-with-electron-gui)
  A better way to make GUIs for your python apps
- [pbiecek/archivist](https://github.com/pbiecek/archivist)
  A set of tools for datasets and plots archiving
- [albinekb/kap-draggable](https://github.com/albinekb/kap-draggable)
  Share recordings with drag and drop
- [vpusher/paper-tree](https://github.com/vpusher/paper-tree)
   Browsable tree of nodes with expandable/collapsible capabilities and actions menu
- [arewedistributedyet/arewedistributedyet](https://github.com/arewedistributedyet/arewedistributedyet)
  Website + Community effort to unlock the peer-to-peer web at arewedistributedyet.com ⚡🌐🔑
- [KennethWangDotDev/TypographyHandbook](https://github.com/KennethWangDotDev/TypographyHandbook)
  A concise, referential guide on best web typographic practices.
- [milligram/milligram](https://github.com/milligram/milligram)
  A minimalist CSS framework.
- [louismerlin/concrete.css](https://github.com/louismerlin/concrete.css)
  A simple and to the point CSS microframework
- [twitterdev/cards-player-samples](https://github.com/twitterdev/cards-player-samples)
  Sample Code for Player Cards, both for stored and streamed video.
- [deanmalmgren/textract](https://github.com/deanmalmgren/textract)
  extract text from any document. no muss. no fuss.
- [martenbjork/sign-up-for-facebook](https://github.com/martenbjork/sign-up-for-facebook)
  A summary of what data Facebook collects and how it can be used
- [nginx/nginx](https://github.com/nginx/nginx)
  An official read-only mirror of http://hg.nginx.org/nginx/ which is updated hourly. Pull requests on GitHub cannot be accepted and will be automatically closed. The proper way to submit changes to nginx is via the nginx development mailing list, see http://nginx.org/en/docs/contributing_changes.html
- [Jaredk3nt/homepage](https://github.com/Jaredk3nt/homepage)
  Custom homepage for use locally in browser
- [espebra/filebin](https://github.com/espebra/filebin)
  Filebin is a web application that facilitates convenient file sharing over the web.
- [caneco/design-tips](https://github.com/caneco/design-tips)
  The “🔥 Design Tips” series, now in TailwindCSS
- [1j01/bookmarklets](https://github.com/1j01/bookmarklets)
  🔖 Fun functions, surreal when applied to websites you're familiar with
- [HumanCellAtlas/humancellatlas.github.io](https://github.com/HumanCellAtlas/humancellatlas.github.io)
  Provides manageable URLs for the Human Cell Atlas JSON schemas. 
- [CognitionGuidedSurgery/cli2web](https://github.com/CognitionGuidedSurgery/cli2web)
  CLI application as a Service via Restful HTTP
- [webrecorder/user-guide](https://github.com/webrecorder/user-guide)
  webrecorder user guide and glossary
- [asciidoc/asciidoc](https://github.com/asciidoc/asciidoc)
  AsciiDoc is a text document format for writing notes, documentation, articles, books, slideshows, man pages & blogs. AsciiDoc can be translated to many formats including HTML, DocBook, PDF, EPUB, and man pages. NOTE: This implementation is written in Python 2, which EOLs in Jan 2020. AsciiDoc development is being continued under @asciidoctor.
- [mblaul/code-playground](https://github.com/mblaul/code-playground)
  A happy little repo to play around with new concepts.
- [beakerbrowser/explore](https://github.com/beakerbrowser/explore)
  A curated list of peer-to-peer websites and apps
- [web-tiki/responsive-grid-of-hexagons](https://github.com/web-tiki/responsive-grid-of-hexagons)
  CSS responsive grid of hexagons
- [thombashi/pytablewriter](https://github.com/thombashi/pytablewriter)
  pytablewriter is a Python library to write a table in various formats: CSV / Elasticsearch / HTML / JavaScript / JSON / LaTeX / LDJSON / LTSV / Markdown / MediaWiki / NumPy / Excel / Pandas / Python / reStructuredText / SQLite / TOML / TSV.
- [juancarlospaco/css-html-js-minify](https://github.com/juancarlospaco/css-html-js-minify)
  StandAlone Async cross-platform Minifier for the Web.
- [dmstern/html2biblatex](https://github.com/dmstern/html2biblatex)
  A tiny bookmarklet for exporting web pages to BibLaTeX (all browsers / no installation).
- [sindresorhus/github-markdown-css](https://github.com/sindresorhus/github-markdown-css)
  The minimal amount of CSS to replicate the GitHub Markdown style
- [LiaungYip/plain-blog](https://github.com/LiaungYip/plain-blog)
  Hugo blog theme for speed demons.
- [nisrulz/app-privacy-policy-generator](https://github.com/nisrulz/app-privacy-policy-generator)
  A simple web app to generate a generic privacy policy for your Android/iOS apps
- [prism-break/prism-break](https://github.com/prism-break/prism-break)
  Privacy/security-oriented software recommendations (mirrored from GitLab)
- [typora/typora-theme-toolkit](https://github.com/typora/typora-theme-toolkit)
  Make things easier to preview/debug a typora theme
- [a11yproject/a11yproject.com](https://github.com/a11yproject/a11yproject.com)
  A community–driven effort to make web accessibility easier.
- [TeaMeow/TocasUI](https://github.com/TeaMeow/TocasUI)
  Tocas UI — A CSS3 and SASS UI library.
- [alienzhou/frontend-tech-list](https://github.com/alienzhou/frontend-tech-list)
  📝 Frontend Tech List for Developers 💡
- [jeroenjanssens/data-science-at-the-command-line](https://github.com/jeroenjanssens/data-science-at-the-command-line)
  Data Science at the Command Line
- [wesbos/JavaScript30](https://github.com/wesbos/JavaScript30)
  30 Day Vanilla JS Challenge
- [victordomingos/Count-files](https://github.com/victordomingos/Count-files)
  A CLI utility written in Python to help you count files, grouped by extension, in a directory. By default, it will count files recursively in current working directory and all of its subdirectories, and will display a table showing the frequency for each file extension (e.g.: .txt, .py, .html, .css) and the total number of files found.
- [lolstring/window98-html-css-js](https://github.com/lolstring/window98-html-css-js)
  Windows 98 on the Web using HTML5, CSS3 and JS.
- [Mobirise/Mobirise](https://github.com/Mobirise/Mobirise)
  Free Website Builder for Bootstrap 4 & AMP
- [ansilove/ansilove.js](https://github.com/ansilove/ansilove.js)
  A script to display ANSi and artscene related file formats on web pages
- [anges244/evie](https://github.com/anges244/evie)
  A production-ready theme for your projects with a minimal style guide https://evie.undraw.co
- [benkeen/responsive-design-bookmarklet-generator](https://github.com/benkeen/responsive-design-bookmarklet-generator)
  Generates custom bookmarklets for testing responsive layouts to show how they look in different viewport sizes.
- [fivethirtyeight/d3-pre](https://github.com/fivethirtyeight/d3-pre)
  Pre-render d3 visualizations
- [textileio/textile-facebook](https://github.com/textileio/textile-facebook)
  simple parsing tool to get your data out of a facebook export
- [wilfredinni/javascript-cheatsheet](https://github.com/wilfredinni/javascript-cheatsheet)
  Basic Javascript Cheat Sheet
- [jmn/jmn.github.io](https://github.com/jmn/jmn.github.io)
  edit on the dev branch, use yarn deploy to deploy
- [crazychicken/t-scroll](https://github.com/crazychicken/t-scroll)
  A modern reveal-on-scroll library with useful options and animations. (Animate Elements On Reveal)
- [1j01/pesterchum](https://github.com/1j01/pesterchum)
  Pesterchum Chat Client, and also a Doc Scratch lightning text effect reaction GIF generator
- [marcobiedermann/search-engine-optimization](https://github.com/marcobiedermann/search-engine-optimization)
  :mag: A helpful checklist / collection of Search Engine Optimization (SEO) tips and techniques.
- [vanhauser-thc/THC-Archive](https://github.com/vanhauser-thc/THC-Archive)
  All releases of the security research group (a.k.a. hackers) The Hacker's Choice
- [uwdata/imMens](https://github.com/uwdata/imMens)
  Real-Time Visual Querying of Big Data
- [pure-css/pure](https://github.com/pure-css/pure)
  A set of small, responsive CSS modules that you can use in every web project.
- [kaushalmodi/hugo-bare-min-theme](https://github.com/kaushalmodi/hugo-bare-min-theme)
  A bare minimum theme for Hugo (https://gohugo.io) to help develop and debug Hugo sites -- https://hugo-bare-min.netlify.com/,
- [cri-o/cri-o.io](https://github.com/cri-o/cri-o.io)
  website content
- [VincentLoy/tweetParser.js](https://github.com/VincentLoy/tweetParser.js)
  tweetParser.js Parse an element containing a tweet and turn URLS, @user & #hashtags into working urls
- [wyzant-dev/vue-radial-progress](https://github.com/wyzant-dev/vue-radial-progress)
  Radial progress bar component for Vue.js
- [surjithctly/documentation-html-template](https://github.com/surjithctly/documentation-html-template)
  A Sample Documentation Template for Themes, Templates and Plugins
- [xizon/uix-kit](https://github.com/xizon/uix-kit)
  A free UI toolkit based on some common libraries for building beautiful responsive website, compatible with Bootstrap v4.
- [kalvn/Shaarli-Material](https://github.com/kalvn/Shaarli-Material)
  Shaarli Material is a theme for Shaarli, the famous personal, minimalist, super-fast, database free, bookmarking service.
- [modulz/modulz-original-design-system-archive](https://github.com/modulz/modulz-original-design-system-archive)
  An open-source design system for building scalable, responsive websites and applications.
- [uwdata/palette-analyzer](https://github.com/uwdata/palette-analyzer)
  Analyzes the local and global distances in [RGB, LAB, UCS, Color Names] model, given a palette.
