# Repositories defined by: list, awesome, curated

also defined by the following keywords: resources, frameworks, libraries, software, apps, free, learning

- [n1trux/awesome-sysadmin](https://github.com/n1trux/awesome-sysadmin)
  A curated list of amazingly awesome open source sysadmin resources.
- [dipakkr/A-to-Z-Resources-for-Students](https://github.com/dipakkr/A-to-Z-Resources-for-Students)
  ✅  Curated list of resources for college students 
- [Pinperepette/awesome-malware-analysis](https://github.com/Pinperepette/awesome-malware-analysis)
  A curated list of awesome malware analysis tools and resources
- [Pinperepette/awesome-python](https://github.com/Pinperepette/awesome-python)
  A curated list of awesome Python frameworks, libraries, software and resources
- [vinta/awesome-python](https://github.com/vinta/awesome-python)
  A curated list of awesome Python frameworks, libraries, software and resources
- [1j01/awesome-attwoods-law](https://github.com/1j01/awesome-attwoods-law)
   A curated list of applications reimplemented in JavaScript
- [fasouto/awesome-dataviz](https://github.com/fasouto/awesome-dataviz)
  :chart_with_upwards_trend:  A curated list of awesome data visualization libraries and resources.
- [brunocvcunha/awesome-userscripts](https://github.com/brunocvcunha/awesome-userscripts)
  📖  A curated list of Awesome Userscripts.
- [nepaul/awesome-web-components](https://github.com/nepaul/awesome-web-components)
  A curated list of awesome web components.
- [Pinperepette/awesome](https://github.com/Pinperepette/awesome)
  A curated list of awesome lists
- [Pinperepette/awesome-appsec](https://github.com/Pinperepette/awesome-appsec)
  A curated list of resources for learning about application security
- [alebcay/awesome-shell](https://github.com/alebcay/awesome-shell)
  A curated list of awesome command-line frameworks, toolkits, guides and gizmos. Inspired by awesome-php.
- [Pinperepette/awesome-shell](https://github.com/Pinperepette/awesome-shell)
  A curated list of awesome command-line frameworks, toolkits, guides and gizmos. Inspired by awesome-php.
- [s-p-k/awesome-shell](https://github.com/s-p-k/awesome-shell)
  A curated list of awesome command-line frameworks, toolkits, guides and gizmos. Inspired by awesome-php.
- [hindupuravinash/the-gan-zoo](https://github.com/hindupuravinash/the-gan-zoo)
  A list of all named GANs!
- [iCHAIT/awesome-subreddits](https://github.com/iCHAIT/awesome-subreddits)
  :memo: A curated list of awesome programming subreddits.
- [tleb/awesome-mastodon](https://github.com/tleb/awesome-mastodon)
  Curated list of awesome Mastodon-related stuff!
- [MFatihMAR/Awesome-Game-Networking](https://github.com/MFatihMAR/Awesome-Game-Networking)
  A Curated List of Game Network Programming Resources
- [webgems/webgems](https://github.com/webgems/webgems)
  A curated list of resources for devs and designers. Join me on devcord.com if you are up for a chit chat :)
- [mikalv/awesome-i2p](https://github.com/mikalv/awesome-i2p)
  A curated list of awesome I2P implementations, libraries, resources, projects, and shiny things. I2P is an anonymous overlay network - a network within a network. It is intended to protect communication from dragnet surveillance and monitoring by third parties such as ISPs.
- [Pinperepette/awesome-cli-apps](https://github.com/Pinperepette/awesome-cli-apps)
  A curated list of command line apps
- [agarrharr/awesome-cli-apps](https://github.com/agarrharr/awesome-cli-apps)
  🖥 📊 🕹 🛠 A curated list of command line apps
- [Jfaler/Philosophize](https://github.com/Jfaler/Philosophize)
  :octocat: A curated list of philosophy resources.
- [pirate/terminals-are-sexy](https://github.com/pirate/terminals-are-sexy)
  💥 A curated list of Terminal frameworks, plugins & resources for CLI lovers.
- [k4m4/terminals-are-sexy](https://github.com/k4m4/terminals-are-sexy)
  💥 A curated list of Terminal frameworks, plugins & resources for CLI lovers.
- [CraryPrimitiveMan/awesome-with-star](https://github.com/CraryPrimitiveMan/awesome-with-star)
  😎 Curated list of awesome lists with github star
- [moul/awesome-ssh](https://github.com/moul/awesome-ssh)
  :computer: A curated list of SSH resources.
- [joho/awesome-code-review](https://github.com/joho/awesome-code-review)
  An "Awesome" list of code review resources - articles, papers, tools, etc
- [jonathandion/awesome-emails](https://github.com/jonathandion/awesome-emails)
  ✉️ An awesome list of resources to build better emails.
- [hemanth/awesome-pwa](https://github.com/hemanth/awesome-pwa)
  Awesome list of progressive web apps! (PR welcomed ;))
- [abhijithvijayan/stargazed](https://github.com/abhijithvijayan/stargazed)
  Creating your own Awesome List of GitHub stars! 
- [tuvtran/project-based-learning](https://github.com/tuvtran/project-based-learning)
  Curated list of project-based tutorials
- [nikitavoloboev/privacy-respecting](https://github.com/nikitavoloboev/privacy-respecting)
  Curated List of Privacy Respecting Services and Software
- [tg-z/awesome-for-beginners](https://github.com/tg-z/awesome-for-beginners)
  A list of awesome beginners-friendly projects.
- [beakerbrowser/explore](https://github.com/beakerbrowser/explore)
  A curated list of peer-to-peer websites and apps
- [EthicalML/awesome-production-machine-learning](https://github.com/EthicalML/awesome-production-machine-learning)
  A curated list of awesome open source libraries to deploy, monitor, version and scale your machine learning
- [Pinperepette/awesome-web-hacking](https://github.com/Pinperepette/awesome-web-hacking)
  A list of web application security
- [atomotic/awesome-web-archiving](https://github.com/atomotic/awesome-web-archiving)
  An Awesome List for getting started with web archiving
- [iipc/awesome-web-archiving](https://github.com/iipc/awesome-web-archiving)
  An Awesome List for getting started with web archiving
- [MunGell/awesome-for-beginners](https://github.com/MunGell/awesome-for-beginners)
  A list of awesome beginners-friendly projects.
- [pirate/awesome-web-archiving](https://github.com/pirate/awesome-web-archiving)
  An Awesome List for getting started with web archiving
- [opsxcq/proxy-list](https://github.com/opsxcq/proxy-list)
  A curated list of free public proxy servers
- [ripienaar/free-for-dev](https://github.com/ripienaar/free-for-dev)
  A list of SaaS, PaaS and IaaS offerings that have free tiers of interest to devops and infradev
- [nikitavoloboev/github-stars](https://github.com/nikitavoloboev/github-stars)
  Curated list of my GitHub stars
- [serhii-londar/open-source-mac-os-apps](https://github.com/serhii-londar/open-source-mac-os-apps)
  🚀 Awesome list of open source applications for macOS.
- [menisadi/awesome-differential-privacy](https://github.com/menisadi/awesome-differential-privacy)
  list of differential-privacy related repositories
- [paralax/awesome-internet-scanning](https://github.com/paralax/awesome-internet-scanning)
  A curated list of awesome Internet port and host scanners, plus related components and much more, with a focus on free and open source projects.
- [clehner/awesome-dat](https://github.com/clehner/awesome-dat)
  Community curated resources for Dat Project
- [rickyplouis/decentralizedhacks](https://github.com/rickyplouis/decentralizedhacks)
  A list of known crypto hacks
- [moshilangzi/Awesome-Incremental-Learning](https://github.com/moshilangzi/Awesome-Incremental-Learning)
  Awesome Incremental Learning
- [dhamaniasad/HeadlessBrowsers](https://github.com/dhamaniasad/HeadlessBrowsers)
  A list of (almost) all headless web browsers in existence
- [sindresorhus/awesome-npm](https://github.com/sindresorhus/awesome-npm)
  Awesome npm resources and tips
- [Pinperepette/awesome-security](https://github.com/Pinperepette/awesome-security)
  A collection of awesome software, libraries, documents, books, resources and cools stuffs about security.
- [zero-to-mastery/resources](https://github.com/zero-to-mastery/resources)
  Here is a list of best resources for the developers (mostly web). Feel free to add your resources as well because sharing is caring 
- [mathisonian/awesome-visualization-research](https://github.com/mathisonian/awesome-visualization-research)
  A list of recommended research papers and other readings on data visualization
- [EmilHvitfeldt/r-color-palettes](https://github.com/EmilHvitfeldt/r-color-palettes)
  Comprehensive list of color palettes available in r ❤️🧡💛💚💙💜
- [facert/awesome-spider](https://github.com/facert/awesome-spider)
  爬虫集合
- [leereilly/games](https://github.com/leereilly/games)
  :video_game: A list of popular/awesome videos games, add-ons, maps, etc. hosted on GitHub. Any genre. Any platform. Any engine.
- [joshbuchea/HEAD](https://github.com/joshbuchea/HEAD)
  🗿 A list of everything that *could* go in the head of your document
- [alienzhou/frontend-tech-list](https://github.com/alienzhou/frontend-tech-list)
  📝 Frontend Tech List for Developers 💡
- [daumann/ECMAScript-new-features-list](https://github.com/daumann/ECMAScript-new-features-list)
  A comprehensive list of new ES features, including ES2015 (ES6), ES2016, ES2017, ES2018, ES2019
- [perf-rocks/perf.rocks](https://github.com/perf-rocks/perf.rocks)
  Curated resources that help you build lightning fast websites
- [0nn0/terminal-mac-cheatsheet](https://github.com/0nn0/terminal-mac-cheatsheet)
  List of my most used commands and shortcuts in the terminal for Mac
- [hughrawlinson/hacks.hughrawlinson.me](https://github.com/hughrawlinson/hacks.hughrawlinson.me)
  Nothing to see here. Just a list of some hacks I wanted to put on a website.
- [Kickball/awesome-selfhosted](https://github.com/Kickball/awesome-selfhosted)
  This is a list of Free Software network services and web applications which can be hosted locally. Selfhosting is the process of locally hosting and managing applications instead of renting from SaaS providers.
- [pybites/myreadinglist](https://github.com/pybites/myreadinglist)
  Rewrite of My Reading List (fbreadinglist.com) PHP -> Django
- [dbohdan/structured-text-tools](https://github.com/dbohdan/structured-text-tools)
  A list of command line tools for manipulating structured text data
- [bnb/awesome-developer-streams](https://github.com/bnb/awesome-developer-streams)
  👩🏿‍💻👨🏾‍💻👩🏼‍💻👨🏽‍💻👩🏻‍💻 Awesome Developers, Streaming
- [Pinperepette/awesome-command-line-apps](https://github.com/Pinperepette/awesome-command-line-apps)
  :shell: Use your terminal shell to do awesome things.
- [duyetdev/awesome-web-scraper](https://github.com/duyetdev/awesome-web-scraper)
  A collection of awesome web scaper, crawler.
- [leinov/awesome-web-you-should-know](https://github.com/leinov/awesome-web-you-should-know)
  🌎awesome web you should know
- [sindresorhus/awesome](https://github.com/sindresorhus/awesome)
  😎 Awesome lists about all kinds of interesting topics
- [eflynch/magnolial](https://github.com/eflynch/magnolial)
  Workflowy Inspired Self-Hosted Nested List Webapp
- [Pinperepette/Awesome-Hacking](https://github.com/Pinperepette/Awesome-Hacking)
  A collection of various awesome lists for hackers, pentesters and security researchers
- [Robpol86/terminaltables](https://github.com/Robpol86/terminaltables)
  Generate simple tables in terminals from a nested list of strings.
- [yi-ge/js-tree-list](https://github.com/yi-ge/js-tree-list)
  Convert list to tree, managing a tree and its nodes.
- [bnb/awesome-hyper](https://github.com/bnb/awesome-hyper)
  🖥 Delightful Hyper plugins, themes, and resources
- [fabtools/fabtools](https://github.com/fabtools/fabtools)
  Tools for writing awesome Fabric files
- [moarpepes/awesome-mesh](https://github.com/moarpepes/awesome-mesh)
  This is a list for mesh networking: Documentation, Free Software mesh protocols, and applications. A mesh network is a network topology in which each node relays data for the network. All mesh nodes cooperate in the distribution of data in the network.
- [clehner/awesome-ipfs](https://github.com/clehner/awesome-ipfs)
  Useful resources for using IPFS and building things on top of it
- [textileio/awesome-ipfs](https://github.com/textileio/awesome-ipfs)
  Useful resources for using IPFS and building things on top of it
- [Pinperepette/awesome-osx-command-line](https://github.com/Pinperepette/awesome-osx-command-line)
  Use your OS X terminal shell to do awesome things.
- [LeCoupa/awesome-cheatsheets](https://github.com/LeCoupa/awesome-cheatsheets)
  👩‍💻👨‍💻 Awesome cheatsheets for popular programming languages, frameworks and development tools. They include everything you should know in one single file.
- [vitalysim/Awesome-Hacking-Resources](https://github.com/vitalysim/Awesome-Hacking-Resources)
  A collection of hacking / penetration testing resources to make you better!
- [relatedcode/ParseAlternatives](https://github.com/relatedcode/ParseAlternatives)
  A collaborative list of Parse alternative backend service providers.
- [lorey/github-stars-by-topic](https://github.com/lorey/github-stars-by-topic)
  :star: Generate a list of your GitHub stars by topic - automatically!
- [BruceDone/awesome-crawler](https://github.com/BruceDone/awesome-crawler)
  A collection of awesome web crawler,spider in different languages
- [abesto/duolingo-to-anki](https://github.com/abesto/duolingo-to-anki)
  Download word list from Duolingo, save into Ankit-compatible format
- [kazzkiq/CodeFlask](https://github.com/kazzkiq/CodeFlask)
  A micro code-editor for awesome web pages.
- [sderosiaux/guidelines-to-create-a-strong-website](https://github.com/sderosiaux/guidelines-to-create-a-strong-website)
  A list of all things to consider when making a website or webapp of quality.
- [knadh/listmonk](https://github.com/knadh/listmonk)
  High performance, self-hosted newsletter and mailing list manager with a modern dashboard. Go + React.
- [YaronWittenstein/productivity-tips-for-mac](https://github.com/YaronWittenstein/productivity-tips-for-mac)
  Awesome Productivity Tips for Mac Developers
- [logeshpaul/UI-Components](https://github.com/logeshpaul/UI-Components)
  Most used UI Components — of web applications. Curated/Most loved components for web development
- [voidcosmos/npkill](https://github.com/voidcosmos/npkill)
  List any node_modules directories in your system, as well as the space they take up. You can then select which ones you want to erase to free up space.
- [davidesantangelo/feedi](https://github.com/davidesantangelo/feedi)
  This service allows you to transform RSS feed into an awesome API. 
- [TryCatchHCF/Cloakify](https://github.com/TryCatchHCF/Cloakify)
  CloakifyFactory - Data Exfiltration & Infiltration In Plain Sight; Convert any filetype into list of everyday strings, using Text-Based Steganography; Evade DLP/MLS Devices, Defeat Data Whitelisting Controls, Social Engineering of Analysts, Evade AV Detection
- [mfranzke/datalist-polyfill](https://github.com/mfranzke/datalist-polyfill)
  Minimal and dependency-free vanilla JavaScript polyfill for the awesome datalist-functionality
- [uptick/react-keyed-file-browser](https://github.com/uptick/react-keyed-file-browser)
  Folder based file browser given a flat keyed list of objects, powered by React.
- [athityakumar/colorls](https://github.com/athityakumar/colorls)
  A Ruby gem that beautifies the terminal's ls command, with color and font-awesome icons. :tada:
- [benedekrozemberczki/awesome-graph-classification](https://github.com/benedekrozemberczki/awesome-graph-classification)
  A collection of important graph embedding, classification and representation learning papers with implementations.
- [halgatewood/file-directory-list](https://github.com/halgatewood/file-directory-list)
  Free Super Clean PHP File Directory Listing Script
- [mahmoud/awesome-python-applications](https://github.com/mahmoud/awesome-python-applications)
  💿 Free software that works great, and also happens to be open-source Python. 
- [ryanoasis/nerd-fonts](https://github.com/ryanoasis/nerd-fonts)
  Iconic font aggregator, collection, & patcher. 3,600+ icons, 40+ patched fonts: Hack, Source Code Pro, more. Glyph collections: Font Awesome, Material Design Icons, Octicons, & more
- [Hack-with-Github/Awesome-Security-Gists](https://github.com/Hack-with-Github/Awesome-Security-Gists)
  A collection of various GitHub gists for hackers, pentesters and security researchers
- [codebox/reading-list-mover](https://github.com/codebox/reading-list-mover)
  A Python utility for moving bookmarks/reading lists between services
- [dmllr/IdealMedia](https://github.com/dmllr/IdealMedia)
  Awesome app to listen music and audiobooks on the device and online at vk.com. Search, download, set as ringtone, sort by albums, authors, folder. Powerful equalizer.
- [StevenBlack/hosts](https://github.com/StevenBlack/hosts)
  Extending and consolidating hosts files from several well-curated sources like adaway.org, mvps.org, malwaredomainlist.com, someonewhocares.org, and potentially others.  You can optionally invoke extensions to block additional sites by category. 
- [30-seconds/30-seconds-of-css](https://github.com/30-seconds/30-seconds-of-css)
  A curated collection of useful CSS snippets you can understand in 30 seconds or less.
- [benedekrozemberczki/awesome-decision-tree-papers](https://github.com/benedekrozemberczki/awesome-decision-tree-papers)
  A collection of research papers on decision, classification and regression trees with implementations.
- [nginx/nginx](https://github.com/nginx/nginx)
  An official read-only mirror of http://hg.nginx.org/nginx/ which is updated hourly. Pull requests on GitHub cannot be accepted and will be automatically closed. The proper way to submit changes to nginx is via the nginx development mailing list, see http://nginx.org/en/docs/contributing_changes.html
- [ipfs-shipyard/ipfs-companion](https://github.com/ipfs-shipyard/ipfs-companion)
  Browser extension that simplifies access to IPFS resources
- [aonez/awesome-mac](https://github.com/aonez/awesome-mac)
   Now we have become very big, Different from the original idea. Collect premium software in various categories.
- [square/maximum-awesome](https://github.com/square/maximum-awesome)
  Config files for vim and tmux.
- [30-seconds/30-seconds-of-react](https://github.com/30-seconds/30-seconds-of-react)
  Curated collection of useful React snippets that you can understand in 30 seconds or less.
- [30-seconds/30-seconds-of-python](https://github.com/30-seconds/30-seconds-of-python)
  A curated collection of useful Python snippets that you can understand in 30 seconds or less.
- [humanetech-community/awesome-humane-tech](https://github.com/humanetech-community/awesome-humane-tech)
  Promoting Solutions that Improve Wellbeing, Freedom and Society
- [30-seconds/30-seconds-of-interviews](https://github.com/30-seconds/30-seconds-of-interviews)
  A curated collection of common interview questions to help you prepare for your next interview.
- [jakejarvis/awesome-shodan-queries](https://github.com/jakejarvis/awesome-shodan-queries)
  🔍 A collection of interesting, funny, and depressing search queries to plug into https://shodan.io/ 👩‍💻
- [syxanash/awesome-gui-websites](https://github.com/syxanash/awesome-gui-websites)
  Websites, web apps, portfolios which look like desktop computer graphical user interfaces
