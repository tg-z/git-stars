# Repositories defined by: terminal, written, game

also defined by the following keywords: based, emulator, fn5, shell, dashboard, color, play

- [sedwards2009/extraterm](https://github.com/sedwards2009/extraterm)
  The swiss army chainsaw of terminal emulators
- [mtytel/cursynth](https://github.com/mtytel/cursynth)
  GNU ncurses terminal synthesizer
- [Hashnode/hashnode-cli](https://github.com/Hashnode/hashnode-cli)
  Hashnode in your terminal
- [ggrossman/term.js](https://github.com/ggrossman/term.js)
  A terminal written in javascript.
- [chjj/term.js](https://github.com/chjj/term.js)
  A terminal written in javascript.
- [b37t1td/termosaur](https://github.com/b37t1td/termosaur)
  Dinosaur game for terminal.
- [schachmat/wego](https://github.com/schachmat/wego)
  weather app for the terminal
- [gizak/termui](https://github.com/gizak/termui)
  Golang terminal dashboard
- [janko/tic-tac-toe](https://github.com/janko/tic-tac-toe)
  Play tic-tac-toe in your Terminal
- [xtermjs/xterm.js](https://github.com/xtermjs/xterm.js)
  A terminal for the web
- [samtay/tetris](https://github.com/samtay/tetris)
  A terminal interface for Tetris
- [chunqiuyiyu/ervy](https://github.com/chunqiuyiyu/ervy)
  Bring charts to terminal.
- [nitin42/terminal-in-react](https://github.com/nitin42/terminal-in-react)
  👨‍💻  A component that renders a terminal
- [tn5250j/tn5250j](https://github.com/tn5250j/tn5250j)
  A 5250 terminal emulator for the AS/400 written in Java
- [bokub/chalk-animation](https://github.com/bokub/chalk-animation)
  :clapper: Colorful animations in terminal output
- [bfontaine/term2048](https://github.com/bfontaine/term2048)
  :tada: 2048 in your terminal
- [gokcehan/lf](https://github.com/gokcehan/lf)
  Terminal file manager
- [JoelOtter/termloop](https://github.com/JoelOtter/termloop)
  Terminal-based game engine for Go, built on top of Termbox
- [cedricium/TicTacToe](https://github.com/cedricium/TicTacToe)
  :x::o:A fun, two player Terminal-based game written in C++
- [rivo/tview](https://github.com/rivo/tview)
  Rich interactive widgets for terminal-based UIs written in Go
- [DyegoCosta/snake-game](https://github.com/DyegoCosta/snake-game)
  Terminal-based Snake game
- [lecram/rover](https://github.com/lecram/rover)
  simple file browser for the terminal
- [notwaldorf/tiny-care-terminal](https://github.com/notwaldorf/tiny-care-terminal)
  💖💻 A little dashboard that tries to take care of you when you're using your terminal.
- [yudai/gotty](https://github.com/yudai/gotty)
  Share your terminal as a web application
- [0nn0/terminal-mac-cheatsheet](https://github.com/0nn0/terminal-mac-cheatsheet)
  List of my most used commands and shortcuts in the terminal for Mac
- [jesseduffield/lazygit](https://github.com/jesseduffield/lazygit)
  simple terminal UI for git commands
- [nbedos/termtosvg](https://github.com/nbedos/termtosvg)
  Record terminal sessions as SVG animations
- [samtay/snake](https://github.com/samtay/snake)
  A terminal interface for Snake
- [WeiChiaChang/stacks-cli](https://github.com/WeiChiaChang/stacks-cli)
  📊 Analyze website stack from the terminal  💻 
- [zeit/hyper](https://github.com/zeit/hyper)
  A terminal built on web technologies
- [0xPrateek/ci_edit](https://github.com/0xPrateek/ci_edit)
  A terminal text editor with mouse support and ctrl+Q to quit.
- [jarun/ddgr](https://github.com/jarun/ddgr)
  :duck: DuckDuckGo from the terminal
- [zyedidia/micro](https://github.com/zyedidia/micro)
  A modern and intuitive terminal-based text editor
- [8Gitbrix/2048Haskell](https://github.com/8Gitbrix/2048Haskell)
  Play 2048 from the command line /terminal !
- [nojvek/matrix-rain](https://github.com/nojvek/matrix-rain)
   The famous Matrix rain effect of falling green characters in a terminal
- [mbadolato/iTerm2-Color-Schemes](https://github.com/mbadolato/iTerm2-Color-Schemes)
  Over 200 terminal color schemes/themes for iTerm/iTerm2. Includes ports to Terminal, Konsole, PuTTY, Xresources, XRDB, Remina, Termite, XFCE, Tilda, FreeBSD VT, Terminator, Kitty, MobaXterm, LXTerminal, Microsoft's Windows Terminal
- [gdamore/tcell](https://github.com/gdamore/tcell)
  Tcell is an alternate terminal package, similar in some ways to termbox, but better in others.
- [barryclark/bashstrap](https://github.com/barryclark/bashstrap)
  A quick way to spruce up your terminal in OSX.
- [Pinperepette/awesome-command-line-apps](https://github.com/Pinperepette/awesome-command-line-apps)
  :shell: Use your terminal shell to do awesome things.
- [danielgtaylor/tech-talk](https://github.com/danielgtaylor/tech-talk)
  Markdown slideshows with a built-in terminal that just works
- [pwildenhain/terminal_playing_cards](https://github.com/pwildenhain/terminal_playing_cards)
  Python :package: for playing card games in the terminal
- [firecat53/urlscan](https://github.com/firecat53/urlscan)
  Mutt and terminal url selector (similar to urlview)
- [andersevenrud/retro-css-shell-demo](https://github.com/andersevenrud/retro-css-shell-demo)
  AnderShell 3000 - Retro looking terminal in CSS
- [Pinperepette/awesome-osx-command-line](https://github.com/Pinperepette/awesome-osx-command-line)
  Use your OS X terminal shell to do awesome things.
- [HFO4/gameboy.live](https://github.com/HFO4/gameboy.live)
  🕹️ A basic gameboy emulator with terminal "Cloud Gaming" support
- [antonio6643/Terminal-Games](https://github.com/antonio6643/Terminal-Games)
  Random terminal games I make in Python when I'm bored/have spare time at school
- [Swordfish90/cool-retro-term](https://github.com/Swordfish90/cool-retro-term)
  A good looking terminal emulator which mimics the old cathode display...
- [chjj/tty.js](https://github.com/chjj/tty.js)
  A terminal for your browser, using node/express/socket.io
- [ggrossman/tty.js](https://github.com/ggrossman/tty.js)
  A terminal for your browser, using node/express/socket.io
- [mixn/carbon-now-cli](https://github.com/mixn/carbon-now-cli)
  🎨 Beautiful images of your code — from right inside your terminal.
- [Pinperepette/web-terminal](https://github.com/Pinperepette/web-terminal)
  Web-Terminal is a terminal server that provides remote CLI via standard web browser and HTTP protocol.
- [mathiasvr/youtube-terminal](https://github.com/mathiasvr/youtube-terminal)
  :tv: Stream YouTube videos as ascii art in the terminal!
- [hparton/terminal.reddit](https://github.com/hparton/terminal.reddit)
  An online terminal-based interface for reading Reddit built with Vue.js.
- [fdehau/tui-rs](https://github.com/fdehau/tui-rs)
  Build terminal user interfaces and dashboards using Rust
- [gnachman/iTerm2](https://github.com/gnachman/iTerm2)
  iTerm2 is a terminal emulator for Mac OS X that does amazing things.
- [JamesTheHacker/passman](https://github.com/JamesTheHacker/passman)
  PassMan is a secure, and easy to use password manager that runs on the terminal! Written in bash.
- [antonmedv/fx](https://github.com/antonmedv/fx)
  Command-line tool and terminal JSON viewer 🔥
- [mps-youtube/mps-youtube](https://github.com/mps-youtube/mps-youtube)
  Terminal based YouTube player and downloader
- [termux/termux-app](https://github.com/termux/termux-app)
  Android terminal and Linux environment - app repository.
- [mofarrell/p2pvc](https://github.com/mofarrell/p2pvc)
  A point to point color terminal video chat.
- [jarun/nnn](https://github.com/jarun/nnn)
  :dolphin: The missing terminal file manager for X
- [termux/termux-packages](https://github.com/termux/termux-packages)
  Android terminal and Linux environment - packages repository.
- [pirate/terminals-are-sexy](https://github.com/pirate/terminals-are-sexy)
  💥 A curated list of Terminal frameworks, plugins & resources for CLI lovers.
- [k4m4/terminals-are-sexy](https://github.com/k4m4/terminals-are-sexy)
  💥 A curated list of Terminal frameworks, plugins & resources for CLI lovers.
- [Pinperepette/bashstrap](https://github.com/Pinperepette/bashstrap)
  Bootstrap for your terminal. A quick way to spruce up OSX terminal. It cuts out the fluff, adds in timesaving features, and provides a solid foundation for customizing your terminal style. Based on Mathias Bynens epic dotfiles - https://github.com/mathiasbynens/dotfiles
- [termux/termux-styling](https://github.com/termux/termux-styling)
  Termux add-on app for customizing the terminal font and color theme.
- [tautvilas/termpage](https://github.com/tautvilas/termpage)
  Termpage allows you to create neat functional webpages that behave like a terminal
- [gribnoysup/wunderbar](https://github.com/gribnoysup/wunderbar)
  Simple horizontal bar chart printer for your terminal
- [miguelmota/cointop](https://github.com/miguelmota/cointop)
  The fastest and most interactive terminal based UI application for tracking cryptocurrencies
- [hit9/img2txt](https://github.com/hit9/img2txt)
  Image to Ascii Text with color support, can output to html or ansi terminal.
- [athityakumar/colorls](https://github.com/athityakumar/colorls)
  A Ruby gem that beautifies the terminal's ls command, with color and font-awesome icons. :tada:
- [faressoft/terminalizer](https://github.com/faressoft/terminalizer)
  🦄 Record your terminal and generate animated gif images or share a web player
- [jcubic/jquery.terminal](https://github.com/jcubic/jquery.terminal)
  jQuery Terminal Emulator - web based terminal
- [stefanhaustein/TerminalImageViewer](https://github.com/stefanhaustein/TerminalImageViewer)
  Small C++ program to display images in a (modern) terminal using RGB ANSI codes and unicode block graphics characters
- [Rigellute/spotify-tui](https://github.com/Rigellute/spotify-tui)
  Spotify for the terminal written in Rust 🚀
- [michael-lazar/rtv](https://github.com/michael-lazar/rtv)
  Browse Reddit from your terminal
- [unconed/TermKit](https://github.com/unconed/TermKit)
  Experimental Terminal platform built on WebKit + node.js. Currently only for Mac and Windows, though the prototype works 90% in any WebKit browser.
- [allinurl/goaccess](https://github.com/allinurl/goaccess)
  GoAccess is a real-time web log analyzer and interactive viewer that runs in a terminal in *nix systems or through your browser.
- [kroitor/asciichart](https://github.com/kroitor/asciichart)
  Nice-looking lightweight console ASCII line charts ╭┈╯ for NodeJS, browsers and terminal, no dependencies
- [maticzav/emma-cli](https://github.com/maticzav/emma-cli)
  📦 Terminal assistant to find and install node packages.
- [clangen/musikcube](https://github.com/clangen/musikcube)
  a cross-platform, terminal-based music player, audio engine, metadata indexer, and server in c++
- [rbaron/awesomenes](https://github.com/rbaron/awesomenes)
  🎮 A NES emulator written in Go
- [gcla/termshark](https://github.com/gcla/termshark)
  A terminal UI for tshark, inspired by Wireshark
- [wtfutil/wtf](https://github.com/wtfutil/wtf)
  The personal information dashboard for your terminal.
- [andrewimm/wasm-gb](https://github.com/andrewimm/wasm-gb)
  Game Boy emulator in WebAssembly and WebGL 2.0
- [1j01/1bpp](https://github.com/1j01/1bpp)
  one bit per pixel game
- [Pinperepette/terminal-notifier](https://github.com/Pinperepette/terminal-notifier)
  Send User Notifications on Mac OS X 10.8 from the command-line.
- [xaroth8088/SystemPanic](https://github.com/xaroth8088/SystemPanic)
  A game where anything can happen
- [1j01/fish-game](https://github.com/1j01/fish-game)
  A fishy board game. 🐟🐠  🦈
- [svetlyak40wt/dotfiler](https://github.com/svetlyak40wt/dotfiler)
  Shell agnostic git based dotfiles package manager, written in Python.
- [andsens/homeshick](https://github.com/andsens/homeshick)
  git dotfiles synchronizer written in bash
- [icyphox/twsh](https://github.com/icyphox/twsh)
  :bird: a twtxt client written in bash
- [ajminich/Jarvis](https://github.com/ajminich/Jarvis)
  A very simple AI-based personal assistant written in Python.
- [1j01/board-game](https://github.com/1j01/board-game)
  A 2 player 3d board game of chance (and strategy)
- [TiraO/fish-game](https://github.com/TiraO/fish-game)
  A fish game.
- [conversejs/converse.js](https://github.com/conversejs/converse.js)
  Web-based XMPP/Jabber chat client written in JavaScript
- [nirmalrizal/cli-tic-tac-toe](https://github.com/nirmalrizal/cli-tic-tac-toe)
  ⚔️ Multiplayer network based cli tic tac toe game
- [ultitech/GLMaze](https://github.com/ultitech/GLMaze)
  GLMaze is a Windows 95 Maze screensaver clone written in C using OpenGL.
- [1j01/hacky-game](https://github.com/1j01/hacky-game)
  Crazy tech demo game thing with automagical LAN multiplayer
