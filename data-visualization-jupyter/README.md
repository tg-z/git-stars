# Repositories defined by: data, visualization, jupyter

also defined by the following keywords: science, library, server, notebook, big, termite, used

- [fivethirtyeight/oscar-ads](https://github.com/fivethirtyeight/oscar-ads)
  Data on Oscar ads placed by Hollywood studios
- [nteract/transformime](https://github.com/nteract/transformime)
  :truck: Mimetype + data -> HTMLElement
- [tthustla/twitter_sentiment_analysis_part2](https://github.com/tthustla/twitter_sentiment_analysis_part2)
  redefining data-cleaning, preparation for visualisation
- [fivethirtyeight/redistricting-atlas-data](https://github.com/fivethirtyeight/redistricting-atlas-data)
  Data used in The Atlas Of Redistricting
- [uwdata/termite-data-server](https://github.com/uwdata/termite-data-server)
  Data Server for Topic Models
- [HumanCellAtlas/data-browser](https://github.com/HumanCellAtlas/data-browser)
- [uwdata/falcon](https://github.com/uwdata/falcon)
  Brushing and linking for big data
- [fivethirtyeight/ap-election-loader](https://github.com/fivethirtyeight/ap-election-loader)
  A Ruby library for loading election data from The Associated Press into MySQL
- [fivethirtyeight/data](https://github.com/fivethirtyeight/data)
  Data and code behind the articles and graphics at FiveThirtyEight
- [vega/datalib](https://github.com/vega/datalib)
  JavaScript data utility library.
- [mahmoud/glom](https://github.com/mahmoud/glom)
  ☄️ Python's nested data operator (and CLI), for all your declarative restructuring needs. Got data? Glom it! ☄️
- [uwdata/perceptual-kernels](https://github.com/uwdata/perceptual-kernels)
  Data & source code for the perceptual kernels study
- [uwdata/voyager](https://github.com/uwdata/voyager)
  Recommendation-Powered Visualization Tool for Data Exploration
- [uwdata/semantic-colors](https://github.com/uwdata/semantic-colors)
  Selecting semantically-resonant colors for data visualization
- [d4t4x/data-selfie](https://github.com/d4t4x/data-selfie)
  Data Selfie - a browser extension to track yourself on Facebook and analyze your data. 
- [martenbjork/sign-up-for-facebook](https://github.com/martenbjork/sign-up-for-facebook)
  A summary of what data Facebook collects and how it can be used
- [jeroenjanssens/data-science-at-the-command-line](https://github.com/jeroenjanssens/data-science-at-the-command-line)
  Data Science at the Command Line
- [vega/datalib-sketch](https://github.com/vega/datalib-sketch)
  Probabilistic data structures for large or streaming data sets.
- [jakevdp/PythonDataScienceHandbook](https://github.com/jakevdp/PythonDataScienceHandbook)
  Python Data Science Handbook: full text in Jupyter Notebooks
- [datasciencemasters/go](https://github.com/datasciencemasters/go)
  The Open Source Data Science Masters
- [nteract/semiotic](https://github.com/nteract/semiotic)
  A data visualization framework combining React & D3
- [uwdata/visualization-curriculum](https://github.com/uwdata/visualization-curriculum)
  A data visualization curriculum of interactive notebooks.
- [uwdata/visual-encoding-effectiveness-data](https://github.com/uwdata/visual-encoding-effectiveness-data)
  Supplement material for "Assessing Effects of Task and Data Distribution on the Effectiveness of Visual Encodings".
- [nhn/tui.chart](https://github.com/nhn/tui.chart)
  🍞📊  Beautiful chart for data visualization.
- [maxatwork/form2js](https://github.com/maxatwork/form2js)
  Javascript library for collecting form data
- [encode/typesystem](https://github.com/encode/typesystem)
  Data validation, serialization, deserialization & form rendering. 🔢
- [nteract/scrapbook](https://github.com/nteract/scrapbook)
  A library for recording and reading data in notebooks.
- [uwdata/termite-visualizations](https://github.com/uwdata/termite-visualizations)
  [development moved to termite-data-server]
- [beakerbrowser/dat-session-data-ext-msg](https://github.com/beakerbrowser/dat-session-data-ext-msg)
  Methods for DEP-0006: Session Data (Extension Message)
- [uwdata/termite-treetm](https://github.com/uwdata/termite-treetm)
  [development moved to termite-data-server]
- [uwdata/termite-stm](https://github.com/uwdata/termite-stm)
  [development moved to termite-data-server]
- [codeneuro/spikefinder](https://github.com/codeneuro/spikefinder)
  benchmarking challenge for spike detection in imaging data
- [minimaxir/reddit-bigquery](https://github.com/minimaxir/reddit-bigquery)
  Code + Jupyter notebook for analyzing and visualizing Reddit Data quickly and easily
- [mathisonian/awesome-visualization-research](https://github.com/mathisonian/awesome-visualization-research)
  A list of recommended research papers and other readings on data visualization
- [glue-viz/glue](https://github.com/glue-viz/glue)
  Linked Data Visualizations Across Multiple Files
- [dewarim/reddit-data-tools](https://github.com/dewarim/reddit-data-tools)
  Tools to work with the big reddit JSON data dump.
- [schmittjoh/serializer](https://github.com/schmittjoh/serializer)
  Library for (de-)serializing data of any complexity (supports XML, JSON, YAML)
- [algorithmica-repository/datascience](https://github.com/algorithmica-repository/datascience)
  It consists of examples, assignments discussed in data science course taken at algorithmica.
- [PatMartin/Dex](https://github.com/PatMartin/Dex)
  Dex : The Data Explorer -- A data visualization tool written in Java/Groovy/JavaFX capable of powerful ETL and publishing web visualizations.
- [scrapjs/render](https://github.com/scrapjs/render)
  :eyeglasses: Stream for rendering audio data
- [d3/d3](https://github.com/d3/d3)
  Bring data to life with SVG, Canvas and HTML. :bar_chart::chart_with_upwards_trend::tada:
- [jordanoverbye/gatsby-source-are.na](https://github.com/jordanoverbye/gatsby-source-are.na)
  Source plugin for pulling data into Gatsby from Are.na
- [libretro/libretro-database](https://github.com/libretro/libretro-database)
  Repository containing cheatcode files, content data files, etc.
- [HumanCellAtlas/ingest-file-archiver](https://github.com/HumanCellAtlas/ingest-file-archiver)
  Service for storing HCA sequencing data in the EBI archives.
- [schmittjoh/JMSSerializerBundle](https://github.com/schmittjoh/JMSSerializerBundle)
  Easily serialize, and deserialize data of any complexity (supports XML, JSON, YAML)
- [atomotic/tabula](https://github.com/atomotic/tabula)
  Tabula is a tool for liberating data tables trapped inside PDF files
- [root-project/root](https://github.com/root-project/root)
  The official repository for ROOT: analyzing, storing and visualizing big data, scientifically
- [adereth/counting-stars](https://github.com/adereth/counting-stars)
  Data processing for my Counting Stars on Github post
- [fasouto/awesome-dataviz](https://github.com/fasouto/awesome-dataviz)
  :chart_with_upwards_trend:  A curated list of awesome data visualization libraries and resources.
- [trekhleb/javascript-algorithms](https://github.com/trekhleb/javascript-algorithms)
  📝 Algorithms and data structures implemented in JavaScript with explanations and links to further readings
- [RandomFractals/vscode-data-preview](https://github.com/RandomFractals/vscode-data-preview)
  Data Preview 🈸 extension for importing 📤 viewing 🔎 slicing 🔪 dicing 🎲  charting 📊 & exporting 📥 large JSON array/config, YAML, Apache Arrow, Avro & Excel data files
- [microsoft/presidio](https://github.com/microsoft/presidio)
  Context aware, pluggable and customizable data protection and PII data anonymization service for text and images
- [DenQ/iron-tree](https://github.com/DenQ/iron-tree)
  Tree (data structure). Many methods
- [nteract/hydrogen](https://github.com/nteract/hydrogen)
  :atom: Run code interactively, inspect data, and plot. All the power of Jupyter kernels, inside your favorite text editor.
- [uwdata/imMens](https://github.com/uwdata/imMens)
  Real-Time Visual Querying of Big Data
- [dbohdan/structured-text-tools](https://github.com/dbohdan/structured-text-tools)
  A list of command line tools for manipulating structured text data
- [uwdata/visual-embedding](https://github.com/uwdata/visual-embedding)
   Data & source code for the visual embedding model
- [textileio/textile-facebook](https://github.com/textileio/textile-facebook)
  simple parsing tool to get your data out of a facebook export
- [scrapy/parsel](https://github.com/scrapy/parsel)
  Parsel lets you extract data from XML/HTML documents using XPath or CSS selectors
- [bokeh/bokeh](https://github.com/bokeh/bokeh)
  Interactive Data Visualization in the browser, from  Python
- [fivethirtyeight/victory](https://github.com/fivethirtyeight/victory)
  A collection of composable React components for building interactive data visualizations
- [PrivateBin/PrivateBin](https://github.com/PrivateBin/PrivateBin)
  A minimalist, open source online pastebin where the server has zero knowledge of pasted data. Data is encrypted/decrypted in the browser using 256 bits AES.
- [Pinperepette/twitter_geolocation](https://github.com/Pinperepette/twitter_geolocation)
  extracts geolocation data from twitter user locations and tweets
- [ankitrohatgi/WebPlotDigitizer](https://github.com/ankitrohatgi/WebPlotDigitizer)
  HTML5 based online tool to extract numerical data from plot images.
- [kjam/python-web-scraping-tutorial](https://github.com/kjam/python-web-scraping-tutorial)
  A Python-based web and data scraping tutorial
- [TryCatchHCF/Cloakify](https://github.com/TryCatchHCF/Cloakify)
  CloakifyFactory - Data Exfiltration & Infiltration In Plain Sight; Convert any filetype into list of everyday strings, using Text-Based Steganography; Evade DLP/MLS Devices, Defeat Data Whitelisting Controls, Social Engineering of Analysts, Evade AV Detection
- [ernw/nmap-parse-output](https://github.com/ernw/nmap-parse-output)
  Converts/manipulates/extracts data from a Nmap scan output.
- [uwdata/aggregate-animation-data](https://github.com/uwdata/aggregate-animation-data)
  Supplement material for "Designing Animated Transitions to Convey Aggregate Operations"
- [unrolled/render](https://github.com/unrolled/render)
  Go package for easily rendering JSON, XML, binary data, and HTML templates responses.
- [alexander-hamme/Tadpole-Tracker-Python](https://github.com/alexander-hamme/Tadpole-Tracker-Python)
  Research project that tracks and records movement data of many Xenopus laevis tadpoles at once, in real time.
- [ricklamers/gridstudio](https://github.com/ricklamers/gridstudio)
  Grid studio is a web-based application for data science with full integration of open source data science frameworks and languages.
- [tinwan/vue2-data-tree](https://github.com/tinwan/vue2-data-tree)
  A tree that data is lazy loaded. Support dragging node, checking node, editing node's name and selecting node.
- [karthikeyankc/HistoryAnalyzer](https://github.com/karthikeyankc/HistoryAnalyzer)
  A Python script to grab some tasty data from your Chrome's history and analyze it.
- [HumanCellAtlas/bronze](https://github.com/HumanCellAtlas/bronze)
  A minimal alarm that reads data from Google Sheets and alerts people via Slack
- [spatie/laravel-view-components](https://github.com/spatie/laravel-view-components)
  A better way to connect data with view rendering in Laravel
- [uwdata/Prefuse](https://github.com/uwdata/Prefuse)
  Prefuse is a set of software tools for creating rich interactive data visualizations in the Java programming language. Prefuse supports a rich set of features for data modeling, visualization, and interaction. It provides optimized data structures for tables, graphs, and trees, a host of layout and visual encoding techniques, and support for animation, dynamic queries, integrated search, and database connectivity. 
- [alferov/array-to-tree](https://github.com/alferov/array-to-tree)
  Convert a plain array of nodes (with pointers to parent nodes) to a nested data structure
- [aalhour/C-Sharp-Algorithms](https://github.com/aalhour/C-Sharp-Algorithms)
  :books: :chart_with_upwards_trend: Plug-and-play class-library project of standard Data Structures and Algorithms in C#
- [datasciencedojo/IntroToTextAnalyticsWithR](https://github.com/datasciencedojo/IntroToTextAnalyticsWithR)
  Public repo for the Data Science Dojo YouTube tutorial series "Introduction to Text Analytics with R".
- [donnemartin/data-science-ipython-notebooks](https://github.com/donnemartin/data-science-ipython-notebooks)
  Data science Python notebooks: Deep learning (TensorFlow, Theano, Caffe, Keras), scikit-learn, Kaggle, big data (Spark, Hadoop MapReduce, HDFS), matplotlib, pandas, NumPy, SciPy, Python essentials, AWS, and various command lines.
- [ritiek/data-science-ipython-notebooks](https://github.com/ritiek/data-science-ipython-notebooks)
  Recently updated with 50 new notebooks! Data science Python notebooks: Deep learning (TensorFlow, Theano, Caffe, Keras), scikit-learn, Kaggle, big data (Spark, Hadoop MapReduce, HDFS), matplotlib, pandas, NumPy, SciPy, Python essentials, AWS, and various command lines.
- [gcarq/inox-patchset](https://github.com/gcarq/inox-patchset)
  Inox patchset tries to provide a minimal Chromium based browser with focus on privacy by disabling data transmission to Google.
- [textileio/photos](https://github.com/textileio/photos)
  Encrypted, secure, decentralized personal data wallet -- technology behind textile.photos
- [emirpasic/gods](https://github.com/emirpasic/gods)
  GoDS (Go Data Structures). Containers (Sets, Lists, Stacks, Maps, Trees), Sets (HashSet, TreeSet, LinkedHashSet), Lists (ArrayList, SinglyLinkedList, DoublyLinkedList), Stacks (LinkedListStack, ArrayStack), Maps (HashMap, TreeMap, HashBidiMap, TreeBidiMap, LinkedHashMap), Trees (RedBlackTree, AVLTree, BTree, BinaryHeap), Comparators, Iterators, Enumerables, Sort, JSON
- [matomo-org/matomo](https://github.com/matomo-org/matomo)
  Liberating Web Analytics. Star us on Github? +1. Matomo is the leading open alternative to Google Analytics that gives you full control over your data. Matomo lets you easily collect data from websites, apps & the IoT and visualise this data and extract insights. Privacy is built-in. We love Pull Requests! 
- [kartik-v/yii2-export](https://github.com/kartik-v/yii2-export)
  A library to export server/db data in various formats (e.g. excel, html, pdf, csv etc.)
- [uwdata/Flare](https://github.com/uwdata/Flare)
  Flare is an ActionScript library for creating visualizations that run in the Adobe Flash Player. From basic charts and graphs to complex interactive graphics, the toolkit supports data management, visual encoding, animation, and interaction techniques. 
- [pydata/pandas-datareader](https://github.com/pydata/pandas-datareader)
  Extract data from a wide range of Internet sources into a pandas DataFrame.
- [beakerbrowser/dat-archive-map-reduce](https://github.com/beakerbrowser/dat-archive-map-reduce)
  Index files in Dat archives with map-reduce to create queryable data views.
- [moarpepes/awesome-mesh](https://github.com/moarpepes/awesome-mesh)
  This is a list for mesh networking: Documentation, Free Software mesh protocols, and applications. A mesh network is a network topology in which each node relays data for the network. All mesh nodes cooperate in the distribution of data in the network.
- [microlinkhq/metascraper](https://github.com/microlinkhq/metascraper)
  Scrape data from websites using Open Graph metadata, regular HTML metadata, and a series of fallbacks.
- [abhat222/Data-Science--Cheat-Sheet](https://github.com/abhat222/Data-Science--Cheat-Sheet)
  Cheat Sheets
- [ownaginatious/fbchat-archive-parser](https://github.com/ownaginatious/fbchat-archive-parser)
  An application for parsing chat history from a Facebook data archive.
- [leogps/ItunesLibraryParser](https://github.com/leogps/ItunesLibraryParser)
  Itunes Library Parser can parse the Itunes library, read the Tracks & Playlists as Java objects and thereby perform all sorts of operations on the files such as read itunes stored data about the tracks and playlists.
- [alexattia/Data-Science-Projects](https://github.com/alexattia/Data-Science-Projects)
  DataScience projects for learning : Kaggle challenges, Object Recognition, Parsing, etc.
- [samyk/evercookie](https://github.com/samyk/evercookie)
  Produces persistent, respawning "super" cookies in a browser, abusing over a dozen techniques. Its goal is to identify users after they've removed standard cookies and other privacy data such as Flash cookies (LSOs), HTML5 storage, SilverLight storage, and others.
- [HumanCellAtlas/metrics](https://github.com/HumanCellAtlas/metrics)
  HCA metrics visualization via Grafana
- [Pinperepette/OSINT-SPY](https://github.com/Pinperepette/OSINT-SPY)
  Performs OSINT scan on email/domain/ip_address/organization using OSINT-SPY. It can be used by Data Miners, Infosec Researchers, Penetration Testers and cyber crime investigator in order to find deep information about their target. If you want to ask something please feel free to reach out to me at sharad@osint-spy.com
- [vega/vega](https://github.com/vega/vega)
  A visualization grammar.
- [donnemartin/dev-setup](https://github.com/donnemartin/dev-setup)
  macOS development environment setup:  Easy-to-understand instructions with automated setup scripts for developer tools like Vim, Sublime Text, Bash, iTerm, Python data analysis, Spark, Hadoop MapReduce, AWS, Heroku, JavaScript web development, Android development, common data stores, and dev-based OS X defaults.
- [NSchrading/redditDataExtractor](https://github.com/NSchrading/redditDataExtractor)
  The reddit Data Extractor is a cross-platform GUI tool for downloading almost any content posted to reddit. Downloads from specific users, specific subreddits, users by subreddit, and with filters on the content is supported. Some intelligence is built in to attempt to avoid downloading duplicate external content.
- [movingobjects/nodes-tadpoles](https://github.com/movingobjects/nodes-tadpoles)
  Lil swimming + linking visualization
- [frontend-collective/react-sortable-tree](https://github.com/frontend-collective/react-sortable-tree)
  Drag-and-drop sortable component for nested data and hierarchies
- [parrt/dtreeviz](https://github.com/parrt/dtreeviz)
  A python library for decision tree visualization and model interpretation.
- [williamngan/pts](https://github.com/williamngan/pts)
  A library for visualization and creative-coding
- [uwdata/draco](https://github.com/uwdata/draco)
  Visualization Constraints and Weight Learning
- [peers/peerjs](https://github.com/peers/peerjs)
  Peer-to-peer data in the browser.
- [Dirkster99/PyNotes](https://github.com/Dirkster99/PyNotes)
  My notebook on using Python with Jupyter Notebook, PySpark etc
- [whichlight/reddit-network-vis](https://github.com/whichlight/reddit-network-vis)
  network visualization of Reddit discussions
- [nteract/ipython-paths](https://github.com/nteract/ipython-paths)
  :city_sunset: Paths for IPython before Jupyter 4.0
- [antvis/g6](https://github.com/antvis/g6)
  A Graph Visualization Framework in JavaScript
- [vega/compassql](https://github.com/vega/compassql)
  CompassQL Query Language for visualization recommendation.
- [geekplux/markvis](https://github.com/geekplux/markvis)
  make visualization in markdown. 📊📈
