# Repositories defined by: files, using, create

also defined by the following keywords: warc, repository, tools, api, project, large, vim

- [ibrahimokdadov/upload_file_python](https://github.com/ibrahimokdadov/upload_file_python)
  Uploading files using Python and Flask
- [mrclay/minify](https://github.com/mrclay/minify)
  Combines. minifies, and serves CSS or Javascript files
- [edwardcapriolo/filecrush](https://github.com/edwardcapriolo/filecrush)
  Remedy small files by combining them into larger ones.
- [cozy/cozy-setup](https://github.com/cozy/cozy-setup)
  Cozy installation files and information
- [libretro/libretro-database](https://github.com/libretro/libretro-database)
  Repository containing cheatcode files, content data files, etc.
- [vuejs/rollup-plugin-vue](https://github.com/vuejs/rollup-plugin-vue)
  Roll .vue files
- [humiaozuzu/dot-vimrc](https://github.com/humiaozuzu/dot-vimrc)
  Maple's vim config files
- [square/maximum-awesome](https://github.com/square/maximum-awesome)
  Config files for vim and tmux.
- [2d-inc/Flare-Flutter](https://github.com/2d-inc/Flare-Flutter)
  Load and get full control of your Flare files in a Flutter project using this library.
- [atom/fuzzy-finder](https://github.com/atom/fuzzy-finder)
  Find and open files quickly
- [fabtools/fabtools](https://github.com/fabtools/fabtools)
  Tools for writing awesome Fabric files
- [nygard/class-dump](https://github.com/nygard/class-dump)
  Generate Objective-C headers from Mach-O files.
- [FileZ/FileZ](https://github.com/FileZ/FileZ)
  Share large files with unique URL
- [git-lfs/git-lfs](https://github.com/git-lfs/git-lfs)
  Git extension for versioning large files
- [mbloch/mapshaper](https://github.com/mbloch/mapshaper)
  Tools for editing Shapefile, GeoJSON, TopoJSON and CSV files
- [makinacorpus/Leaflet.FileLayer](https://github.com/makinacorpus/Leaflet.FileLayer)
  Loads files locally (GeoJSON, KML, GPX) as layers using HTML5 File API
- [atomotic/tabula](https://github.com/atomotic/tabula)
  Tabula is a tool for liberating data tables trapped inside PDF files
- [sannies/mp4parser](https://github.com/sannies/mp4parser)
  A Java API to read, write and create MP4 files
- [1j01/anypalette.js](https://github.com/1j01/anypalette.js)
  🌈🎨🏳‍🌈 Load all kinds of palette files 💄🧡🎃💛🍋🍏💚📗💙📘🔮💜
- [cozy/cozy-files](https://github.com/cozy/cozy-files)
  This repository was part of CozyV2 which has been deprecated
- [afterlogic/aurora-files](https://github.com/afterlogic/aurora-files)
  Aurora Files is an open-source file storage platform.
- [square/javapoet](https://github.com/square/javapoet)
  A Java API for generating .java source files.
- [superdima05/tidalgrabber](https://github.com/superdima05/tidalgrabber)
  If you have a lossless subscription, and you want to get .flac files, you can use this python script.
- [internetarchive/warc](https://github.com/internetarchive/warc)
  Python library for reading and writing warc files
- [learn-co-students/js-if-else-files-lab-bootcamp-prep-000](https://github.com/learn-co-students/js-if-else-files-lab-bootcamp-prep-000)
- [machawk1/warcreate](https://github.com/machawk1/warcreate)
  Chrome extension to "Create WARC files from any webpage"
- [harelba/q](https://github.com/harelba/q)
  q - Run SQL directly on CSV or TSV files  
- [jordansamuel/PASTE](https://github.com/jordansamuel/PASTE)
  Paste is a project that started from the files pastebin.com used before it was bought.
- [FileNation/FileNation](https://github.com/FileNation/FileNation)
  The simplest way to send your files around the world using IPFS. ✏️ 🗃
- [svg/svgo](https://github.com/svg/svgo)
  :tiger: Node.js tool for optimizing SVG files
- [glue-viz/glue](https://github.com/glue-viz/glue)
  Linked Data Visualizations Across Multiple Files
- [andysturrock/find-duplicate-files](https://github.com/andysturrock/find-duplicate-files)
  Python scripts for finding duplicate files
- [dogukanerkut/bookmark-everything](https://github.com/dogukanerkut/bookmark-everything)
  This tool enables you to add bookmarks to your project files so you can reach them easily.
- [N0taN3rd/node-warc](https://github.com/N0taN3rd/node-warc)
  Parse And Create Web ARChive  (WARC) files with node.js
- [CenterForOpenScience/modular-file-renderer](https://github.com/CenterForOpenScience/modular-file-renderer)
  A Python package for rendering files to HTML via an embeddable iframe
- [PDF-Archiver/PDF-Archiver](https://github.com/PDF-Archiver/PDF-Archiver)
  A tool for tagging files and archiving tasks.
- [webrecorder/warcit](https://github.com/webrecorder/warcit)
  Convert Directories, Files and ZIP Files to Web Archives (WARC)
- [mholt/archiver](https://github.com/mholt/archiver)
  Easily create & extract archives, and compress & decompress files of various formats
- [tabulapdf/tabula-java](https://github.com/tabulapdf/tabula-java)
  Extract tables from PDF files
- [rakyll/drive](https://github.com/rakyll/drive)
  Pull or push Google Drive files
- [cryptomator/cryptomator](https://github.com/cryptomator/cryptomator)
  Multi-platform transparent client-side encryption of your files in the cloud
- [UnnoTed/fileb0x](https://github.com/UnnoTed/fileb0x)
  a better customizable tool to embed files in go; also update embedded files remotely without restarting the server
- [rossem/RedditStorage](https://github.com/rossem/RedditStorage)
  Store files onto reddit subreddits.
- [mathiasbynens/dotfiles](https://github.com/mathiasbynens/dotfiles)
  :wrench: .files, including ~/.macos — sensible hacker defaults for macOS
- [RaRe-Technologies/smart_open](https://github.com/RaRe-Technologies/smart_open)
  Utils for streaming large files (S3, HDFS, gzip, bz2...)
- [Pinperepette/dotfiles-2](https://github.com/Pinperepette/dotfiles-2)
  .files, including ~/.osx — sensible hacker defaults for OS X
- [eugeneware/warc](https://github.com/eugeneware/warc)
  Parse WARC (Web Archive Files) as a node.js stream
- [JasonHinds13/PyFiling](https://github.com/JasonHinds13/PyFiling)
  Python script that organizes files in a folder or directory according to file type/extension.
- [briankendall/check-time-machine](https://github.com/briankendall/check-time-machine)
  Python script for checking to make sure all of your files are actually backed up by Time Machine, and offers options for fixing files that are not.
- [gjtorikian/html-proofer](https://github.com/gjtorikian/html-proofer)
  Test your rendered HTML files to make sure they're accurate.
- [feross/bitmidi.com](https://github.com/feross/bitmidi.com)
  🎹 Listen to free MIDI songs, download the best MIDI files, and share the best MIDIs on the web
- [tmk907/PlaylistsNET](https://github.com/tmk907/PlaylistsNET)
  Simple library for reading and writing playlist's files. Supported formats: m3u, pls, wpl, zpl.
- [ikreymer/webarchive-indexing](https://github.com/ikreymer/webarchive-indexing)
  Tools for bulk indexing of WARC/ARC files on Hadoop, EMR or local file system.
- [calibr/node-bookmarks-parser](https://github.com/calibr/node-bookmarks-parser)
  Parses Firefox/Chrome HTML bookmarks files
- [srod/node-minify](https://github.com/srod/node-minify)
  Light Node.js module that compress javascript, css and html files
- [SeSc838/txt2csv_python](https://github.com/SeSc838/txt2csv_python)
  little helper to convert txt to csv files
- [vpistis/OrganizeMediaFiles](https://github.com/vpistis/OrganizeMediaFiles)
  a collection of Python scripts that help you organize media files into a directory tree "year/month" based on metadata , using exiftool
- [Dirkster99/MachineLearningWithPython](https://github.com/Dirkster99/MachineLearningWithPython)
  Starter files for Pluralsight course: Understanding Machine Learning with Python
- [IMGIITRoorkee/django-filemanager](https://github.com/IMGIITRoorkee/django-filemanager)
  A simple Django app to browse files on server
- [mholt/PapaParse](https://github.com/mholt/PapaParse)
  Fast and powerful CSV (delimited text) parser that gracefully handles large files and malformed input
- [beakerbrowser/dat-archive-map-reduce](https://github.com/beakerbrowser/dat-archive-map-reduce)
  Index files in Dat archives with map-reduce to create queryable data views.
- [arso-project/archipel](https://github.com/arso-project/archipel)
  An app to share archives of files and folders in a peer to peer network
- [Augustyniak/FileExplorer](https://github.com/Augustyniak/FileExplorer)
  FileExplorer is a powerful iOS file browser that allows its users to choose and remove files and/or directories
- [Pierian-Data/Complete-Python-3-Bootcamp](https://github.com/Pierian-Data/Complete-Python-3-Bootcamp)
  Course Files for Complete Python 3 Bootcamp Course on Udemy
- [RandomFractals/vscode-data-preview](https://github.com/RandomFractals/vscode-data-preview)
  Data Preview 🈸 extension for importing 📤 viewing 🔎 slicing 🔪 dicing 🎲  charting 📊 & exporting 📥 large JSON array/config, YAML, Apache Arrow, Avro & Excel data files
- [naucon/File](https://github.com/naucon/File)
  This package contains php classes to access, change, copy, delete, move, rename files and directories.
- [jvilk/BrowserFS](https://github.com/jvilk/BrowserFS)
  BrowserFS is an in-browser filesystem that emulates the Node JS filesystem API and supports storing and retrieving files from various backends.
- [kvpb/.files](https://github.com/kvpb/.files)
  Runcoms, configuration files, setups, scripts, templates and hacks.
- [aziz/SublimeFileBrowser](https://github.com/aziz/SublimeFileBrowser)
  Ditch sidebar and browse your files in a normal tab with keyboard, like a pro!
- [adrienjoly/npm-pdfreader](https://github.com/adrienjoly/npm-pdfreader)
  🚜 Read text and parse tables from PDF files. Includes automatic column detection, and rule-based parsing.
- [KhashayarDanesh/codesync](https://github.com/KhashayarDanesh/codesync)
  A docker image for performing live files sync between a container and the host
- [rclone/rclone](https://github.com/rclone/rclone)
  "rsync for cloud storage" - Google Drive, Amazon Drive, S3, Dropbox, Backblaze B2, One Drive, Swift, Hubic, Cloudfiles, Google Cloud Storage, Yandex Files
- [victordomingos/Count-files](https://github.com/victordomingos/Count-files)
  A CLI utility written in Python to help you count files, grouped by extension, in a directory. By default, it will count files recursively in current working directory and all of its subdirectories, and will display a table showing the frequency for each file extension (e.g.: .txt, .py, .html, .css) and the total number of files found.
- [1j01/BrowserFS](https://github.com/1j01/BrowserFS)
  "BrowserFS is an in-browser filesystem that emulates the Node JS filesystem API and supports storing and retrieving files from various backends." It's pretty cool. I'm using it for https://98.js.org/
- [textileio/minimal-client-demo](https://github.com/textileio/minimal-client-demo)
  Simple project with minimal http client functionality to add files to an existing local thread
- [chrislusf/seaweedfs](https://github.com/chrislusf/seaweedfs)
  SeaweedFS is a simple and highly scalable distributed file system. There are two objectives:  to store billions of files! to serve the files fast! SeaweedFS implements an object store with O(1) disk seek and an optional Filer with POSIX interface, supporting S3 API, Rack-Aware Erasure Coding for warm storage, FUSE mount, Hadoop compatible, WebDAV.
- [cpojer/mootools-filemanager](https://github.com/cpojer/mootools-filemanager)
  A filemanager for the web based on MooTools that allows you to (pre)view, upload and modify files and folders via the browser.
- [gildas-lormeau/SingleFileZ](https://github.com/gildas-lormeau/SingleFileZ)
  SingleFileZ is a Web Extension for saving web pages as self-extracting HTML/ZIP hybrid files.
- [StevenBlack/hosts](https://github.com/StevenBlack/hosts)
  Extending and consolidating hosts files from several well-curated sources like adaway.org, mvps.org, malwaredomainlist.com, someonewhocares.org, and potentially others.  You can optionally invoke extensions to block additional sites by category. 
- [MCApollo/Athena-project](https://github.com/MCApollo/Athena-project)
  makepkg like build-files for cross-building iOS packages (Moved to https://github.com/MCApollo/repo)
- [prasathmani/tinyfilemanager](https://github.com/prasathmani/tinyfilemanager)
  Web based File Manager in single PHP file, Manage your files efficiently and easily with Tiny File Manager
- [AshveenBansal98/Python-Text-Search](https://github.com/AshveenBansal98/Python-Text-Search)
   A search engine for searching text/phrase in set of .txt files, based on inverted indexing and boyer moore algorithm. 
- [TND/django-files-widget](https://github.com/TND/django-files-widget)
  Django AJAX upload widget and model field for multiple files or images, featuring drag & drop uploading, upload progress bar, sortable image gallery
- [leogps/ItunesLibraryParser](https://github.com/leogps/ItunesLibraryParser)
  Itunes Library Parser can parse the Itunes library, read the Tracks & Playlists as Java objects and thereby perform all sorts of operations on the files such as read itunes stored data about the tracks and playlists.
- [kefranabg/readme-md-generator](https://github.com/kefranabg/readme-md-generator)
  📄 CLI that generates beautiful README.md files
- [joesmithjaffa/jenkins-shell](https://github.com/joesmithjaffa/jenkins-shell)
  Automating Jenkins Hacking using Shodan API
- [zinas/the-90s-polyfil](https://github.com/zinas/the-90s-polyfil)
  Only a true visionary could think about using one of the most modern tools the web platform offers, to re-create some of the most horrific things ever created
- [wkhtmltopdf/wkhtmltopdf](https://github.com/wkhtmltopdf/wkhtmltopdf)
  Convert HTML to PDF using Webkit (QtWebKit)
- [tutorialzine/cute-files](https://github.com/tutorialzine/cute-files)
  A command line utility that turns the current working directory into a pretty online file browser
- [ukwa/webarchive-discovery](https://github.com/ukwa/webarchive-discovery)
  WARC and ARC indexing and discovery tools.
- [RelaxedJS/ReLaXed](https://github.com/RelaxedJS/ReLaXed)
  Create PDF documents using web technologies
- [philipbl/duplicate-images](https://github.com/philipbl/duplicate-images)
  A script to find and delete duplicate images using pHash.
- [esc0rtd3w/wifi-hacker](https://github.com/esc0rtd3w/wifi-hacker)
  Shell Script For Attacking Wireless Connections Using Built-In Kali Tools. Supports All Securities (WEP, WPS, WPA, WPA2)
- [vim/vim](https://github.com/vim/vim)
  The official Vim repository
- [teticio/Deej-A.I.](https://github.com/teticio/Deej-A.I.)
  Create automatic playlists by using Deep Learning to *listen* to the music
- [ianepperson/telnetsrvlib](https://github.com/ianepperson/telnetsrvlib)
  Telnet server using gevent
- [basicallydan/forkability](https://github.com/basicallydan/forkability)
  :fork_and_knife: A linter for your repository.
- [zeit/now-desktop](https://github.com/zeit/now-desktop)
  An example of building a desktop application using the Now API
- [oduwsdl/ipwb](https://github.com/oduwsdl/ipwb)
  InterPlanetary Wayback: A distributed and persistent archive replay system using IPFS
- [WiedenKennedyNYC/Internal-SAG](https://github.com/WiedenKennedyNYC/Internal-SAG)
  CLI to create animated SVGs from directory of frames. These work similar to GIFs, but have better colour spectrum, and on the whole lower file size when using compressed JPGs
- [veltman/flubber](https://github.com/veltman/flubber)
  Tools for smoother shape animations.
- [iview/iview-cli](https://github.com/iview/iview-cli)
  Create an iView project in visual way
- [atomotic/public-web-archives](https://github.com/atomotic/public-web-archives)
  A listing of world wide web archives, for humans and machines using Web Archive Manifest (WAM) yaml format
- [ikreymer/cdx-index-client](https://github.com/ikreymer/cdx-index-client)
  A command-line tool for using CommonCrawl Index API at http://index.commoncrawl.org/
- [guardianproject/PixelKnot](https://github.com/guardianproject/PixelKnot)
  Image stego app using the F5 algorithm
- [atomotic/uv-app-starter](https://github.com/atomotic/uv-app-starter)
  Create a IIIF-enabled website using universalviewer.io and host it for free on Github Pages
- [damonbauer/npm-build-boilerplate](https://github.com/damonbauer/npm-build-boilerplate)
  A collection of packages that build a website using npm scripts.
- [webrecorder/cdxj-indexer](https://github.com/webrecorder/cdxj-indexer)
  CDXJ Indexing of WARC/ARCs
- [Zeecka/PassGen](https://github.com/Zeecka/PassGen)
  Wordlist Generator using key-words, options and recursivity
- [digitalmethodsinitiative/trackmap](https://github.com/digitalmethodsinitiative/trackmap)
  Scripts needed to support Trackography project
- [alexgibson/wavepad](https://github.com/alexgibson/wavepad)
  An experimental synthesizer built using the Web Audio API (PWA).
- [39aldo39/spaRSS-DecSync](https://github.com/39aldo39/spaRSS-DecSync)
  Android application to sync RSS without a server using DecSync
- [internetarchive/warcprox](https://github.com/internetarchive/warcprox)
  WARC writing MITM HTTP/S proxy
- [notwaldorf/tiny-care-terminal](https://github.com/notwaldorf/tiny-care-terminal)
  💖💻 A little dashboard that tries to take care of you when you're using your terminal.
- [eloyz/reddit](https://github.com/eloyz/reddit)
  Scrapy (Python Framework) Example using reddit.com
- [ultitech/GLMaze](https://github.com/ultitech/GLMaze)
  GLMaze is a Windows 95 Maze screensaver clone written in C using OpenGL.
- [clehner/awesome-ipfs](https://github.com/clehner/awesome-ipfs)
  Useful resources for using IPFS and building things on top of it
- [textileio/awesome-ipfs](https://github.com/textileio/awesome-ipfs)
  Useful resources for using IPFS and building things on top of it
- [Pinperepette/tweetfeels](https://github.com/Pinperepette/tweetfeels)
  Real-time sentiment analysis in Python using twitter's streaming api
