# Repositories defined by: cli, tool, reddit

also defined by the following keywords: package, ip, based, json, docker, service, websites

- [atomotic/wikidata_suggest](https://github.com/atomotic/wikidata_suggest)
  a CLI suggestion tool for Wikidata entities
- [Pinperepette/tg](https://github.com/Pinperepette/tg)
  tg cli
- [vysheng/tg](https://github.com/vysheng/tg)
  telegram-cli
- [tiagocoutinho/multivisor](https://github.com/tiagocoutinho/multivisor)
  Centralized supervisor WebUI and CLI
- [s-p-k/lud](https://github.com/s-p-k/lud)
  cli tool to search Urban Dictionary
- [sachaos/todoist](https://github.com/sachaos/todoist)
  Todoist CLI Client. I ❤️ Todoist and CLI.
- [docker/cli](https://github.com/docker/cli)
  The Docker CLI
- [codimd/cli](https://github.com/codimd/cli)
  A tiny CLI for CodiMD
- [HumanCellAtlas/zentool](https://github.com/HumanCellAtlas/zentool)
  CLI tool for working with ZenHub / GitHub / Google Sheets
- [ritiek/url-decoder](https://github.com/ritiek/url-decoder)
  CLI tool to decode URLs back to readable form
- [wolfg1969/oh-my-stars](https://github.com/wolfg1969/oh-my-stars)
  An offline CLI tool to search your GitHub Stars.
- [jspm/jspm-cli](https://github.com/jspm/jspm-cli)
  Package management CLI
- [you-dont-need/You-Dont-Need-GUI](https://github.com/you-dont-need/You-Dont-Need-GUI)
  Stop relying on GUI; CLI **ROCKS**
- [nirmalrizal/cli-tic-tac-toe](https://github.com/nirmalrizal/cli-tic-tac-toe)
  ⚔️ Multiplayer network based cli tic tac toe game
- [Cotix/cReddit](https://github.com/Cotix/cReddit)
  CLI Reddit client written in C. Oh, crossplatform too!
- [dutiyesh/chrome-extension-cli](https://github.com/dutiyesh/chrome-extension-cli)
  🚀 The CLI for your next Chrome Extension
- [CognitionGuidedSurgery/cli2web](https://github.com/CognitionGuidedSurgery/cli2web)
  CLI application as a Service via Restful HTTP
- [exercism/cli](https://github.com/exercism/cli)
  A Go based command line tool for exercism.io.
- [sindresorhus/meow](https://github.com/sindresorhus/meow)
  CLI app helper
- [Y2Z/monolith](https://github.com/Y2Z/monolith)
  :black_large_square: CLI tool for saving complete web pages as a single HTML file
- [ldn-softdev/jtc](https://github.com/ldn-softdev/jtc)
  JSON manipulation and transformation tool
- [Pinperepette/pirateflix](https://github.com/Pinperepette/pirateflix)
  :movie_camera: Stream piratebay movies directly from CLI
- [hemanta212/blogger-cli](https://github.com/hemanta212/blogger-cli)
  A cli tool to convert and manage jupyter notebook blogs. Proudly host your notebooks even as a static site. 
- [mkideal/cli](https://github.com/mkideal/cli)
  CLI - A package for building command line app with go
- [Pinperepette/PyPhisher](https://github.com/Pinperepette/PyPhisher)
  A simple python tool for phishing
- [hoop33/limo](https://github.com/hoop33/limo)
  A CLI for managing starred Git repositories
- [Hashnode/hashnode-cli](https://github.com/Hashnode/hashnode-cli)
  Hashnode in your terminal
- [mahmoud/glom](https://github.com/mahmoud/glom)
  ☄️ Python's nested data operator (and CLI), for all your declarative restructuring needs. Got data? Glom it! ☄️
- [Pinperepette/cansina](https://github.com/Pinperepette/cansina)
  Web Content Discovery Tool
- [ArtSabintsev/GitFame](https://github.com/ArtSabintsev/GitFame)
  A Swift CLI script that logs your GitHub Stars and Forks.
- [6IX7ine/shodanwave](https://github.com/6IX7ine/shodanwave)
  Shodanwave is a tool for exploring and obtaining information from Netwave IP Camera. 
- [Urigo/graphql-cli](https://github.com/Urigo/graphql-cli)
  📟  Command line tool for common GraphQL development workflows
- [atomiks/reddit-user-analyser](https://github.com/atomiks/reddit-user-analyser)
  Tool to analyse a Reddit user
- [atomotic/pmwct](https://github.com/atomotic/pmwct)
  Poor Man's Web Curator Tool
- [streamlink/streamlink](https://github.com/streamlink/streamlink)
  CLI for extracting streams from various websites to a video player of your choosing
- [gildas-lormeau/SingleFile](https://github.com/gildas-lormeau/SingleFile)
  Web Extension for Firefox/Chrome and CLI tool to save a faithful copy of a complete web page as a single HTML file
- [metachar/Hatch](https://github.com/metachar/Hatch)
  Hatch is a brute force tool that is used to brute force most websites
- [sharkdp/hyperfine](https://github.com/sharkdp/hyperfine)
  A command-line benchmarking tool
- [rgcr/m-cli](https://github.com/rgcr/m-cli)
   Swiss Army Knife for macOS 
- [textileio/node-chat](https://github.com/textileio/node-chat)
  A simple cli chat app using Textile
- [gdrive-org/gdrive](https://github.com/gdrive-org/gdrive)
  Google Drive CLI Client
- [oclif/oclif](https://github.com/oclif/oclif)
  Node.js Open CLI Framework. Built with 💜 by Heroku.
- [jfhbrook/node-ecstatic](https://github.com/jfhbrook/node-ecstatic)
  A static file server middleware that works with core http, express or on the CLI!
- [k4m4/terminals-are-sexy](https://github.com/k4m4/terminals-are-sexy)
  💥 A curated list of Terminal frameworks, plugins & resources for CLI lovers.
- [pirate/terminals-are-sexy](https://github.com/pirate/terminals-are-sexy)
  💥 A curated list of Terminal frameworks, plugins & resources for CLI lovers.
- [webextension-toolbox/webextension-toolbox](https://github.com/webextension-toolbox/webextension-toolbox)
  Small CLI toolbox for cross-browser WebExtension development
- [thehackingsage/hacktronian](https://github.com/thehackingsage/hacktronian)
  All in One Hacking Tool for Linux & Android
- [ipfs-shipyard/ipfs-deploy](https://github.com/ipfs-shipyard/ipfs-deploy)
  Zero-Config CLI to Deploy Static Websites to IPFS
- [sindresorhus/internal-ip-cli](https://github.com/sindresorhus/internal-ip-cli)
  Get your internal IP address
- [1j01/card-game-generator](https://github.com/1j01/card-game-generator)
  🃏🎴 A tool for making/prototyping tabletop games
- [hashicorp/vault](https://github.com/hashicorp/vault)
  A tool for secrets management, encryption as a service, and privileged access management
- [unbug/codelf](https://github.com/unbug/codelf)
  A search tool helps dev to solve the naming things problem.
- [trimstray/sandmap](https://github.com/trimstray/sandmap)
  Nmap on steroids! Simple CLI with the ability to run pure Nmap engine, 31 modules with 459 scan profiles.
- [pod4g/hiper](https://github.com/pod4g/hiper)
  🚀 A statistical analysis tool for performance testing 
- [1j01/pixelweaver](https://github.com/1j01/pixelweaver)
  🎨🎲 Reproducible procedural drawing tool (pre-alpha)
- [joshuaclayton/unused](https://github.com/joshuaclayton/unused)
  A command line tool to identify unused code.
- [Softwire/Workflowy2Web](https://github.com/Softwire/Workflowy2Web)
  A tool for converting a content outline in Workflowy to a working prototype in HTML
- [htmlhint/HTMLHint](https://github.com/htmlhint/HTMLHint)
  ⚙️ The static code analysis tool you need for your HTML
- [uwdata/voyager](https://github.com/uwdata/voyager)
  Recommendation-Powered Visualization Tool for Data Exploration
- [attardi/wikiextractor](https://github.com/attardi/wikiextractor)
  A tool for extracting plain text from Wikipedia dumps
- [gphotosuploader/gphotos-uploader-cli](https://github.com/gphotosuploader/gphotos-uploader-cli)
  Command line tool to mass upload media folders to your google photos account(s) (Mac OS / Linux)
- [InfinetyEs/Nova-Filemanager](https://github.com/InfinetyEs/Nova-Filemanager)
  A Filemanager tool for Laravel Nova
- [npm/cli](https://github.com/npm/cli)
  a package manager for JavaScript, report bugs & get support at:
- [theaveragedude/mediacat](https://github.com/theaveragedude/mediacat)
  Simple tool to grab magnet URLs from the web
- [Pinperepette/web-terminal](https://github.com/Pinperepette/web-terminal)
  Web-Terminal is a terminal server that provides remote CLI via standard web browser and HTTP protocol.
- [WiedenKennedyNYC/Internal-SAG](https://github.com/WiedenKennedyNYC/Internal-SAG)
  CLI to create animated SVGs from directory of frames. These work similar to GIFs, but have better colour spectrum, and on the whole lower file size when using compressed JPGs
- [svg/svgo](https://github.com/svg/svgo)
  :tiger: Node.js tool for optimizing SVG files
- [guarinogabriel/Mac-CLI](https://github.com/guarinogabriel/Mac-CLI)
   OS X command line tools for developers – The ultimate tool to manage your Mac. It provides a huge set of command line commands that automatize the usage of your OS X system.
- [Rajkumrdusad/Tool-X](https://github.com/Rajkumrdusad/Tool-X)
  Tool-X is a kali linux hacking Tool installer. Tool-X developed for termux and other android terminals. using Tool-X you can install almost 263 hacking tools in termux app and other linux based distributions.
- [1j01/jsoneditor](https://github.com/1j01/jsoneditor)
  A web-based tool to view, edit and format JSON
- [sindresorhus/ipify-cli](https://github.com/sindresorhus/ipify-cli)
  Get your public IP address
- [mavispuford/RedditSaveTransfer](https://github.com/mavispuford/RedditSaveTransfer)
  A little tool for pulling saved posts from your Reddit account. 
- [PDF-Archiver/PDF-Archiver](https://github.com/PDF-Archiver/PDF-Archiver)
  A tool for tagging files and archiving tasks.
- [posthtml/posthtml](https://github.com/posthtml/posthtml)
  PostHTML is a tool to transform HTML/XML with JS plugins
- [Pinperepette/OSXAuditor](https://github.com/Pinperepette/OSXAuditor)
  OS X Auditor is a free Mac OS X computer forensics tool
- [alichtman/shallow-backup](https://github.com/alichtman/shallow-backup)
  Git-integrated backup tool for macOS and Linux devs.
- [atomotic/tabula](https://github.com/atomotic/tabula)
  Tabula is a tool for liberating data tables trapped inside PDF files
- [denisidoro/navi](https://github.com/denisidoro/navi)
  An interactive cheatsheet tool for the command-line
- [Xyntax/FileSensor](https://github.com/Xyntax/FileSensor)
  Dynamic file detection tool based on crawler 基于爬虫的动态敏感文件探测工具 
- [antonmedv/fx](https://github.com/antonmedv/fx)
  Command-line tool and terminal JSON viewer 🔥
- [donnemartin/haxor-news](https://github.com/donnemartin/haxor-news)
  Browse Hacker News like a haxor: A Hacker News command line interface (CLI).
- [gskinner/regexr](https://github.com/gskinner/regexr)
  RegExr is a HTML/JS based tool for creating, testing, and learning about Regular Expressions.
- [ankitrohatgi/WebPlotDigitizer](https://github.com/ankitrohatgi/WebPlotDigitizer)
  HTML5 based online tool to extract numerical data from plot images.
- [prettydiff/prettydiff](https://github.com/prettydiff/prettydiff)
  Beautifier and language aware code comparison tool for many languages. It also minifies and a few other things.
- [ndrwhr/tracer](https://github.com/ndrwhr/tracer)
  Simple hacked together tool for creating traced doodles that live on my home page.
- [dylanaraps/neofetch](https://github.com/dylanaraps/neofetch)
  🖼️  A command-line system information tool written in bash 3.2+
- [WuTheFWasThat/vimflowy](https://github.com/WuTheFWasThat/vimflowy)
  An open source productivity tool drawing inspiration from workflowy and vim
- [DocNow/twarc](https://github.com/DocNow/twarc)
  A command line tool (and Python library) for archiving Twitter JSON
- [m4rk3r/s4cmd](https://github.com/m4rk3r/s4cmd)
  Super S3 command line tool
- [textileio/mozaik](https://github.com/textileio/mozaik)
  Mozaïk is a tool based on nodejs / react / d3 to easily craft beautiful dashboards.
- [dogukanerkut/bookmark-everything](https://github.com/dogukanerkut/bookmark-everything)
  This tool enables you to add bookmarks to your project files so you can reach them easily.
- [Warflop/CloudBunny](https://github.com/Warflop/CloudBunny)
  CloudBunny is a tool to capture the real IP of the server that uses a WAF as a proxy or protection. In this tool we used three search engines to search domain information: Shodan, Censys and Zoomeye.
- [adversarialtools/discord-delete](https://github.com/adversarialtools/discord-delete)
  Powerful tool to delete Discord message history.
- [operasoftware/ssh-key-authority](https://github.com/operasoftware/ssh-key-authority)
  A tool for managing SSH key access to any number of servers.
- [electerious/Ackee](https://github.com/electerious/Ackee)
  Self-hosted, Node.js based analytics tool for those who care about privacy.
- [reviewdog/reviewdog](https://github.com/reviewdog/reviewdog)
  :dog: Automated code review tool integrated with any code analysis tools regardless of programming language
- [WeiChiaChang/stacks-cli](https://github.com/WeiChiaChang/stacks-cli)
  📊 Analyze website stack from the terminal  💻 
- [vuejs/vue-cli](https://github.com/vuejs/vue-cli)
  🛠️ Standard Tooling for Vue.js Development
- [n8n-io/n8n](https://github.com/n8n-io/n8n)
  Free node based Workflow Automation Tool. Easily automate tasks across different services.
- [Pinperepette/creepy](https://github.com/Pinperepette/creepy)
  A geolocation OSINT tool. Offers geolocation information gathering through social networking platforms.
- [textileio/textile-facebook](https://github.com/textileio/textile-facebook)
  simple parsing tool to get your data out of a facebook export
- [m3ng9i/IP-resolver](https://github.com/m3ng9i/IP-resolver)
  A command-line tool for getting a domain's IPs from multiple name servers.
- [codingo/Reconnoitre](https://github.com/codingo/Reconnoitre)
  A security tool for multithreaded information gathering and service enumeration whilst building directory structures to store results, along with writing out recommendations for further testing.
- [sharkdp/pastel](https://github.com/sharkdp/pastel)
  A command-line tool to generate, analyze, convert and manipulate colors
- [preactjs/preact-cli](https://github.com/preactjs/preact-cli)
  😺 Your next Preact PWA starts in 30 seconds.
- [danburzo/percollate](https://github.com/danburzo/percollate)
  🌐 → 📖 A command-line tool to turn web pages into beautifully formatted PDFs
- [ritiek/plugin.video.megacmd](https://github.com/ritiek/plugin.video.megacmd)
  A very minimal Kodi addon to stream videos from your MEGA account using MEGAcmd command-line tool
- [node-js-libs/cli](https://github.com/node-js-libs/cli)
  Rapidly build command line apps
- [UnnoTed/fileb0x](https://github.com/UnnoTed/fileb0x)
  a better customizable tool to embed files in go; also update embedded files remotely without restarting the server
- [mitchellh/cli](https://github.com/mitchellh/cli)
  A Go library for implementing command-line interfaces.
- [iview/iview-cli](https://github.com/iview/iview-cli)
  Create an iView project in visual way
- [victordomingos/Count-files](https://github.com/victordomingos/Count-files)
  A CLI utility written in Python to help you count files, grouped by extension, in a directory. By default, it will count files recursively in current working directory and all of its subdirectories, and will display a table showing the frequency for each file extension (e.g.: .txt, .py, .html, .css) and the total number of files found.
- [ikreymer/cdx-index-client](https://github.com/ikreymer/cdx-index-client)
  A command-line tool for using CommonCrawl Index API at http://index.commoncrawl.org/
- [mixn/carbon-now-cli](https://github.com/mixn/carbon-now-cli)
  🎨 Beautiful images of your code — from right inside your terminal.
- [NSchrading/redditDataExtractor](https://github.com/NSchrading/redditDataExtractor)
  The reddit Data Extractor is a cross-platform GUI tool for downloading almost any content posted to reddit. Downloads from specific users, specific subreddits, users by subreddit, and with filters on the content is supported. Some intelligence is built in to attempt to avoid downloading duplicate external content.
- [ivangreene/arena-cli](https://github.com/ivangreene/arena-cli)
  are.na Command Line Interface
- [jefftriplett/url2markdown-cli](https://github.com/jefftriplett/url2markdown-cli)
  :page_with_curl: Fetch a url and translate it to markdown in one command
- [kefranabg/readme-md-generator](https://github.com/kefranabg/readme-md-generator)
  📄 CLI that generates beautiful README.md files
- [mozilla/web-ext](https://github.com/mozilla/web-ext)
  A command line tool to help build, run, and test web extensions
- [martyav/git-CLI-cheat-sheet](https://github.com/martyav/git-CLI-cheat-sheet)
  A cheatsheet for using Git on the command line
- [kenogo/spotify-lyrics-cli](https://github.com/kenogo/spotify-lyrics-cli)
  Migrated to Gitlab, this Github repo will not receive updates.
- [Pinperepette/oh-my-zsh](https://github.com/Pinperepette/oh-my-zsh)
  A community-driven framework for managing your zsh configuration. Includes 120+ optional plugins (rails, git, OSX, hub, capistrano, brew, ant, macports, etc), over 120 themes to spice up your morning, and an auto-update tool so that makes it easy to keep up with the latest updates from the community.
- [todotxt/todo.txt-cli](https://github.com/todotxt/todo.txt-cli)
  ☑️ A simple and extensible shell script for managing your todo.txt file.
- [PatMartin/Dex](https://github.com/PatMartin/Dex)
  Dex : The Data Explorer -- A data visualization tool written in Java/Groovy/JavaFX capable of powerful ETL and publishing web visualizations.
- [robbyrussell/oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)
  🙃 A delightful community-driven (with 1,300+ contributors) framework for managing your zsh configuration. Includes 200+ optional plugins (rails, git, OSX, hub, capistrano, brew, ant, php, python, etc), over 140 themes to spice up your morning, and an auto-update tool so that makes it easy to keep up with the latest updates from the community.
- [webpack/webpack-cli](https://github.com/webpack/webpack-cli)
  Webpack's Command Line Interface
- [Pinperepette/awesome-cli-apps](https://github.com/Pinperepette/awesome-cli-apps)
  A curated list of command line apps
- [agarrharr/awesome-cli-apps](https://github.com/agarrharr/awesome-cli-apps)
  🖥 📊 🕹 🛠 A curated list of command line apps
- [urfave/cli](https://github.com/urfave/cli)
  A simple, fast, and fun package for building command line apps in Go
- [blackdotsh/getIPIntel](https://github.com/blackdotsh/getIPIntel)
  IP Intelligence is a free Proxy VPN TOR and Bad IP detection tool to prevent Fraud, stolen content, and malicious users. Block proxies, VPN connections, web host IPs, TOR IPs, and compromised systems with a simple API. GeoIP lookup available.
- [1j01/suboptimal-research-tool](https://github.com/1j01/suboptimal-research-tool)
  "Research" topics automatically
- [gatewayapps/kamino](https://github.com/gatewayapps/kamino)
  Github issue cloning tool
- [react-native-community/cli](https://github.com/react-native-community/cli)
  React Native command line tools
- [trimstray/the-book-of-secret-knowledge](https://github.com/trimstray/the-book-of-secret-knowledge)
  A collection of inspiring lists, manuals, cheatsheets, blogs, hacks, one-liners, cli/web tools and more.
- [fabiospampinato/webtorrent-cli](https://github.com/fabiospampinato/webtorrent-cli)
  WebTorrent, the streaming torrent client. For the command line.
- [arnellebalane/mdi-cli](https://github.com/arnellebalane/mdi-cli)
  Generate material design icons from the command line
- [twintproject/twint](https://github.com/twintproject/twint)
  An advanced Twitter scraping & OSINT tool written in Python that doesn't use Twitter's API, allowing you to scrape a user's followers, following, Tweets and more while evading most API limitations.
- [maticzav/emma-cli](https://github.com/maticzav/emma-cli)
  📦 Terminal assistant to find and install node packages.
- [ritiek/muxnect](https://github.com/ritiek/muxnect)
  Send input to just about any interactive command-line tool through a local web server
