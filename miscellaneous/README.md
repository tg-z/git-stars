# Repositories defined by: icagicagicagicagicagicagicagicagicagicagicagicagicagicagicag, ls0tls0tls0tls0tls0tls0tls0tls0tls0tls0tls0tls0tls0tls0tls0t, icagicagicagicagicagicagicagicagicagicagicagicagicagicagicb8

also defined by the following keywords: icagicagicagicagicagicagicagicagicagicagicagicagicagicagihwk, music, spotify, windows, player, built, macos

- [Alexander-Miller/treemacs](https://github.com/Alexander-Miller/treemacs)
- [r9y9/wavenet_vocoder](https://github.com/r9y9/wavenet_vocoder)
  WaveNet vocoder
- [carbon-design-system/carbon](https://github.com/carbon-design-system/carbon)
  A design system built by IBM
- [Rigellute/spotify-tui](https://github.com/Rigellute/spotify-tui)
  Spotify for the terminal written in Rust 🚀
- [frontend-collective/react-sortable-tree](https://github.com/frontend-collective/react-sortable-tree)
  Drag-and-drop sortable component for nested data and hierarchies
- [1j01/98](https://github.com/1j01/98)
  💿 Web-based Windows 98 desktop recreation 💾
- [freeCodeCamp/devdocs](https://github.com/freeCodeCamp/devdocs)
  API Documentation Browser
- [omarroth/invidious](https://github.com/omarroth/invidious)
  Invidious is an alternative front-end to YouTube
- [wilfredinni/javascript-cheatsheet](https://github.com/wilfredinni/javascript-cheatsheet)
  Basic Javascript Cheat Sheet
- [learn-co-students/js-if-else-files-lab-bootcamp-prep-000](https://github.com/learn-co-students/js-if-else-files-lab-bootcamp-prep-000)
- [jgthms/bulma](https://github.com/jgthms/bulma)
  Modern CSS framework based on Flexbox
- [usefathom/fathom](https://github.com/usefathom/fathom)
  Fathom. Simple, privacy-focused website analytics. Built with Golang & Preact.
- [vanhauser-thc/thc-hydra](https://github.com/vanhauser-thc/thc-hydra)
  hydra
- [clangen/musikcube](https://github.com/clangen/musikcube)
  a cross-platform, terminal-based music player, audio engine, metadata indexer, and server in c++
- [gribnoysup/wunderbar](https://github.com/gribnoysup/wunderbar)
  Simple horizontal bar chart printer for your terminal
- [altdesktop/playerctl](https://github.com/altdesktop/playerctl)
  🎧 mpris command-line controller and library for spotify, vlc, audacious, bmp, cmus, and others.
- [boramalper/magnetico](https://github.com/boramalper/magnetico)
  Autonomous (self-hosted) BitTorrent DHT search engine suite.
- [nukeop/nuclear](https://github.com/nukeop/nuclear)
  Desktop music player for streaming from free sources
- [voidcosmos/npkill](https://github.com/voidcosmos/npkill)
  List any node_modules directories in your system, as well as the space they take up. You can then select which ones you want to erase to free up space.
- [danburzo/percollate](https://github.com/danburzo/percollate)
  🌐 → 📖 A command-line tool to turn web pages into beautifully formatted PDFs
- [google/web-starter-kit](https://github.com/google/web-starter-kit)
  Web Starter Kit - a workflow for multi-device websites
- [florinpop17/app-ideas](https://github.com/florinpop17/app-ideas)
  A Collection of application ideas which can be used to improve your coding skills.
- [spandanb/ascii_tree](https://github.com/spandanb/ascii_tree)
  Create beautiful ascii trees
- [marktext/marktext](https://github.com/marktext/marktext)
  📝A simple and elegant markdown editor, available for Linux, macOS and Windows.
- [Borewit/music-metadata](https://github.com/Borewit/music-metadata)
  Stream and file based music metadata parser for node. Supporting a wide range of audio and tag formats.
- [1j01/anypalette.js](https://github.com/1j01/anypalette.js)
  🌈🎨🏳‍🌈 Load all kinds of palette files 💄🧡🎃💛🍋🍏💚📗💙📘🔮💜
- [yangshun/tree-node-cli](https://github.com/yangshun/tree-node-cli)
  🌲Lists the contents of directories in a tree-like format, similar to the Linux tree command
- [JasonKessler/scattertext](https://github.com/JasonKessler/scattertext)
  Beautiful visualizations of how language differs among document types.
- [ahmed-dinar/vuex-flash](https://github.com/ahmed-dinar/vuex-flash)
  VueJs Flash Message Component within Vuex
- [Emupedia/emupedia.github.io](https://github.com/Emupedia/emupedia.github.io)
  The purpose of Emupedia is to serve as a nonprofit meta-resource, hub and community for those interested mainly in video game preservation which aims to digitally collect, archive and preserve games and software to make them available online accessible by a user-friendly UI that simulates several retro operating systems for educational purposes.
- [SpaceVim/SpaceVim](https://github.com/SpaceVim/SpaceVim)
  A community-driven modular vim distribution - The ultimate vim configuration
- [andre-simon/highlight](https://github.com/andre-simon/highlight)
  Source code to formatted text converter 
- [eKoopmans/html2pdf.js](https://github.com/eKoopmans/html2pdf.js)
  Client-side HTML-to-PDF rendering using pure JS.
- [BurntSushi/xsv](https://github.com/BurntSushi/xsv)
  A fast CSV command line toolkit written in Rust.
- [rougier/numpy-tutorial](https://github.com/rougier/numpy-tutorial)
  Numpy beginner tutorial
- [zeit/now-examples](https://github.com/zeit/now-examples)
  Examples of ZEIT Now projects you can deploy yourself
- [rgcr/m-cli](https://github.com/rgcr/m-cli)
   Swiss Army Knife for macOS 
- [HoussemCharf/FunUtils](https://github.com/HoussemCharf/FunUtils)
  Some codes i wrote to help me with me with my daily errands ;)
- [divio/django-filer](https://github.com/divio/django-filer)
  File and Image Management Application for django
- [chubin/cheat.sh](https://github.com/chubin/cheat.sh)
  the only cheat sheet you need
- [gildas-lormeau/SingleFile](https://github.com/gildas-lormeau/SingleFile)
  Web Extension for Firefox/Chrome and CLI tool to save a faithful copy of a complete web page as a single HTML file
- [padraigfl/packard-belle](https://github.com/padraigfl/packard-belle)
  Windows 98 React Component Library
- [ipfs-shipyard/ipfs-companion](https://github.com/ipfs-shipyard/ipfs-companion)
  Browser extension that simplifies access to IPFS resources
- [Pinperepette/SCANNER-INURLBR](https://github.com/Pinperepette/SCANNER-INURLBR)
  Advanced search in search engines, enables analysis provided to exploit GET / POST capturing emails & urls, with an internal custom validation junction for each target / url found.
- [drduh/macOS-Security-and-Privacy-Guide](https://github.com/drduh/macOS-Security-and-Privacy-Guide)
  Guide to securing and improving privacy on macOS
- [meodai/color-names](https://github.com/meodai/color-names)
  Massive color dictionary 🌈
- [donnemartin/data-science-ipython-notebooks](https://github.com/donnemartin/data-science-ipython-notebooks)
  Data science Python notebooks: Deep learning (TensorFlow, Theano, Caffe, Keras), scikit-learn, Kaggle, big data (Spark, Hadoop MapReduce, HDFS), matplotlib, pandas, NumPy, SciPy, Python essentials, AWS, and various command lines.
- [Pinperepette/terminal-notifier](https://github.com/Pinperepette/terminal-notifier)
  Send User Notifications on Mac OS X 10.8 from the command-line.
- [ritiek/data-science-ipython-notebooks](https://github.com/ritiek/data-science-ipython-notebooks)
  Recently updated with 50 new notebooks! Data science Python notebooks: Deep learning (TensorFlow, Theano, Caffe, Keras), scikit-learn, Kaggle, big data (Spark, Hadoop MapReduce, HDFS), matplotlib, pandas, NumPy, SciPy, Python essentials, AWS, and various command lines.
- [XAMPPRocky/tokei](https://github.com/XAMPPRocky/tokei)
  A program that allows you to count your code, quickly.
- [react-native-community/cli](https://github.com/react-native-community/cli)
  React Native command line tools
- [intika/Librefox](https://github.com/intika/Librefox)
  Librefox: Firefox with privacy enhancements
- [Zulko/eagle.js](https://github.com/Zulko/eagle.js)
  A hackable slideshow framework built with Vue.js
- [Pinperepette/wig](https://github.com/Pinperepette/wig)
  WebApp Information Gatherer
- [jasonqng/genius-lyrics-search](https://github.com/jasonqng/genius-lyrics-search)
  Python script for searching Genius.com API
- [forwardemail/free-email-forwarding](https://github.com/forwardemail/free-email-forwarding)
  :closed_lock_with_key: Forward Email is the best free email forwarding for custom domains.
- [textileio/mozaik-ext-time](https://github.com/textileio/mozaik-ext-time)
  Mozaïk time/calendar widgets
- [fusic/filebinder](https://github.com/fusic/filebinder)
  Filebinder: Simple file attachment plugin for CakePHP
- [StevenBlack/hosts](https://github.com/StevenBlack/hosts)
  Extending and consolidating hosts files from several well-curated sources like adaway.org, mvps.org, malwaredomainlist.com, someonewhocares.org, and potentially others.  You can optionally invoke extensions to block additional sites by category. 
- [OpenMined/PySyft](https://github.com/OpenMined/PySyft)
  A library for encrypted, privacy preserving deep learning
- [sepandhaghighi/art](https://github.com/sepandhaghighi/art)
  🎨 ASCII art library for Python
- [bnb/awesome-hyper](https://github.com/bnb/awesome-hyper)
  🖥 Delightful Hyper plugins, themes, and resources
- [pytorch/pytorch](https://github.com/pytorch/pytorch)
  Tensors and Dynamic neural networks in Python with strong GPU acceleration
- [werk85/node-html-to-text](https://github.com/werk85/node-html-to-text)
  Advanced html to text converter
- [robinmoisson/staticrypt](https://github.com/robinmoisson/staticrypt)
  Password protect a static HTML page
- [skyepn/telechat-py](https://github.com/skyepn/telechat-py)
  A Python rewrite of the classic Telechat telnet chat server
- [SwagLyrics/SwagLyrics-For-Spotify](https://github.com/SwagLyrics/SwagLyrics-For-Spotify)
  📃 Get lyrics of currently playing Spotify song so you don't sing along with the wrong ones and embarrass yourself later. Very fast.
- [udilia/create-social-network](https://github.com/udilia/create-social-network)
  Create Social Network by running one command. Demo: https://worldexplorer.netlify.com/
- [thibaudgg/video_info](https://github.com/thibaudgg/video_info)
  Get video info from Dailymotion, VK, Vimeo, Wistia and YouTube url.
- [Pinperepette/httpie](https://github.com/Pinperepette/httpie)
  HTTPie is a command line HTTP client, a user-friendly cURL replacement.
- [1j01/elementary.css](https://github.com/1j01/elementary.css)
  elementary OS's stylesheet converted to browser CSS
- [aziz/SublimeFileBrowser](https://github.com/aziz/SublimeFileBrowser)
  Ditch sidebar and browse your files in a normal tab with keyboard, like a pro!
- [davatorium/rofi](https://github.com/davatorium/rofi)
  Rofi: A window switcher, application launcher and dmenu replacement
- [cdown/yturl](https://github.com/cdown/yturl)
  YouTube videos on the command line
- [Rajkumrdusad/Tool-X](https://github.com/Rajkumrdusad/Tool-X)
  Tool-X is a kali linux hacking Tool installer. Tool-X developed for termux and other android terminals. using Tool-X you can install almost 263 hacking tools in termux app and other linux based distributions.
- [shrinerb/shrine](https://github.com/shrinerb/shrine)
  File Attachment toolkit for Ruby applications
- [Alhadis/Accordion](https://github.com/Alhadis/Accordion)
  Silky-smooth accordion widgets with no external dependencies.
- [rohitkumbhar/record-hacks](https://github.com/rohitkumbhar/record-hacks)
  record-hacks is a Java library that will help you track "creative solutions" in your code. 
- [bmatzelle/gow](https://github.com/bmatzelle/gow)
  Unix command line utilities installer for Windows.
- [quicktype/quicktype](https://github.com/quicktype/quicktype)
  Generate types and converters from JSON, Schema, and GraphQL
- [idank/explainshell](https://github.com/idank/explainshell)
  match command-line arguments to their help text
- [pirate/fish-functions](https://github.com/pirate/fish-functions)
  :wrench: My utility belt of fish functions, writing these has saved me many hours in the long run... I hope...
- [hukaibaihu/vue-org-tree](https://github.com/hukaibaihu/vue-org-tree)
  A simple organization tree based on Vue2.x
- [PaddlePaddle/ERNIE](https://github.com/PaddlePaddle/ERNIE)
  An Implementation of ERNIE For Language Understanding (including Pre-training models and Fine-tuning tools)
- [internetarchive/warcprox](https://github.com/internetarchive/warcprox)
  WARC writing MITM HTTP/S proxy
- [dmstern/html2biblatex](https://github.com/dmstern/html2biblatex)
  A tiny bookmarklet for exporting web pages to BibLaTeX (all browsers / no installation).
