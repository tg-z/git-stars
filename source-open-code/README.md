# Repositories defined by: source, open, code

also defined by the following keywords: platform, free, privacy, flash, php, project, built

- [nicholas-ochoa/OpenSC2K](https://github.com/nicholas-ochoa/OpenSC2K)
  OpenSC2K - An Open Source remake of Sim City 2000 by Maxis
- [open-source-flash/open-source-flash](https://github.com/open-source-flash/open-source-flash)
  Petition to open source Flash and Shockwave spec
- [wulkano/kap](https://github.com/wulkano/kap)
  An open-source screen recorder built with web technology
- [datasciencemasters/go](https://github.com/datasciencemasters/go)
  The Open Source Data Science Masters
- [subins2000/search](https://github.com/subins2000/search)
  An Open Source Search Engine
- [FreeTubeApp/FreeTube](https://github.com/FreeTubeApp/FreeTube)
  An Open Source YouTube app for privacy
- [1j01/sputnik](https://github.com/1j01/sputnik)
  Open source RSS reader
- [afterlogic/aurora-files](https://github.com/afterlogic/aurora-files)
  Aurora Files is an open-source file storage platform.
- [adobe/brackets](https://github.com/adobe/brackets)
  An open source code editor for the web, written in JavaScript, HTML and CSS.
- [jaeksoft/opensearchserver](https://github.com/jaeksoft/opensearchserver)
  Open-source Enterprise Grade Search Engine Software
- [lintool/warcbase](https://github.com/lintool/warcbase)
  Warcbase is an open-source platform for managing analyzing web archives
- [elastic/elasticsearch](https://github.com/elastic/elasticsearch)
  Open Source, Distributed, RESTful Search Engine
- [carsonip/Penguin-Subtitle-Player](https://github.com/carsonip/Penguin-Subtitle-Player)
  An open-source, cross-platform standalone subtitle player
- [serhii-londar/open-source-mac-os-apps](https://github.com/serhii-londar/open-source-mac-os-apps)
  🚀 Awesome list of open source applications for macOS.
- [mahmoud/awesome-python-applications](https://github.com/mahmoud/awesome-python-applications)
  💿 Free software that works great, and also happens to be open-source Python. 
- [n1trux/awesome-sysadmin](https://github.com/n1trux/awesome-sysadmin)
  A curated list of amazingly awesome open source sysadmin resources.
- [facebook/docusaurus](https://github.com/facebook/docusaurus)
  Easy to maintain open source documentation websites.
- [appacademy/welcome-to-open](https://github.com/appacademy/welcome-to-open)
  Overview of App Academy Open
- [go-ego/riot](https://github.com/go-ego/riot)
  Go Open Source, Distributed, Simple and efficient Search Engine 
- [megous/megatools](https://github.com/megous/megatools)
  Open-source command line tools for accessing Mega.co.nz cloud storage.
- [pd4d10/git-touch](https://github.com/pd4d10/git-touch)
  Open source GitHub App built with Flutter
- [pverrecchia/OpenDAW](https://github.com/pverrecchia/OpenDAW)
  Open source online digital audio workstation
- [M66B/FairEmail](https://github.com/M66B/FairEmail)
  Open source, privacy friendly email app for Android
- [uwdata/perceptual-kernels](https://github.com/uwdata/perceptual-kernels)
  Data & source code for the perceptual kernels study
- [videojs/video.js](https://github.com/videojs/video.js)
  Video.js - open source HTML5 & Flash video player
- [quicksilver/Quicksilver](https://github.com/quicksilver/Quicksilver)
  Quicksilver Project Source
- [home-assistant/home-assistant](https://github.com/home-assistant/home-assistant)
  :house_with_garden: Open source home automation that puts local control and privacy first
- [WuTheFWasThat/vimflowy](https://github.com/WuTheFWasThat/vimflowy)
  An open source productivity tool drawing inspiration from workflowy and vim
- [standardnotes/web](https://github.com/standardnotes/web)
  A free, open-source, and completely encrypted notes app. | https://standardnotes.org
- [nikolamilosevic86/owasp-seraphimdroid](https://github.com/nikolamilosevic86/owasp-seraphimdroid)
  OWASP Seraphimdroid is an open source project with aim to create, as a community, an open platform for education and protection of Android users against privacy and security threats.
- [servocoder/RichFilemanager](https://github.com/servocoder/RichFilemanager)
  An open-source file manager. Up-to-date for PHP, Java, ASHX, ASP, NodeJs & Python 3 Flask. Contributions are welcome!
- [Automattic/juice](https://github.com/Automattic/juice)
  Juice inlines CSS stylesheets into your HTML source.
- [internetarchive/heritrix3](https://github.com/internetarchive/heritrix3)
  Heritrix is the Internet Archive's open-source, extensible, web-scale, archival-quality web crawler project.  
- [tbellembois/gobkm](https://github.com/tbellembois/gobkm)
  Ultra minimalist single user open source bookmark manager.
- [freeCodeCamp/freeCodeCamp](https://github.com/freeCodeCamp/freeCodeCamp)
  The https://www.freeCodeCamp.org open source codebase and curriculum. Learn to code for free together with millions of people.
- [ritiek/scancode-toolkit](https://github.com/ritiek/scancode-toolkit)
  :mag_right: ScanCode detects licenses, copyrights, package manifests & dependencies and more by scanning code ... to discover and inventory open source and third-party packages used in your code.
- [modulz/modulz-original-design-system-archive](https://github.com/modulz/modulz-original-design-system-archive)
  An open-source design system for building scalable, responsive websites and applications.
- [notabugio/notabug](https://github.com/notabugio/notabug)
  A decentralized link aggregator with reddit’s classic, open source UI
- [1j01/atom-minimap](https://github.com/1j01/atom-minimap)
  A preview of the full source code.
- [wtfutil/wtfdocs](https://github.com/wtfutil/wtfdocs)
  The source for https://wtfutil.com
- [simogeo/Filemanager](https://github.com/simogeo/Filemanager)
  An open-source file manager released under MIT license. Up-to-date for PHP connector. This package is DEPRECATED. Now, please use RichFileManager available at : https://github.com/servocoder/RichFilemanager.
- [tabler/tabler](https://github.com/tabler/tabler)
  Tabler is free and open-source HTML Dashboard UI Kit built on Bootstrap
- [DaAwesomeP/tab-counter](https://github.com/DaAwesomeP/tab-counter)
  A button badge that shows the number of tabs open in a window
- [PrivateBin/PrivateBin](https://github.com/PrivateBin/PrivateBin)
  A minimalist, open source online pastebin where the server has zero knowledge of pasted data. Data is encrypted/decrypted in the browser using 256 bits AES.
- [happyfish100/fastdfs](https://github.com/happyfish100/fastdfs)
  FastDFS is an open source high performance distributed file system (DFS). It's major functions include: file storing, file syncing and file accessing, and design for high capacity and load balance.
- [atom/fuzzy-finder](https://github.com/atom/fuzzy-finder)
  Find and open files quickly
- [seemoo-lab/opendrop](https://github.com/seemoo-lab/opendrop)
  An open Apple AirDrop implementation written in Python
- [expo/expo](https://github.com/expo/expo)
  An open-source platform for making universal native apps with React. Expo runs on Android, iOS, and the web.
- [Piwigo/Piwigo](https://github.com/Piwigo/Piwigo)
  Manage your photos with Piwigo, a full featured open source photo gallery application for the web. Star us on Github! More than 200 plugins and themes available. Join us and contribute!
- [transloadit/uppy](https://github.com/transloadit/uppy)
  The next open source file uploader for web browsers :dog: 
- [cryptee/web-client](https://github.com/cryptee/web-client)
  Cryptee's web client source code for all platforms.
- [MediaFire/mediafire-python-open-sdk](https://github.com/MediaFire/mediafire-python-open-sdk)
- [outline/outline](https://github.com/outline/outline)
  The fastest wiki and knowledge base for growing teams. Beautiful, feature rich, markdown compatible and open source.
- [gigablast/open-source-search-engine](https://github.com/gigablast/open-source-search-engine)
  Nov 20 2017 -- A distributed open source search engine and spider/crawler written in C/C++ for Linux on Intel/AMD. From gigablast dot com, which has binaries for download. See the README.md file at the very bottom of this page for instructions.
- [bhavsec/reconspider](https://github.com/bhavsec/reconspider)
  🔎 Most Advanced Open Source Intelligence (OSINT) Framework for scanning IP Address, Emails, Websites, Organizations.
- [jordanoverbye/gatsby-source-are.na](https://github.com/jordanoverbye/gatsby-source-are.na)
  Source plugin for pulling data into Gatsby from Are.na
- [asciidoctor/asciidoctor](https://github.com/asciidoctor/asciidoctor)
  :gem: A fast, open source text processor and publishing toolchain, written in Ruby, for converting AsciiDoc content to HTML 5, DocBook 5, and other formats.
- [paralax/awesome-internet-scanning](https://github.com/paralax/awesome-internet-scanning)
  A curated list of awesome Internet port and host scanners, plus related components and much more, with a focus on free and open source projects.
- [MediaFire/mediafire-php-open-sdk](https://github.com/MediaFire/mediafire-php-open-sdk)
- [pirate/ArchiveBox](https://github.com/pirate/ArchiveBox)
  🗃 The open source self-hosted web archive. Takes browser history/bookmarks/Pocket/Pinboard/etc., saves HTML, JS, PDFs, media, and more...
- [Permaweb/permaweb](https://github.com/Permaweb/permaweb)
  Source code for Permaweb, the writing app for busy people.
- [ricklamers/gridstudio](https://github.com/ricklamers/gridstudio)
  Grid studio is a web-based application for data science with full integration of open source data science frameworks and languages.
- [EthicalML/awesome-production-machine-learning](https://github.com/EthicalML/awesome-production-machine-learning)
  A curated list of awesome open source libraries to deploy, monitor, version and scale your machine learning
- [uwdata/visual-embedding](https://github.com/uwdata/visual-embedding)
   Data & source code for the visual embedding model
- [NativeScript/NativeScript](https://github.com/NativeScript/NativeScript)
  NativeScript is an open source framework for building truly native mobile apps with JavaScript. Use web skills, like Angular and Vue.js, FlexBox and CSS, and get native UI and performance on iOS and Android.
- [square/javapoet](https://github.com/square/javapoet)
  A Java API for generating .java source files.
- [oracle/opengrok](https://github.com/oracle/opengrok)
  OpenGrok is a fast and usable source code search and cross reference engine, written in Java
- [docker/docker.github.io](https://github.com/docker/docker.github.io)
  Source repo for Docker's Documentation
- [dalehenrich/filetree](https://github.com/dalehenrich/filetree)
  Monticello repository for directory-based Monticello packages enabling the use of git, svn, etc. for managing Smalltalk source code.
- [andre-simon/highlight](https://github.com/andre-simon/highlight)
  Source code to formatted text converter 
- [TheOdinProject/curriculum](https://github.com/TheOdinProject/curriculum)
  The open curriculum for learning web development
- [EnergizedProtection/block](https://github.com/EnergizedProtection/block)
  Let's make an annoyance free, better open internet, altogether!
- [oclif/oclif](https://github.com/oclif/oclif)
  Node.js Open CLI Framework. Built with 💜 by Heroku.
- [m4rk3r/react-collapsible](https://github.com/m4rk3r/react-collapsible)
  React component to wrap content in Collapsible element with trigger to open and close.
- [ryanoasis/nerd-fonts](https://github.com/ryanoasis/nerd-fonts)
  Iconic font aggregator, collection, & patcher. 3,600+ icons, 40+ patched fonts: Hack, Source Code Pro, more. Glyph collections: Font Awesome, Material Design Icons, Octicons, & more
- [bluquar/react-redux-soundcloud](https://github.com/bluquar/react-redux-soundcloud)
  Learn React + Redux by building a SoundCloud Client. Full tutorial included. Source Code for main tutorial but also extensions included.
- [danielmahal/Rumpetroll](https://github.com/danielmahal/Rumpetroll)
  Rumpetroll is a massive-multiplayer experiment. It's purpose was to try out new open web technologies like WebSockets and Canvas.
- [tcoxon/fishpye](https://github.com/tcoxon/fishpye)
  A non-recursive voxel-based real-time raytracer in Python, OpenCL and OpenGL, with support for fish-eye/panoramic field-of-views, lighting (camera as the light source) and portals.
- [ptsource/Developer-Platform](https://github.com/ptsource/Developer-Platform)
  :eight_spoked_asterisk: PTSource Developer Platform is a free professional IDE, Source Code Editor and Compiler, tools, templates and  libs for professionals and students. It offers many powerful features for programming languages and syntax highlighing for over 100 languages.
- [microlinkhq/metascraper](https://github.com/microlinkhq/metascraper)
  Scrape data from websites using Open Graph metadata, regular HTML metadata, and a series of fallbacks.
- [chrishunt/git-pissed](https://github.com/chrishunt/git-pissed)
  gitting pissed about your code
- [matomo-org/matomo](https://github.com/matomo-org/matomo)
  Liberating Web Analytics. Star us on Github? +1. Matomo is the leading open alternative to Google Analytics that gives you full control over your data. Matomo lets you easily collect data from websites, apps & the IoT and visualise this data and extract insights. Privacy is built-in. We love Pull Requests! 
- [prettier/prettier](https://github.com/prettier/prettier)
  Prettier is an opinionated code formatter.
- [rubik/radon](https://github.com/rubik/radon)
  Various code metrics for Python code
- [Ivoah/minimap](https://github.com/Ivoah/minimap)
  Generate minimaps of your code
- [twitterdev/cards-player-samples](https://github.com/twitterdev/cards-player-samples)
  Sample Code for Player Cards, both for stored and streamed video.
- [algorithm-visualizer/algorithm-visualizer](https://github.com/algorithm-visualizer/algorithm-visualizer)
  :fireworks:Interactive Online Platform that Visualizes Algorithms from Code
